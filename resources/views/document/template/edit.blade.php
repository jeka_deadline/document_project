@extends('layouts.bootstrap')

@section('breadcrumbs')

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Главная</a></li>
            <li class="breadcrumb-item"><a href="{{ route('document.template.index') }}">Список шаблонов для документов</a></li>
            <li class="breadcrumb-item active" aria-current="page">Обновление шаблона</li>
        </ol>
    </nav>

@stop
@section('content')

    <div class="row">
        <div class="col-lg-12">
              <h2>Обновление шаблона</h2>
              <form method="post" action="{{ route('document.template.update', ['template' => $template]) }}">
                  {{ csrf_field() }}
                  {{ method_field('PUT') }}

                  <div class="form-group">
                      <label class="control-label" for="">Название</label>
                      <input class="form-control" type="text" name="name" value="{{ old('name', $template->name) }}">

                      @if ($errors->has('name'))

                          <div class="text-danger">

                                {{ $errors->first('name') }}

                          </div>

                      @endif

                  </div>

                  <!-- block types -->
                  <div class="form-group">
                      <label class="control-label" for="">Тип</label>
                      <select class="form-control" name="type">

                          @foreach ($documentTemplateTypes as $type => $label)

                              <option value="{{ $type }}" {{ (old('type', $template->type) == $type) ? 'selected' : '' }} >{{ $label }}</option>

                          @endforeach

                      </select>

                      @if ($errors->has('type'))

                          <div class="text-danger">

                                {{ $errors->first('type') }}

                          </div>

                      @endif

                  </div>
                  <!-- end block types -->

                  <div class="form-group">
                      <label class="control-label" for="">Текст</label>
                      <textarea name="body" id="" class="ckeditor-input form-control" rows="20">{{ old('body', $template->body) }}</textarea>

                      @if ($errors->has('body'))

                          <div class="text-danger">

                                {{ $errors->first('body') }}

                          </div>

                      @endif

                  </div>

                  @include('document/template/list_placeholders')

                  <?php if (Auth::user()->isCanSaveAsDocumentTemplate()) : ?>

                      <a href="#" class="btn btn-success" data-url="{{ route('document.template.store') }}" id="save-as">Сохранить как</a>

                  <?php endif; ?>
                  <?php if (Auth::user()->isAdmin()) : ?>

                      <input type="submit" value="Обновить" class="btn btn-primary">

                  <?php endif; ?>

                  <a class="btn btn-info" href="{{ route('factory.index') }}">Назад</a>
              </form>
        </div>
    </div>

@stop
@section('javascript')

    <script src="https://cdn.ckeditor.com/4.9.2/full/ckeditor.js"></script>
    <script>
        CKEDITOR.replace('body');

        $(document).on('click', '#save-as', function( e ) {
            e.preventDefault();

            var form = $(this.closest('form'));

            $(form).attr('action' , $(this).data('url'));
            $(form).find('[name=_method]').remove();

            $(form).trigger('submit');
        });
    </script>

@stop