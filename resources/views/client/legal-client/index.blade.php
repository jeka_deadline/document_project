@extends('layouts.bootstrap')

@section('breadcrumbs')

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Главная</a></li>
            <li class="breadcrumb-item active" aria-current="page">Клиенты: юридические лица</li>
        </ol>
    </nav>

@stop
@section('content')

    <div class="row">
          <div class="col-lg-12">

                <?php if (Auth::user()->isCanCreateLegalClient()) : ?>

                    <a class="btn btn-success" href="{{ route('legal.client.create') }}">Добавить клиента</a>

                <?php endif; ?>

                <br><br>
                <table class="table">
                      <thead>
                            <tr>
                                  <th>Название полное</th>
                                  <th>Фамилия контактного лица</th>
                                  <th>Имя контактного лица</th>
                                  <th>Отчество контактного лица</th>
                                  <th>Операции</th>
                            </tr>
                      </thead>
                      <tbody>

                          @foreach ($clients as $client)

                              <tr>
                                  <td>
                                      {{ $client->full_name }}
                                  </td>
                                  <td>
                                      {{ $client->personal_surname }}
                                  </td>
                                  <td>
                                      {{ $client->personal_name }}
                                  </td>
                                  <td>
                                      {{ $client->personal_lastname }}
                                  </td>
                                  <td>

                                      <?php if (Auth::user()->isCanEditLegalClient()) : ?>

                                          <a href="{{ route('legal.client.edit', ['client' => $client]) }}"><i class="fa fa-edit fa-2x"></i></a>

                                      <?php endif; ?>
                                      <?php if (Auth::user()->isCanViewLegalClient()) : ?>

                                          <a href="{{ route('legal.client.view', ['client' => $client]) }}"><i class="fa fa-eye fa-2x"></i></a>

                                      <?php endif; ?>
                                      <?php if (Auth::user()->isCanDeleteLegalClient()) : ?>

                                          <form class="delete-form" method="post" action="{{ route('legal.client.delete', ['client' => $client]) }}" style="display: inline">
                                              {{ csrf_field() }}
                                              {{ method_field('DELETE') }}

                                              <button style="color: #337ab7; border: none; background: none"><i class="fa fa-trash fa-2x"></i></button>
                                          </form>

                                      <?php endif; ?>

                                  </td>
                              </tr>

                          @endforeach

                      </tbody>
                </table>

                {{ $clients->links() }}
          </div>
    </div>

@stop