<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\ClientService;
use App\Http\Requests\LegalClientRequest;
use App\Models\Client\LegalClient;
use Illuminate\Support\Facades\Auth;

/**
 * Legal client controller.
 */
class LegalClientController extends Controller
{
    /**
     * Client service.
     *
     * @var \App\Services\ClientService $clientService
     * @access private
     */
    private $clientService;

    /**
     * Controller constructor.
     *
     * @param \App\Services\ClientService $clientService Client service
     * @return void
     */
    public function __construct (ClientService $clientService)
    {
        $this->clientService = $clientService;
    }

    /**
     * Display page with all legal clients.
     *
     * @param \Illuminate\Http\Request $request Request
     * @return \Illuminate\Support\Facades\View
     */
    public function index(Request $request)
    {
        if (!Auth::user()->isCanViewLegalClients()) {
            $this->accessDanied();
        }

        return view('client.legal-client.index', [
            'clients' => $this->clientService->legalClientsWithPaginate(),
        ]);
    }

    /**
     * Display page with form create new legal client.
     *
     * @param \Illuminate\Http\Request $request Request
     * @return \Illuminate\Support\Facades\View
     */
    public function create(Request $request)
    {
        if (!Auth::user()->isCanCreateLegalClient()) {
            $this->accessDanied();
        }

        return view('client.legal-client.create');
    }

    /**
     * Create new legal client.
     *
     * @param \App\Http\Requests\LegalClientRequest $request Request for legal client
     * @return \Illuminate\Support\Facades\Redirect
     */
    public function store(LegalClientRequest $request)
    {
        if (!Auth::user()->isCanCreateLegalClient()) {
            $this->accessDanied();
        }

        $client = $this->clientService->createNewLegalClient($request->all());

        if ($request->ajax()) {
            return response()->json([
                'status' => true,
                'key' => 'l-' . $client->id,
                'value' => $client->personalFullName,
                'contactFullName' => $client->contactFullName,
            ]);
        }

        return redirect(route('legal.client.index'));
    }

    /**
     * Display page with client information.
     *
     * @param \App\Models\Client\LegalClient $client Model legal client
     * @param \Illuminate\Http\Request $request Request
     * @return \Illuminate\Support\Facades\View
     */
    public function view(LegalClient $client, Request $request)
    {
        if (!Auth::user()->isCanViewLegalClient()) {
            $this->accessDanied();
        }

        return view('client.legal-client.view', compact('client'));
    }

    /**
     * Display page with form update legal client.
     *
     * @param \App\Models\Client\LegalClient $client Model legal client
     * @param \Illuminate\Http\Request $request Request
     * @return \Illuminate\Support\Facades\View
     */
    public function edit(LegalClient $client, Request $request)
    {
        if (!Auth::user()->isCanEditLegalClient()) {
            $this->accessDanied();
        }

        return view('client.legal-client.update', compact('client'));
    }

    /**
     * Update legal client.
     *
     * @param \App\Models\Client\LegalClient $client Model legal client
     * @param \App\Http\Requests\LegalClientRequest $request Request for update legal client
     * @return \Illuminate\Support\Facades\Redirect
     */
    public function update(LegalClient $client, LegalClientRequest $request)
    {
        if (!Auth::user()->isCanEditLegalClient()) {
            $this->accessDanied();
        }

        $this->clientService->updateLegalClient($client, $request->all());

        return redirect(route('legal.client.index'));
    }

    /**
     * Delete legal client.
     *
     * @param \App\Models\Client\LegalClient $client Model legal client
     * @param \Illuminate\Http\Request $request Request
     * @return \Illuminate\Support\Facades\Redirect
     */
    public function delete(LegalClient $client, Request $request)
    {
        if (!Auth::user()->isCanDeleteLegalClient()) {
            $this->accessDanied();
        }

        $client->delete();

        return redirect(route('legal.client.index'));
    }
}
