@extends('layouts.bootstrap')

@section('breadcrumbs')

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Главная</a></li>
            <li class="breadcrumb-item"><a href="{{ route('worker.index') }}">Список сотрудников</a></li>
            <li class="breadcrumb-item active" aria-current="page">Обновление сотрудника: {{ $worker->surname }} {{ $worker->name }}</li>
        </ol>
    </nav>

@stop
@section('content')

    <div class="row">
        <div class="col-lg-12">
              <h2>Изменение сотрудника</h2>
              <form method="post" action="{{ route('worker.update', ['worker' => $worker]) }}">
                  {{ csrf_field() }}
                  {{ method_field('PUT') }}

                  <div class="form-group">
                      <label class="control-label" for="">Фамилия</label>
                      <input class="form-control" type="text" name="surname" value="{{ old('surname', $worker->surname) }}">

                      @if ($errors->has('surname'))

                          <div class="text-danger">

                                {{ $errors->first('surname') }}

                          </div>

                      @endif

                  </div>

                  <div class="form-group">
                      <label class="control-label" for="">Имя</label>
                      <input class="form-control" type="text" name="name" value="{{ old('name', $worker->name) }}">

                      @if ($errors->has('name'))

                          <div class="text-danger">

                                {{ $errors->first('name') }}

                          </div>

                      @endif

                  </div>

                  <div class="form-group">
                      <label class="control-label" for="">Отчество</label>
                      <input class="form-control" type="text" name="lastname" value="{{ old('lastname', $worker->lastname) }}">

                      @if ($errors->has('lastname'))

                          <div class="text-danger">

                                {{ $errors->first('lastname') }}

                          </div>

                      @endif

                  </div>

                  <div class="form-group">
                      <label class="control-label" for="">Роль работника</label>
                      <select class="form-control" name="role" id="">

                            @foreach ($listRoles as $roleId => $roleName)

                                <option value="{{ $roleId }}" {{ (old('role', $worker->userRole->role_id) == $roleId) ? 'selected' : '' }}>{{ $roleName }}</option>

                            @endforeach

                      </select>

                      @if ($errors->has('role'))

                          <div class="text-danger">

                                {{ $errors->first('role') }}

                          </div>

                      @endif

                  </div>

                  <div class="form-group">
                      <label class="control-label" for="">Email</label>
                      <input class="form-control email-mask" type="text" name="email" value="{{ old('email', $worker->user->email) }}">

                      @if ($errors->has('email'))

                          <div class="text-danger">

                                {{ $errors->first('email') }}

                          </div>

                      @endif

                  </div>

                  <div class="form-group">
                      <label class="control-label" for="">Пароль</label>
                      <input class="form-control" type="password" name="password" value="">

                      @if ($errors->has('password'))

                          <div class="text-danger">

                                {{ $errors->first('password') }}

                          </div>

                      @endif

                  </div>

                  <div class="block-phones">
                      <label for="">Телефоны</label>

                      @if (old('phones', $worker->phones))
                          @foreach (old('phones', $worker->phones) as $phoneValue)

                              <div class="form-group target">
                                  <div class="input-group">
                                      <input type="text" class="form-control phone-mask" name="phones[]" value="{{ $phoneValue }}">
                                      <span class="input-group-btn">
                                          <button type="button" class="btn btn-danger addel-delete"><i class="fa fa-remove"></i></button>
                                      </span>
                                  </div>
                              </div>

                          @endforeach
                      @else

                          <div class="form-group target">
                              <div class="input-group">
                                  <input type="text" class="form-control phone-mask" name="phones[]" value="">
                                  <span class="input-group-btn">
                                      <button type="button" class="btn btn-danger addel-delete"><i class="fa fa-remove"></i></button>
                                  </span>
                              </div>
                          </div>

                      @endif

                      <button type="button" class="btn btn-success addel-add"><i class="fa fa-plus"></i></button>
                  </div>

                  <div class="form-group">
                      <label class="control-label" for="">Дополнительный email</label>
                      <input class="form-control email-mask" type="text" name="additional_email" value="{{ old('additional_email', $worker->additional_email) }}">

                      @if ($errors->has('additional_email'))

                          <div class="text-danger">

                                {{ $errors->first('additional_email') }}

                          </div>

                      @endif

                  </div>

                  <div class="block-additional-phones">
                      <label for="">Дополнительные телефоны</label>

                      @if (old('additional_phones', $worker->additional_phones))
                          @foreach (old('additional_phones', $worker->additional_phones) as $phoneValue)

                              <div class="form-group target">
                                  <div class="input-group">
                                      <input type="text" class="form-control phone-mask" name="additional_phones[]" value="{{ $phoneValue }}">
                                      <span class="input-group-btn">
                                          <button type="button" class="btn btn-danger addel-delete"><i class="fa fa-remove"></i></button>
                                      </span>
                                  </div>
                              </div>

                          @endforeach
                      @else

                          <div class="form-group target">
                              <div class="input-group">
                                  <input type="text" class="form-control phone-mask" name="additional_phones[]" value="">
                                  <span class="input-group-btn">
                                      <button type="button" class="btn btn-danger addel-delete"><i class="fa fa-remove"></i></button>
                                  </span>
                              </div>
                          </div>

                      @endif

                      <button type="button" class="btn btn-success addel-add"><i class="fa fa-plus"></i></button>
                  </div>

                  <div class="form-group">
                      <label class="control-label" for="">Комментарий</label>
                      <textarea class="form-control" type="text" name="comment">{{ old('comment', $worker->comment) }}</textarea>

                      @if ($errors->has('comment'))

                          <div class="text-danger">

                                {{ $errors->first('comment') }}

                          </div>

                      @endif

                  </div>

                  <input type="submit" value="Обновить" class="btn btn-primary">
                  <a class="btn btn-info" href="{{ route('worker.index') }}">Назад</a>
              </form>
        </div>
    </div>

@stop