<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableFactoryPayments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('factory_payments', function (Blueprint $table) {
            $table->date('date_dispatch')->nullable()->change();
            $table->date('date_payment')->nullable()->change();
            $table->float('sum')->nullable()->change();
            $table->float('bank_transfer')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('factory_payments', function (Blueprint $table) {
            $table->string('date_dispatch', 255)->nullable()->change();
            $table->string('date_payment', 255)->nullable()->change();
            $table->string('sum', 255)->nullable()->change();
            $table->string('bank_transfer', 255)->nullable()->change();
        });
    }
}
