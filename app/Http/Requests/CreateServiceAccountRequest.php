<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateServiceAccountRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'delivery' => 'required|in:yes,no',
            'mkad' => 'string|max:10|nullable',
            'km_outer_mkad' => 'numeric|nullable',
            'sum_delivery' => 'numeric|nullable',
            'floor' => 'integer|min:1|nullable',
            'manual' => 'numeric|nullable',
            'manual_large_sized' => 'numeric|nullable',
            'on_lift' => 'numeric|nullable',
            'sum_climb' => 'numeric|nullable',
            'assembly' => 'required|in:yes,no',
            'sum_assembly' => 'numeric|nullable',
            'removing_packaging' => 'numeric|nullable',
            'type_payment' => 'required|string|max:15',
            'course' => 'required|numeric',
            'full_sum_eur' => 'required|numeric',
            'full_sum_rub' => 'required|numeric',
            'promotion_sum_eur' => 'numeric|nullable',
            'full_sum_eur_with_promotion' => 'numeric|nullable',
            'full_sum_rub_with_promotion' => 'numeric|nullable',
        ];
    }
}
