@extends('layouts.bootstrap')

@section('css')
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.2.0/css/ion.rangeSlider.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.2.0/css/ion.rangeSlider.skinFlat.min.css">
@stop

@section('breadcrumbs')

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Главная</a></li>
            <li class="breadcrumb-item active" aria-current="page">Список договоров</li>
        </ol>
    </nav>

@stop
@section('content')

    <div class="row">
          <div class="col-lg-12">

                <?php if (Auth::user()->isCanCreateDocument()) : ?>

                    <a class="btn btn-success" href="{{ route('document.create') }}">Создать договор</a>

                <?php endif; ?>
                <?php if (Auth::user()->isCanUpdateCalendar()) : ?>

                    <a class="btn btn-primary" href="{{ route('admin.calendar.update') }}">Обновить производственный календарь</a>

                <?php endif; ?>

                <br><br>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="well">
                            <form action="">
                                <div class="row">
                                    <input type="hidden" id="document-max-sum" value="{{ $documentMaxSum }}">
                                    <input type="hidden" id="filter-sum" value="{{ app('request')->input('sum') }}">
                                    <div class="col-lg-4">
                                        <label for="">Сумма:</label>
                                        <input id="slider-sum" type="text" name="sum">
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label for="">Дата:</label>
                                            <input type="text" name="dates" value="{{ app('request')->input('dates') }}" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label for="">Менеджер:</label>
                                            <select name="manager_worker_id" class="form-control">
                                                <option value=""></option>

                                                @foreach ($listManagers as $worker)

                                                    <option value="{{ $worker->id }}" {{ (app('request')->input('manager_worker_id') == $worker->id) ? 'selected' : '' }}>{{ $worker->full_name }}</option>

                                                @endforeach

                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label for="">Тип оплаты:</label>
                                            <select name="type_payment" class="form-control">
                                                <option value="">Все</option>

                                                @foreach ($listTypePayments as $type => $label)

                                                    <option value="{{ $type }}" {{ (app('request')->input('type_payment') == $type) ? 'selected' : '' }}>{{ $label }}</option>

                                                @endforeach

                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label for="">Процент оплаты</label>
                                            <select name="percentage_payment" class="form-control">
                                                <option value="">Все</option>

                                                @foreach ($listPercentagePayments as $type => $label)

                                                    <option value="{{ $type }}" {{ (app('request')->input('percentage_payment') == $type) ? 'selected' : '' }}>{{ $label }}</option>

                                                @endforeach

                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label for="">Клиент</label>
                                            <input type="text" name="client" value="{{ app('request')->input('client') }}" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label for="">Номер договора</label>
                                            <input type="text" name="number" value="{{ app('request')->input('number') }}" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <br>
                                        <input type="submit" class="btn btn-primary" value="Применить">

                                        @if (!is_null($documentsFilterSum))

                                            <p>Сумма: <b>{{ $documentsFilterSum }} €</b></p>
                                            <p>Внесенная сумма: <b>{{ $documentsFilterEnterEur }} €</b></p>

                                        @endif

                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div style="font-size: 0.9em" class="table-responsive">
                    <table class="table">
                          <thead>
                              <tr>
                                  <th>№</th>
                                  <th>Операции</th>
                                  <th>Тип продажи</th>
                                  <th>Статус договора</th>
                                  <th>Статус заказа</th>
                                  <th>Опл. пол.</th>
                                  <th>Дата</th>
                                  <th>Клиент</th>
                                  <th>Подготовил</th>
                                  <th>Менеджер</th>
                                  <th>Фабрика</th>
                                  <th>Срок исполнения, дней</th>
                                  <th>Дата завершения</th>
                                  <th>Сумма, EUR</th>
                                  <th>Внесено, EUR</th>
                                  <th>Внесено %</th>
                                  <th>Остаток %</th>
                                  <th>Тип оплаты</th>
                              </tr>
                          </thead>
                           <tbody>

                              @foreach ($documents as $document)

                                  <tr>
                                      <td>{{ $document->register_number }}</td>
                                      <td>

                                          <?php if (Auth::user()->isCanEditDocument()) : ?>

                                              <a href="{{ route('document.edit', ['document' => $document]) }}"><i class="fa fa-edit fa-2x"></i></a>

                                          <?php endif; ?>
                                          <?php if (Auth::user()->isCanViewDocument()) : ?>

                                              <a href="{{ route('document.view', ['document' => $document]) }}"><i class="fa fa-eye fa-2x"></i></a>

                                          <?php endif; ?>
                                          <?php if (Auth::user()->isCanDeleteDocument()) : ?>

                                              <form class="delete-form" method="post" action="{{ route('document.delete', ['document' => $document]) }}" style="display: inline">
                                                    {{ csrf_field() }}
                                                    {{ method_field('DELETE') }}

                                                    <button style="color: #337ab7; border: none; background: none"><i class="fa fa-trash fa-2x"></i></button>
                                              </form>

                                          <?php endif; ?>

                                      </td>
                                      <td>{{ $document->humanName }}</td>
                                      <td>{{ $document->humanState }}</td>
                                      <td>{{ $document->humanOrderState }}</td>
                                      <td>{{ ($document->sum !== $document->sumAllPayments) ? 'Нет' : 'Да' }}</td>
                                      <td>{{ $document->date->format('d.m.Y') }}</td>
                                      <td>{{ $document->clientFullName }}</td>
                                      <td>{{ $document->workerFullName }}</td>
                                      <td>{{ $document->managerFullName }}</td>
                                      <td></td>
                                      <td>{{ $document->period_execution }}</td>
                                      <td>{{ $document->date_finish->format('d.m.Y') }}</td>
                                      <td>{{ number_format($document->sum, 2, '.', '') }}</td>
                                      <td>{{ number_format($document->sumAllPayments, 2, '.', '') }}</td>
                                      <td>{{ number_format($document->percentage_enter, 2, '.', '') }}</td>
                                      <td>{{ number_format(100 - $document->percentage_enter, 2, '.', '') }}</td>
                                      <td>{{ $document->humanTypePayment }}</td>
                                  </tr>

                              @endforeach

                          </tbody>
                    </table>
                </div>

                 {{ $documents->appends(request()->input())->links() }}

          </div>
    </div>

@stop
@section('javascript')
    @parent

    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.2.0/js/ion.rangeSlider.min.js"></script>

    <script>
        $(function() {
            let filterSum = $('#filter-sum').val();
            let maxSum = $('#document-max-sum').val();

            let rangeValues = filterSum.split(';');

            let from = 0;
            let to = maxSum;

            if (rangeValues[ 0 ] !== undefined) {
                from = rangeValues[ 0 ];
            }

            if (rangeValues[ 1 ] !== undefined) {
                to = rangeValues[ 1 ];
            }


            $("#slider-sum").ionRangeSlider({
                type: "double",
                grid: true,
                min: 0,
                max: maxSum,
                from: from,
                to: to,
                prefix: "€"
            });

            $('input[name="dates"]').daterangepicker({
                autoUpdateInput: false,
                locale: {
                    cancelLabel: 'Clear'
                }
            });

            $('input[name="dates"]').on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
            });

            $('input[name="dates"]').on('cancel.daterangepicker', function(ev, picker) {
                $(this).val('');
            });
        });
    </script>

@stop