<?php

namespace App\Services;

use StdClass;
use DateTime;
use App\Models\Document\Document;
use App\Models\Document\DocumentPrepayment;
use App\Models\Client\LegalClient;
use App\Models\Client\IndividualClient;
use App\Helpers\UUID;
use Illuminate\Support\Facades\DB;
use App\Models\User\Worker;
use App\Models\Shop\Shop;
use App\Models\Document\DocumentTemplate;
use App\Models\Document\DocumentTemplateRelation;
use App\Models\Factory;
use App\Models\Product;
use App\Models\Document\DocumentAdditionalField;

/**
 * Document service.
 */
class DocumentService
{
    /**
     * Sum all documents by filter.
     *
     * @access private
     * @var int|null $filterSum
     */
    private $filterSum = null;

    private $filterEnterEur = null;

    /**
     * Get list documents with paginate.
     *
     * @param int $countOnPage Count items on page.
     * @return
     */
    public function filter($request, $countOnPage = 15)
    {
        $filterSum = $request->sum;
        $filterDatesRange = $request->dates;
        $filterManager = $request->manager_worker_id;
        $filterTypePayment = $request->type_payment;
        $filterPercentagePayment = $request->percentage_payment;
        $filterClient = $request->client;
        $filterNumber = $request->number;

        $documents =  Document::query();

        $flagFilter = false;

        if (!is_null($filterManager)) {
            $documents->where('manager_worker_id', $filterManager);
            $flagFilter = true;
        }

        if (!is_null($filterDatesRange)) {
            $dates = explode(' - ', $filterDatesRange);
            $dates[ 0 ] = (new DateTime($dates[ 0 ]))->format('Y-m-d');
            $dates[ 1 ] = (new DateTime($dates[ 1 ]))->format('Y-m-d');
            $documents->whereBetween('date', $dates);
            $flagFilter = true;
        }

        if (!is_null($filterPercentagePayment)) {
            $sign = null;

            switch ($filterPercentagePayment) {
                case 'less_70':
                    $sign = '<';
                    break;
                case 'equal_70':
                    $sign = '=';
                    break;
                case 'great_70':
                    $sign = '>';
                    break;
            }

            if (!is_null($sign)) {
                $documents->where('percentage_enter', $sign, 70);
                $flagFilter = true;
            }
        }

        if (!is_null($filterClient)) {
            $documents->where('client_name', 'like', '%' . $filterClient . '%');
            $flagFilter = true;
        }

        if (!is_null($filterNumber)) {
            $documents->where('register_number', 'like', '%' . $filterNumber . '%');
            $flagFilter = true;
        }

        if (!is_null($filterSum)) {
            $sum = explode(';', $filterSum);
            $documents->whereBetween('sum', $sum);
            $flagFilter = true;
        }

        if (!is_null($filterTypePayment)) {
            $documents->where('type_payment', $filterTypePayment);
            $flagFilter = true;
        }

        if ($flagFilter) {
            $sum = 0;
            $enterSum = 0;

            foreach ($documents->get() as $document) {
                $sum += $document->sum;
                $enterSum += $document->eur_enter;
            }

            $this->filterSum = $sum;
            $this->filterEnterEur = $enterSum;
        }

        $documents->orderBy('date', 'DESC');

        return $documents->paginate($countOnPage);
    }

    /**
     * List documents with paginate.
     *
     * @param int $countOnPage Count documents on page.
     * @return
     */
    public function paginate($countOnPage = 15)
    {
        return Document::paginate();
    }

    /**
     * Get sum with filtered documents.
     *
     * @return int
     */
    public function getFilterSum()
    {
        return $this->filterSum;
    }

    public function getFilterEnterEur()
    {
        return $this->filterEnterEur;
    }

    /**
     * Get information for select inputs on form document.
     *
     * @return StdClass
     */
    public function getDropDownInformation()
    {
        $object = new StdClass();

        $object->documentTypes = $this->getDocumentTypes();
        $object->listClients = $this->getListClients();
        $object->paymentTypes = $this->getPaymentTypes();
        $object->states = $this->getStates();
        $object->listManagers = $this->getListManagers();
        $object->listShops = $this->getListShops();

        return $object;
    }

    public function getListPercentagePayments()
    {
        return [
            'less_70' => 'Меньше 70%',
            'equal_70' => 'Равно 70%',
            'great_70' => 'Больше 70%',
        ];
    }

    /**
     * Get currencies from cbr Ru.
     * TODO: cache
     *
     * @return StdClass
     */
    public function getCurrencies($percentageMarkup = 2)
    {
        $currencies = self::getCbrCurrencies();

        $object = new StdClass();

        $object->usd = number_format($currencies->Valute->USD->Value, 2, '.', '');
        $object->eur = number_format($currencies->Valute->EUR->Value, 2, '.', '');
        $object->usd_markup = $object->usd;
        $object->eur_markup = $object->eur;

        if ($percentageMarkup > 0) {

            $percent = $percentageMarkup / 100;

            $object->usd_markup += number_format($object->usd * $percent, 2, '.', '');
            $object->eur_markup += number_format($object->eur * $percent, 2, '.', '');

        }

        $object->cross_currrency = number_format($object->eur_markup / $object->usd_markup, 2, '.', '');

        return $object;
    }

    /**
     * Get default currenices list.
     *
     * @static
     *
     * @return array
     */
    public static function getStaticCurrencies()
    {
        $currencies = self::getCbrCurrencies();

        return [
            'eur' => number_format($currencies->Valute->EUR->Value, 2, '.', ''),
            'usd' => number_format($currencies->Valute->USD->Value, 2, '.', ''),
        ];
    }

    /**
     * Get currencies list from cbr.
     *
     * @static
     *
     * @return string JSON format
     */
    public static function getCbrCurrencies()
    {
        $dirCurrencies = storage_path('currencies');

        if (!is_dir($dirCurrencies)) {
            mkdir($dirCurrencies, 0777);
        }

        $fileCurrencies = $dirCurrencies . '/' . date('Y_m_d') . '.json';

        if (file_exists($fileCurrencies) || is_file($fileCurrencies)) {
            $currencies = json_decode(file_get_contents($fileCurrencies));
        } else {
            $currenciesJson = file_get_contents('https://www.cbr-xml-daily.ru/daily_json.js');

            $currencies = json_decode($currenciesJson);

            $date = new DateTime($currencies->Date);

            if ($date->format('Y-m-d') !== date('Y-m-d')) {
                $currenciesJson = file_get_contents('https://' . substr($currencies->PreviousURL, 2));
                $currencies = json_decode($currenciesJson);
            }

            file_put_contents($fileCurrencies , $currenciesJson);
        }

        return $currencies;
    }

    /**
     * Calculation document sum, percentage and other information.
     *
     * @param array $fields List form parameters for calculation document sum.
     * @return array
     */
    public function calculation($fields)
    {
        $currencies = $this->getCurrencies($fields[ 'percentage_currrency' ]);

        $ratioEurToUsd = $fields[ 'cross_currrency' ];

        $sumDocumentEur = $fields[ 'sum' ];
        $percentagePrepayment = $fields[ 'percentage_prepayment' ];

        if (!empty($percentagePrepayment)) {
            $sumDocumentRub = $fields[ 'sum' ] * $currencies->eur_markup;
            $sumDocumentUsd = $fields[ 'sum' ] * $ratioEurToUsd;
            $enterEur = $sumDocumentEur / 100 * $percentagePrepayment;
            $enterUsd = $sumDocumentUsd / 100 * $percentagePrepayment;
            $enterRub = $sumDocumentRub / 100 * $percentagePrepayment;
            $remainderEur = $sumDocumentEur - $enterEur;
            $percentageRemainder = 100 - $percentagePrepayment;
            $totalEur = $enterEur;
        } else {
            $enterEur = $fields[ 'enter_eur' ];
            $enterUsd = $fields[ 'enter_usd' ];
            $enterRub = $fields[ 'enter_rub' ];

            $totalEur = $enterEur;

            if (!empty($enterUsd)) {
                $totalEur += $enterUsd / $ratioEurToUsd;
            }

            if (!empty($enterRub)) {
                $totalEur += $enterRub / $currencies->eur_markup;
            }

            $remainderEur = $sumDocumentEur - $totalEur;
            $percentagePrepayment = $totalEur / $sumDocumentEur * 100;
            $percentageRemainder = 100 - $percentagePrepayment;
        }

        return [
            'percentage_prepayment' => number_format($percentagePrepayment, 2, '.', ''),
            'enter_eur' => number_format($enterEur, 2, '.' , ''),
            'enter_usd' => number_format($enterUsd, 2, '.' , ''),
            'enter_rub' => number_format($enterRub, 2, '.' , ''),
            'remainder_eur' => number_format($remainderEur, 2, '.' , ''),
            'prepayment_amount_eur' => number_format($totalEur, 2, '.', ''),
            'prepayment_amount_rub' => number_format($totalEur * $currencies->eur_markup, 2, '.', ''),
            'percentage_remainder' => number_format($percentageRemainder, 2, '.', ''),
        ];
    }

    /**
     * Calculation document sum, percentage and other information.
     *
     * @param array $fields List form parameters for calculation document sum.
     * @return array
     */
    public function calculationSurcharge($fields, $document)
    {
        $currencies = $this->getCurrencies($fields[ 'percentage_currrency' ]);

        $ratioEurToUsd = $fields[ 'cross_currrency' ];

        $sumDocumentEur = $document->sum;

        $enterEur = $fields[ 'enter_eur' ];
        $enterUsd = $fields[ 'enter_usd' ];
        $enterRub = $fields[ 'enter_rub' ];

        $totalEur = $enterEur;

        if (!empty($enterUsd)) {
            $totalEur += $enterUsd / $ratioEurToUsd;
        }

        if (!empty($enterRub)) {
            $totalEur += $enterRub / $currencies->eur_markup;
        }

        $remainderEur = $sumDocumentEur - $document->sumAllPayments - $totalEur;
        $percentagePrepayment = ($totalEur + $document->sumAllPayments) / $sumDocumentEur * 100;

        return [
            'percentage_prepayment' => number_format($percentagePrepayment, 2, '.', ''),
            'enter_eur' => number_format($enterEur, 2, '.' , ''),
            'enter_usd' => number_format($enterUsd, 2, '.' , ''),
            'enter_rub' => number_format($enterRub, 2, '.' , ''),
            'remainder_eur' => number_format($remainderEur, 2, '.' , ''),
            'prepayment_amount_eur' => number_format($totalEur, 2, '.', ''),
            'prepayment_amount_rub' => number_format($totalEur * $currencies->eur_markup, 2, '.', ''),
        ];
    }

    /**
     * Calculation date finish by count work dates.
     *
     * @param int $countPeriodExecution Count execution work dates from form.
     * @return string
     */
    public function calculationDateFinish($countPeriodExecution)
    {
        $currentDate = new DateTime();
        $holidays = $this->getHolidays();
        $periodExecutionWithHolidays = $this->calculationPeriodExecutionWithHolidays($countPeriodExecution, $holidays);

        $currentDate->modify('+' . $periodExecutionWithHolidays . ' days');

        while (isset($holidays[ $currentDate->format('Y-m-d') ])) {
            $currentDate->modify('+1 days');
        }

        return $currentDate->format('Y-m-d');
    }

    /**
     * Create new document.
     *
     * @param array $fields User form create document fields.
     * @param int $userAuthId User auth id.
     * @return null|\App\Models\Document\Document
     */
    public function createNewDocument($fields, $userAuthId)
    {
        DB::beginTransaction();

        $document = Document::create($this->getDocumentFieldsByRequest($fields, $userAuthId));

        if (!$document) {
            DB::rollBack();

            return null;
        }

        $calucationFields = [
            'percentage_currrency'  => $fields[ 'percentage_currrency' ],
            'cross_currrency'       => $fields[ 'cross_currrency' ],
            'sum'                   => $fields[ 'sum' ],
            'percentage_prepayment' => $fields[ 'percentage_prepayment' ],
            'enter_eur'             => $fields[ 'enter_eur' ],
            'enter_usd'             => $fields[ 'enter_usd' ],
            'enter_rub'             => $fields[ 'enter_rub' ],
        ];

        if ($documentPrepayment = DocumentPrepayment::create($this->getDocumentPrepaymentFields($fields, $document->id))) {
            $documentAdditionalFields = new DocumentAdditionalField();
            $documentAdditionalFields->document_id = $document->id;
            $documentAdditionalFields->save();

            DB::commit();
            $document->update([
                'percentage_enter' => $documentPrepayment->percentage_prepayment,
                'eur_enter' => $documentPrepayment->prepayment_amount_eur,
            ]);

            return $document;
        }

        DB::rollBack();

        return null;
    }

    /**
     * Update document.
     *
     * @param array $fields Document request update fields.
     * @param \App\Models\Document\Document $document Document model.
     * @return void
     */
    public function updateDocument($fields, Document $document)
    {
        $currentDate = date('Y-m-d H:i:s');
        if (isset($fields[ 'performance' ])) {
            $document->performance_date = $currentDate;
        }

        if (!isset($fields[ 'ternimated' ])) {
            $document->text_ternimated = null;
        } else {
            $document->text_ternimated = $fields[ 'text_ternimated' ];
            $document->ternimated_date = $currentDate;
        }

        $document->manager_worker_id = $fields[ 'manager_worker_id' ];
        $document->order_state = $fields[ 'order_state' ];
        $document->state = $fields[ 'state' ];
        $document->client_name = $document->client->full_name;

        if (!$document->register_number) {
            $document->register_number = $fields[ 'register_number' ];
            $document->from = $currentDate;
        }

        $document->confirm_pro_form = $fields[ 'confirm_pro_form' ];

        if ($document->state === Document::STATE_DEVELOPED && (count($document->allPayments) == 1) && isset($fields[ 'sum' ])) {
            $document->sum = $fields[ 'sum' ];
            $document->percentage_enter = $document->eur_enter / $fields[ 'sum' ] * 100;

            $document->firstPrepayment->percentage_prepayment = $document->percentage_enter;
            $document->firstPrepayment->percentage_remainder = 100 - $document->firstPrepayment->percentage_prepayment;
            $document->firstPrepayment->remainder_eur = $document->sum - $document->firstPrepayment->eur_enter;

            $document->firstPrepayment->save();
        }

        $document->save();
    }

    /**
     * Create new document payment from user.
     *
     * @param array $fields User payment information from request,
     * @param \App\Models\Document\Document $document Document model.
     * @return array
     */
    public function createNewDocumentPayment($fields, Document $document)
    {
        $calculation = $this->calculationSurcharge($fields, $document);

        $fields[ 'percentage_prepayment' ] = $calculation[ 'percentage_prepayment' ] - $document->percentageAllPayments;
        $fields[ 'percentage_remainder' ] = 100 - $fields[ 'percentage_prepayment' ];
        $fields[ 'prepayment_amount_eur' ] = $calculation[ 'prepayment_amount_eur' ];
        $fields[ 'prepayment_amount_rub' ] = $calculation[ 'prepayment_amount_rub' ];

        $response = [
            'status' => false,
        ];

        if ($fields[ 'prepayment_amount_eur' ] + $document->sumAllPayments > $document->sum || empty((float)$fields[ 'prepayment_amount_eur' ])) {
            return $response;
        }

        $newPayment = DocumentPrepayment::create($this->getDocumentPrepaymentFields($fields, $document->id));

        if ($newPayment) {
            $document->update([
                'percentage_enter' => $document->percentage_enter + $newPayment->percentage_prepayment,
                'eur_enter' => $document->eur_enter + $newPayment->prepayment_amount_eur,
            ]);
            $response[ 'status' ] = true;
        }

        return $response;
    }

    /**
     * Get list document templates for attach to document.
     *
     * @param \App\Models\Document\Document $document Document model.
     * @param string $typeTemplate Type template.
     * @return \App\Models\Document\DocumentTemplate[] $list document templates.
     */
    public function getTemplatesForAttach(Document $document, $typeTemplate)
    {
        $documentTemplateIds = $document->templates()->get()->pluck('id', 'id');

        return DocumentTemplate::where('type', $typeTemplate)
            ->whereNotIn('id', $documentTemplateIds)
            ->get();
    }

    /**
     * Attach new template to document.
     *
     * @param \App\Models\Document\Document $document Document model.
     * @param int $documentTemplateId Document template id.
     * @return bool
     */
    public function attachTemplateToDocument(Document $document, $documentTemplateId)
    {
        $attachedTemplate = new DocumentTemplateRelation();
        $attachedTemplate->document_id = $document->id;
        $attachedTemplate->document_template_id = $documentTemplateId;

        if ($attachedTemplate->save()) {
            return true;
        }

        return false;
    }

    /**
     * Get list workers with role managers.
     *
     * @return
     */
    public function getListManagers()
    {
        return Worker::managers();
    }

    /**
     * Get max sun from documents.
     *
     * @return int
     */
    public function getDocumentMaxSum()
    {
        return Document::max('sum');
    }

    /**
     * Get document last register number.
     *
     * @return string
     */
    public function getLastRegisterNumber()
    {
        $lastNumber = Document::whereYear('created_at', date('Y'))->max('register_number');

        if (is_null($lastNumber)) {
            return 0;
        }

        return $lastNumber;
    }

    /**
     * Get list order statuses by document type.
     *
     * @param string $documentTypeName Document type.
     * @return array
     */
    public function getListOrderStatuses($documentTypeName)
    {
        switch ($documentTypeName) {
            case Document::DOCUMENT_TYPE_EXPOSITION:
                return Document::getExpositionStatuses();
            case Document::DOCUMENT_TYPE_ORDER:
                return Document::getOrderStatuses();
            default:
                return [];
        }
    }

    /**
     * Get list payment types.
     *
     * @return array
     */
    public function getListTypePayments()
    {
        return Document::getPaymentTypes();
    }

    /**
     * Save document additioanl field.
     *
     * @param \App\Models\Document\Document $document Document model.
     * @param string $key Key document additional field.
     * @param string $value Value document additional field.
     * @return bool
     */
    public function saveAdditionalField(Document $document, $key, $value)
    {
        if (!$document->additionalFields) {

            $document->additionalFields()->create([
                'document_id' => $document->id,
            ]);
        }

        if ($document->additionalFields()->update([
            $key => $value,
        ])) {
            return true;
        }

        return false;
    }

    /**
     * Get list shops.
     *
     * @return \App\Models\Shop\Shop[]
     */
    public function getListShops()
    {
        return Shop::all();
    }

    /**
     * Get document types.
     *
     * @return array
     */
    public function getDocumentTypes()
    {
        return Document::getDocumentTypes();
    }

    /**
     * Get document payment types.
     *
     * @return array
     */
    public function getPaymentTypes()
    {
        return Document::getPaymentTypes();
    }

    public function getListMonths()
    {
        return [
            1 => 'Январь',
            2 => 'Февраль',
            3 => 'Март',
            4 => 'Апрель',
            5 => 'Май',
            6 => 'Июнь',
            7 => 'Июль',
            8 => 'Август',
            9 => 'Сентябрь',
            10 => 'Октябрь',
            11 => 'Ноябрь',
            12 => 'Декабрь',
        ];
    }

    /**
     * Get list clients.
     *
     * @access private
     * @return array
     */
    private function getListClients()
    {
        $listLegalClients = LegalClient::get()
            ->mapWithKeys(function($item) {
                return ['l-' . $item->id => $item->PersonalFullName];
            })
            ->all();

        $listIndividualClients = IndividualClient::get()
            ->mapWithKeys(function($item) {
                return ['i-' . $item->id => $item->fullName];
            })
            ->all();

        return array_merge($listLegalClients, $listIndividualClients);
    }

    /**
     * Get document states.
     *
     * @access private
     * @return array
     */
    private function getStates()
    {
        return Document::getStates();
    }

    /**
     * Get holiday dates.
     *
     * @access private
     * @return array
     */
    private function getHolidays()
    {
        $currentYear = date('Y');
        $nextYear = $currentYear + 1;

        $file = storage_path("calendar/years/{$currentYear}.php");

        if (file_exists($file) && is_file($file)) {
            $holidays = include($file);
        } else {
            $holidays = [];
        }

        $file = storage_path("calendar/years/{$nextYear}.php");

        if (file_exists($file) && is_file($file)) {
            $holidaysNextYear = include($file);

            $holidays = $holidays + $holidaysNextYear;
        }

        return $holidays;
    }

    /**
     * Calculating period executions with holidays.
     *
     * @access private
     * @param int $countPeriodExecution Count days execution from form.
     * @param array $holidays List holiday dates,
     * @return int
     */
    private function calculationPeriodExecutionWithHolidays($countPeriodExecution, $holidays)
    {
        $i = 0;

        while ($i < $countPeriodExecution) {
            $i++;
            $loopCurrentDate = new DateTime();
            $date = $loopCurrentDate->modify('+' . $i . ' days')->format('Y-m-d');

            if (isset($holidays[ $date ])) {
                $countPeriodExecution++;
            }

            unset($loopCurrentDate);
        }

        return $countPeriodExecution;
    }

    /**
     * Get document client information by client form.
     *
     * @access private
     * @param string $clientRequestValue Client from value.
     * @return array
     */
    private function getClientData($clientRequestValue)
    {
        $clientData = explode('-', $clientRequestValue);

        switch ($clientData[ 0 ]) {
            case 'l':
                return [
                    'client' => LegalClient::where(['id' => $clientData[ 1 ]])->firstOrFail(),
                    'type' => Document::CLIENT_TYPE_LEGAL,
                ];
            default:
                return [
                    'client' => IndividualClient::where(['id' => $clientData[ 1 ]])->firstOrFail(),
                    'type' => Document::CLIENT_TYPE_INDIVIDUAL,
                ];
        }
    }

    /**
     * Get document fields by request.
     *
     * @access private
     * @param array $fields Form request fields.
     * @param int $userAuthId User auth id.
     * @return array
     */
    private function getDocumentFieldsByRequest($fields, $userAuthId)
    {
        $currentDate = new DateTime();

        $documentFields = [
            'name'                 => $fields[ 'name' ],
            'code'                 => UUID::create(),
            'worker_id'            => $userAuthId,
            'date'                 => $currentDate->format('Y-m-d'),
            'type_payment'         => $fields[ 'type_payment' ],
            'state'                => $fields[ 'state' ],
            'register_number'      => $fields[ 'register_number' ],
            'from'                 => (!empty($fields[ 'register_number' ])) ? $currentDate->format('Y-m-d H:i:s') : null,
            'shop_id'              => $fields[ 'shop_id' ],
            'sum'                  => $fields[ 'sum' ],
            'percentage_currrency' => $fields[ 'percentage_currrency' ],
            'confirm_pro_form'     => $fields[ 'confirm_pro_form' ],
            'period_execution'     => $fields[ 'period_execution' ],
            'date_finish'          => $fields[ 'date_finish' ],
            'manager_worker_id'    => $fields[ 'manager_worker_id' ],
        ];

        $clientData = $this->getClientData($fields[ 'client' ]);

        $documentFields[ 'client_id' ] = (int)$clientData[ 'client' ]->id;
        $documentFields[ 'client_name' ] = $clientData[ 'client' ]->full_name;
        $documentFields[ 'client_type' ] = $clientData[ 'type' ];

        return $documentFields;
    }

    /**
     * Get prepayment fields for new payment.
     *
     * @param array $fields Request infromation.
     * @param int $documentId Document id.
     * @return array
     */
    private function getDocumentPrepaymentFields($fields, $documentId)
    {
        $currencies = $this->getCurrencies($fields[ 'percentage_currrency' ]);

        return [
            'document_id'           => $documentId,
            'percentage_prepayment' => $fields[ 'percentage_prepayment' ],
            'percentage_remainder'  => $fields[ 'percentage_remainder' ],
            'remainder_eur'         => $fields[ 'remainder_eur' ],
            'percentage_currrency'  => number_format($fields[ 'percentage_currrency' ], 2, '.', ''),
            'type_payment'          => $fields[ 'type_payment' ],
            'enter_eur'             => empty($fields[ 'enter_eur' ]) ? 0 : $fields[ 'enter_eur' ],
            'enter_usd'             => empty($fields[ 'enter_usd' ]) ? 0 : $fields[ 'enter_usd' ],
            'enter_rub'             => empty($fields[ 'enter_rub' ]) ? 0 : $fields[ 'enter_rub' ],
            'prepayment_amount_eur' => $fields[ 'prepayment_amount_eur' ],
            'prepayment_amount_rub' => $fields[ 'prepayment_amount_rub' ],
            'course_cb_eur'         => $currencies->eur,
            'course_cb_usd'         => $currencies->usd,
            'course_cb_eur_markup'  => $currencies->eur_markup,
            'course_cb_usd_markup'  => $currencies->usd_markup,
            'cross_currrency'       => $fields[ 'cross_currrency' ],
        ];
    }
}
