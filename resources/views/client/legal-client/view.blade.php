@extends('layouts.bootstrap')

@section('breadcrumbs')

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Главная</a></li>
            <li class="breadcrumb-item"><a href="{{ route('legal.client.index') }}">Клиенты: юридические лица</a></li>
            <li class="breadcrumb-item active" aria-current="page">Просмотр клиента</li>
        </ol>
    </nav>

@stop
@section('content')

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <table style="width: 100%">
                        <tr>
                            <td>
                                <h4>Информация об клиенте</h4>
                            </td>

                            <?php if (Auth::user()->isCanEditLegalClient()) : ?>

                                <td style="text-align: right"><a class="btn btn-warning" style="font-size: 14px" href="{{ route('legal.client.edit', ['client' => $client]) }}">Редактировать</a></td>

                            <?php endif; ?>

                        </tr>
                    </table>
                </div>
                <div class="panel-body">
                    <div class="box box-info">
                        <div class="box-body">
                            <div class="col-lg-3">
                                <div  align="center">
                                    <img alt="User Pic" src="/images/nobody_m.original.jpg" id="profile-image1" class="img-circle img-responsive">
                                </div>
                                <br>
                                <!-- /input-group -->
                            </div>
                            <div class="col-lg-3">
                                <h4 style="color:#00b1b1;">{{ $client->personal_full_name }}</h4>
                            </div>
                            <div class="col-lg-6">
                                <table class="table">
                                      <tr>
                                          <td>Наименование полное:</td>
                                          <td>{{ $client->full_name }}</td>
                                      </tr>
                                      <tr>
                                          <td>Наименование короткое:</td>
                                          <td>{{ $client->short_name }}</td>
                                      </tr>
                                      <tr>
                                          <td>Юр. адрес:</td>
                                          <td>{{ $client->address }}</td>
                                      </tr>
                                      <tr>
                                          <td>Телефон:</td>
                                          <td>{{ $client->phone }}</td>
                                      </tr>
                                      <tr>
                                          <td>Email:</td>
                                          <td>{{ $client->email }}</td>
                                      </tr>
                                      <tr>
                                          <td>ОГРН:</td>
                                          <td>{{ $client->ogrn }}</td>
                                      </tr>
                                      <tr>
                                          <td>ИНН:</td>
                                          <td>{{ $client->inn }}</td>
                                      </tr>
                                      <tr>
                                          <td>КПП:</td>
                                          <td>{{ $client->kpp }}</td>
                                      </tr>
                                      <tr>
                                          <td>Банк:</td>
                                          <td>{{ $client->bank }}</td>
                                      </tr>
                                      <tr>
                                          <td>БИК:</td>
                                          <td>{{ $client->bik }}</td>
                                      </tr>
                                      <tr>
                                          <td>р/c:</td>
                                          <td>{{ $client->checking_account }}</td>
                                      </tr>
                                      <tr>
                                          <td>Фамилия генерального директора:</td>
                                          <td>{{ $client->general_director_surname }}</td>
                                      </tr>
                                      <tr>
                                          <td>Имя генерального директора:</td>
                                          <td>{{ $client->general_director_name }}</td>
                                      </tr>
                                      <tr>
                                          <td>Отчество генерального директора:</td>
                                          <td>{{ $client->general_director_lastname }}</td>
                                      </tr>
                                      <tr>
                                          <td>Email генерального директора:</td>
                                          <td>{{ $client->general_director_email }}</td>
                                      </tr>
                                      <tr>
                                          <td>Телефон генерального директора:</td>
                                          <td>{{ $client->general_director_phone }}</td>
                                      </tr>
                                      <tr>
                                          <td>Фамилия контактного лица:</td>
                                          <td>{{ $client->personal_surname }}</td>
                                      </tr>
                                      <tr>
                                          <td>Имя контактного лица:</td>
                                          <td>{{ $client->personal_name }}</td>
                                      </tr>
                                      <tr>
                                          <td>Отчество контактного лица:</td>
                                          <td>{{ $client->personal_lastname }}</td>
                                      </tr>
                                      <tr>
                                          <td>Email контактного лица:</td>
                                          <td>{{ $client->personal_email }}</td>
                                      </tr>
                                      <tr>
                                          <td>Телефон контактного лица:</td>
                                          <td>{{ $client->personal_phone }}</td>
                                      </tr>
                                      <tr>
                                          <td>Адрес доставки:</td>
                                          <td>{{ $client->shipping_address }}</td>
                                      </tr>
                                </table>
                            </div>
                        </div>
                        <!-- /.box -->
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop