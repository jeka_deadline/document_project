<?php

namespace App\Models\Document;

use Illuminate\Database\Eloquent\Model;

/**
 * Model class for table `document_templates`.
 */
class DocumentTemplate extends Model
{
    /**
     * Document template act.
     *
     * @var string
     */
    const TYPE_ACT = 'act';

    /**
     * Document template bill.
     *
     * @var string
     */
    const TYPE_BILL = 'bill';

    /**
     * Document template contract.
     *
     * @var string
     */
    const TYPE_CONTRACT = 'contract';

    /**
     * {@inheritdoc}
     *
     * @access protected
     * @var array $fillable
     */
    protected $fillable = [
        'name',
        'body',
        'type',
    ];

    /**
     * Get document template types.
     *
     * @static
     * @return array
     */
    public static function getTypes()
    {
        return [
            self::TYPE_CONTRACT => 'Договор',
            self::TYPE_BILL => 'Вексель',
            self::TYPE_ACT => 'Акт',
        ];
    }

    /**
     * Get document human type name virtual attribute.
     *
     * @return null|string
     */
    public function getTypeNameAttribute()
    {
        switch ($this->type) {
            case self::TYPE_CONTRACT:
                return 'Договор';
            case self::TYPE_BILL:
                return 'Вексель';
            case self::TYPE_ACT:
                return 'Акт';
            default:
                return null;
        }
    }
}
