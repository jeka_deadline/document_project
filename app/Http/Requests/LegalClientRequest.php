<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LegalClientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'full_name' => 'required|max:255',
            'short_name' => 'required|max:100',
            'address' => 'required',
            'email' => 'email|max:100|nullable',
            'phone' => 'required|regex:#^\+\d{11,13}$#',
            'ogrn' => 'required|regex:#^\d{13}$#',
            'inn' => 'required|regex:#^\d{10}$#',
            'kpp' => 'required|regex:#^\d{9}$#',
            'bank' => 'required|max:255',
            'bik' => 'required|regex:#^\d{9}$#',
            'checking_account' => 'required|max:255',
            'general_director_surname' => 'required|max:50',
            'general_director_name' => 'required|max:50',
            'general_director_lastname' => 'max:50',
            'general_director_email' => 'nullable|email|max:100',
            'general_director_phone' => 'required|regex:#^\+\d{11,13}$#',
            'personal_surname' => 'required|max:50',
            'personal_name' => 'required|max:50',
            'personal_lastname' => 'max:50',
            'personal_email' => 'nullable|email|max:100',
            'personal_phone' => 'required|regex:#^\+\d{11,13}$#',
            'shipping_address' => 'required',
        ];
    }
}
