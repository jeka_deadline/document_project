@extends('layouts.bootstrap')

@section('breadcrumbs')

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Главная</a></li>
            <li class="breadcrumb-item"><a href="{{ route('individual.client.index') }}">Клиенты: физические лица</a></li>
            <li class="breadcrumb-item active" aria-current="page">Добавление клиента</li>
        </ol>
    </nav>

@stop
@section('content')

    @include('client.individual-client.form_create', ['showButtons' => true])

@stop