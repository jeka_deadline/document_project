<?php

namespace App\Http\Controllers\Document;

use PDF;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Document\Document;
use App\Services\ServiceAccountService;
use App\Http\Requests\CreateServiceAccountRequest;
use App\Models\Document\ServiceAccount;
use Illuminate\Support\Facades\Auth;

/**
 * Service account controller.
 */
class ServiceAccountController extends Controller
{
    /**
     * Account service.
     *
     * @access private
     * @var \App\Services\ServiceAccountService $serviceAccountService
     */
    private $serviceAccountService;

    /**
     * Controller constructor.
     *
     * @param \App\Services\ServiceAccountService $serviceAccountService Account service
     * @return void
     */
    public function __construct(ServiceAccountService $serviceAccountService)
    {
        $this->serviceAccountService = $serviceAccountService;
    }

    /**
     * Get modal content form attach account service to document.
     *
     * @param \Illuminate\Http\Request $request Request.
     * @param \App\Models\Document\Document $document Document model.
     * @return string JSON response with modal content.
     */
    public function getAddServiceAccountForm(Request $request, Document $document)
    {
        if (!Auth::user()->isCanCreateServiceAccount()) {
            $this->accessDanied();
        }

        $response[ 'content' ] = view('document.view_modal_new_service_account', [
            'course' => $document->firstPrepayment->course_cb_eur_markup,
            'typePayments' => Document::getTypePaymentsForServiceAccount(),
            'document' => $document,
        ])->render();

        return response()->json($response);
    }

    /**
     * Change course by payment type.
     *
     * @param \Illuminate\Http\Request $request Request.
     * @param \App\Models\Document\Document $document Document model.
     * @return string JSON response with course.
     */
    public function changeTypePayment(Request $request, Document $document)
    {
        $type = $request->input('type_payment', Document::PAYMENT_TYPE_CASH);

        $response[ 'course' ] = $this->serviceAccountService->getCourseWithTypePayment($type, $document);

        return response()->json($response);
    }

    /**
     * Attach account service to document.
     *
     * @param \App\Http\Requests\CreateServiceAccountRequest $request User request.
     * @param \App\Models\Document\Document $document Document model.
     * @return string JSON response.
     */
    public function attachServiceAccountToDocument(CreateServiceAccountRequest $request, Document $document)
    {
        if (!Auth::user()->isCanCreateServiceAccount()) {
            $this->accessDanied();
        }

        $response = [
            'status' => false,
        ];

        if ($this->serviceAccountService->attachToDocument($request->all(), $document)) {
            $response[ 'status' ] = true;
        }

        $response[ 'content' ] = view('document/view_block_service_accounts', compact('document'))->render();

        return response()->json($response);
    }

    /**
     * View service account page.
     *
     * @param \Illuminate\Http\Request $request Request.
     * @param \App\Models\Document\ServiceAccount $serviceAccount Service account model.
     * @return \Illuminate\Support\Facades\View
     */
    public function view(Request $request, ServiceAccount $serviceAccount)
    {
        if (!Auth::user()->isCanViewServiceAccount()) {
            $this->accessDanied();
        }

        $viewBlockCourse = true;

        return view('document.service_account', compact('serviceAccount', 'viewBlockCourse'));
    }

    /**
     * View service account page in pdf format.
     *
     * @param \Illuminate\Http\Request $request Request.
     * @param \App\Models\Document\ServiceAccount $serviceAccount Service account model.
     * @return \Illuminate\Support\Facades\View
     */
    public function pdf(Request $request, ServiceAccount $serviceAccount)
    {
        if (!Auth::user()->isCanViewPdfServiceAccount()) {
            $this->accessDanied();
        }

        $viewBlockCourse = false;

        $pdf = PDF::loadView('document.service_account_pdf', compact('serviceAccount', 'viewBlockCourse'));

        return $pdf->stream('document.pdf');
    }
}
