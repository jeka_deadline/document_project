<?php

namespace App\Services;

use App\Models\Client\IndividualClient;
use App\Models\Client\LegalClient;

/**
 * Service for clients.
 */
class ClientService
{
    /**
     * Get list individual clients with pagination.
     *
     * @param int $countOnPage Count items on page
     * @return
     */
    public function individualClientsWithPaginate($countOnPage = 15)
    {
        return IndividualClient::paginate($countOnPage);
    }

    /**
     * Create new individual client.
     *
     * @param array $fields
     * @return \App\Models\Client\IndividualClient
     */
    public function createNewIndividualClient($fields)
    {
        if (isset($fields[ 'register_address_equal_shipping_address' ]) && $fields[ 'register_address_equal_shipping_address' ] === 'on') {
            $fields[ 'shipping_address' ] = $fields[ 'register_address' ];
        }

        return IndividualClient::create($fields);
    }

    /**
     * Update individual client.
     *
     * @param \App\Models\Client\IndividualClient $individualClient Model individual client
     * @param array $fields
     * @return void
     */
    public function updateIndividualClient(IndividualClient $individualClient, $fields)
    {
        $individualClient->update($fields);
    }

    /**
     * Get list legal clients with pagination.
     *
     * @param int $countOnPage Count items on page
     * @return
     */
    public function legalClientsWithPaginate($countOnPage = 15)
    {
        return LegalClient::paginate($countOnPage);
    }

    /**
     * Create new legal client.
     *
     * @param array $fields
     * @return \App\Models\Client\LegalClient
     */
    public function createNewLegalClient($fields)
    {
        return LegalClient::create($fields);
    }

    /**
     * Update legal client.
     *
     * @param \App\Models\Client\LegalClient $legalClient Model legal client
     * @param array $fields
     * @return void
     */
    public function updateLegalClient(LegalClient $legalClient, $fields)
    {
        $legalClient->update($fields);
    }

    /**
     * Get client contact full name.
     *
     * @param string $clientId Client id.
     * @return string
     */
    public function getClientContactFullNameBySelect($clientId)
    {
        $clientDataId = explode('-', $clientId);

        if (!isset($clientDataId[ 0 ]) || !isset($clientDataId[ 1 ])) {
            dd($clientDataId);
            return null;
        }

        switch ($clientDataId[ 0 ]) {
            case 'l':
                $client = LegalClient::where('id', $clientDataId[ 1 ])->firstOrFail();
                return $client->contactFullName;
            case 'i':
                $client = IndividualClient::where('id', $clientDataId[ 1 ])->firstOrFail();
                return $client->contactFullName;
            default:
                return '';
        }
    }
}
