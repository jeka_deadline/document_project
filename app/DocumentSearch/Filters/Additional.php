<?php

namespace App\DocumentSearch\Filters;

use Illuminate\Database\Eloquent\Builder;

class Additional implements Filterable
{
    public static function apply(Builder $builder, $additionalFields)
    {
        return $builder->whereHas('additionalFields', function($query) use ($additionalFields) {
            foreach ($additionalFields as $field => $value) {
                if (is_null($value)) {
                    continue;
                }
                $query->where($field, 'like', '%' . $value . '%');
            }

            return $query;
        });
    }
}