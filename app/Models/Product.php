<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Model class for table `products`.
 */
class Product extends Model
{
    /**
     * Product location shop.
     *
     * @var string
     */
    const LOCATION_SHOP = 'shop';

    /**
     * Product location stock.
     *
     * @var string
     */
    const LOCATION_STOCK = 'stock';

    /**
     * Product status production.
     *
     * @var string
     */
    const STATUS_PRODUCTION = 'production';

    /**
     * Product status transportation.
     *
     * @var string
     */
    const STATUS_TRANSPORTATION = 'transportation';

    /**
     * Product status coming sweet.
     *
     * @var string
     */
    const STATUS_COMING_SWEET = 'coming-sweet';

    /**
     * Product status warehousing.
     *
     * @var string
     */
    const STATUS_WAREHOUSING = 'warehousing';

    /**
     * Product status delivery customer.
     *
     * @var string
     */
    const STATUS_DELIVERY_CUSTOMER = 'delivery-customer';

    /**
     * Product status delivered.
     *
     * @var string
     */
    const STATUS_DELIVERED = 'delivered';

    /**
     * Product status store.
     *
     * @var string
     */
    const STATUS_STORE = 'store';

    /**
     * Product status shipping pickup.
     *
     * @var string
     */
    const STATUS_SHIPPING_PICKUP = 'shipping-pickup';

    /**
     * Product status delivery.
     *
     * @var string
     */
    const STATUS_DELIVERY = 'delivery';

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    protected $fillable = [
        'name',
        'model',
        'color',
        'count',
        'count_packages',
        'price',
        'factory_id',
        'location',
        'status',
        'comment',
    ];

    /**
     * Get product formatted properties.
     *
     * @return string
     */
    public function getPropertiesAttribute()
    {
        return sprintf('%s / %s / %s / %s / %s / %s / %s',
            $this->name,
            $this->count,
            $this->count_packages,
            $this->price,
            $this->factory->name,
            $this->humanStatus,
            $this->comment
        );
    }

    /**
     * Get human status.
     *
     * @return null|string
     */
    public function getHumanStatusAttribute()
    {
        $statuses = array_merge(self::getListOrderStatuses(), self::getListExpositionStatuses());

        if (isset($statuses[ $this->status ])) {
            return $statuses[ $this->status ];
        }

        return null;
    }

    /**
     * Get list orders statuses.
     *
     * @static
     * @return array
     */
    public static function getListOrderStatuses()
    {
        return [
            Product::STATUS_PRODUCTION => 'производство',
            Product::STATUS_TRANSPORTATION => 'транспортировка',
            Product::STATUS_COMING_SWEET => 'приход на слад',
            Product::STATUS_WAREHOUSING => 'хранение на складе',
            Product::STATUS_DELIVERY_CUSTOMER => 'доставка клиенту',
            Product::STATUS_DELIVERED => 'доставлено',
        ];
    }

    /**
     * Get list exposition statuses.
     *
     * @static
     * @return array
     */
    public static function getListExpositionStatuses()
    {
        return [
            Product::STATUS_STORE => 'магазин/склад',
            Product::STATUS_SHIPPING_PICKUP => 'отгрузка / самовывоз',
            Product::STATUS_DELIVERY => 'доставка',
            Product::STATUS_DELIVERED => 'доставлено',
        ];
    }

    /**
     * Relation with factory.
     *
     * @return
     */
    public function factory()
    {
        return $this->belongsTo('App\Models\Factory');
    }

    /**
     * Relation with document.
     *
     * @return
     */
    public function document()
    {
        return $this->belongsTo('App\Models\Document\Document');
    }
}
