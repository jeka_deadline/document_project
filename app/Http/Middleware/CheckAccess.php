<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Setting;

class CheckAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $pin = $request->session()->get(Setting::KEY_ENTER_PIN);

        if (!$pin) {
            return redirect('news');
        }

        return $next($request);
    }
}
