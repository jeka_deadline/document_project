<?php

namespace App\DocumentSearch\Filters;

use Illuminate\Database\Eloquent\Builder;

class RegisterNumber implements Filterable
{
    public static function apply(Builder $builder, $value)
    {
        return $builder->where('register_number', $value);
    }
}