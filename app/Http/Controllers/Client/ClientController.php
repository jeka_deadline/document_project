<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\ClientService;

/**
 * Client controller.
 */
class ClientController extends Controller
{
    /**
     * client service.
     *
     * @var \App\Services\ClientService $clientService
     * @access private
     */
    private $clientService;

    /**
     * Controller constructor.
     *
     * @param \App\Services\ClientService $workerService Client service
     * @return void
     */
    public function __construct(ClientService $clientService)
    {
        $this->clientService = $clientService;
    }

    /**
     * Display page with all individual clients.
     *
     * @param \Illuminate\Http\Request $request Request
     * @return \Illuminate\Support\Facades\View
     */
    public function getClientContactFullName(Request $request)
    {
        return response()->json([
            'contact_full_name' => $this->clientService->getClientContactFullNameBySelect($request->client),
        ]);
    }
}
