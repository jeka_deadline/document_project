<?php

namespace App\DocumentSearch\Filters;

use DateTime;
use Illuminate\Database\Eloquent\Builder;

class DateFinish implements Filterable
{
    public static function apply(Builder $builder, $value)
    {
        $dateData = explode(' - ', $value);

        if (isset($dateData[ 0 ], $dateData[ 1 ])) {
            $dateTime = new DateTime($dateData[ 0 ]);
            $dateData[ 0 ] = $dateTime->format('Y-m-d');
            $dateTime = new DateTime($dateData[ 1 ]);
            $dateData[ 1 ] = $dateTime->format('Y-m-d');

            return $builder->whereBetween('date_finish', $dateData);
        }

        return $builder;
    }
}