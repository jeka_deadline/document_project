$(function() {

    // dynamic phone inputs
    if ($('.block-phones').length) {
        $('.block-phones').addel({
            classes: {
                target: 'target'
            },
            animation: {
                duration: 100
            },
            events: {
                added: function(e) {
                    if ($(e.added).find('.phone-mask').length) {
                        $(e.added).find('.phone-mask').inputmask("+9{11,13}", {
                            clearIncomplete: true,
                        });
                    }
                }
            }
        });
    }

    // dynamic additional phone inputs
    if ($('.block-additional-phones').length) {
        $('.block-additional-phones').addel({
            classes: {
                target: 'target'
            },
            animation: {
                duration: 100
            },
            events: {
                added: function(e) {
                    if ($(e.added).find('.phone-mask').length) {
                        $(e.added).find('.phone-mask').inputmask("+9{11,13}", {
                            clearIncomplete: true,
                        });
                    }
                }
            }
        });
    }

    $(document).on('submit', '.delete-form', function( e ) {
        return confirm('Вы действительно хотите удалить эту запись?');
    });

    // select2 init
    if ($('.select2').length) {
        $('.select2').select2({
            placeholder: '',
        });
    }

    // phone input mask
    if ($(".phone-mask").length) {
        $(".phone-mask").inputmask("+9{11,13}", {
            clearIncomplete: true,
        });
    }

    // ogrn input mask
    if ($(".ogrn-mask").length) {
        $(".ogrn-mask").inputmask("9{13}", {
            clearIncomplete: true,
        });
    }

    // inn input mask
    if ($(".inn-mask").length) {
        $(".inn-mask").inputmask("9{10}", {
            clearIncomplete: true,
        });
    }

    // kpp input mask
    if ($(".kpp-mask").length) {
        $(".kpp-mask").inputmask("9{9}", {
            clearIncomplete: true,
        });
    }

    // bik input mask
    if ($(".bik-mask").length) {
        $(".bik-mask").inputmask("049{7}", {
            clearIncomplete: true,
        });
    }

    if ($('.integer-positive-mask').length) {
        $('.integer-positive-mask').inputmask({
            alias: 'integer',
            allowMinus: false,
            rightAlign: false,
        });
    }

    if ($('.numeric-positive-mask').length) {
        $('.numeric-positive-mask').inputmask({
            alias: 'numeric',
            allowMinus: false,
            rightAlign: false,
        });
    }

    if ($('.email-mask').length) {
        $('.email-mask').inputmask({
            mask: "*{1,20}[.*{1,20}][.*{1,20}][.*{1,20}]@*{1,20}[.*{2,6}][.*{1,2}]",
            greedy: false,
            onBeforePaste: function(pastedValue, opts) {
                pastedValue = pastedValue.toLowerCase();
                return pastedValue.replace("mailto:", "");
            },
            definitions: {
                '*': {
                    validator: "[0-9A-Za-z!#$%&'*+/=?^_`{|}~\-]",
                    casing: "lower"
                }
            }
        });
    }
});