<div id="block-document-info" class="panel panel-primary">
    <div class="panel-heading">{{ $document->clientFullName }} | {{ $document->created_at }}</div>
    <div class="panel-body">
          <div class="row">
                <div class="col-lg-12">
                    <p><b>Договор №</b> {{ $document->register_number }} <b>Тип</b> {{ $document->humanName}}</p>
                    <p><b>Дата создания</b> <span style="text-decoration: underline;">{{ $document->created_at->format('d.m.Y') }}</span> / <b>Срок в рабочих днях:</b> <span style="text-decoration: underline;">{{ $document->period_execution }}</span> / <b>Дата завершения:</b> <span style="text-decoration: underline;">{{ $document->date_finish->format('d.m.Y') }}</span></p>
                </div>
                <div class="col-lg-8">
                    <p><b>Организация:</b> {{ $document->organization }}</p>
                    <p><b>Создал:</b> {{ $document->workerFullName }}</p>
                    <p><b>Менеджер:</b> {{ $document->managerFullName }}</p>
                    <p><b>Сумма договора:</b> {{ $document->sum }} EUR</p>
                    <p><b>Остаток:</b> {{ $document->sum - $document->sumAllPayments }} EUR</p>
                    <p><b>Предоплата, %:</b> {{ number_format($document->percentageAllPayments, 2, '.', '') }}</p>


                    <p><b>Проформа:</b> {{ $document->confirm_pro_form }} <b>Дата:</b> {{ $document->created_at->format('d.m.y') }}</p>
                </div>

                <div class="col-lg-4">
                    <br>
                    <div class="panel panel-success">
                          <div class="panel-body">
                                <p><b>Курс ЦБ:</b> <span style="display: inline-block; padding: 2px 7px; margin: 0px 5px; border: 1px solid">{{ $document->percentage_currrency }}</span>% </p>
                          </div>
                    </div>
                </div>
          </div>
    </div>
</div>
