<?php

namespace App\DocumentSearch\Filters;

use DateTime;
use Illuminate\Database\Eloquent\Builder;

class ServiceAccount implements Filterable
{
    public static function apply(Builder $builder, $serviceFields)
    {
        $flagEmpty = true;

        foreach ($serviceFields as $value) {
            if (!empty($value)) {
                $flagEmpty = false;
                break;
            }
        }

        if ($flagEmpty) {
            return $builder;
        }

        return $builder->whereHas('serviceAccount', function($query) use ($serviceFields) {
            foreach ($serviceFields as $key => $value) {
                if ($key == 'date') {
                    $dateData = explode(' - ', $value);

                    if (isset($dateData[ 0 ], $dateData[ 1 ])) {
                        $dateTime = new DateTime($dateData[ 0 ]);
                        $dateData[ 0 ] = $dateTime->format('Y-m-d') . ' 00:00:00';
                        $dateTime = new DateTime($dateData[ 1 ]);
                        $dateData[ 1 ] = $dateTime->format('Y-m-d') . ' 23:59:59';

                        $query->whereBetween('created_at', $dateData);
                    }
                }
                if ($key == 'eur') {
                    $value = explode(';', $value);

                    if (isset($value[ 0 ], $value[ 1 ])) {
                        $query->where('full_sum_eur_with_promotion', '>=', $value[ 0 ])
                            ->where('full_sum_eur_with_promotion', '<=', $value[ 1 ]);
                    }
                }
            }
        });
    }
}