<table id="view-service-account-table">
    <tr>
        <td style="width: 25%">
            <span style="font-weight: bold; font-size: 26px">La Casa</span>
            <p>МЕБЕЛЬНЫЙ САЛОН</p>
        </td>
        <td>
            <p>Поставщик: {{ $serviceAccount->organization }}</p>
            <p>Т/ф: {{ $serviceAccount->organizationPhone }}</p>
            <p>адресс: {{ $serviceAccount->organizationAddress }}</p>
        </td>
        <td></td>
    </tr>
    <tr>
        <td colspan="3">К счету № {{ $serviceAccount->documentRegisterNumber }}</td>
    </tr>
    <tr>
        <td colspan="3">Дата доставки: {{ $serviceAccount->date_delivery }}</td>
    </tr>
    <tr>
        <td colspan="3">Дата сборки: {{ $serviceAccount->date_assembly }}</td>
    </tr>
    <tr>
        <td colspan="3">Заказчик: {{ $serviceAccount->clientFullName }}</td>
    </tr>
    <tr>
        <td colspan="3">Доверенное лицо: {{ $serviceAccount->clientContactFullName }}</td>
    </tr>
    <tr>
        <td colspan="3">Телефон: {{ $serviceAccount->clientPhones }}</td>
    </tr>
    <tr>
        <td colspan="3">Адрес: {{ $serviceAccount->clientShippingAddress }}</td>
    </tr>

    @if ($viewBlockCourse)

        <tr>
            <td></td>
            <td></td>
            <td>Курс: {{ $serviceAccount->course }}</td>
        </tr>

    @endif

    <tr>
        <td colspan="3">
            <table border="1" cellpadding="0" cellspacing="0">
                <tr>
                    <th style="width: 25%">Наименование</th>
                    <th>Базовая цена</th>
                    <th>Сумма</th>
                </tr>
                <tr>
                    <td>1. Доставка до подъезда</td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>а) в пределах МКАД</td>
                    <td>45 ЕВРО</td>
                    <td>{{ ($serviceAccount->mkad == 'mkad') ? $serviceAccount->sum_delivery : '-' }}</td>
                </tr>
                <tr>
                    <td>б) за МКАД</td>
                    <td>1 ЕВРО/км</td>
                    <td>{{ ($serviceAccount->mkad != 'mkad') ? $serviceAccount->sum_delivery : '-' }}</td>
                </tr>
                <tr>
                    <td>2. Подъем на {{ $serviceAccount->floor }} этаж и занос в квартиру</td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>а) вручную</td>
                    <td>5 ЕВРО/этаж</td>
                    <td>{{ $serviceAccount->manual }}</td>
                </tr>
                <tr>
                    <td>б)вручную крупногабаритный груз</td>
                    <td>10 ЕВРО/этаж</td>
                    <td>{{ $serviceAccount->manual_large_sized }}</td>
                </tr>
                <tr>
                    <td>в) на лифте</td>
                    <td>10 ЕВРО</td>
                    <td>{{ $serviceAccount->on_lift }}</td>
                </tr>
                <tr>
                    <td>
                        3. Сборка мебели и установка
                        <p>(не включает  возможные дополнительные работы, необходимость в которых, возникает в процессе сборки мебели).</p>
                    </td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>4. Примечание: вынос упаковки</td>
                    <td>договорная</td>
                    <td>{{ $serviceAccount->removing_packaging }}</td>
                </tr>
                <tr>
                    <td></td>
                    <td>Сумма в ЕВРО</td>
                    <td>{{ $serviceAccount->full_sum_eur }}</td>
                </tr>
                <tr>
                    <td></td>
                    <td>Скидка</td>
                    <td>{{ $serviceAccount->promotion_sum_eur }}</td>
                </tr>
                <tr>
                    <td></td>
                    <td>Итого к оплате ЕВРО</td>
                    <td>{{ $serviceAccount->finishSumEur }}</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="3"><br><b>Прием мебели осуществляет заказчик. В случае невозможности принять мебель заказчиком, при отгрузке товара доверенному лицу заказчика, обязательно наличие доверенности. В случае не соблюдения выше указанных условий, товар не отгружается.</b></td>
    </tr>
    <tr>
        <td colspan="3">Фирма не производит подключение техники и сантехники!!!</td>
    </tr>
    <tr>
        <td colspan="3">Оплата производиться в рублях по курсу ЕВРО ЦБ РФ +2% на день оплаты.</td>
    </tr>
    <tr>
        <td colspan="3">Безналичная оплата производится в рублях по курсу ЕВРО ЦБ РФ+3% на день оплаты.</td>
    </tr>
    <tr>
        <td colspan="3">Итого оплачено: {{ ($serviceAccount->promotion_sum_eur) ? $serviceAccount->full_sum_rub_with_promotion : $serviceAccount->full_sum_rub }} рублей</td>
    </tr>
    <tr>
        <td colspan="3">Согласовано с заказчиком:</td>
    </tr>
    <tr>
        <td>Продавец:</td>
        <td>Заказчик:</td>
        <td></td>
    </tr>
    <tr>
        <td colspan="3" style="text-align: center; font-weight: bold;">ОТГРУЗКУ РАЗРЕШАЮ:</td>
    </tr>
    <tr>
        <td colspan="3">Контакты сервисной службы</td>
    </tr>
    <tr>
        <td>Доставка и сборка</td>
        <td>
            <p>Кондратьева Лариса Сергеевна</p>
            <p>dostavkalacasa@yandex.ru</p>
        </td>
        <td>
            <p>8-499-189-50-32</p>
        </td>
    </tr>
</table>
