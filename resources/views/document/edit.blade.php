@extends('layouts.bootstrap')

@section('css')

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css">

@stop
@section('breadcrumbs')

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Главная</a></li>
            <li class="breadcrumb-item"><a href="{{ route('document.index') }}">Список договоров</a></li>
            <li class="breadcrumb-item active" aria-current="page">Обновление договора</li>
        </ol>
    </nav>

@stop
@section('content')

    <div class="row">
        <div class="col-lg-12">
              <h2>Обновление договора</h2>
              <form method="post" action="{{ route('document.update', compact('document')) }}">
                  {{ csrf_field() }}
                  {{ method_field('PUT') }}

                  <div>

                      <!-- Nav tabs -->
                      <ul class="nav nav-tabs" role="tablist">
                          <li role="presentation" class="active"><a href="#main" aria-controls="main" role="tab" data-toggle="tab">Основное</a></li>
                          <li role="presentation"><a href="#main2" aria-controls="main2" role="tab" data-toggle="tab">Основное 2</a></li>
                      </ul>

                      <!-- Tab panes -->
                      <div class="tab-content">
                          <div role="tabpanel" class="tab-pane active" id="main">

                              <!-- block name -->
                              <div class="form-group">
                                  <label class="control-label" for="">Наименование</label>
                                  <input type="text" class="form-control" value="{{ $document->humanName }}" disabled>
                              </div>
                              <!-- end block name -->

                              <!-- block code -->
                              <div class="form-group">
                                  <label class="control-label" for="">Код</label>
                                  <input type="text" class="form-control" name="code" value="{{ $document->code }}" disabled>
                              </div>
                              <!-- end block code -->

                              <!-- block prepared -->
                              <div class="form-group">
                                  <label class="control-label" for="">Подготовил</label>
                                  <input type="text" class="form-control" name="prepared" value="{{ $document->workerFullName }}" disabled>

                                  @if ($errors->has('prepared'))

                                      <div class="text-danger">

                                            {{ $errors->first('prepared') }}

                                      </div>

                                  @endif

                              </div>
                              <!-- end block prepared -->

                              <!-- block date -->
                              <div class="form-group">
                                  <label class="control-label" for="">Дата</label>
                                  <input type="text" class="form-control" name="date" value="{{ $document->date }}" disabled>
                              </div>
                              <!-- end block date -->

                              <!-- block manager -->
                              <div class="form-group">
                                  <label class="control-label" for="">Менеджер</label>
                                  <select class="form-control select2" name="manager_worker_id" {{ (!$document->canEdit) ? 'disabled' : '' }}>
                                      <option value=""></option>

                                      @foreach ($dropDownInformation->listManagers as $worker)

                                          <option value="{{ $worker->id }}" {{ (old('manager_worker_id', $document->manager_worker_id) == $worker->id) ? 'selected' : '' }}>{{ $worker->full_name }}</option>

                                      @endforeach

                                  </select>

                                  @if ($errors->has('manager_worker_id'))

                                      <div class="text-danger">

                                            {{ $errors->first('manager_worker_id') }}

                                      </div>

                                  @endif

                              </div>
                              <!-- end block manager -->

                              <!-- block client -->
                              <div class="form-group">
                                  <label class="control-label" for="">Клиент</label>
                                  <input type="text" class="form-control" value="{{ $document->clientFullName }}" disabled>
                              </div>
                              <!-- end block client -->

                              <!-- block contact client -->
                              <div class="form-group">
                                  <label class="control-label" for="">Контактное лицо клиента</label>
                                  <input id="input-contact-client" type="text" class="form-control" name="contact_client" value="{{ $document->clientContactFullName }}" disabled>

                                  @if ($errors->has('contact_client'))

                                      <div class="text-danger">

                                            {{ $errors->first('contact_client') }}

                                      </div>

                                  @endif

                              </div>
                              <!-- end block contact client -->

                          </div>
                          <div role="tabpanel" class="tab-pane" id="main2">

                              <!-- block type document -->
                              <div class="form-group">
                                  <label class="control-label" for="">Вид</label>
                                  <input type="text" class="form-control" value="{{ $document->humanTypePayment }}" disabled>
                              </div>
                              <!-- end block type document -->

                              <!-- block state -->
                              <div class="form-group">
                                  <label class="control-label" for="">Состояние</label>
                                  <select class="form-control" name="state" {{ (!$document->canEdit) ? 'disabled' : '' }}>

                                      @foreach ($dropDownInformation->states as $state => $label)

                                          <option value="{{ $state }}" {{ (old('state', $document->state)) == $state ? 'selected' : '' }}>{{ $label }}</option>

                                      @endforeach

                                  </select>

                                  @if ($errors->has('state'))

                                      <div class="text-danger">

                                            {{ $errors->first('state') }}

                                      </div>

                                  @endif

                              </div>
                              <!-- end block state -->

                              @if ($document->state == $document::STATE_DEVELOPED)

                                  <div class="form-group">
                                      <label class="control-label" for="">Сумма</label>
                                      <input class="form-control" type="text" name="sum" value="{{ $document->sum }}">
                                  </div>

                              @endif

                              <!-- block order state -->
                              <div class="form-group">
                                  <label class="control-label" for="">Статус заказа</label>
                                  <select class="form-control" name="order_state" {{ (!$document->canEdit) ? 'disabled' : '' }}>
                                      <option value=""></option>

                                      @foreach ($listOrderStatuses as $state => $label)

                                          <option value="{{ $state }}" {{ (old('order_state', $document->order_state)) == $state ? 'selected' : '' }}>{{ $label }}</option>

                                      @endforeach

                                  </select>

                                  @if ($errors->has('order_state'))

                                      <div class="text-danger">

                                            {{ $errors->first('order_state') }}

                                      </div>

                                  @endif

                              </div>
                              <!-- end block order state -->

                              <!-- block register number -->
                              <div class="row">
                                    <div class="col-lg-6">

                                        @if ($document->register_number)

                                            <div class="form-group">
                                                <label class="control-label" for="">Рег. номер</label>
                                                <input type="text" class="form-control" value="{{ $document->register_number }}" disabled>
                                            </div>

                                        @else

                                            <div class="form-group">
                                                <label class="control-label" for="">Рег. номер</label>
                                                <input type="text" class="form-control numeric-positive-mask" name="register_number" value="{{ old('register_number', $document->register_number) }}" {{ (!$document->canEdit) ? 'disabled' : '' }}>
                                                <div class="help-block">Последний рег. номер - {{ $lastRegisterNumber }}</div>

                                                @if ($errors->has('register_number'))

                                                    <div class="text-danger">

                                                          {{ $errors->first('register_number') }}

                                                    </div>

                                                @endif

                                            </div>

                                        @endif

                                    </div>
                                    <div class="col-lg-6">

                                        <div class="form-group">
                                            <label class="control-label" for="">от:</label>
                                            <input type="text" class="form-control" name="from" value="{{ $document->from }}" disabled>
                                          </div>

                                    </div>
                              </div>
                              <!-- end block register number -->

                              <!-- block organization -->
                              <div class="form-group">
                                  <label class="control-label" for="">Организация</label>
                                  <input type="text" class="form-control" value="{{ $document->organization }}" disabled>
                              </div>
                              <!-- end block organization -->

                              <!-- block confirm pro form -->
                              <div class="form-group">
                                  <label class="control-label" for="">Подтверждение проформы</label>
                                  <input type="text" id="input-pro-form" class="form-control" name="confirm_pro_form" value="{{ old('confirm_pro_form', $document->confirm_pro_form) }}" {{ (!$document->canEdit) ? 'disabled' : '' }}>

                                  @if ($errors->has('confirm_pro_form'))

                                      <div class="text-danger">

                                            {{ $errors->first('confirm_pro_form') }}

                                      </div>

                                  @endif

                                  @if ($document->canEdit)

                                      <div id="datetimepicker"></div>

                                  @endif

                              </div>
                              <!-- end block confirm pro form -->

                              <!-- block period execution -->
                              <div class="form-group">
                                  <label class="control-label" for="">Срок исполнения</label>
                                  <input type="text" class="form-control" value="{{ $document->period_execution }}" disabled>
                              </div>
                              <!-- end block period execution -->

                              <!-- block date_finish -->
                              <div class="form-group">
                                  <label class="control-label" for="">Дата окончания</label>
                                  <input type="text" class="form-control" value="{{ $document->date_finish }}" disabled>
                              </div>
                              <!-- block date_finish -->

                              <!-- block performance -->
                              <div class="row">
                                    <div class="col-lg-3">
                                        <label for="">Дата исполнения</label>
                                    </div>
                                    <div class="col-lg-3">

                                        <div class="form-group">
                                            <input type="text" class="form-control" value="{{ $document->performance_date }}" disabled>
                                        </div>

                                    </div>
                                    <div class="col-lg-2">

                                        <div class="checkbox">
                                            <label><input type="checkbox" name="performance" {{ ($document->performance_date ? 'checked' : '' ) }} {{ (!$document->canEdit) ? 'disabled' : '' }}>Исполнено</label>
                                        </div>

                                    </div>
                              </div>
                              <!-- end block performance -->

                              <!-- block ternimated -->
                              <div class="row">
                                  <div class="col-lg-2">

                                      <div class="checkbox">
                                          <label><input type="checkbox" name="ternimated" {{ ($document->ternimated_date ? 'checked' : '' ) }} {{ (!$document->canEdit) ? 'disabled' : '' }}>Расторгнут</label>
                                      </div>

                                  </div>
                                  <div class="col-lg-10">

                                       <div class="form-group">
                                          <input type="text" class="form-control" name="text_ternimated" value="{{ old('text_ternimated', $document->text_ternimated) }}" {{ (!$document->canEdit) ? 'disabled' : '' }}>

                                          @if ($errors->has('text_ternimated'))

                                              <div class="text-danger">

                                                    {{ $errors->first('text_ternimated') }}

                                              </div>

                                          @endif

                                      </div>

                                  </div>
                              </div>
                              <!-- end block ternimated -->

                          </div>
                      </div>

                  </div>

                  @if ($document->canEdit)

                      <input type="submit" value="Обновить" class="btn btn-primary">

                  @endif

                  <a class="btn btn-info" href="{{ route('document.index') }}">Назад</a>
              </form>
        </div>
    </div>

@stop
@section('javascript')
    @parent

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
    <script src="/js/document.js"></script>

@stop