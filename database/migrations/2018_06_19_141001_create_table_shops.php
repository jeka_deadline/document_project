<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableShops extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // create table shops
        Schema::create('shops', function (Blueprint $table) {
            $table->increments('id');
            $table->string('full_name', 255);
            $table->string('short_name', 100);
            $table->mediumText('address');
            $table->string('phone', 20);
            $table->string('ogrn', 13);
            $table->string('inn', 10);
            $table->string('kpp', 9);
            $table->string('bank', 255);
            $table->string('bik', 9);
            $table->string('checking_account', 255);
            $table->timestamps();
        });

        // create table shop user leaders
        Schema::create('shop_user_leaders', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('role_id');
            $table->unsignedInteger('shop_id');
            $table->unsignedInteger('worker_id');
            $table->timestamps();

            $table->index('worker_id');
            $table->foreign('worker_id')
                ->references('id')
                ->on('workers')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->index('shop_id');
            $table->foreign('shop_id')
                ->references('id')
                ->on('shops')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->index('role_id');
            $table->foreign('role_id')
                ->references('id')
                ->on('roles')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shop_user_leaders');
        Schema::dropIfExists('shops');
    }
}
