<?php

namespace App\Services;

use App\Models\User\Role;
use App\Models\User\UserRole;
use App\Repositories\User\UserRepository;
use App\Models\User\Worker;
use Illuminate\Support\Facades\Auth;

/**
 * Service for worker controller.
 */
class WorkerService
{
    /**
     * User repository.
     *
     * @access protected
     * @var \App\Repositories\User\UserRepository $userRepository
     */
    protected $userRepository;

    /**
     * Constructor for class.
     *
     * @param \App\Repositories\User\UserRepository User repository
     * @return void
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * Get list workers.
     *
     * @param int $paginate Count workers on page.
     * @return mixed
     */
    public function getWorkersWithPaginate($paginate = 15)
    {
        return $this->userRepository->getWorkersWithPaginate($paginate);
    }

    /**
     * Pluck list roles.
     *
     * @param string $key Field name key name.
     * @param string $value Field name key value.
     * @return array
     */
    public function pluckListRoles($key = 'id', $value = 'humanName')
    {
        $roles = Role::all();

        $list = [];

        $isAdmin = Auth::user()->isAdmin();

        foreach ($roles as $role) {
            if (!$isAdmin && $role->name == 'admin') {
                continue;
            }
            $list[ $role->{$key} ] = $role->{$value};
        }

        return $list;
    }

    /**
     * Create new worker.
     *
     * @param array $fields Worker fields.
     * @return bool
     */
    public function createNewWorker($fields)
    {
        $user = $this->userRepository->createNewUser($fields[ 'email' ], $fields[ 'password' ]);

        if (!$user) {
            return false;
        }

        if (!$this->attachRoleForUser($user->id, $fields[ 'role' ]) ||
            !$this->userRepository->createNewWorker($user->id, $fields)
        ) {
            return false;
        }

        return true;
    }

    /**
     * Update worker.
     *
     * @param \App\Models\User\Worker $worker Worker model
     * @param array $fields Worker update fields
     * @return void
     */
    public function updateWorker(Worker $worker, $fields)
    {
        $this->userRepository->updateUser($worker->user, $fields[ 'email' ], $fields[ 'password' ]);
        $this->attachRoleForUser($worker->user->id, $fields[ 'role' ]);
        $this->userRepository->updateWorker($worker, $fields);
    }

    /**
     * Attach role for user.
     *
     * @access private
     * @param int $userId User id.
     * @param string $roleName Role name.
     * @return bool
     */
    private function attachRoleForUser($userId, $roleName)
    {
        $role = Role::where('id', $roleName)
            ->first();

        if (!$role) {
            return false;
        }

        $role = UserRole::updateOrCreate([
            'user_id' => $userId,
        ], [
            'user_id' => $userId,
            'role_id' => $role->id,
        ]);

        if (!$role) {

            return false;
        }

        return true;
    }
}
