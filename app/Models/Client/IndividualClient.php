<?php

namespace App\Models\Client;

use Illuminate\Database\Eloquent\Model;

/**
 * Model for table `individual_clients`
 */
class IndividualClient extends Model
{
    /**
     * {@inheritdoc}
     *
     * @access protected
     * @var array $fillable
     */
    protected $fillable = [
        'surname',
        'name',
        'lastname',
        'register_address',
        'shipping_address',
        'email',
        'phones',
        'additional_surname',
        'additional_name',
        'additional_lastname',
        'additional_phones',
        'additional_email',
        'comment',
    ];

    /**
     * Setter for set client phones.
     *
     * @param string $value Phones value.
     * @return void
     */
    public function setPhonesAttribute($value)
    {
        if (!is_array($value)) {
            $value = [$value];
        }

        $this->attributes[ 'phones' ] = json_encode($value);
    }

    /**
     * Setter for set client additional phones.
     *
     * @param string $value Additional phones value.
     * @return void
     */
    public function setAdditionalPhonesAttribute($value)
    {
        if (!is_array($value)) {
            $value = [$value];
        }

        $this->attributes[ 'additional_phones' ] = json_encode($value);
    }

    /**
     * Get phones as array.
     *
     * @return array
     */
    public function getPhonesAttribute()
    {
        $phones = json_decode($this->attributes[ 'phones' ]);

        if (!is_array($phones)) {
            return [];
        }

        return $phones;
    }

    /**
     * Get additional phones as array.
     *
     * @return array
     */
    public function getAdditionalPhonesAttribute()
    {
        $phones = json_decode($this->attributes[ 'additional_phones' ]);

        if (!is_array($phones)) {
            return [];
        }

        return $phones;
    }

    /**
     * Get individual client full name.
     *
     * @return array
     */
    public function getFullNameAttribute()
    {
        return $this->surname . ' ' . $this->name . ' ' . $this->lastname;
    }

    /**
     * Get individual client contact full name.
     *
     * @return string
     */
    public function getContactFullNameAttribute()
    {
        return $this->additional_surname . ' ' . $this->additional_name . ' ' . $this->additional_lastname;
    }
}
