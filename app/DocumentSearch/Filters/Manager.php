<?php

namespace App\DocumentSearch\Filters;

use Illuminate\Database\Eloquent\Builder;

class Manager implements Filterable
{
    public static function apply(Builder $builder, $value)
    {
        return $builder->whereHas('manager', function($query) use ($value) {
            return $query->where('surname', 'like', '%' . $value . '%')
                ->orWhere('name', 'like', '%' . $value . '%')
                ->orWhere('lastname', 'like', '%' . $value . '%');
        });
    }
}