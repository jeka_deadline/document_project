@extends('layouts.bootstrap')

@section('breadcrumbs')

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Главная</a></li>
            <li class="breadcrumb-item"><a href="{{ route('legal.client.index') }}">Клиенты: юридические лица</a></li>
            <li class="breadcrumb-item active" aria-current="page">Обновление клиента</li>
        </ol>
    </nav>

@stop
@section('content')

    <div class="row">
        <div class="col-lg-12">
              <h2>Обновление клиента: {{ $client->personal_surname }} {{ $client->personal_name }}</h2>
              <form method="post" action="{{ route('legal.client.update', ['client' => $client]) }}">
                  {{ csrf_field() }}
                  {{ method_field('PUT') }}

                  <div class="form-group">
                      <label class="control-label" for="">Наименование полное</label>
                      <input class="form-control" type="text" name="full_name" value="{{ old('full_name', $client->full_name) }}">

                      @if ($errors->has('full_name'))

                          <div class="text-danger">

                                {{ $errors->first('full_name') }}

                          </div>

                      @endif

                  </div>

                  <div class="form-group">
                      <label class="control-label" for="">Наименование короткое</label>
                      <input class="form-control" type="text" name="short_name" value="{{ old('short_name', $client->short_name) }}">

                      @if ($errors->has('short_name'))

                          <div class="text-danger">

                                {{ $errors->first('short_name') }}

                          </div>

                      @endif

                  </div>

                  <div class="form-group">
                      <label class="control-label" for="">Юр. адрес</label>
                      <textarea class="form-control" rows="6" type="text" name="address">{{ old('address', $client->address) }}</textarea>

                      @if ($errors->has('address'))

                          <div class="text-danger">

                                {{ $errors->first('address') }}

                          </div>

                      @endif

                  </div>

                  <div class="form-group">
                      <label class="control-label" for="">Телефон</label>
                      <input class="form-control phone-mask" type="text" name="phone" value="{{ old('phone', $client->phone) }}">

                      @if ($errors->has('phone'))

                          <div class="text-danger">

                                {{ $errors->first('phone') }}

                          </div>

                      @endif

                  </div>

                  <div class="form-group">
                      <label class="control-label" for="">Email</label>
                      <input class="form-control email-mask" type="text" name="email" value="{{ old('email', $client->email) }}">

                      @if ($errors->has('email'))

                          <div class="text-danger">

                                {{ $errors->first('email') }}

                          </div>

                      @endif

                  </div>

                  <div class="form-group">
                      <label class="control-label" for="">ОГРН</label>
                      <input class="form-control ogrn" type="text" name="ogrn" value="{{ old('ogrn', $client->ogrn) }}">

                      @if ($errors->has('ogrn'))

                          <div class="text-danger">

                                {{ $errors->first('ogrn') }}

                          </div>

                      @endif

                  </div>


                 <div class="form-group">
                      <label class="control-label" for="">ИНН</label>
                      <input class="form-control inn" type="text" name="inn" value="{{ old('inn', $client->inn) }}">

                      @if ($errors->has('inn'))

                          <div class="text-danger">

                                {{ $errors->first('inn') }}

                          </div>

                      @endif

                  </div>

                  <div class="form-group">
                      <label class="control-label" for="">КПП</label>
                      <input class="form-control kpp" type="text" name="kpp" value="{{ old('kpp', $client->kpp) }}">

                      @if ($errors->has('kpp'))

                          <div class="text-danger">

                                {{ $errors->first('kpp') }}

                          </div>

                      @endif

                  </div>

                  <div class="form-group">
                      <label class="control-label" for="">Банк</label>
                      <input class="form-control" type="text" name="bank" value="{{ old('bank', $client->bank) }}">

                      @if ($errors->has('bank'))

                          <div class="text-danger">

                                {{ $errors->first('bank') }}

                          </div>

                      @endif

                  </div>

                  <div class="form-group">
                      <label class="control-label" for="">БИК</label>
                      <input class="form-control bik" type="text" name="bik" value="{{ old('bik', $client->bik) }}">

                      @if ($errors->has('bik'))

                          <div class="text-danger">

                                {{ $errors->first('bik') }}

                          </div>

                      @endif

                  </div>

                  <div class="form-group">
                      <label class="control-label" for="">р/с</label>
                      <input class="form-control" type="text" name="checking_account" value="{{ old('checking_account', $client->checking_account) }}">

                      @if ($errors->has('checking_account'))

                          <div class="text-danger">

                                {{ $errors->first('checking_account') }}

                          </div>

                      @endif

                  </div>

                  <div class="form-group">
                      <label class="control-label" for="">Фамилия генерального директора</label>
                      <input class="form-control" type="text" name="general_director_surname" value="{{ old('general_director_surname', $client->general_director_surname) }}">

                      @if ($errors->has('general_director_surname'))

                          <div class="text-danger">

                                {{ $errors->first('general_director_surname') }}

                          </div>

                      @endif

                  </div>

                  <div class="form-group">
                      <label class="control-label" for="">Имя генерального директора</label>
                      <input class="form-control" type="text" name="general_director_name" value="{{ old('general_director_name', $client->general_director_name) }}">

                      @if ($errors->has('general_director_name'))

                          <div class="text-danger">

                                {{ $errors->first('general_director_name') }}

                          </div>

                      @endif

                  </div>

                  <div class="form-group">
                      <label class="control-label" for="">Отчество генерального директора</label>
                      <input class="form-control" type="text" name="general_director_lastname" value="{{ old('general_director_lastname', $client->general_director_lastname) }}">

                      @if ($errors->has('general_director_lastname'))

                          <div class="text-danger">

                                {{ $errors->first('general_director_lastname') }}

                          </div>

                      @endif

                  </div>

                  <div class="form-group">
                      <label class="control-label" for="">Email генерального директора</label>
                      <input class="form-control email-mask" type="text" name="general_director_email" value="{{ old('general_director_email', $client->general_director_email) }}">

                      @if ($errors->has('general_director_email'))

                          <div class="text-danger">

                                {{ $errors->first('general_director_email') }}

                          </div>

                      @endif

                  </div>
                  <div class="form-group">
                      <label class="control-label" for="">Телефон генерального директора</label>
                      <input class="form-control phone-mask" type="text" name="general_director_phone" value="{{ old('general_director_phone', $client->general_director_phone) }}">

                      @if ($errors->has('general_director_phone'))

                          <div class="text-danger">

                                {{ $errors->first('general_director_phone') }}

                          </div>

                      @endif

                  </div>

                  <div class="form-group">
                      <label class="control-label" for="">Фамилия контактного лица</label>
                      <input class="form-control" type="text" name="personal_surname" value="{{ old('personal_surname', $client->personal_surname) }}">

                      @if ($errors->has('personal_surname'))

                          <div class="text-danger">

                                {{ $errors->first('personal_surname') }}

                          </div>

                      @endif

                  </div>

                  <div class="form-group">
                      <label class="control-label" for="">Имя контактного лица</label>
                      <input class="form-control" type="text" name="personal_name" value="{{ old('personal_name', $client->personal_name) }}">

                      @if ($errors->has('personal_name'))

                          <div class="text-danger">

                                {{ $errors->first('personal_name') }}

                          </div>

                      @endif

                  </div>

                  <div class="form-group">
                      <label class="control-label" for="">Отчество контактного лица</label>
                      <input class="form-control" type="text" name="personal_lastname" value="{{ old('personal_lastname', $client->personal_lastname) }}">

                      @if ($errors->has('personal_lastname'))

                          <div class="text-danger">

                                {{ $errors->first('personal_lastname') }}

                          </div>

                      @endif

                  </div>

                  <div class="form-group">
                      <label class="control-label" for="">Email контактного лица</label>
                      <input class="form-control email-mask" type="text" name="personal_email" value="{{ old('personal_email', $client->personal_email) }}">

                      @if ($errors->has('personal_email'))

                          <div class="text-danger">

                                {{ $errors->first('personal_email') }}

                          </div>

                      @endif

                  </div>
                  <div class="form-group">
                      <label class="control-label" for="">Телефон контактного лица</label>
                      <input class="form-control phone-mask" type="text" name="personal_phone" value="{{ old('personal_phone', $client->personal_phone) }}">

                      @if ($errors->has('personal_phone'))

                          <div class="text-danger">

                                {{ $errors->first('personal_phone') }}

                          </div>

                      @endif

                  </div>

                  <div class="form-group">
                      <label class="control-label" for="">Адрес доставки</label>
                      <textarea class="form-control" rows="6" type="text" name="shipping_address">{{ old('shipping_address', $client->shipping_address) }}</textarea>

                      @if ($errors->has('shipping_address'))

                          <div class="text-danger">

                                {{ $errors->first('shipping_address') }}

                          </div>

                      @endif

                  </div>

                  <input type="submit" value="Обновить" class="btn btn-primary">
                  <a class="btn btn-info" href="{{ route('legal.client.index') }}">Назад</a>
              </form>
        </div>
    </div>

@stop