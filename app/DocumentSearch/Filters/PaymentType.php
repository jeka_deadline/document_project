<?php

namespace App\DocumentSearch\Filters;

use Illuminate\Database\Eloquent\Builder;

class PaymentType implements Filterable
{
    public static function apply(Builder $builder, $value)
    {
        return $builder->where('type_payment', $value);
    }
}