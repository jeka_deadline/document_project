<?php

namespace App\Services;

use App\Models\Document\Document;
use App\Models\Document\ServiceAccount;

/**
 * Service account service
 */
class ServiceAccountService
{
    /**
     * Get course currency by type_payment
     *
     * @param string $type TYpe payment.
     * @param \App\Models\Document\Document $document Model document.
     * @return float
     */
    public function getCourseWithTypePayment($type, Document $document)
    {
        switch ($type) {
            case Document::PAYMENT_TYPE_CASH:
                $result = $document->firstPrepayment->course_cb_eur_markup;
                break;
            case Document::PAYMENT_TYPE_NON_CASH:
                $course = $document->firstPrepayment->course_cb_eur;
                $result = $course + $course * 0.03;
                break;
            default:
                $result = 0;
        }

        return sprintf("%.2f", floor($result * pow(10, 2)) / pow(10, 2));
    }

    /**
     * Attach service account do document.
     *
     * @param array $fields Request fields.
     * @param \App\Models\Document\Document $document Model document.
     * @return bool
     */
    public function attachToDocument($fields, Document $document)
    {
        $course = $this->getCourseWithTypePayment($fields[ 'type_payment' ], $document);

        if ($fields[ 'type_payment' ] == Document::PAYMENT_TYPE_NON_CASH) {
            $paymentPercentage = 3;
        } else {
            $paymentPercentage = 2;
        }

        $sumDelivery = 0;

        if ($fields[ 'delivery' ] === 'yes') {
            $sumDelivery = (int)$fields[ 'sum_delivery' ];
        }

        $sumClimb = (int)$fields[ 'sum_climb' ];
        $sumAssembly = (int)$fields[ 'sum_assembly' ];
        $sumRemovingPackaging = (int)$fields[ 'removing_packaging' ];

        $fullSumEur = $sumDelivery + $sumClimb + $sumAssembly + $sumRemovingPackaging;
        $fullSumRub = $this->convertEurToRub($fullSumEur, $course);

        if ($fields[ 'promotion_sum_eur' ]) {
            $promotionSumEur = $fields[ 'promotion_sum_eur' ];
            $sumWithPromotionEur = $fullSumEur - $promotionSumEur;
            $sumWithPromotionRub = $this->convertEurToRub($sumWithPromotionEur, $course);
        } else {
            $promotionSumEur = null;
            $sumWithPromotionEur = $fullSumEur;
            $sumWithPromotionRub = $this->convertEurToRub($fullSumEur, $course);
        }

        $serviceAccount = new ServiceAccount();

        $serviceAccount->fill($fields);
        $serviceAccount->document_id = $document->id;
        $serviceAccount->payment_percentage = $paymentPercentage;
        $serviceAccount->course = $course;
        $serviceAccount->full_sum_eur = $fullSumEur;
        $serviceAccount->full_sum_rub = $fullSumRub;
        $serviceAccount->promotion_sum_eur = $promotionSumEur;
        $serviceAccount->full_sum_eur_with_promotion = $sumWithPromotionEur;
        $serviceAccount->full_sum_rub_with_promotion = $sumWithPromotionRub;

        return $serviceAccount->save();
    }

    public function getMaxSum()
    {
        return ServiceAccount::max('full_sum_eur_with_promotion');
    }

    /**
     * Convert eur to rub.
     *
     * @access private
     * @param float $sum Sum.
     * @param float $course Course.
     * @return float
     */
    private function convertEurToRub($sum, $course)
    {
        return number_format($sum *= $course, 2, '.', '');
    }
}
