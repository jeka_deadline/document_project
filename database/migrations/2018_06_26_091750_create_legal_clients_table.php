<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLegalClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // create table legal clients
        Schema::create('legal_clients', function (Blueprint $table) {
            $table->increments('id');
            $table->string('full_name', 255);
            $table->string('short_name', 100);
            $table->mediumText('address');
            $table->string('email', 100);
            $table->string('phone', 20);
            $table->string('ogrn', 13);
            $table->string('inn', 10);
            $table->string('kpp', 9);
            $table->string('bank', 255);
            $table->string('bik', 9);
            $table->string('checking_account', 255);
            $table->string('general_director_surname', 50);
            $table->string('general_director_name', 50);
            $table->string('general_director_lastname', 50);
            $table->string('general_director_phone', 20);
            $table->string('general_director_email', 100);
            $table->string('personal_surname', 50);
            $table->string('personal_name', 50);
            $table->string('personal_lastname', 50);
            $table->string('personal_phone', 20);
            $table->string('personal_email', 100);
            $table->string('shipping_address');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // drop table legal clients
        Schema::dropIfExists('legal_clients');
    }
}
