<?php

namespace App\Models\Document;

use Illuminate\Database\Eloquent\Model;

/**
 * Model class for table `document_additional_fields`
 */
class DocumentAdditionalField extends Model
{
    /**
     * {@inheritdoc}
     *
     * @return id
     */
    protected $fillable = [
        'document_id',
    ];
}
