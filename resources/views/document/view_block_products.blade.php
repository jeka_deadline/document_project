<table id="block-products" style="width: 100%">

    @foreach ($document->products as $product)

        <tr>
            <td>{{ $loop->index + 1 }}. {{ $product->properties }}</td>
            <td>

                <?php if (Auth::user()->isCanEditAttachDocumentProduct()) : ?>

                    <a class="btn-edit-product" href="{{ route('document.get.edit.product.form', compact('product')) }}"><i class="fa fa-edit fa-2x"></i></a>

                <?php endif; ?>
                <?php if (Auth::user()->isCanDeleteAttachDocumentProduct()) : ?>

                    <form id="ajax-delete-product" method="post" action="{{ route('document.product.delete', compact('product')) }}" style="display: inline">
                          {{ csrf_field() }}
                          {{ method_field('DELETE') }}

                          <button style="color: #337ab7; border: none; background: none"><i class="fa fa-trash fa-2x"></i></button>
                    </form>

                <?php endif; ?>

            </td>
        </tr>

    @endforeach

</table>