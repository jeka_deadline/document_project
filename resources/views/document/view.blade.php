@extends('layouts.bootstrap')

@section('css')

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css">

@stop
@section('breadcrumbs')

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Главная</a></li>
            <li class="breadcrumb-item"><a href="{{ route('document.index') }}">Список договоров</a></li>
            <li class="breadcrumb-item active" aria-current="page">Просмотр договора</li>
        </ol>
    </nav>

@stop
@section('content')

    <div class="row">
        <div class="col-lg-12">

            <?php if (Auth::user()->isCanDeleteDocument()) : ?>

                <form class="delete-form" method="post" action="{{ route('document.delete', ['document' => $document]) }}" style="margin-bottom: 10px;">
                      {{ csrf_field() }}
                      {{ method_field('DELETE') }}

                      <button class="btn btn-danger">Удалить</button>
                </form>

            <?php endif; ?>

            @include('document/view_block_document_info', compact('document'))

            <div class="panel panel-primary">
                <div class="panel-body">
                    <p><b>Клиент:</b></p>
                    <p><a href="{{ route($document->client_type . '.client.view', ['client' => $document->client]) }}" target="_blank">{{ $document->clientFullName }}</a></p>
                    <p><b>Телефоны:</b> {{ $document->clientPhones }}</p>
                    <p><b>Адрес поставки:</b> {{ $document->clientShippingAddress }}</p>
                    <p><b>Комментарий:</b> {{ $document->clientComment }}</p>
                </div>
            </div>

            <div class="panel panel-primary">
                <div class="panel-body">

                      <p><b>Договора: </b></p>

                      @include('document/view_block_documents', [
                          'templates' => $document->templates,
                          'document' => $document,
                      ])

                      @if ($document->canEdit && Auth::user()->isCanAttachDocumentTemplate())

                          <div style="text-align: center;">
                              <div id="btn-attach-documents" data-url="{{ route('document.get.attach.template.form', compact('document')) }}" class="btn-group" role="group">
                                  <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                      Добавить
                                      <span class="caret"></span>
                                  </button>
                                  <ul class="dropdown-menu">
                                      <li><a href="#" data-type="contract">Добавить договор</a></li>
                                      <li><a href="#" data-type="act">Добавить акт</a></li>
                                      <li><a href="#" data-type="bill">Добавить вексель</a></li>
                                  </ul>
                              </div>
                          </div>

                      @endif

                </div>
            </div>

            @if ($document->canEdit && Auth::user()->isCanCreateDocumentNewPayment())

                @include('document/view_block_payments', [
                    'documentPayments' => $document->allPayments,
                    'document' => $document,
                ])

            @endif

            <div class="panel panel-primary">
                <div class="panel-body">

                      <p><b>Товар: </b></p>

                      @include('document/view_block_products', [
                          'document' => $document,
                      ])

                      @if ($document->canEdit && Auth::user()->isCanAttachDocumentProduct())

                          <div style="text-align: center;">
                              <a href="#" class="btn btn-info" id="btn-modal-new-product" data-url="{{ route('document.get.attach.product.form', compact('document')) }}">Новый товар</a>
                          </div>

                      @endif

                </div>
            </div>

            <div class="panel panel-primary">
                <div class="panel-body">

                      <p><b>Сервисный счет: </b></p>

                      @include('document/view_block_service_accounts', [
                          'document' => $document,
                      ])

                      @if ($document->canEdit && !$document->serviceAccount && 1)

                          <div style="text-align: center;">
                              <a href="#" class="btn btn-info" id="btn-modal-new-service-account" data-url="{{ route('document.service-account.form', compact('document')) }}">Новый сервисный счет</a>
                          </div>

                      @endif

                </div>
            </div>

        </div>
    </div>

    @if (($document->sum != $document->sumAllPayments) && ($document->canEdit) && (Auth::user()->isCanCreateDocumentNewPayment()))

        @include('document/view_modal_new_payment', compact('document', 'currencies'))

    @endif

@stop
@section('javascript')
    @parent

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
    <script src="/js/document.js"></script>

@stop