<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Model for table settings.
 */
class Setting extends Model
{
    /**
     * Key for seting enter pin for site.
     *
     * @var string
     */
    const KEY_ENTER_PIN = 'enter-pin';

    /**
     * Scope for find setting enter pin.
     *
     * @param
     * @return
     */
    public function scopeEnterPin($query)
    {
        return $query->where('key', self::KEY_ENTER_PIN);
    }
}
