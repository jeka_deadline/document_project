<?php

use Illuminate\Database\Seeder;

class ManagerRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            ['name' => 'manager'],
        ]);
    }
}
