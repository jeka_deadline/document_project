<?php

namespace App\DocumentSearch\Filters;

use DateTime;
use Illuminate\Database\Eloquent\Builder;

class FactoryPayment implements Filterable
{
    public static function apply(Builder $builder, $factoryPaymentFields)
    {
        $flagEmpty = true;

        foreach ($factoryPaymentFields as $value) {
            if (!empty($value)) {
                $flagEmpty = false;
                break;
            }
        }

        if ($flagEmpty) {
            return $builder;
        }

        foreach ($factoryPaymentFields as $nameField => $value) {
            if (is_null($value)) {
                continue;
            }

            if ($nameField == 'date_dispatch') {
                $dateData = explode(' - ', $value);

                $dateTime = new DateTime($dateData[ 0 ]);
                $dateData[ 0 ] = $dateTime->format('Y-m-d');
                $dateTime = new DateTime($dateData[ 1 ]);
                $dateData[ 1 ] = $dateTime->format('Y-m-d');

                if (isset($dateData[ 0 ], $dateData[ 1 ])) {
                    $builder->whereHas('factoryPayments', function($query) use ($dateData) {
                        return $query->whereBetween('date_dispatch', $dateData);
                    });
                }
            }

            if ($nameField == 'date_payment') {
                $dateData = explode(' - ', $value);

                $dateTime = new DateTime($dateData[ 0 ]);
                $dateData[ 0 ] = $dateTime->format('Y-m-d');
                $dateTime = new DateTime($dateData[ 1 ]);
                $dateData[ 1 ] = $dateTime->format('Y-m-d');

                if (isset($dateData[ 0 ], $dateData[ 1 ])) {
                    $builder->whereHas('factoryPayments', function($query) use ($dateData) {
                        return $query->whereBetween('date_payment', $dateData);
                    });
                }
            }

            if ($nameField == 'invoice') {
                $builder->whereHas('factoryPayments', function($query) use ($value) {
                    return $query->where('invoice', 'like', '%' . $value . '%');
                });
            }

            if ($nameField == 'sum') {
                $value = explode(';', $value);

                if (isset($value[ 0 ], $value[ 1 ])) {
                    $builder->whereHas('factoryPayments', function($query) use ($value) {
                        return $query->where('sum', '>=', $value[ 0 ])
                            ->where('sum', '<=', $value[ 1 ]);
                    });
                }
            }

            if ($nameField == 'bank_transfer') {
                $value = explode(';', $value);

                if (isset($value[ 0 ], $value[ 1 ])) {
                    $builder->whereHas('factoryPayments', function($query) use ($value) {
                        return $query->where('bank_transfer', '>=', $value[ 0 ])
                            ->where('bank_transfer', '<=', $value[ 1 ]);
                    });
                }
            }
        }

        return $builder;
    }
}