@extends('layouts.bootstrap')

@section('content')

    @if (Auth::user()->isCanViewPdfServiceAccount())

        <a href="{{ route('document.service-account.pdf', compact('serviceAccount')) }}" class="btn btn-primary" target="_blank">PDF</a>

    @endif

    @include('document.service_account_page', compact('serviceAccount', 'viewBlockCourse'))

@stop