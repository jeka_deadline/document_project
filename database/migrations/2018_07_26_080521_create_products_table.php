<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('document_id')->unsigned();
            $table->foreign('document_id')
                ->references('id')
                ->on('documents')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->string('name', 255);
            $table->string('model', 255);
            $table->string('color', 100);
            $table->integer('count')->unsigned();
            $table->integer('count_packages')->unsigned();
            $table->double('price');

            $table->integer('factory_id')->unsigned();
            $table->foreign('factory_id')
                ->references('id')
                ->on('factories')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->string('location', 20);
            $table->string('status', 20);
            $table->text('comment')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
