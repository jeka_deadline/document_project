<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title">Добавление сервисного счета</h4>
    <div class="row">
        <form action="{{ route('document.service-account.attach', compact('document')) }}" method="post" id="attach-service-account">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="">Доставка</label>
                            <div class="radio">
                                <label><input type="radio" name="delivery" value="yes" checked>Да</label>
                            </div>
                            <div class="radio">
                                <label><input type="radio" name="delivery" value="no">Нет</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="">МКАД / За МКАД</label>
                            <div class="radio">
                                <label><input type="radio" name="mkad" value="mkad" checked>МКАД</label>
                            </div>
                            <div class="radio">
                                <label><input type="radio" name="mkad" value="outer_mkad">За МКАД</label>
                            </div>

                            <div class="row">
                              <div class="col-lg-12"><input type="text" class="form-control" name="km_outer_mkad" placeholder="КМ" disabled></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-2">
                        <label for="">Итого за доставку</label>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <input type="text" class="form-control" name="sum_delivery">
                        </div>
                    </div>
                    <div class="col-lg-1">EUR</div>

                    <div class="col-lg-12 form-group">
                        <label for="">Дата доставки</label>
                        <input class="form-control datetimepicker" type="text" name="date_delivery">
                    </div>
                </div>

                <hr>

                <h4>Подъем</h4>

                <div class="row">
                    <div class="col-lg-3">
                        <div class="form-group">
                            <br>
                            <br>
                            <label for="">Этаж</label>
                            <input type="text" class="form-control" name="floor">
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <br>
                            <br>
                            <label for="">Вручную</label>
                            <input type="text" class="form-control" name="manual">
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label for="">Вручную крупногабаритный груз</label>
                            <input type="text" class="form-control" name="manual_large_sized">
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <br>
                            <br>
                            <label for="">На лифте</label>
                            <input type="text" class="form-control" name="on_lift">
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <label for="">Итого за подъем</label>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <input type="text" class="form-control" name="sum_climb">
                        </div>
                    </div>
                    <div class="col-lg-1">EUR</div>
                </div>

                <hr>

                <h4>Сборка</h4>

                <div class="form-group">
                    <label for="">Сборка</label>
                    <div class="radio">
                        <label><input type="radio" name="assembly" value="yes" checked>Да</label>
                    </div>
                    <div class="radio">
                        <label><input type="radio" name="assembly" value="no">Нет</label>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-5">
                        <div class="form-group">
                            <label for="">Итого за сборку</label>
                            <input type="text" class="form-control" name="sum_assembly">
                        </div>
                    </div>
                    <div class="col-lg-1">
                        <br>
                        EUR
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="">Вынос упаковки</label>
                            <input type="text" class="form-control" name="removing_packaging">
                        </div>
                    </div>
                    <div class="col-lg-1">
                        <br>
                        EUR
                    </div>
                    <div class="col-lg-12 form-group">
                        <label for="">Дата сборки</label>
                        <input class="form-control datetimepicker" type="text" name="date_assembly">
                    </div>
                </div>

                <hr>

                <div style="text-align: center;">
                    <a href="#" class="btn btn-warning" id="btn-sum-service-account">Суммировать сервисный счет</a>
                </div>

                <hr>

                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="">Тип оплаты</label>
                            <select name="type_payment" class="form-control" id="select-type-payment" data-url="{{ route('document.service-account.change-type-payment', compact('document')) }}">

                                @foreach ($typePayments as $key => $label)

                                    <option value="{{ $key }}">{{ $label }}</option>

                                @endforeach

                            </select>
                        </div>

                        <div class="row">
                            <div class="col-lg-3">
                                <label for="">Итого</label>
                            </div>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="full_sum_eur" readonly>
                            </div>
                            <div class="col-lg-1">
                                EUR
                            </div>
                        </div>

                        <br>

                        <div class="row">
                            <div class="col-lg-3">
                                <label for="">Скидка</label>
                            </div>
                            <div class="col-lg-6">
                                <input type="text" class="form-control" name="promotion_sum_eur">
                            </div>
                            <div class="col-lg-1">
                                EUR
                            </div>
                        </div>

                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="">Курс</label>
                            <input type="text" class="form-control" value="{{ $course }}" name="course" readonly>
                        </div>

                        <div class="row">
                            <div class="col-lg-9">
                                <input type="text" class="form-control" name="full_sum_rub" readonly>
                            </div>
                            <div class="col-lg-1">
                                Руб
                            </div>
                        </div>

                        <div class="row">
                          <div class="col-lg-12">
                              <br>
                              <a class="btn btn-info" id="btn-sum-with-promotion" href="#">Окончательный расчет</a>
                          </div>
                        </div>
                    </div>
                </div>

                <h4>Итого со скидкой</h4>

                <div class="row">
                    <div class="col-lg-1">Итого</div>
                    <div class="col-lg-4">
                        <input type="text" class="form-control" name="full_sum_eur_with_promotion" readonly>
                    </div>
                    <div class="col-lg-1">EUR</div>
                    <div class="col-lg-4">
                        <input type="text" class="form-control" name="full_sum_rub_with_promotion" readonly>
                    </div>
                    <div class="col-lg-1">Руб</div>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="modal-footer">
    <button class="btn btn-success" type="sumbit" form="attach-service-account">Добавить</button>
    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
</div>