<?php

namespace App\DocumentSearch\Filters;

use Illuminate\Database\Eloquent\Builder;

class Shop implements Filterable
{
    public static function apply(Builder $builder, $value)
    {
        return $builder->where('shop_id', $value);
    }
}