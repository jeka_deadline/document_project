<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;

/**
 * Model class for table `user_roles`.
 */
class UserRole extends Model
{
    /**
     * {@inheritdoc}
     *
     * @access protected
     * @var array $fillable
     */
    protected $fillable = [
        'user_id',
        'role_id',
    ];

    public function role()
    {
        return $this->belongsTo('\App\Models\User\Role');
    }
}
