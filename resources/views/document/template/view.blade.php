@extends('layouts.bootstrap')

@section('breadcrumbs')

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Главная</a></li>
            <li class="breadcrumb-item"><a href="{{ route('factory.index') }}">Список шаблонов для документов</a></li>
            <li class="breadcrumb-item active" aria-current="page">Просмотр шаблона</li>
        </ol>
    </nav>

@stop
@section('content')

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <table style="width: 100%">
                        <tr>
                            <td>
                                <h4>Информация об шаблоне</h4>
                            </td>

                            <?php if (Auth::user()->isCanEditDocumentTemplate()) : ?>

                                <td style="text-align: right"><a class="btn btn-warning" style="font-size: 14px" href="{{ route('document.template.edit', ['template' => $template]) }}">Редактировать</a></td>

                            <?php endif; ?>

                        </tr>
                    </table>
                </div>
                <div class="panel-body">
                    <div class="box box-info">
                        <div class="box-body">
                            <div class="col-lg-3">
                                <div  align="center">
                                    <img alt="User Pic" src="/images/Document.png" id="profile-image1" class="img-responsive">
                                </div>
                                <br>
                                <!-- /input-group -->
                            </div>
                            <div class="col-lg-9">
                                <div>
                                    <h3 style="color:#00b1b1;">Название</h3>
                                    <span>
                                        <p>{{ $template->name }}</p>
                                    </span>
                                    <h4 style="color:#00b1b1;">Тип</h4>
                                    <span>
                                        <p>{{ $template->typeName }}</p>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <h4 style="center; color:#00b1b1;">Текст шаблона:</h4>
                            <div class="well">
                                {!! $template->body !!}
                            </div>
                        </div>
                        <!-- /.box -->
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop