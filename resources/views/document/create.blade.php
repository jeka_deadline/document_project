@extends('layouts.bootstrap')

@section('css')

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css">

@stop
@section('breadcrumbs')

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Главная</a></li>
            <li class="breadcrumb-item"><a href="{{ route('document.index') }}">Список договоров</a></li>
            <li class="breadcrumb-item active" aria-current="page">Создание договора</li>
        </ol>
    </nav>

@stop
@section('content')

    <div class="row">
        <div class="col-lg-12">
              <h2>Создание договора</h2>
              <form method="post" action="{{ route('document.store') }}">
                  {{ csrf_field() }}

                  <div>

                      <!-- Nav tabs -->
                      <ul class="nav nav-tabs" role="tablist">
                          <li role="presentation" class="active"><a href="#main" aria-controls="main" role="tab" data-toggle="tab">Основное</a></li>
                          <li role="presentation"><a href="#main2" aria-controls="main2" role="tab" data-toggle="tab">Основное 2</a></li>
                      </ul>

                      <!-- Tab panes -->
                      <div class="tab-content">
                          <div role="tabpanel" class="tab-pane active" id="main">

                              <!-- block name -->
                              <div class="form-group">
                                  <label class="control-label" for="">Наименование</label>
                                  <select class="form-control" name="name">

                                      @foreach ($dropDownInformation->documentTypes as $type => $label)

                                          <option value="{{ $type }}" {{ (old('name') == $type) ? 'selected' : '' }} >{{ $label }}</option>

                                      @endforeach

                                  </select>

                                  @if ($errors->has('name'))

                                      <div class="text-danger">

                                            {{ $errors->first('name') }}

                                      </div>

                                  @endif

                              </div>
                              <!-- end block name -->

                              <!-- block prepared -->
                              <div class="form-group">
                                  <label class="control-label" for="">Подготовил</label>
                                  <input type="text" class="form-control" name="prepared" value="{{ Auth::user()->fullName }}" readonly>

                                  @if ($errors->has('prepared'))

                                      <div class="text-danger">

                                            {{ $errors->first('prepared') }}

                                      </div>

                                  @endif

                              </div>
                              <!-- end block prepared -->

                              <!-- block manager -->
                              <div class="form-group">
                                  <label class="control-label" for="">Менеджер</label>
                                  <select class="form-control select2" name="manager_worker_id">
                                      <option value=""></option>

                                      @foreach ($dropDownInformation->listManagers as $worker)

                                          <option value="{{ $worker->id }}" {{ (old('manager_worker_id') == $worker->id) ? 'selected' : '' }}>{{ $worker->full_name }}</option>

                                      @endforeach

                                  </select>

                                  @if ($errors->has('manager_worker_id'))

                                      <div class="text-danger">

                                            {{ $errors->first('manager_worker_id') }}

                                      </div>

                                  @endif

                              </div>
                              <!-- end block manager -->

                              <!-- block client -->
                              <div class="form-group">
                                  <label class="control-label" for="">Клиент</label>
                                  <select id="choose-client" class="form-control select2" name="client" data-url="{{ route('ajax.get.client.by.select') }}">
                                      <option value=""></option>

                                      @foreach ($dropDownInformation->listClients as $key => $value)

                                          <option value="{{ $key }}" {{ (old('client') == $key) ? 'selected' : '' }}>{{ $value }}</option>

                                      @endforeach

                                  </select>

                                  @if ($errors->has('client'))

                                      <div class="text-danger">

                                            {{ $errors->first('client') }}

                                      </div>

                                  @endif

                                  <br><br>

                                  @if (Auth::user()->isCanCreateIndividualClient() || Auth::user()->isCanCreateLegalClient())

                                      <a href="#" class="btn btn-primary" id="add-client">Добавить клиента</a>

                                  @endif

                              </div>
                              <!-- end block client -->

                              <!-- block contact client -->
                              <div class="form-group">
                                  <label class="control-label" for="">Контактное лицо клиента</label>
                                  <input id="input-contact-client" type="text" class="form-control" name="contact_client" value="{{ old('contact_client') }}" readonly>

                                  @if ($errors->has('contact_client'))

                                      <div class="text-danger">

                                            {{ $errors->first('contact_client') }}

                                      </div>

                                  @endif

                              </div>
                              <!-- end block contact client -->

                          </div>
                          <div role="tabpanel" class="tab-pane" id="main2">

                              <!-- block type document -->
                              <div class="form-group">
                                  <label class="control-label" for="">Вид</label>
                                  <select class="form-control" name="type_payment">

                                      @foreach ($dropDownInformation->paymentTypes as $type => $label)

                                          <option value="{{ $type }}" {{ (old('type_payment') == $type) ? 'selected' : '' }}>{{ $label }}</option>

                                      @endforeach

                                  </select>

                                  @if ($errors->has('type_payment'))

                                      <div class="text-danger">

                                            {{ $errors->first('type_payment') }}

                                      </div>

                                  @endif

                              </div>
                              <!-- end block type document -->

                              <!-- block state -->
                              <div class="form-group">
                                  <label class="control-label" for="">Состояние</label>
                                  <select class="form-control" name="state">

                                      @foreach ($dropDownInformation->states as $state => $label)

                                          <option value="{{ $state }}" {{ (old('state')) == $state ? 'selected' : '' }}>{{ $label }}</option>

                                      @endforeach

                                  </select>

                                  @if ($errors->has('state'))

                                      <div class="text-danger">

                                            {{ $errors->first('state') }}

                                      </div>

                                  @endif

                              </div>
                              <!-- end block state -->

                              <!-- block register number -->
                              <div class="row">
                                    <div class="col-lg-6">

                                        <div class="form-group">
                                            <label class="control-label" for="">Рег. номер</label>
                                            <input type="text" class="form-control numeric-positive-mask" name="register_number" value="{{ old('register_number') }}">
                                            <div class="help-block">Последний рег. номер - {{ $lastRegisterNumber }}</div>

                                            @if ($errors->has('register_number'))

                                                <div class="text-danger">

                                                      {{ $errors->first('register_number') }}

                                                </div>

                                            @endif

                                        </div>

                                    </div>
                                    <div class="col-lg-6">

                                        <div class="form-group">
                                            <label class="control-label" for="">от:</label>
                                            <input type="text" class="form-control" name="from" value="{{ old('from') }}" disabled>

                                            @if ($errors->has('from'))

                                                <div class="text-danger">

                                                      {{ $errors->first('from') }}

                                                </div>

                                            @endif

                                          </div>

                                    </div>
                              </div>
                              <!-- end block register number -->

                              <!-- block organization -->
                              <div class="form-group">
                                  <label class="control-label" for="">Организация</label>
                                  <select class="form-control" name="shop_id">

                                      @foreach ($dropDownInformation->listShops as $shop)

                                          <option value="{{ $shop->id }}" {{ ((old('shop_id') == $shop->id) || ($shop->default)) ? 'selected' : '' }}>{{ $shop->full_name }}</option>

                                      @endforeach

                                  </select>

                                  @if ($errors->has('shop_id'))

                                      <div class="text-danger">

                                            {{ $errors->first('shop_id') }}

                                      </div>

                                  @endif

                              </div>
                              <!-- end block organization -->

                              <!-- block sum with currency -->
                              <div class="row" id="block-calculation" style="margin: 0px 0px 15px; border: 2px solid #999; border-radius: 10px;">
                                  <div class="col-lg-4">
                                      <h4>Сумма</h4>

                                      <div class="form-group">
                                          <label for="" class="control-label">Сумма договора</label>
                                          <input type="text" class="form-control numeric-positive-mask" name="sum" value="{{ old('sum') }}">

                                          @if ($errors->has('sum'))

                                              <div class="text-danger">

                                                    {{ $errors->first('sum') }}

                                              </div>

                                          @endif

                                      </div>

                                      <div class="form-group">
                                          <label for="" class="control-label">% предоплаты</label>
                                          <input type="text" class="form-control numeric-positive-mask" name="percentage_prepayment" value="{{ old('percentage_prepayment') }}">

                                          @if ($errors->has('percentage_prepayment'))

                                              <div class="text-danger">

                                                    {{ $errors->first('percentage_prepayment') }}

                                              </div>

                                          @endif

                                      </div>

                                      <div class="form-group">
                                          <label for="" class="control-label">% остаток</label>
                                          <input type="text" class="form-control" name="percentage_remainder" value="" readonly>

                                          @if ($errors->has('percentage_remainder'))

                                              <div class="text-danger">

                                                    {{ $errors->first('percentage_remainder') }}

                                              </div>

                                          @endif

                                      </div>

                                      <div class="form-group">
                                          <label for="" class="control-label">EUR остаток</label>
                                          <input type="text" class="form-control" name="remainder_eur" value="" readonly>

                                          @if ($errors->has('remainder_eur'))

                                              <div class="text-danger">

                                                    {{ $errors->first('remainder_eur') }}

                                              </div>

                                          @endif

                                      </div>

                                  </div>
                                  <div class="col-lg-4">
                                      <h4>Вносимые</h4>

                                      <div class="form-group">
                                          <label for="" class="control-label">EUR</label>
                                          <input type="text" class="form-control numeric-positive-mask" value="{{ old('enter_eur') }}" name="enter_eur">
                                          <div class="help-block"></div>

                                          @if ($errors->has('enter_eur'))

                                              <div class="text-danger">

                                                    {{ $errors->first('enter_eur') }}

                                              </div>

                                          @endif

                                      </div>

                                      <div class="form-group">
                                          <label for="" class="control-label">$</label>
                                          <input type="text" class="form-control numeric-positive-mask" value="{{ old('enter_usd') }}" name="enter_usd">
                                          <div class="help-block"></div>

                                          @if ($errors->has('enter_usd'))

                                              <div class="text-danger">

                                                    {{ $errors->first('enter_usd') }}

                                              </div>

                                          @endif

                                      </div>

                                      <div class="form-group">
                                          <label for="" class="control-label">Рубли</label>
                                          <input type="text" class="form-control numeric-positive-mask" value="{{ old('enter_rub') }}" name="enter_rub">
                                          <div class="help-block"></div>

                                          @if ($errors->has('enter_rub'))

                                              <div class="text-danger">

                                                    {{ $errors->first('enter_rub') }}

                                              </div>

                                          @endif

                                      </div>

                                  </div>
                                  <div class="col-lg-4">
                                      <h4>Размер предоплаты</h4>

                                      <div class="form-group">
                                          <label for="" class="control-label">EUR</label>
                                          <input type="text" class="form-control numeric-positive-mask" value="" name="prepayment_amount_eur" readonly>

                                          @if ($errors->has('prepayment_amount_eur'))

                                              <div class="text-danger">

                                                    {{ $errors->first('prepayment_amount_eur') }}

                                              </div>

                                          @endif

                                      </div>

                                      <div class="form-group">
                                          <label for="" class="control-label">Рубли</label>
                                          <input type="text" class="form-control numeric-positive-mask" value="" name="prepayment_amount_rub" readonly>

                                          @if ($errors->has('prepayment_amount_rub'))

                                              <div class="text-danger">

                                                    {{ $errors->first('prepayment_amount_rub') }}

                                              </div>

                                          @endif

                                      </div>

                                  </div>

                                  <div class="col-lg-12">
                                      <hr>
                                  </div>

                                  <div class="col-lg-4">

                                      <h4>Курс валют ЦБ</h4>
                                      <table class="table">
                                          <tr>
                                              <td style="width: 150px">Курс доллара</td>
                                              <td>{{ $currencies->usd }}</td>
                                          </tr>
                                          <tr>
                                              <td>Курс евро</td>
                                              <td>{{ $currencies->eur }}</td>
                                          </tr>
                                      </table>

                                  </div>
                                  <div class="col-lg-4">

                                      <!-- block plus percentage currrency -->
                                      <div class="form-group">
                                          <label class="control-label" for="">Процент увеличения курса валют</label>
                                          <input type="text" class="form-control numeric-positive-mask" data-usd="{{ $currencies->usd }}" data-eur="{{ $currencies->eur }}" name="percentage_currrency" value="{{ old('percentage_currrency', 2) }}">

                                          @if ($errors->has('percentage_currrency'))

                                              <div class="text-danger">

                                                    {{ $errors->first('percentage_currrency') }}

                                              </div>

                                          @endif

                                      </div>
                                      <!-- end block plus percentage currency -->

                                  </div>
                                  <div class="col-lg-4">

                                      <h4>Курс валют с учетом наценки</h4>
                                      <table class="table" id="currency-markup">
                                          <tr>
                                              <td style="width: 150px">Курс доллара</td>
                                              <td>{{ $currencies->usd_markup }}</td>
                                          </tr>
                                          <tr>
                                              <td>Курс евро</td>
                                              <td>{{ $currencies->eur_markup }}</td>
                                          </tr>
                                      </table>

                                  </div>

                                  <div class="col-lg-12">

                                      <!-- block plus cross currrency -->
                                      <div class="form-group">
                                          <label class="control-label" for="">Кросс курс валют</label>
                                          <input type="text" class="form-control numeric-positive-mask" name="cross_currrency" value="{{ old('cross_currrency', $currencies->cross_currrency) }}">

                                          @if ($errors->has('cross_currrency'))

                                              <div class="text-danger">

                                                    {{ $errors->first('cross_currrency') }}

                                              </div>

                                          @endif

                                      </div>
                                      <!-- end block plus cross currency -->

                                  </div>
                                  <div class="col-lg-12" style="margin-bottom: 15px;">
                                      <a href="#" id="calculation-sum" data-url="{{ route('document.calculation') }}" class="btn btn-warning">Расчитать</a>
                                  </div>

                              </div>
                              <!-- end block sum with currency -->

                              <!-- block confirm pro form -->
                              <div class="form-group">
                                  <label class="control-label" for="">Подтверждение проформы</label>
                                  <input type="text" id="input-pro-form" class="form-control" name="confirm_pro_form" value="{{ old('confirm_pro_form') }}">

                                  @if ($errors->has('confirm_pro_form'))

                                      <div class="text-danger">

                                            {{ $errors->first('confirm_pro_form') }}

                                      </div>

                                  @endif

                                  <div id="datetimepicker"></div>

                              </div>
                              <!-- end block confirm pro form -->

                              <!-- block period execution -->
                              <div class="form-group">
                                  <label class="control-label" for="">Срок исполнения</label>
                                  <input type="text" class="form-control integer-positive-mask" data-url="{{ route('document.calculation.date-finish') }}" name="period_execution" value="{{ old('period_execution') }}">

                                  @if ($errors->has('period_execution'))

                                      <div class="text-danger">

                                            {{ $errors->first('period_execution') }}

                                      </div>

                                  @endif

                              </div>
                              <!-- end block period execution -->

                              <!-- block date_finish -->
                              <div class="form-group">
                                  <label class="control-label" for="">Дата окончания</label>
                                  <input type="text" class="form-control" name="date_finish" value="{{ old('date_finish') }}" readonly>

                                  @if ($errors->has('date_finish'))

                                      <div class="text-danger">

                                            {{ $errors->first('date_finish') }}

                                      </div>

                                  @endif

                              </div>
                              <!-- block date_finish -->

                          </div>
                      </div>

                  </div>

                  <input type="submit" value="Добавить" class="btn btn-success">
                  <a class="btn btn-info" href="{{ route('document.index') }}">Назад</a>
              </form>
        </div>
    </div>

    @if (Auth::user()->isCanCreateIndividualClient() || Auth::user()->isCanCreateLegalClient())

        <div class="modal fade" tabindex="-1" id="modal-add-client" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Добавление нового клиента</h4>
                    </div>
                    <div class="modal-body">
                        <div>

                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist">

                                @if (Auth::user()->isCanCreateIndividualClient())

                                    <li role="presentation" class="active"><a href="#individual" aria-controls="individual" role="tab" data-toggle="tab">Физическое лицо</a></li>

                                @endif
                                @if (Auth::user()->isCanCreateLegalClient())

                                    <li role="presentation" class="{{ (!Auth::user()->isCanCreateIndividualClient()) ? 'active' : '' }}"><a href="#legal" aria-controls="legal" role="tab" data-toggle="tab">Юридическое лицо</a></li>

                                @endif

                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">

                                @if (Auth::user()->isCanCreateIndividualClient())

                                    <div role="tabpanel" class="tab-pane active" id="individual">

                                        @include('client.individual-client.form_create')

                                    </div>

                                @endif
                                @if (Auth::user()->isCanCreateLegalClient())

                                    <div role="tabpanel" class="{{ (!Auth::user()->isCanCreateIndividualClient()) ? 'tab-pane active' : 'tab-pane' }}" id="legal">

                                        @include('client.legal-client.form_create')

                                    </div>

                                @endif

                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                        <button type="button" id="modal-new-client-submit" class="btn btn-primary">Добавить</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

    @endif

@stop
@section('javascript')
    @parent

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
    <script src="/js/document.js"></script>

@stop