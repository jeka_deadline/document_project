<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title">Добавление нового документа</h4>
    <div class="row">
        <form action="{{ route('document.attach.template', compact('document')) }}" method="post" id="attach-new-template">
            <div class="col-lg-12">
                  <div class="form-group">
                      <label for="">Шаблон</label>
                      <select name="template_id" class="form-control">

                          @foreach ($templates as $template)

                              <option value="{{ $template->id }}">{{ $template->name }} | {{ $template->created_at }}</option>

                          @endforeach

                      </select>
                  </div>
            </div>
        </form>
    </div>
</div>
<div class="modal-footer">
    <button class="btn btn-success" type="sumbit" form="attach-new-template">Прикрепить</button>
    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
</div>
