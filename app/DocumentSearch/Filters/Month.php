<?php

namespace App\DocumentSearch\Filters;

use Illuminate\Database\Eloquent\Builder;

class Month implements Filterable
{
    public static function apply(Builder $builder, $value)
    {
        return $builder->whereMonth('created_at', $value);
    }
}