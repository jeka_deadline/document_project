<?php

namespace App\Repositories\User;

use App\User;
use App\Models\User\Worker;

/**
 * User repository.
 */
class UserRepository
{
    /**
     * Create new user.
     *
     * @param string $email User email
     * @param string $password User password
     * @return \App\User|null
     */
    public function createNewUser($email, $password)
    {
        return User::create([
            'email' => $email,
            'password' => bcrypt($password),
        ]);
    }

    /**
     * Update user.
     *
     * @param \App\User $user User model.
     * @param string $email User email.
     * @param string $password User password.
     * @return void
     */
    public function updateUser(User $user, $email, $password)
    {
        $user->email = $email;

        if (!empty($password)) {
            $user->password = bcrypt($password);
        }

        $user->save();
    }

    /**
     * Create new worker.
     *
     * @param int $userId User id.
     * @param array $fields Worker fields.
     * @return
     */
    public function createNewWorker($userId, $fields)
    {
        $fields[ 'user_id' ] = $userId;

        return Worker::create($fields);
    }

    /**
     * Update worker.
     *
     * @param \App\Models\User\Worker $worker Worker model.
     * @param array $fields Worker fields.
     * @return void
     */
    public function updateWorker(Worker $worker, $fields)
    {
        $worker->update($fields);
    }

    /**
     * Get workers list with pagination.
     *
     * @param int $paginate Count workers on page.
     * @return
     */
    public function getWorkersWithPaginate($paginate = 15)
    {
        return Worker::paginate($paginate);
    }
}
