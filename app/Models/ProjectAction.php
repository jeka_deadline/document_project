<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectAction extends Model
{
    /**
     * Action list documents.
     *
     * @var string
     */
    const ACTION_LIST_DOCUMENTS = 'list-documents';

    /**
     * Action edit document.
     *
     * @var string
     */
    const ACTION_EDIT_DOCUMENT = 'edit-document';

    /**
     * Action create document.
     *
     * @var string
     */
    const ACTION_CREATE_DOCUMENT = 'create-document';

    /**
     * Action view document.
     *
     * @var string
     */
    const ACTION_VIEW_DOCUMENT = 'view-document';

    /**
     * Action attach document template to document.
     *
     * @var string
     */
    const ACTION_ATTACH_DOCUMENT_TEMPLATE = 'attach-document-demplate';

    /**
     * Action list product to document.
     *
     * @var string
     */
    const ACTION_ATTACH_PRODUCT = 'attach-product';

    /**
     * Action edit attached product document.
     *
     * @var string
     */
    const ACTION_EDIT_ATTACH_PRODUCT = 'edit-attach-product';

    /**
     * Action delete attached product document.
     *
     * @var string
     */
    const ACTION_DELETE_ATTACH_PRODUCT = 'delete-attach-product';

    /**
     * Action create new document payment.
     *
     * @var string
     */
    const ACTION_CREATE_NEW_PAYMENT = 'create-new-payment';

    /**
     * Action create document service account.
     *
     * @var string
     */
    const ACTION_CREATE_SERVICE_ACCOUNT = 'create-service-account';

    /**
     * Action view document service account.
     *
     * @var string
     */
    const ACTION_VIEW_SERVICE_ACCOUNT = 'view-service-account';

    /**
     * Action view document service account in pdf format.
     *
     * @var string
     */
    const ACTION_SERVICE_ACCOUNT_TO_PDF = 'service-account-to-pdf';

    /**
     * Action delete document.
     *
     * @var string
     */
    const ACTION_DELETE_DOCUMENT = 'delete-document';

    /**
     * Action list workers.
     *
     * @var string
     */
    const ACTION_LIST_WORKERS = 'list-workers';

    /**
     * Action create worker.
     *
     * @var string
     */
    const ACTION_CREATE_WORKER = 'create-worker';

    /**
     * Action edit worker.
     *
     * @var string
     */
    const ACTION_EDIT_WORKER = 'edit-worker';

    /**
     * Action view worker.
     *
     * @var string
     */
    const ACTION_VIEW_WORKER = 'view-worker';

    /**
     * Action delete worker.
     *
     * @var string
     */
    const ACTION_DELETE_WORKER = 'delete-worker';

    /**
     * Action list shops.
     *
     * @var string
     */
    const ACTION_LIST_SHOPS = 'list-shops';

    /**
     * Action create shop.
     *
     * @var string
     */
    const ACTION_CREATE_SHOP = 'create-shop';

    /**
     * Action edit shop.
     *
     * @var string
     */
    const ACTION_EDIT_SHOP = 'edit-shop';

    /**
     * Action view shop.
     *
     * @var string
     */
    const ACTION_VIEW_SHOP = 'view-shop';

    /**
     * Action delete shop.
     *
     * @var string
     */
    const ACTION_DELETE_SHOP = 'delete-shop';

    /**
     * Action list legal clients.
     *
     * @var string
     */
    const ACTION_LIST_LEGAL_CLIENTS = 'list-legal-clients';

    /**
     * Action create legal client.
     *
     * @var string
     */
    const ACTION_CREATE_LEGAL_CLIENT = 'create-legal-client';

    /**
     * Action edit legal client.
     *
     * @var string
     */
    const ACTION_EDIT_LEGAL_CLIENT = 'edit-legal-client';

    /**
     * Action view legal client.
     *
     * @var string
     */
    const ACTION_VIEW_LEGAL_CLIENT = 'view-legal-client';

    /**
     * Action delete legal client.
     *
     * @var string
     */
    const ACTION_DELETE_LEGAL_CLIENT = 'delete-legal-client';

    /**
     * Action list individual clients.
     *
     * @var string
     */
    const ACTION_LIST_INDIVIDUAL_CLIENTS = 'list-individual-clients';

    /**
     * Action create individual client.
     *
     * @var string
     */
    const ACTION_CREATE_INDIVIDUAL_CLIENT = 'create-individual-client';

    /**
     * Action edit individual client.
     *
     * @var string
     */
    const ACTION_EDIT_INDIVIDUAL_CLIENT = 'edit-individual-client';

    /**
     * Action view individual client.
     *
     * @var string
     */
    const ACTION_VIEW_INDIVIDUAL_CLIENT = 'view-individual-client';

    /**
     * Action delete individual client.
     *
     * @var string
     */
    const ACTION_DELETE_INDIVIDUAL_CLIENT = 'delete-individual-client';

    /**
     * Action list factories.
     *
     * @var string
     */
    const ACTION_LIST_FACTORIES = 'list-factories';

    /**
     * Action create factory.
     *
     * @var string
     */
    const ACTION_CREATE_FACTORY = 'create-factory';

    /**
     * Action edit factory.
     *
     * @var string
     */
    const ACTION_EDIT_FACTORY = 'edit-factory';

    /**
     * Action view factory.
     *
     * @var string
     */
    const ACTION_VIEW_FACTORY = 'view-factory';

    /**
     * Action delete factory.
     *
     * @var string
     */
    const ACTION_DELETE_FACTORY = 'delete-factory';

    /**
     * Action list document templates.
     *
     * @var string
     */
    const ACTION_LIST_DOCUMENT_TEMPLATES = 'list-document-templates';

    /**
     * Action create document template.
     *
     * @var string
     */
    const ACTION_CREATE_DOCUMENT_TEMPLATE = 'create-document-template';

    /**
     * Action edit document template.
     *
     * @var string
     */
    const ACTION_EDIT_DOCUMENT_TEMPLATE = 'edit-document-template';

    /**
     * Action view document template.
     *
     * @var string
     */
    const ACTION_VIEW_DOCUMENT_TEMPLATE = 'view-document-template';

    /**
     * Action delete document template.
     *
     * @var string
     */
    const ACTION_DELETE_DOCUMENT_TEMPLATE = 'delete-document-template';

    /**
     * Action save document template as new.
     *
     * @var string
     */
    const ACTION_SAVE_AS_DOCUMENT_TEMPLATE = 'save-as-document-template';

    /**
     * Action list full documents template.
     *
     * @var string
     */
    const ACTION_TABLE_DOCUMENTS = 'table-documents';

    /**
     * Action update calendar.
     *
     * @var string
     */
    const ACTION_UPDATE_CALENDAR = 'update-calendar';

    /**
     * Action list settings.
     *
     * @var string
     */
    const ACTION_LIST_SETTINGS = 'list-settings';

    /**
     * Action edit setting.
     *
     * @var string
     */
    const ACTION_EDIT_SETTING = 'edit-setting';

    /**
     * Get list actionf for role by id.
     *
     * @static
     * @return array
     */
    public static function getListProjectActionsByRoleId($roleId)
    {
        return ProjectActionRole::where('role_id', $roleId)->get()
            ->keyBy('project_action_id');
    }
}
