@extends('layouts.bootstrap')

@section('breadcrumbs')

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Главная</a></li>
            <li class="breadcrumb-item"><a href="{{ route('legal.client.index') }}">Клиенты: юридические лица</a></li>
            <li class="breadcrumb-item active" aria-current="page">Добавление клиента</li>
        </ol>
    </nav>

@stop
@section('content')

    @include('client.legal-client.form_create', ['showButtons' => true])

@stop