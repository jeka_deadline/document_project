@extends('layouts.bootstrap')

@section('breadcrumbs')

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Главная</a></li>
            <li class="breadcrumb-item"><a href="{{ route('admin.roles.index') }}">Список ролей</a></li>
            <li class="breadcrumb-item active" aria-current="page">Настройка роли</li>
        </ol>
    </nav>

@stop
@section('content')

    <div class="row">
        <div class="col-lg-12">
              <h2>Настройка роли: {{ $role->humanName }}</h2>
              <form method="post" action="{{ route('admin.roles.update', ['role' => $role]) }}">
                  {{ csrf_field() }}
                  {{ method_field('PUT') }}

                  @foreach ($listProjectActions as $projectAction)

                      <div class="checkbox">
                          <label for="">
                              <input type="checkbox" name="actions[{{ $projectAction->id }}]" {{ (isset($roleActions[ $projectAction->id ])) ? 'checked' : '' }}>
                              {{ $projectAction->description }}
                          </label>
                      </div>

                  @endforeach

                  <input type="submit" value="Обновить" class="btn btn-primary">
                  <a class="btn btn-info" href="{{ route('admin.settings.index') }}">Назад</a>
              </form>
        </div>
    </div>

@stop