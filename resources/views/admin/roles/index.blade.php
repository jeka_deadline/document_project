@extends('layouts.bootstrap')

@section('breadcrumbs')

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Главная</a></li>
            <li class="breadcrumb-item active" aria-current="page">Роли</li>
        </ol>
    </nav>

@stop
@section('content')

    <div class="row">
          <div class="col-lg-12">
                <table class="table">
                      <thead>
                            <tr>
                                  <th>Название</th>
                                  <th>Операции</th>
                            </tr>
                      </thead>
                      <tbody>

                          @foreach ($roles as $role)

                              <tr>
                                  <td>
                                      {{ $role->humanName }}
                                  </td>
                                  <td>
                                      <a href="{{ route('admin.roles.edit', ['role' => $role]) }}"><i class="fa fa-edit fa-2x"></i></a>
                                  </td>
                              </tr>

                          @endforeach

                      </tbody>
                </table>
          </div>
    </div>

@stop