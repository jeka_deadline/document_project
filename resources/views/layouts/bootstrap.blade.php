<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
      <meta name="description" content="">
      <meta name="author" content="">
      <meta name="csrf-token" content="{{ csrf_token() }}">
      <link rel="icon" href="/favicon.ico">
      <title>Static Top Navbar Example for Bootstrap</title>
      <!-- Bootstrap core CSS -->
      <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
      <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
      <!-- Custom styles for this template -->
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css" crossorigin="anonymous">
      <link rel="stylesheet" href="/css/main.css">
      @yield('css')
      <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
      <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
   </head>
   <body>
      <!-- Static navbar -->
      <nav class="navbar navbar-default navbar-static-top">
         <div class="container-fluid">
            <div class="navbar-header">
               <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
               <span class="sr-only">Toggle navigation</span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               </button>
               <a class="navbar-brand" href="#">Project name</a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav">

                    @if (Route::has('login'))
                        @auth

                            <li class="active"><a href="{{ url('/') }}">Home</a></li>

                        @else

                            <li class="active"><a href="{{ route('login') }}">Login</a></li>

                        @endauth
                    @endif

                </ul>

                @guest
                @else

                    <form action="{{ route('logout') }}" method="post" style="display: inline-block; float: right">
                        {{ csrf_field() }}

                        <span>EUR: {{ $currencies[ 'eur' ] }} | USD: {{ $currencies['usd'] }}</span>

                        <input type="submit" name="" value="Logout" style="border: none; height: 50px; padding: 0px 20px;">
                    </form>

                @endguest


            </div>
            <!--/.nav-collapse -->
         </div>
      </nav>
      <div class="container-fluid">

          @yield('breadcrumbs')

          @guest
              <div class="col-lg-12">

                  @yield('content')

              </div>

          @else

              <div class="row">
                  <div class="col-lg-2">
                      <ul class="list-group">
                          <li class="list-group-item"><a href="/">Главная</a></li>

                          <?php if (Auth::user()->isCanViewListDocuments()) : ?>

                              <li class="list-group-item"><a href="{{ route('document.index') }}">Договоры</a></li>

                          <?php endif; ?>
                          <?php if (Auth::user()->isAdmin()) : ?>

                              <li class="list-group-item"><a href="{{ route('admin.roles.index') }}">Роли</a></li>

                          <?php endif; ?>
                          <?php if (Auth::user()->isCanViewWorkers()) : ?>

                              <li class="list-group-item"><a href="{{ route('worker.index') }}">Сотрудники</a></li>

                          <?php endif; ?>
                          <?php if (Auth::user()->isCanViewShops()) : ?>

                              <li class="list-group-item"><a href="{{ route('shop.index') }}">Магазины</a></li>

                          <?php endif; ?>
                          <?php if (Auth::user()->isCanViewIndividualClients()) : ?>

                              <li class="list-group-item"><a href="{{ route('individual.client.index') }}">Клиенты: физические лица</a></li>

                          <?php endif; ?>
                          <?php if (Auth::user()->isCanViewLegalClients()) : ?>

                              <li class="list-group-item"><a href="{{ route('legal.client.index') }}">Клиенты: юридические лица</a></li>

                          <?php endif; ?>
                          <?php if (Auth::user()->isCanViewFactories()) : ?>

                              <li class="list-group-item"><a href="{{ route('factory.index') }}">Фабрики</a></li>

                          <?php endif; ?>
                          <?php if (Auth::user()->isCanViewDocumentTemplates()) : ?>

                              <li class="list-group-item"><a href="{{ route('document.template.index') }}">Шаблоны документов</a></li>

                          <?php endif; ?>

                          <?php if (Auth::user()->isCanViewTableDocuments()) : ?>

                              <li class="list-group-item"><a href="{{ route('document.table.index') }}">Таблица документов</a></li>

                          <?php endif; ?>
                          <?php if (Auth::user()->isCanViewSettings()) : ?>

                              <li class="list-group-item"><a href="{{ route('admin.settings.index') }}">Настройки</a></li>

                          <?php endif; ?>

                      </ul>
                  </div>
                  <div class="col-lg-10">

                      @yield('content')

                  </div>
              </div>

          @endguest

          <div class="modal fade" tabindex="-1" id="modal-core" role="dialog">
              <div class="modal-dialog" role="document">
                  <div class="modal-content">
                  </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
          </div><!-- /.modal -->

      </div>

      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/inputmask/inputmask.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/inputmask/jquery.inputmask.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/inputmask/inputmask.numeric.extensions.min.js"></script>
      <script src="/js/vendor/duplicate/addel.jquery.js"></script>
      <script src="/js/main.js"></script>

      @yield('javascript')

   </body>
</html>