<?php

namespace App\Models\Shop;

use Illuminate\Database\Eloquent\Model;

/**
 * Model class for table `shop_user_leaders`.
 */
class ShopUserLeader extends Model
{
    /**
     * Relation with table workers.
     *
     * @return
     */
    public function worker()
    {
        return $this->belongsTo('App\Models\User\Worker');
    }
}
