@extends('layouts.bootstrap')

@section('breadcrumbs')

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Главная</a></li>
            <li class="breadcrumb-item active" aria-current="page">Просмотр шаблона</li>
        </ol>
    </nav>

@stop
@section('content')

    <div class="row">
        <div class="col-lg-12">

            <a class="btn btn-primary" href="{{ route('document.template.with.document.topdf', compact('document', 'template')) }}" target="_blank">Print</a>
            <a class="btn btn-success" href="{{ route('document.template.with.document.todoc', compact('document', 'template')) }}" target="_blank">Save</a>

            {!! $page !!}

        </div>
    </div>

@stop