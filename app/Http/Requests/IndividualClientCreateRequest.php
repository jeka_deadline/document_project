<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class IndividualClientCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'surname' => 'required|max:50',
            'name' => 'required|max:50',
            'lastname' => 'max:50',
            'register_address' => 'string|nullable',
            'shipping_address' => 'required_unless:register_address_equal_shipping_address,on',
            'phones.*' => 'required',
            'email' => 'nullable|email|max:100',
            'additional_email' => 'nullable|email|max:100',
            'additional_surname' => 'max:50',
            'additional_name' => 'max:50',
            'additional_lastname' => 'max:50',
            'additional_phones' => 'array',
            'comment' => 'nullable|string',
        ];
    }
}
