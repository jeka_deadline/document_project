<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title">Добавление нового платежа на фабрику</h4>
    <div class="row">
        <form action="{{ route('document.factory.payment.update', compact('factoryPayment')) }}" method="post" id="create-factory-template">
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="">Отправка платежки на фабрику</label>
                    <input type="text" class="form-control datetimepicker" name="date_dispatch" value="{{ $factoryPayment->date_dispatch }}">
                </div>
                <div class="form-group">
                    <label for="">Дата платежа</label>
                    <input type="text" class="form-control datetimepicker" name="date_payment" value="{{ $factoryPayment->date_payment }}">
                </div>
                <div class="form-group">
                    <label for="">Инвойс</label>
                    <input type="text" class="form-control" name="invoice" value="{{ $factoryPayment->invoice }}">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="">Сумма платежа EUR</label>
                    <input type="text" class="form-control numeric-positive-mask" name="sum" value="{{ $factoryPayment->sum }}">
                </div>
                <div class="form-group">
                    <label for="">Бансковский перевод EUR</label>
                    <input type="text" class="form-control numeric-positive-mask" name="bank_transfer" value="{{ $factoryPayment->bank_transfer }}">
                </div>
                <input type="submit" class="btn btn-success" value="Сохранить">
            </div>
        </form>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
</div>
