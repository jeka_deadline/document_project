<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\ClientService;
use App\Http\Requests\IndividualClientCreateRequest;
use App\Http\Requests\IndividualClientUpdateRequest;
use App\Models\Client\IndividualClient;
use Illuminate\Support\Facades\Auth;

/**
 * Individual client controller.
 */
class IndividualClientController extends Controller
{
    /**
     * client service.
     *
     * @var \App\Services\ClientService $clientService
     * @access private
     */
    private $clientService;

    /**
     * Controller constructor.
     *
     * @param \App\Services\ClientService $workerService Client service
     * @return void
     */
    public function __construct (ClientService $clientService)
    {
        $this->clientService = $clientService;
    }

    /**
     * Display page with all individual clients.
     *
     * @param \Illuminate\Http\Request $request Request
     * @return \Illuminate\Support\Facades\View
     */
    public function index(Request $request)
    {
        if (!Auth::user()->isCanViewIndividualClients()) {
            $this->accessDanied();
        }

        return view('client.individual-client.index', [
            'clients' => $this->clientService->individualClientsWithPaginate(),
        ]);
    }

    /**
     * Display page with form create new individual client.
     *
     * @param \Illuminate\Http\Request $request Request
     * @return \Illuminate\Support\Facades\View
     */
    public function create(Request $request)
    {
        if (!Auth::user()->isCanCreateIndividualClient()) {
            $this->accessDanied();
        }

        return view('client.individual-client.create');
    }

    /**
     * Create new individual client.
     *
     * @param \App\Http\Requests\IndividualClientCreateRequest $request Request for individual client
     * @return \Illuminate\Support\Facades\Redirect
     */
    public function store(IndividualClientCreateRequest $request)
    {
        if (!Auth::user()->isCanCreateIndividualClient()) {
            $this->accessDanied();
        }

        $client = $this->clientService->createNewIndividualClient($request->all());

        if ($request->ajax()) {
            return response()->json([
                'status' => true,
                'key' => 'i-' . $client->id,
                'value' => $client->fullName,
                'contactFullName' => $client->contactFullName,
            ]);
        }

        return redirect(route('individual.client.index'));
    }

    /**
     * Display page with client information.
     *
     * @param \App\Models\Client\IndividualClient $client Model individual client
     * @param \Illuminate\Http\Request $request Request
     * @return \Illuminate\Support\Facades\View
     */
    public function view(IndividualClient $client, Request $request)
    {
        if (!Auth::user()->isCanViewIndividualClient()) {
            $this->accessDanied();
        }

        return view('client.individual-client.view', compact('client'));
    }

    /**
     * Display page with form update individual client.
     *
     * @param \App\Models\Client\IndividualClient $client Model individual client
     * @param \Illuminate\Http\Request $request Request
     * @return \Illuminate\Support\Facades\View
     */
    public function edit(IndividualClient $client, Request $request)
    {
        if (!Auth::user()->isCanEditIndividualClient()) {
            $this->accessDanied();
        }

        return view('client.individual-client.update', compact('client'));
    }

    /**
     * Update individual client.
     *
     * @param \App\Models\Client\IndividualClient $client Model individual client
     * @param \App\Http\Requests\IndividualClientUpdateRequest $request Request for update individual client
     * @return \Illuminate\Support\Facades\Redirect
     */
    public function update(IndividualClient $client, IndividualClientUpdateRequest $request)
    {
        if (!Auth::user()->isCanEditIndividualClient()) {
            $this->accessDanied();
        }

        $this->clientService->updateIndividualClient($client, $request->all());

        return redirect(route('individual.client.index'));
    }

    /**
     * Delete individual client.
     *
     * @param \App\Models\Client\IndividualClient $client Model individual client
     * @param \Illuminate\Http\Request $request Request
     * @return \Illuminate\Support\Facades\Redirect
     */
    public function delete(IndividualClient $client, Request $request)
    {
        if (!Auth::user()->isCanDeleteIndividualClient()) {
            $this->accessDanied();
        }

        $client->delete();

        return redirect(route('individual.client.index'));
    }
}
