<?php

use Illuminate\Database\Seeder;
use App\Models\ProjectAction;

class ProjectActionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('project_actions')->insert([
            [
                'name' => ProjectAction::ACTION_LIST_DOCUMENTS,
                'description' => 'Просмотр списка договоров',
            ],
            [
                'name' => ProjectAction::ACTION_EDIT_DOCUMENT,
                'description' => 'Редактирование договора',
            ],
            [
                'name' => ProjectAction::ACTION_CREATE_DOCUMENT,
                'description' => 'Создание договора',
            ],
            [
                'name' => ProjectAction::ACTION_VIEW_DOCUMENT,
                'description' => 'Просмотр договора',
            ],
            [
                'name' => ProjectAction::ACTION_ATTACH_DOCUMENT_TEMPLATE,
                'description' => 'Прикрепление шаблона документа к договору',
            ],
            [
                'name' => ProjectAction::ACTION_ATTACH_PRODUCT,
                'description' => 'Добавление товара к договору',
            ],
            [
                'name' => ProjectAction::ACTION_EDIT_ATTACH_PRODUCT,
                'description' => 'Редактирование прикрепляемого товара к договору',
            ],
            [
                'name' => ProjectAction::ACTION_DELETE_ATTACH_PRODUCT,
                'description' => 'Удаление прикрепляемого товара к договору',
            ],
            [
                'name' => ProjectAction::ACTION_CREATE_NEW_PAYMENT,
                'description' => 'Создание новой оплаты к договору',
            ],
            [
                'name' => ProjectAction::ACTION_CREATE_SERVICE_ACCOUNT,
                'description' => 'Создание сервисного счета к договору',
            ],
            [
                'name' => ProjectAction::ACTION_VIEW_SERVICE_ACCOUNT,
                'description' => 'Просмотр сервисного счета к договору',
            ],
            [
                'name' => ProjectAction::ACTION_SERVICE_ACCOUNT_TO_PDF,
                'description' => 'Просмотр сервисного счета в pdf формате',
            ],
            [
                'name' => ProjectAction::ACTION_DELETE_DOCUMENT,
                'description' => 'Удаление договора',
            ],
            [
                'name' => ProjectAction::ACTION_LIST_WORKERS,
                'description' => 'Просмотр списка сотрудников',
            ],
            [
                'name' => ProjectAction::ACTION_CREATE_WORKER,
                'description' => 'Добавление сотрудников',
            ],
            [
                'name' => ProjectAction::ACTION_EDIT_WORKER,
                'description' => 'Редактирование сотрудника',
            ],
            [
                'name' => ProjectAction::ACTION_VIEW_WORKER,
                'description' => 'Просмотр сотрудника',
            ],
            [
                'name' => ProjectAction::ACTION_DELETE_WORKER,
                'description' => 'Удаление сотрудника',
            ],
            [
                'name' => ProjectAction::ACTION_LIST_SHOPS,
                'description' => 'Просмотр списка магазинов',
            ],
            [
                'name' => ProjectAction::ACTION_CREATE_SHOP,
                'description' => 'Создание магазина',
            ],
            [
                'name' => ProjectAction::ACTION_EDIT_SHOP,
                'description' => 'Редактирование магазина',
            ],
            [
                'name' => ProjectAction::ACTION_VIEW_SHOP,
                'description' => 'Просмотр магазина',
            ],
            [
                'name' => ProjectAction::ACTION_DELETE_SHOP,
                'description' => 'Удаление магазина',
            ],
            [
                'name' => ProjectAction::ACTION_LIST_LEGAL_CLIENTS,
                'description' => 'Просмотр списка юридических лиц',
            ],
            [
                'name' => ProjectAction::ACTION_CREATE_LEGAL_CLIENT,
                'description' => 'Добавление юридического лица',
            ],
            [
                'name' => ProjectAction::ACTION_EDIT_LEGAL_CLIENT,
                'description' => 'Редактирование юридического лица',
            ],
            [
                'name' => ProjectAction::ACTION_VIEW_LEGAL_CLIENT,
                'description' => 'Просмотр юридического лица',
            ],
            [
                'name' => ProjectAction::ACTION_DELETE_LEGAL_CLIENT,
                'description' => 'Удаление юридического лица',
            ],
            [
                'name' => ProjectAction::ACTION_LIST_INDIVIDUAL_CLIENTS,
                'description' => 'Просмотр списка физических лиц',
            ],
            [
                'name' => ProjectAction::ACTION_CREATE_INDIVIDUAL_CLIENT,
                'description' => 'Добавление физического лица',
            ],
            [
                'name' => ProjectAction::ACTION_EDIT_INDIVIDUAL_CLIENT,
                'description' => 'Редактирование физического лица',
            ],
            [
                'name' => ProjectAction::ACTION_VIEW_INDIVIDUAL_CLIENT,
                'description' => 'Просмотр физического лица',
            ],
            [
                'name' => ProjectAction::ACTION_DELETE_INDIVIDUAL_CLIENT,
                'description' => 'Удаление физического лица',
            ],
            [
                'name' => ProjectAction::ACTION_LIST_FACTORIES,
                'description' => 'Просмотр списка фабрик',
            ],
            [
                'name' => ProjectAction::ACTION_CREATE_FACTORY,
                'description' => 'Создание фабрики',
            ],
            [
                'name' => ProjectAction::ACTION_EDIT_FACTORY,
                'description' => 'Редактирование фабрики',
            ],
            [
                'name' => ProjectAction::ACTION_VIEW_FACTORY,
                'description' => 'Просмотр фабрики',
            ],
            [
                'name' => ProjectAction::ACTION_DELETE_FACTORY,
                'description' => 'Удаление фабрики',
            ],
            [
                'name' => ProjectAction::ACTION_LIST_DOCUMENT_TEMPLATES,
                'description' => 'Просмотр списка шаблонов для договоров',
            ],
            [
                'name' => ProjectAction::ACTION_CREATE_DOCUMENT_TEMPLATE,
                'description' => 'Создание шаблона для договора',
            ],
            [
                'name' => ProjectAction::ACTION_EDIT_DOCUMENT_TEMPLATE,
                'description' => 'Редактирование шаблона для договора',
            ],
            [
                'name' => ProjectAction::ACTION_VIEW_DOCUMENT_TEMPLATE,
                'description' => 'Просмотр шаблона для договора',
            ],
            [
                'name' => ProjectAction::ACTION_DELETE_DOCUMENT_TEMPLATE,
                'description' => 'Удаление шаблона для договора',
            ],
            [
                'name' => ProjectAction::ACTION_SAVE_AS_DOCUMENT_TEMPLATE,
                'description' => 'Сохранение нового шаблона для договора',
            ],
            [
                'name' => ProjectAction::ACTION_TABLE_DOCUMENTS,
                'description' => 'Просмотр таблицы документов',
            ],
            [
                'name' => ProjectAction::ACTION_UPDATE_CALENDAR,
                'description' => 'Обновление производственного календаря',
            ],
            [
                'name' => ProjectAction::ACTION_LIST_SETTINGS,
                'description' => 'Просмотр списка настроек',
            ],
            [
                'name' => ProjectAction::ACTION_EDIT_SETTING,
                'description' => 'Редактирование настроек',
            ],
        ]);
    }
}
