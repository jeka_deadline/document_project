<?php

namespace App\DocumentSearch\Filters;

use Illuminate\Database\Eloquent\Builder;

class Year implements Filterable
{
    public static function apply(Builder $builder, $value)
    {
        return $builder->whereYear('created_at', $value);
    }
}