<?php

namespace App\Http\Controllers\Document;

use PDF;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Document\DocumentTemplate;
use App\Http\Requests\DocumentTemplateRequest;
use App\Services\DocumentTemplateService;
use App\Models\Document\Document;
use App\Models\Document\DocumentTemplateRelation;
use Illuminate\Support\Facades\Auth;
use App\Libs\HtmlToDoc\HtmlToDoc;

/**
 * Document template controller.
 */
class TemplateController extends Controller
{
    /**
     * Document template service.
     *
     * @access private
     * @var \App\Services\DocumentTemplateService $documentTemplateService
     */
    private $documentTemplateService;

    /**
     * Controller constructor.
     *
     * @param \App\Services\DocumentTemplateService $documentTemplateService Document template service
     * @return void
     */
    public function __construct(DocumentTemplateService $documentTemplateService)
    {
        $this->documentTemplateService = $documentTemplateService;
    }

    /**
     * Display page document templates.
     *
     * @param \Illuminate\Http\Request $request Request
     * @return \Illuminate\Support\Facades\View
     */
    public function index(Request $request)
    {
        if (!Auth::user()->isCanViewDocumentTemplates()) {
            $this->accessDanied();
        }

        return view('document.template.index', [
            'documentTemplates' => $this->documentTemplateService->paginate()
        ]);
    }

    /**
     * Display page with document template form for create new document template.
     *
     * @param \Illuminate\Http\Request $request Request
     * @return \Illuminate\Support\Facades\View
     */
    public function create(Request $request)
    {
        if (!Auth::user()->isCanCreateDocumentTemplate()) {
            $this->accessDanied();
        }

        return view('document.template.create', [
            'documentTemplateTypes' => $this->documentTemplateService->getDocumentTemplateTypes(),
        ]);
    }

    /**
     * Store new document template.
     *
     * @param \App\Http\Requests\DocumentTemplateRequest $request Document template request
     * @return \Illuminate\Support\Facades\Redirect
     */
    public function store(DocumentTemplateRequest $request)
    {
        if (Auth::user()->isCanCreateDocumentTemplate() || Auth::user()->isCanSaveAsDocumentTemplate()) {
            $this->documentTemplateService->createNewTemplate($request->all());

            return redirect()->route('document.template.index');
        }

        $this->accessDanied();
    }

    /**
     * Display page with document template information.
     *
     * @param \Illuminate\Http\Request $request Request
     * @param \App\Models\Document\DocumentTemplate $template Document template model
     * @return \Illuminate\Support\Facades\View
     */
    public function view(Request $request, DocumentTemplate $template)
    {
        if (!Auth::user()->isCanViewDocumentTemplate()) {
            $this->accessDanied();
        }

        return view('document.template.view', compact('template'));
    }

    /**
     * Display page with document template form for edit document template information.
     *
     * @param \Illuminate\Http\Request $request Request
     * @param \App\Models\Document\DocumentTemplate $template Document template model
     * @return \Illuminate\Support\Facades\View
     */
    public function edit(Request $request, DocumentTemplate $template)
    {
        if (!Auth::user()->isCanEditDocumentTemplate()) {
            $this->accessDanied();
        }

        return view('document.template.edit', [
            'template' => $template,
            'documentTemplateTypes' => $this->documentTemplateService->getDocumentTemplateTypes(),
        ]);
    }

    /**
     * Update document template information.
     *
     * @param \App\Http\Requests\DocumentTemplateRequest $request Document template request
     * @param \App\Models\DocumentTemplate $template Document template model
     * @return \Illuminate\Support\Facades\Redirect
     */
    public function update(DocumentTemplateRequest $request, DocumentTemplate $template)
    {
        if (Auth::user()->isCanEditDocumentTemplate() && Auth::user()->isAdmin()) {
            $this->documentTemplateService->updateTemplate($template, $request->all());

            return redirect()->route('document.template.index');
        }

        $this->accessDanied();
    }

    /**
     * Delete document template.
     *
     * @param \Illuminate\Http\Request $request Request
     * @param \App\Models\DocumentTemplate $template Document template model
     * @return \Illuminate\Support\Facades\Redirect
     */
    public function delete(Request $request, DocumentTemplate $template)
    {
        if (!Auth::user()->isCanDeleteDocumentTemplate()) {
            $this->accessDanied();
        }

        $template->delete();

        return redirect(route('document.template.index'));
    }

    /**
     * Display page document templates with document information.
     *
     * @param \App\Models\Document\Document $document Document model.
     * @param \App\Models\Document\DocumentTemplate $template Document template model.
     * @return \Illuminate\Support\Facades\View
     */
    public function showTemplateWithDocument(Document $document, DocumentTemplate $template)
    {
        $page = $this->documentTemplateService->generateTemplatePage($document, $template);

        return view('document.template.with_document', compact('page', 'document', 'template'));
    }

    /**
     * Delete relation between document and document template.
     *
     * @param \App\Models\Document\Document $document Document model.
     * @param \App\Models\Document\DocumentTemplate $template Document template model.
     * @return \Illuminate\Support\Facades\Redirect
     */
    public function deleteTemplateRelationWithDocument(Document $document, DocumentTemplate $template)
    {
        $this->documentTemplateService->deleteRelationTemplateAndDocument($document, $template);

        return redirect(route('document.view', compact('document')));
    }

    /**
     * Display page document templates with document information pdf.
     *
     * @param \App\Models\Document\Document $document Document model.
     * @param \App\Models\Document\DocumentTemplate $template Document template model.
     * @return \Illuminate\Support\Facades\View
     */
    public function toPdf(Document $document, DocumentTemplate $template)
    {
        $page = $this->documentTemplateService->generateTemplatePage($document, $template);

        $pdf = PDF::loadView('document.template.with_document_pdf', compact('page'));

        return $pdf->stream(time() . '.pdf');
    }

    /**
     * Download page document templates with document information docx.
     *
     * @param \App\Models\Document\Document $document Document model.
     * @param \App\Models\Document\DocumentTemplate $template Document template model.
     * @return \Illuminate\Support\Facades\View
     */
    public function toDoc(Document $document, DocumentTemplate $template)
    {
        $page = $this->documentTemplateService->generateTemplatePage($document, $template);

        $html = str_replace(array("\n", "\r"), '', $page);
        $html = str_replace('&nbsp;', '', $html);
        $html = str_replace(array('&lt;', '&gt;', '&amp;'), array('_lt_', '_gt_', '_amp_'), $html);
        $html = html_entity_decode($html, ENT_QUOTES, 'UTF-8');
        $html = str_replace('&', '&amp;', $html);
        $html = str_replace(array('_lt_', '_gt_', '_amp_'), array('&lt;', '&gt;', '&amp;'), $html);
        $html = str_replace('\t', '', $html);

        $name = time();

        $doc = new HtmlToDoc();
        $doc->createDoc($html, $name, true);
    }
}
