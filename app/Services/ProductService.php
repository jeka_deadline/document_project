<?php

namespace App\Services;

use App\Models\Product;
use App\Models\Document\Document;

/**
 * Product service
 */
class ProductService
{
    /**
     * Get list product locations.
     *
     * @return array
     */
    public function getListProductLocations()
    {
        return [
            Product::LOCATION_SHOP => 'В магазине',
            Product::LOCATION_STOCK => 'На складе',
        ];
    }

    /**
     * Get list product statuses by document type.
     *
     * @param string $documentType Document type.
     * @return array
     */
    public function getListProductStatuses($documentType)
    {
        switch ($documentType) {
            case Document::DOCUMENT_TYPE_ORDER:
                return Product::getListOrderStatuses();
            case Document::DOCUMENT_TYPE_EXPOSITION:
                return Product::getListExpositionStatuses();
            default:
                return [];
        }
    }

    /**
     * Attach product to document.
     *
     * @param \App\Models\Document\Document $document Document model.
     * @param array List product fields from request.
     * @return null|\App\Models\Product
     */
    public function attachProductToDocument(Document $document, $fields)
    {
        $product = new Product();

        $product->fill($fields);
        $product->document_id = $document->id;

        if ($product->save()) {
            return $product;
        }

        return null;
    }

    /**
     * Update product.
     *
     * @param \App\Models\Product $product Product model.
     * @param array List product fields from request.
     * @return bool
     */
    public function updateProduct(Product $product, $fields)
    {
        return $product->update($fields);
    }
}
