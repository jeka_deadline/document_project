<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateNewDocumentPaymentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cross_currrency' => 'required|numeric|min:0.1',
            'enter_eur' => 'required_without_all:enter_usd,enter_rub|numeric|nullable',
            'enter_usd' => 'required_without_all:enter_eur,enter_rub|numeric|nullable',
            'enter_rub' => 'required_without_all:enter_eur,enter_usd|numeric|nullable',
        ];
    }
}
