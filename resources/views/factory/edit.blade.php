@extends('layouts.bootstrap')

@section('breadcrumbs')

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Главная</a></li>
            <li class="breadcrumb-item"><a href="{{ route('factory.index') }}">Список фабрик</a></li>
            <li class="breadcrumb-item active" aria-current="page">Обновление фабрики</li>
        </ol>
    </nav>

@stop
@section('content')

    <div class="row">
        <div class="col-lg-12">
              <h2>Обновление фабрики</h2>
              <form method="post" action="{{ route('factory.update', ['factory' => $factory]) }}">
                  {{ csrf_field() }}
                  {{ method_field('PUT') }}

                  <div class="form-group">
                      <label class="control-label" for="">Наименование</label>
                      <input class="form-control" type="text" name="name" value="{{ old('name', $factory->name) }}">

                      @if ($errors->has('name'))

                          <div class="text-danger">

                                {{ $errors->first('name') }}

                          </div>

                      @endif

                  </div>

                  <div class="form-group">
                      <label class="control-label" for="">Страна</label>
                      <input class="form-control" type="text" name="country" value="{{ old('country', $factory->country) }}">

                      @if ($errors->has('country'))

                          <div class="text-danger">

                                {{ $errors->first('country') }}

                          </div>

                      @endif

                  </div>

                  <div class="form-group">
                      <label class="control-label" for="">Описание</label>
                      <textarea class="form-control" rows="6" type="text" name="description">{{ old('description', $factory->description) }}</textarea>

                      @if ($errors->has('description'))

                          <div class="text-danger">

                                {{ $errors->first('description') }}

                          </div>

                      @endif

                  </div>

                  <input type="submit" value="Обновить" class="btn btn-primary">
                  <a class="btn btn-info" href="{{ route('factory.index') }}">Назад</a>
              </form>
        </div>
    </div>

@stop