$(function() {
    $('[name=enter_eur], [name=enter_rub], [name=enter_usd]').focus(function( e ) {
        $('[name=percentage_prepayment]').val('');
    });

    $('[name=percentage_prepayment]').focus(function( e ) {
        $('[name=enter_eur]').val('');
        $('[name=enter_rub]').val('');
        $('[name=enter_usd]').val('');
    });

    $('#add-client').click(function( e ) {
        e.preventDefault();

        $.each($('.modal form'), function(index, form) {
            form.reset();

            $(form).find('.text-danger').remove();
        });

        $('#modal-add-client').modal();
    });

    $('[name=percentage_currrency]').keyup(function( e ) {
        let usd = $(this).data('usd');
        let eur = $(this).data('eur');
        let percentValue = $(this).val();
        let percent = $(this).val() / 100;
        let usdMarkup = usd;
        let eurMarkup = eur;

        if (!isNaN(percentValue) && percentValue != 0 && percentValue != '') {

            usdMarkup += usd * percent;
            eurMarkup += eur * percent;

        }

        $($('#currency-markup').find('td')[ 1 ]).html(usdMarkup.toFixed(2));
        $($('#currency-markup').find('td')[ 3 ]).html(eurMarkup.toFixed(2));
    });

    $('#calculation-sum').click(function( e ) {
        e.preventDefault();

        var self = $(this);

        $(this).attr('disabled', 'disabled');
        let data = {};

        $('#block-calculation input').each(function(index, value) {
            data[ $(value).attr('name') ] = $(value).val();
        });

        $.ajax({
            url: $(this).data('url'),
            method: 'post',
            data: data,
            dataType: 'json',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
        }).done(function(data) {
            $(self).removeAttr('disabled');
            $('#block-calculation div.text-danger').remove();
            if (data.status) {
                $.each(data.content, function(key, value) {
                    switch (key) {
                        case 'enter_eur':
                        case 'enter_usd':
                        case 'enter_rub':
                            $('#block-calculation input[name=' + key + ']').closest('.form-group').find('.help-block').html(value);
                            break;
                        default:
                            $('#block-calculation input[name=' + key + ']').val(value);
                            break;
                    }
                });
            }
        }).fail(function(data) {
            $(self).removeAttr('disabled');
            if (data.status == 422) {
                showAjaxErrors($('#block-calculation'), data.responseJSON);
            }
        });
    });

    $('#calculation-sum-surcharge').click(function( e ) {
        e.preventDefault();

        var self = $(this);

        $(this).attr('disabled', 'disabled');
        let form = $(this).closest('form');

        $.ajax({
            url: $(form).data('calculation-url'),
            method: $(form).attr('method'),
            data: $(form).serialize(),
            dataType: 'json',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
        }).done(function(data) {
            $(self).removeAttr('disabled');
            $(form).find('div.text-danger').remove();
            if (data.status) {
                $.each(data.content, function(key, value) {
                    switch (key) {
                        case 'enter_eur':
                        case 'enter_usd':
                        case 'enter_rub':
                            break;
                        default:
                            $(form).find('input[name=' + key + ']').val(value);
                            break;
                    }
                });
            }
        }).fail(function(data) {
            $(self).removeAttr('disabled');
            if (data.status == 422) {
                showAjaxErrors($(form), data.responseJSON);
            }
        });
    });

    // create new client
    $('#modal-new-client-submit').click(function( e ) {
        e.preventDefault();

        let form = $(this).closest('.modal').find('.tab-pane.active form');

        if (!$(form).length) {
            return null;
        }

        $.ajax({
            url: $(form).attr('action'),
            data: $(form).serialize(),
            method: $(form).attr('method'),
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
        }).done(function(data) {
            if (data.status) {
                var newClientOption = new Option(data.value, data.key, true, true);

                $('select[name=client]').append(newClientOption).trigger('change');
                $('#modal-add-client').modal('hide');

            }
        }).fail(function(data) {
            if (data.status == 422) {
                showAjaxErrors(form, data.responseJSON);
            }
        });
    });
    // end create new client

    $(document).on('click', '#modal-new-payment-submit', function( e ) {
        e.preventDefault();
        $(this).attr('disabled', 'disabled');
        var self = $(this);

        var form = $(this).closest('.modal-content').find('form');

        $.ajax({
            url: $(form).attr('action'),
            data: $(form).serialize(),
            method: $(form).attr('method'),
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
        }).done(function(data) {
            $(self).removeAttr('disabled');
            if (data.status) {
                location.reload();
            }
            console.log(data);
        }).fail(function(data) {
            $(self).removeAttr('disabled');
            if (data.status == 422) {
                showAjaxErrors(form, data.responseJSON);
            }
        });
    })

    $('[name=period_execution]').keyup(function( e ) {
        if ($(this).val() == '') {
            $('[name=date_finish]').val('');
            return;
        }

        let data = {};

        data[ $(this).attr('name') ] = $(this).val();

        $.ajax({
            url: $(this).data('url'),
            method: 'post',
            data: data,
            dataType: 'json',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
        }).done(function(data) {
            if (data.status) {
                $('[name=date_finish]').val(data.dateFinish);
            }
        });
    });

    $(document).on('change', '#choose-client', function (e) {
        let data = {};

        if ($(this).val() == '') {
            $('#input-contact-client').val('');
            return;
        }

        data[ $(this).attr('name') ] = $(this).val();

        $.ajax({
            url: $(this).data('url'),
            method: 'get',
            data: data,
            dataType: 'json'
        }).done(function(data) {
            $('#input-contact-client').val(data.contact_full_name);
        });
    });

    if ($('#datetimepicker').length) {

        $('#datetimepicker').datepicker({
            inline: true,
            sideBySide: true,
            todayHighlight: true,
            format: 'mm/dd/yyyy',
        }).on('changeDate', function(e) {
            var date = new Date();
            date.setTime(e.date.valueOf());
            $('#input-pro-form').val(date.toLocaleDateString());
        });

    }

    $(document).on('click', '#btn-modal-new-payment', function( e ) {
        e.preventDefault();

        $(document).find('#modal-new-payment').modal();
    });

    $(document).on('click', '#btn-attach-documents a', function( e ) {
        e.preventDefault();

        $.ajax({
            url: $(this).closest('#btn-attach-documents').data('url'),
            method: 'get',
            data: {typeTemplate : $(this).data('type')},
            dataType: 'json'
        }).done(function(data) {
            $(document).find('#modal-core .modal-content').html(data.content);
            $(document).find('#modal-core').modal();
        });
    });

    $(document).on('submit', '#attach-new-template', function( e ) {
        e.preventDefault();

        var form = $(this);

        $.ajax({
            url: $(this).attr('action'),
            method: $(this).attr('method'),
            data: $(this).serialize(),
            dataType: 'json',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
        }).done(function(data) {
            if (data.status) {
                $(document).find('#modal-core').modal('hide');
                $(document).find('#block-documents').replaceWith(data.content);
            }
        }).fail(function(data) {
            if (data.status == 422) {
                showAjaxErrors(form, data.responseJSON);
            }
        });
    });

    $(document).on('submit', '#attach-new-product', function( e ) {
        e.preventDefault();

        var form = $(this);

        $.ajax({
            url: $(this).attr('action'),
            method: $(this).attr('method'),
            data: $(this).serialize(),
            dataType: 'json',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
        }).done(function(data) {
            if (data.status) {
                $(document).find('#modal-core').modal('hide');
                $(document).find('#block-products').replaceWith(data.content);
            }
        }).fail(function(data) {
            if (data.status == 422) {
                showAjaxErrors(form, data.responseJSON);
            }
        });
    });

    $(document).on('click', '#btn-modal-new-product', function( e ) {
        e.preventDefault();

        $.ajax({
            url: $(this).data('url'),
            method: 'get',
            dataType: 'json'
        }).done(function(data) {
            $(document).find('#modal-core .modal-content').html(data.content);
            $(document).find('#modal-core').modal();
        });
    });

    $(document).on('click', '.btn-edit-product', function( e ) {
        e.preventDefault();

        $.ajax({
            url: $(this).attr('href'),
            method: 'get',
            dataType: 'json'
        }).done(function(data) {
            $(document).find('#modal-core .modal-content').html(data.content);
            $(document).find('#modal-core').modal();
        });
    });

    $(document).on('click', '#btn-modal-new-service-account', function( e ) {
        e.preventDefault();

        $.ajax({
            url: $(this).data('url'),
            method: 'get',
            dataType: 'json'
        }).done(function(data) {
            $(document).find('#modal-core .modal-content').html(data.content);
            $(document).find('.datetimepicker').datepicker({
                inline: true,
                sideBySide: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd',
                startDate: new Date(),
            });
            $(document).find('#modal-core').modal();
        });
    });

    $(document).on('submit', '#ajax-delete-product', function( e ) {
        e.preventDefault();

        var result = confirm('Вы действительно хотите удалить эту запись?');
        var form = $(this);

        if (!result) {
            return false;
        }

        $.ajax({
            url: $(this).attr('action'),
            method: 'post',
            dataType: 'json',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
        }).done(function(data) {
            if (data.status) {
                $(form).closest('tr').remove();
            }
        });
    });

    $(document).on('click', 'input[type=radio][name=delivery][value=no]', function( e ) {
        $(document).find('input[name=mkad]').attr('disabled', 'disabled');
        $(document).find('input[name=km_outer_mkad]').attr('disabled', 'disabled').val('');
        $(document).find('input[name=sum_delivery]').attr('disabled', 'disabled').val('');
    });

    $(document).on('click', 'input[type=radio][name=delivery][value=yes]', function( e ) {
        $(document).find('input[name=mkad]').removeAttr('disabled');
        $(document).find('input[name=sum_delivery]').removeAttr('disabled');

        if ($(document).find('input[name=mkad]:checked').val() == 'mkad' ) {
            $(document).find('input[name=km_outer_mkad]').attr('disabled', 'disabled').val('');
        } else {
            $(document).find('input[name=km_outer_mkad]').removeAttr('disabled');
        }
    });

    $(document).on('click', 'input[type=radio][name=mkad][value=mkad]', function( e ) {
        $(document).find('input[name=km_outer_mkad]').attr('disabled', 'disabled').val('');
    });

    $(document).on('click', 'input[type=radio][name=mkad][value=outer_mkad]', function( e ) {
        $(document).find('input[name=km_outer_mkad]').removeAttr('disabled');
    });

    $(document).on('click', '#btn-sum-service-account', function( e ) {
        e.preventDefault();

        var sum = calculationSumServiceAccount();
        var course = $(document).find('input[name=course]').val();
        var sumRub = convertEurToRub(sum, course);

        $(document).find('input[name=full_sum_eur]').val(sum);
        $(document).find('input[name=full_sum_rub]').val(sumRub);
    });

    $(document).on('click', '#btn-sum-with-promotion', function( e ) {
        e.preventDefault();

        var sum = parseInt($(document).find('input[name=full_sum_eur]').val());
        var promotion = parseInt($(document).find('input[name=promotion_sum_eur]').val());
        var course = $(document).find('input[name=course]').val();

        if (!isNaN(sum) && !isNaN(promotion)) {
            sum = sum - promotion;
            var sumRub = convertEurToRub(sum, course);

            $(document).find('input[name=full_sum_eur_with_promotion]').val(sum);
            $(document).find('input[name=full_sum_rub_with_promotion]').val(sumRub);
        }
    });

    $(document).on('change', '#select-type-payment', function( e ) {
        e.preventDefault();

        $.ajax({
            url: $(this).data('url'),
            method: 'get',
            data: {type_payment: $(this).val()},
            dataType: 'json',
        }).done(function(data) {
            var course = data.course;
            var sum = calculationSumServiceAccount();
            $(document).find('input[name=course]').val(course);
            var sumRub = convertEurToRub(sum, course);
            $(document).find('input[name=full_sum_eur]').val(sum);
            $(document).find('input[name=full_sum_rub]').val(sumRub);

            var sumWithPromotion = $(document).find('[name=full_sum_eur_with_promotion]').val();

            if (sumWithPromotion) {
                $(document).find('input[name=full_sum_rub_with_promotion]').val(convertEurToRub(sumWithPromotion, course));
            }
        });
    });

    $(document).on('submit', '#attach-service-account', function( e ) {
        e.preventDefault();

        var form = $(this);

        $.ajax({
            url: $(this).attr('action'),
            method: $(this).attr('method'),
            data: $(this).serialize(),
            dataType: 'json',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
        }).done(function(data) {
            if (data.status) {
                $(document).find('#modal-core').modal('hide');
                $(document).find('#block-service-accounts').replaceWith(data.content);
                $(document).find('#btn-modal-new-service-account').remove();
            }
        }).fail(function(data) {
            if (data.status == 422) {
                showAjaxErrors(form, data.responseJSON);
            }
        });
    });

    $('.edittable').dblclick(function( e ) {
        if ($(this).find('textarea').length) {
            return false;
        }

        var val = $(this).html();
        $(this).html(`<textarea class='form-control' style="width: 200px; height: 150px">${val}</textarea>`);
        $(document).find('.edittable textarea').focus();
    });

    $(document).on('click', function( e ) {
        if ($(e.target).closest('td.edittable').find('textarea').length) {
            return;
        }

        if ($(document).find('.edittable textarea').length) {
          var textarea = $(document).find('.edittable textarea');
          var td = $(textarea).closest('td');
          var tr = $(textarea).closest('tr');
          var val = $(textarea).val();

          $.ajax({
            url: $(tr).data('url'),
            method: 'post',
            data: {key: $(td).data('key'), value: val},
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
          }).done(function(data) {
              if (data.status) {
                  $(td).html(val);
              }
          })
        }
    });

    $(document).on('click', '#btn-modal-new-factory-payment', function( e ) {
        e.preventDefault();

        $.ajax({
            url: $(this).data('url'),
            method: 'get',
            dataType: 'json'
        }).done(function(data) {
            $(document).find('#modal-core .modal-content').html(data.content);
            $(document).find('#modal-core .datetimepicker').datepicker({
                inline: true,
                sideBySide: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd',
                //startDate: new Date(),
            });
            $(document).find('#modal-core .numeric-positive-mask').inputmask({
                alias: 'numeric',
                allowMinus: false,
                rightAlign: false,
            });
            $(document).find('#modal-core').modal();
        });
    });

    $(document).on('submit', '#create-factory-template', function( e ) {
        e.preventDefault();

        var form = $(this);

        $.ajax({
            url: $(this).attr('action'),
            method: $(this).attr('method'),
            data: $(this).serialize(),
            dataType: 'json',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
        }).done(function(data) {
            if (data.status) {
                $(document).find('#modal-core').modal('hide');
                $(document).find('[data-document-id=' + data.documentId + ']').replaceWith(data.content);
                $($(document).find('[data-document-id=' + data.documentId + ']').closest('tr')).find('.td_bank_transfer').html(data.sumBunkTransfer);
                $($(document).find('[data-document-id=' + data.documentId + ']').closest('tr')).find('.td_bank_transfer_remainder').html(data.sumBankTransferRemainder);
                $($(document).find('[data-document-id=' + data.documentId + ']').closest('tr')).find('.td_cost_price').html(data.costPrice);
            }
        }).fail(function(data) {
            if (data.status == 422) {
                showAjaxErrors(form, data.responseJSON);
            }
        });
    });

    $(document).on('click', '.edit-factory-payment', function( e ) {
        $.ajax({
            url: $(this).data('url'),
            method: 'get',
            dataType: 'json'
        }).done(function(data) {
            $(document).find('#modal-core .modal-content').html(data.content);
            $(document).find('#modal-core .datetimepicker').datepicker({
                inline: true,
                sideBySide: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd',
            });
            $(document).find('#modal-core .numeric-positive-mask').inputmask({
                alias: 'numeric',
                allowMinus: false,
                rightAlign: false,
            });
            $(document).find('#modal-core').modal();
        });
    });
});

function showAjaxErrors(block, errors)
{
    $(block).find('div.text-danger').remove();
    let htmlError = '<div class="text-danger">{errorMessage}</div>';
    $.each(errors, function(key, value) {
        let textError = value[ 0 ];
        let error = htmlError.replace('{errorMessage}', textError);

        if (key.indexOf('.') != -1) {
            key = key.substr(0, key.indexOf('.'));
            let name = key + '\\[\\]';
            $(error).insertAfter($(block).find('[name=' + name + ']').closest('.form-group').last());
        } else {
            $(error).insertAfter($(block).find(`[name=${key}]`));
        }
    });
}

function calculationSumServiceAccount()
{
    var sumDelivery = 0;

    if ($(document).find('input[name=sum_delivery]').attr('disabled') === undefined) {
        sumDelivery = parseInt($(document).find('input[name=sum_delivery]').val());
    }

    var sumClimb = parseInt($(document).find('input[name=sum_climb]').val());
    var sumAssembly = parseInt($(document).find('input[name=sum_assembly]').val());
    var sumRemovingPackaging = parseInt($(document).find('input[name=removing_packaging]').val());

    if (isNaN(sumClimb)) {
        sumClimb = 0;
    }

    if (isNaN(sumDelivery)) {
        sumDelivery = 0;
    }

    if (isNaN(sumAssembly)) {
        sumAssembly = 0;
    }

    if (isNaN(sumRemovingPackaging)) {
        sumRemovingPackaging = 0;
    }

    return sumDelivery + sumClimb + sumAssembly + sumRemovingPackaging;
}

function convertEurToRub(sum, course)
{
    var sum = sum * course;

    return sum.toFixed(2);
}

function calculationSumServiceAccountWithPromotion(sum, promotion)
{
    return sum - promotion;
}
