<?php

namespace App\Services;

use App\Models\Document\FactoryPayment;
use App\Models\Document\Document;

/**
 * Factory payment service
 */
class FactoryPaymentService
{
    /**
     * Create new factory.
     *
     * @param array $fields Factory fields
     * @return \App\Models\Factory
     */
    public function createNewPayment($fields, Document $document)
    {
        $model = new FactoryPayment();

        $model->fill($fields);
        $model->document_id = $document->id;

        return $model->save();
    }

    public function getMaxSum()
    {
        return FactoryPayment::max('sum');
    }

    public function getMaxBankTransfer()
    {
        return FactoryPayment::max('bank_transfer');
    }

    public function updatePayment($fields, FactoryPayment $factoryPayment)
    {
        return $factoryPayment->update($fields);
    }
}
