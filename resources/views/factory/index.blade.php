@extends('layouts.bootstrap')

@section('breadcrumbs')

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Главная</a></li>
            <li class="breadcrumb-item active" aria-current="page">Список фабрик</li>
        </ol>
    </nav>

@stop
@section('content')

    <div class="row">
          <div class="col-lg-12">

                <?php if (Auth::user()->isCanCreateFactory()) : ?>

                    <a class="btn btn-success" href="{{ route('factory.create') }}">Добавить фабрику</a>

                <?php endif; ?>

                <br><br>
                <table class="table">
                      <thead>
                            <tr>
                                  <th>Название</th>
                                  <th>Страна</th>
                                  <th>Операции</th>
                            </tr>
                      </thead>
                      <tbody>

                          @foreach ($factories as $factory)

                              <tr>
                                  <td>
                                      {{ $factory->name }}
                                  </td>
                                  <td>
                                      {{ $factory->country }}
                                  </td>
                                  <td>

                                      <?php if (Auth::user()->isCanEditFactory()) : ?>

                                          <a href="{{ route('factory.edit', ['factory' => $factory]) }}"><i class="fa fa-edit fa-2x"></i></a>

                                      <?php endif; ?>
                                      <?php if (Auth::user()->isCanViewFactory()) : ?>

                                          <a href="{{ route('factory.view', ['factory' => $factory]) }}"><i class="fa fa-eye fa-2x"></i></a>

                                      <?php endif; ?>
                                      <?php if (Auth::user()->isCanDeleteFactory()) : ?>

                                          <form class="delete-form" method="post" action="{{ route('factory.delete', ['factory' => $factory]) }}" style="display: inline">
                                              {{ csrf_field() }}
                                              {{ method_field('DELETE') }}

                                              <button style="color: #337ab7; border: none; background: none"><i class="fa fa-trash fa-2x"></i></button>
                                          </form>

                                     <?php endif; ?>

                                  </td>
                              </tr>

                          @endforeach

                      </tbody>
                </table>

                {{ $factories->links() }}
          </div>
    </div>

@stop