<?php

namespace App\DocumentSearch\Filters;

use Illuminate\Database\Eloquent\Builder;

class Factory implements Filterable
{
    public static function apply(Builder $builder, $value)
    {
        return $builder->whereHas('products', function($query) use ($value) {
            return $query->where('factory_id', $value);
        });
    }
}