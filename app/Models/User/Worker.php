<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

/**
 * Model class for table `workers`.
 */
class Worker extends Model
{
    /**
     * {@inheritdoc}
     *
     * @access protected
     * @var array $fillable
     */
    protected $fillable = [
        'surname',
        'name',
        'lastname',
        'email',
        'phones',
        'additional_email',
        'additional_phones',
        'comment',
        'user_id',
    ];

    /**
     * Setter for set worker phones.
     *
     * @param string $value Phones value.
     * @return void
     */
    public function setPhonesAttribute($value)
    {
        if (!is_array($value)) {
            $value = [$value];
        }

        $this->attributes[ 'phones' ] = json_encode($value);
    }

    /**
     * Setter for set worker additional phones.
     *
     * @param string $value Additional phones value.
     * @return void
     */
    public function setAdditionalPhonesAttribute($value)
    {
        if (!is_array($value)) {
            $value = [$value];
        }

        $this->attributes[ 'additional_phones' ] = json_encode($value);
    }

    /**
     * Get phones as array.
     *
     * @return array
     */
    public function getPhonesAttribute()
    {
        $phones = json_decode($this->attributes[ 'phones' ]);

        if (!is_array($phones)) {
            return [];
        }

        return $phones;
    }

    /**
     * Get additional phones as array.
     *
     * @return array
     */
    public function getAdditionalPhonesAttribute()
    {
        $phones = json_decode($this->attributes[ 'additional_phones' ]);

        if (!is_array($phones)) {
            return [];
        }

        return $phones;
    }

    /**
     * Get worker full name.
     *
     * @return array
     */
    public function getFullNameAttribute()
    {
        return $this->surname . ' ' . $this->name . ' ' . $this->lastname;
    }

    /**
     * Relation with table users.
     *
     * @return
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Relation with table user roles.
     *
     * @return
     */
    public function userRole()
    {
        return $this->hasOne('App\Models\User\UserRole', 'user_id', 'user_id');
    }

    /**
     * Scope for get workers with role general director.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return
     */
    public function scopeGeneralDirectors(Builder $query)
    {
        $role = Role::director()->first();

        return $query->join('user_roles', function($join) use ($role) {
            $join->on('user_roles.user_id', '=', 'workers.user_id')
                ->where('role_id', '=', $role->id);
        })
        ->select('workers.*')
        ->get();
    }

    /**
     * Scope for get workers with role shop director.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return
     */
    public function scopeShopDirectors(Builder $query)
    {
        $role = Role::shopDirector()->first();

        return $query->join('user_roles', function($join) use ($role) {
            $join->on('user_roles.user_id', '=', 'workers.user_id')
                ->where('role_id', '=', $role->id);
        })
        ->select('workers.*')
        ->get();
    }

    /**
     * Scope for get workers with role deputy shop director.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return
     */
    public function scopeDeputyShopDirectors(Builder $query)
    {
        $role = Role::shopDeputyDirector()->first();

        return $query->join('user_roles', function($join) use ($role) {
            $join->on('user_roles.user_id', '=', 'workers.user_id')
                ->where('role_id', '=', $role->id);
        })
        ->select('workers.*')
        ->get();
    }

    /**
     * Scope for get workers with role managers.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return
     */
    public function scopeManagers(Builder $query)
    {
        $role = Role::manager()->first();

        return $query->join('user_roles', function($join) use ($role) {
            $join->on('user_roles.user_id', '=', 'workers.user_id')
                ->where('role_id', '=', $role->id);
        })
        ->select('workers.*')
        ->get();
    }
}
