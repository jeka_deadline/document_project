<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentAdditionalFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('document_additional_fields', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('document_id')->unsigned();
            $table->foreign('document_id')
                ->references('id')
                ->on('documents')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->string('office', 255)->nullable();
            $table->string('proform_office', 255)->nullable();
            $table->string('proform_number', 255)->nullable();
            $table->string('proform_sum', 255)->nullable();
            $table->string('proform_date_confirm', 255)->nullable();
            $table->string('proform_volume', 255)->nullable();
            $table->string('am', 255)->nullable();
            $table->string('stalker_delivery', 255)->nullable();
            $table->string('stalker_arrival', 255)->nullable();
            $table->string('production_time', 255)->nullable();
            $table->string('volume', 255)->nullable();
            $table->string('weight', 255)->nullable();
            $table->string('count_package', 255)->nullable();
            $table->string('packing_list', 255)->nullable();
            $table->string('comming_stock_date', 255)->nullable();
            $table->string('comming_count_places', 255)->nullable();
            $table->string('consumption_stock_date', 255)->nullable();
            $table->string('consumption_count_places', 255)->nullable();
            $table->string('stock_remainder', 255)->nullable();
            $table->string('state_stock', 255)->nullable();
            $table->string('return_stock_date', 255)->nullable();
            $table->string('return_count_package', 255)->nullable();
            $table->string('state_return', 255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('document_additional_fields');
    }
}
