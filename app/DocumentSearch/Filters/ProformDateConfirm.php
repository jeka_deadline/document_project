<?php

namespace App\DocumentSearch\Filters;

use Illuminate\Database\Eloquent\Builder;

class ProformDateConfirm implements Filterable
{
    public static function apply(Builder $builder, $dateRange)
    {
        $dateRange = explode(' - ', $dateRange);

        if (isset($dateRange[ 0 ], $dateRange[ 1 ])) {
            return $builder->whereHas('additionalFields', function($query) use ($dateRange) {
                  return $query->whereBetween('proform_date_confirm', $dateRange);
            });
        }

        return $builder;
    }
}