<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CalculationSumRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sum' => 'required|numeric',
            'percentage_prepayment' => 'required_without_all:enter_eur,enter_usd,enter_rub|numeric|between:0,100.00|nullable',
            'enter_eur' => 'required_without_all:percentage_prepayment,enter_usd,enter_rub|numeric|nullable',
            'enter_usd' => 'required_without_all:enter_eur,percentage_prepayment,enter_rub|numeric|nullable',
            'enter_rub' => 'required_without_all:enter_eur,enter_usd,percentage_prepayment|numeric|nullable',
            'cross_currrency' => 'required|numeric|min:0.1',
        ];
    }
}
