<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Setting;

/**
 * News controller.
 */
class NewsController extends Controller
{
    /**
     * View page with news.
     *
     * @param \Illuminate\Http\Request $request Request
     * @return \Illuminate\Support\Facades\View|\Illuminate\Support\Facades\Redirect
     */
    public function index(Request $request)
    {
        $pin = $request->session()->get(Setting::KEY_ENTER_PIN);

        if ($pin) {
            return redirect(route('main'));
        }

        return view('news');
    }

    /**
     * Check enter pin from user fom.
     *
     * @param \Illuminate\Http\Request $request Request
     * @return \Illuminate\Support\Facades\Redirect
     */
    public function enterPin(Request $request)
    {
        $enterPin = Setting::enterPin()->firstOrFail();

        $userPin = $request->key;

        if ($userPin !== $enterPin->value) {
            return redirect(route('news'));
        }

        $request->session()->put(Setting::KEY_ENTER_PIN, $userPin);

        return redirect(route('main'));
    }
}
