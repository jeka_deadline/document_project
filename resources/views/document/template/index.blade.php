@extends('layouts.bootstrap')

@section('breadcrumbs')

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Главная</a></li>
            <li class="breadcrumb-item active" aria-current="page">Список шаблонов для документов</li>
        </ol>
    </nav>

@stop
@section('content')

    <div class="row">
          <div class="col-lg-12">

                <?php if (Auth::user()->isCanCreateDocumentTemplate()) : ?>

                    <a class="btn btn-success" href="{{ route('document.template.create') }}">Добавить шаблон</a>

                <?php endif; ?>

                <br><br>
                <table class="table">
                      <thead>
                            <tr>
                                  <th>Название</th>
                                  <th>Дата создания</th>
                                  <th>Операции</th>
                            </tr>
                      </thead>
                      <tbody>

                          @foreach ($documentTemplates as $template)

                              <tr>
                                  <td>{{ $template->name }}</td>
                                  <td>{{ $template->created_at }}</td>
                                  <td>

                                  <?php if (Auth::user()->isCanEditDocumentTemplate()) : ?>

                                      <a href="{{ route('document.template.edit', ['template' => $template]) }}"><i class="fa fa-edit fa-2x"></i></a>

                                  <?php endif; ?>
                                  <?php if (Auth::user()->isCanViewDocumentTemplate()) : ?>

                                      <a href="{{ route('document.template.view', ['template' => $template]) }}"><i class="fa fa-eye fa-2x"></i></a>

                                  <?php endif; ?>
                                  <?php if (Auth::user()->isCanDeleteDocumentTemplate()) : ?>

                                      <form class="delete-form" method="post" action="{{ route('document.template.delete', ['template' => $template]) }}" style="display: inline">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}

                                            <button style="color: #337ab7; border: none; background: none"><i class="fa fa-trash fa-2x"></i></button>
                                      </form>

                                  <?php endif; ?>

                              </td>
                              </tr>

                          @endforeach

                      </tbody>
                </table>

                {{ $documentTemplates->links() }}
          </div>
    </div>

@stop