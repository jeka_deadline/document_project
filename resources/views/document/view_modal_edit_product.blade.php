<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title">Редактирование товара</h4>
    <div class="row">
        <form action="{{ route('document.update.product', compact('product')) }}" method="post" id="attach-new-product">
            <div class="col-lg-12">
                  <div class="form-group">
                      <label for="">Название</label>
                      <input name="name" class="form-control" value="{{ $product->name }}">
                  </div>

                  <div class="form-group">
                      <label for="">Модель</label>
                      <input name="model" class="form-control" value="{{ $product->model }}">
                  </div>

                  <div class="form-group">
                      <label for="">Цвет</label>
                      <input name="color" class="form-control" value="{{ $product->color }}">
                  </div>

                  <div class="form-group">
                      <label for="">Количество</label>
                      <input name="count" class="form-control" value="{{ $product->count }}">
                  </div>

                  <div class="form-group">
                      <label for="">Количество упаковок</label>
                      <input name="count_packages" class="form-control" value="{{ $product->count_packages }}">
                  </div>

                  <div class="form-group">
                      <label for="">Цена</label>
                      <input name="price" class="form-control" value="{{ $product->price }}">
                  </div>

                  <div class="form-group">
                      <label for="">Фабрика</label>
                      <select name="factory_id" id="" class="form-control">

                          @foreach ($factories as $factory)

                              <option value="{{ $factory->id }}" {{ ($product->factory_id == $factory->id) ? 'selected' : '' }}>{{ $factory->name }}</option>

                          @endforeach

                      </select>
                  </div>

                  <div class="form-group">
                      <label for="">Месторасположение</label>
                      <select name="location" id="" class="form-control">

                          @foreach ($locations as $key => $label)

                              <option value="{{ $key }}" {{ ($product->key == $key) ? 'selected' : '' }}>{{ $label }}</option>

                          @endforeach

                      </select>
                  </div>

                  <div class="form-group">
                      <label for="">Статус</label>
                      <select name="status" id="" class="form-control">

                          @foreach ($statuses as $type => $label)

                              <option value="{{ $type }}" {{ ($product->status == $type) ? 'selected' : '' }}>{{ $label }}</option>

                          @endforeach

                      </select>
                  </div>

                  <div class="form-group">
                      <label for="">Комментарий</label>
                      <textarea name="comment" id="" class="form-control">{{ $product->comment }}</textarea>
                  </div>
            </div>
        </form>
    </div>
</div>
<div class="modal-footer">
    <button class="btn btn-primary" type="sumbit" form="attach-new-product">Обновить</button>
    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
</div>