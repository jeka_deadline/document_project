<?php

namespace App\Models\Document;

use Illuminate\Database\Eloquent\Model;
use App\Models\Document\Document;

/**
 * Service account model for table `service_accounts`
 */
class ServiceAccount extends Model
{
    /**
     * {@inheritdoc}
     *
     * @return array $fillable
     */
    protected $fillable = [
        'delivery',
        'mkad',
        'km_outer_mkad',
        'sum_delivery',
        'floor',
        'manual',
        'manual_large_sized',
        'on_lift',
        'sum_climb',
        'assembly',
        'sum_assembly',
        'removing_packaging',
        'type_payment',
        'date_assembly',
        'date_delivery',
    ];

    /**
     * Get organization.
     *
     * @return null|string
     */
    public function getOrganizationAttribute()
    {
        if (!$this->document) {
            return null;
        }

        return $this->document->organization;
    }

    /**
     * Get organization address.
     *
     * @return null|string
     */
    public function getOrganizationAddressAttribute()
    {
        if (!$this->document) {
            return null;
        }

        return $this->document->organizationAddress;
    }

    /**
     * Get organization phone.
     *
     * @return null|string
     */
    public function getOrganizationPhoneAttribute()
    {
        if (!$this->document) {
            return null;
        }

        return $this->document->organizationPhone;
    }

    /**
     * Get document register number.
     *
     * @return null|string
     */
    public function getDocumentRegisterNumberAttribute()
    {
        if (!$this->document) {
            return null;
        }

        return $this->document->register_number;
    }

    /**
     * Get client full name.
     *
     * @return null|string
     */
    public function getClientFullNameAttribute()
    {
        if (!$this->document) {
            return null;
        }

        return $this->document->clientFullName;
    }

    /**
     * Get client phones.
     *
     * @return null|string
     */
    public function getClientPhonesAttribute()
    {
        if (!$this->document) {
            return null;
        }

        return $this->document->clientPhones;
    }

    /**
     * Get client shipping address.
     *
     * @return null|string
     */
    public function getClientShippingAddressAttribute()
    {
        if (!$this->document) {
            return null;
        }

        return $this->document->clientShippingAddress;
    }

    /**
     * Get client contact full name.
     *
     * @return null|string
     */
    public function getClientContactFullNameAttribute()
    {
        if (!$this->document) {
            return null;
        }

        return $this->document->clientContactFullName;
    }

    /**
     * Get human type payment.
     *
     * @return null|string
     */
    public function getHumanTypePaymentAttribute()
    {
        switch ($this->type_payment) {
            case Document::PAYMENT_TYPE_CASH:
                return 'Наличный';
            case Document::PAYMENT_TYPE_NON_CASH:
                return 'Безналичный';
        }

        return null;
    }

    /**
     * Get finish sum in eur.
     *
     * @return null|string
     */
    public function getFinishSumEurAttribute()
    {
        if ($this->promotion_sum_eur) {
            return $this->full_sum_eur_with_promotion;
        }

        return $this->full_sum_eur;
    }

    /**
     * Get finish sum in rub.
     *
     * @return null|string
     */
    public function getFinishSumRubAttribute()
    {
        if ($this->promotion_sum_eur) {
            return $this->full_sum_rub_with_promotion;
        }

        return $this->full_sum_rub;
    }

    /**
     * Relation with document.
     *
     * @return
     */
    public function document()
    {
        return $this->belongsTo('App\Models\Document\Document');
    }
}
