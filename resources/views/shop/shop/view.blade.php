@extends('layouts.bootstrap')

@section('breadcrumbs')

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Главная</a></li>
            <li class="breadcrumb-item"><a href="{{ route('shop.index') }}">Список магазинов</a></li>
            <li class="breadcrumb-item active" aria-current="page">Просмотр магазина</li>
        </ol>
    </nav>

@stop
@section('content')

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <table style="width: 100%">
                        <tr>
                            <td>
                                <h4>Информация об магазине</h4>
                            </td>

                            <?php if (Auth::user()->isCanEditShop()) : ?>

                                <td style="text-align: right"><a class="btn btn-warning" style="font-size: 14px" href="{{ route('shop.edit', ['shop' => $shop]) }}">Редактировать</a></td>

                            <?php endif; ?>

                        </tr>
                    </table>
                </div>
                <div class="panel-body">
                    <div class="box box-info">
                        <div class="box-body">
                            <div class="col-lg-3">
                                <div  align="center">
                                    <img alt="User Pic" src="/images/Store_icon.png" id="profile-image1" class="img-responsive">
                                </div>
                                <br>
                                <!-- /input-group -->
                            </div>
                            <div class="col-lg-3">
                                <div>
                                    <h3 style="color:#00b1b1;">Генеральный директор</h3>
                                    <span>
                                        <p>{{ (!is_null($shop->general_director)) ? $shop->general_director->full_name : '' }}</p>
                                    </span>
                                </div>
                                <div>
                                    <h4 style="color:#00b1b1;">Директор магазина</h4>
                                    <span>
                                        <p>{{ (!is_null($shop->shop_director)) ? $shop->shop_director->full_name : '' }}</p>
                                    </span>
                                </div>
                                <div>
                                    <h5 style="color:#00b1b1;">Заместители директора магазина</h5>
                                    <span>

                                        @foreach ($shop->shop_deputy_directors as $shopDeputyDirector )

                                            <p>{{ $shopDeputyDirector->full_name }}</p>

                                        @endforeach

                                    </span>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <table class="table">
                                      <tr>
                                          <td>Наименование полное:</td>
                                          <td>{{ $shop->full_name }}</td>
                                      </tr>
                                      <tr>
                                          <td>Наименование короткое:</td>
                                          <td>{{ $shop->short_name }}</td>
                                      </tr>
                                      <tr>
                                          <td>Юр. адрес:</td>
                                          <td>{{ $shop->address }}</td>
                                      </tr>
                                      <tr>
                                          <td>Телефон:</td>
                                          <td>{{ $shop->phone }}</td>
                                      </tr>
                                      <tr>
                                          <td>ОГРН:</td>
                                          <td>{{ $shop->ogrn }}</td>
                                      </tr>
                                      <tr>
                                          <td>ИНН:</td>
                                          <td>{{ $shop->inn }}</td>
                                      </tr>
                                      <tr>
                                          <td>КПП:</td>
                                          <td>{{ $shop->kpp }}</td>
                                      </tr>
                                      <tr>
                                          <td>Банк:</td>
                                          <td>{{ $shop->bank }}</td>
                                      </tr>
                                      <tr>
                                          <td>БИК:</td>
                                          <td>{{ $shop->bik }}</td>
                                      </tr>
                                      <tr>
                                          <td>р/с:</td>
                                          <td>{{ $shop->checking_account }}</td>
                                      </tr>
                                </table>
                            </div>
                        </div>
                        <!-- /.box -->
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop