<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AttachProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'model' => 'required|string|max:255',
            'color' => 'required|string|max:100',
            'count' => 'required|integer|min:1',
            'count_packages' => 'required|integer|min:0',
            'price' => 'required|numeric',
            'factory_id' => 'required|exists:factories,id',
            'location' => 'required',
            'status' => 'required',
            'comment' => 'string|nullable',
        ];
    }
}
