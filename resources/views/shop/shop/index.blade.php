@extends('layouts.bootstrap')

@section('breadcrumbs')

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Главная</a></li>
            <li class="breadcrumb-item active" aria-current="page">Список магазинов</li>
        </ol>
    </nav>

@stop

@section('content')

    <div class="row">
          <div class="col-lg-12">

                <?php if (Auth::user()->isCanCreateShop()) : ?>

                    <a class="btn btn-success" href="{{ route('shop.create') }}">Добавить магазин</a>

                <?php endif; ?>
                <br><br>
                <table class="table">
                      <thead>
                            <tr>
                                  <th>Полное название</th>
                                  <th>Адрес</th>
                                  <th>Директор магазина</th>
                                  <th>Операции</th>
                            </tr>
                      </thead>
                      <tbody>

                          @foreach ($shops as $shop)

                              <tr>
                                  <td>
                                      {{ $shop->full_name }}
                                  </td>
                                  <td>
                                      {{ $shop->address }}
                                  </td>
                                  <td>{{ (!is_null($shop->general_director)) ? $shop->general_director->full_name : '' }}</td>
                                  <td>
                                      <?php if (Auth::user()->isCanEditShop()) : ?>

                                          <a href="{{ route('shop.edit', ['shop' => $shop]) }}"><i class="fa fa-edit fa-2x"></i></a>

                                      <?php endif; ?>
                                      <?php if (Auth::user()->isCanViewShop()) : ?>

                                          <a href="{{ route('shop.view', ['shop' => $shop]) }}"><i class="fa fa-eye fa-2x"></i></a>

                                      <?php endif; ?>
                                      <?php if (Auth::user()->isCanDeleteShop()) : ?>

                                          <form class="delete-form" method="post" action="{{ route('shop.delete', ['shop' => $shop]) }}" style="display: inline">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}

                                                <button style="color: #337ab7; border: none; background: none"><i class="fa fa-trash fa-2x"></i></button>
                                          </form>

                                      <?php endif; ?>
                                  </td>
                              </tr>

                          @endforeach

                      </tbody>
                </table>

                {{ $shops->links() }}
          </div>
    </div>

@stop