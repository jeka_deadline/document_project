@extends('layouts.bootstrap')

@section('breadcrumbs')

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Главная</a></li>
            <li class="breadcrumb-item active" aria-current="page">Клиенты: физические лица</li>
        </ol>
    </nav>

@stop
@section('content')

    <div class="row">
          <div class="col-lg-12">

                <?php if (Auth::user()->isCanCreateIndividualClient()) : ?>

                    <a class="btn btn-success" href="{{ route('individual.client.create') }}">Добавить клиента</a>

                <?php endif; ?>

                <br><br>
                <table class="table">
                      <thead>
                            <tr>
                                  <th>Фамилия</th>
                                  <th>Имя</th>
                                  <th>Отчество</th>
                                  <th>Email</th>
                                  <th>Телефоны</th>
                                  <th>Операции</th>
                            </tr>
                      </thead>
                      <tbody>

                          @foreach ($clients as $client)

                              <tr>
                                  <td>
                                      {{ $client->surname }}
                                  </td>
                                  <td>
                                      {{ $client->name }}
                                  </td>
                                  <td>
                                      {{ $client->lastname }}
                                  </td>
                                  <td>
                                      {{ $client->email }}
                                  </td>
                                  <td>

                                      @foreach ($client->phones as $phone)

                                          {{ $phone }}

                                          <br>

                                      @endforeach

                                  </td>
                                  <td>
                                      <?php if (Auth::user()->isCanEditIndividualClient()) : ?>

                                          <a href="{{ route('individual.client.edit', ['client' => $client]) }}"><i class="fa fa-edit fa-2x"></i></a>

                                      <?php endif; ?>
                                      <?php if (Auth::user()->isCanViewIndividualClient()) : ?>

                                          <a href="{{ route('individual.client.view', ['client' => $client]) }}"><i class="fa fa-eye fa-2x"></i></a>

                                      <?php endif; ?>
                                      <?php if (Auth::user()->isCanDeleteIndividualClient()) : ?>

                                          <form class="delete-form" method="post" action="{{ route('individual.client.delete', ['client' => $client]) }}" style="display: inline">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}

                                                <button style="color: #337ab7; border: none; background: none"><i class="fa fa-trash fa-2x"></i></button>
                                          </form>

                                      <?php endif; ?>

                                  </td>
                              </tr>

                          @endforeach

                      </tbody>
                </table>

                {{ $clients->links() }}
          </div>
    </div>

@stop