<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title">Добавление нового товара</h4>
    <div class="row">
        <form action="{{ route('document.attach.product', compact('document')) }}" method="post" id="attach-new-product">
            <div class="col-lg-12">
                  <div class="form-group">
                      <label for="">Название</label>
                      <input name="name" class="form-control">
                  </div>

                  <div class="form-group">
                      <label for="">Модель</label>
                      <input name="model" class="form-control">
                  </div>

                  <div class="form-group">
                      <label for="">Цвет</label>
                      <input name="color" class="form-control">
                  </div>

                  <div class="form-group">
                      <label for="">Количество</label>
                      <input name="count" class="form-control">
                  </div>

                  <div class="form-group">
                      <label for="">Количество упаковок</label>
                      <input name="count_packages" class="form-control">
                  </div>

                  <div class="form-group">
                      <label for="">Цена</label>
                      <input name="price" class="form-control">
                  </div>

                  <div class="form-group">
                      <label for="">Фабрика</label>
                      <select name="factory_id" id="" class="form-control">

                          @foreach ($factories as $factory)

                              <option value="{{ $factory->id }}">{{ $factory->name }}</option>

                          @endforeach

                      </select>
                  </div>

                  <div class="form-group">
                      <label for="">Месторасположение</label>
                      <select name="location" id="" class="form-control">

                          @foreach ($locations as $key => $label)

                              <option value="{{ $key }}">{{ $label }}</option>

                          @endforeach

                      </select>
                  </div>

                  <div class="form-group">
                      <label for="">Статус</label>
                      <select name="status" id="" class="form-control">

                          @foreach ($statuses as $type => $label)

                              <option value="{{ $type }}">{{ $label }}</option>

                          @endforeach

                      </select>
                  </div>

                  <div class="form-group">
                      <label for="">Комментарий</label>
                      <textarea name="comment" id="" class="form-control">

                      </textarea>
                  </div>
            </div>
        </form>
    </div>
</div>
<div class="modal-footer">
    <button class="btn btn-success" type="sumbit" form="attach-new-product">Добавить</button>
    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
</div>