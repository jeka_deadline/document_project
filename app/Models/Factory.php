<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Model for table `factories`
 */
class Factory extends Model
{
    /**
     * {@inheritdoc}
     *
     * @access protected
     * @var array $fillable
     */
    protected $fillable = [
        'name', 'country', 'description',
    ];
}
