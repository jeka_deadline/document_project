@extends('layouts.bootstrap')

@section('css')

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css">

@stop
@section('breadcrumbs')

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Главная</a></li>
            <li class="breadcrumb-item"><a href="{{ route('shop.index') }}">Список магазинов</a></li>
            <li class="breadcrumb-item active" aria-current="page">Добавление магазина</li>
        </ol>
    </nav>

@stop

@section('content')

    <div class="row">
        <div class="col-lg-12">
              <h2>Добавление магазина</h2>
              <form method="post" action="{{ route('shop.store') }}">
                  {{ csrf_field() }}

                  <div class="form-group">
                      <label class="control-label" for="">Генеральный директор</label>
                      <select class="select2 form-control" name="general_director_id">

                          @foreach ($listGeneralDirectors as $worker)

                              <option value="{{ $worker->id }}" {{ (old('general_director_id') == $worker->id) ? 'selected' : '' }}>
                                {{ $worker->full_name }}
                              </option>

                          @endforeach

                      </select>

                      @if ($errors->has('general_director_id'))

                          <div class="text-danger">

                                {{ $errors->first('general_director_id') }}

                          </div>

                      @endif

                  </div>

                  <div class="form-group">
                      <label class="control-label" for="">Директор магазина</label>
                      <select class="select2 form-control" name="shop_director_id">

                          @foreach ($listShopDirectors as $worker)

                              <option value="{{ $worker->id }}" {{ (old('shop_director_id') == $worker->id) ? 'selected' : '' }}>
                                {{ $worker->full_name }}
                              </option>

                          @endforeach

                      </select>

                      @if ($errors->has('shop_director_id'))

                          <div class="text-danger">

                                {{ $errors->first('shop_director_id') }}

                          </div>

                      @endif

                  </div>

                  <div class="form-group">
                      <label class="control-label" for="">Заместители директора магазина</label>
                      <select class="select2 form-control" name="deputy_shop_director_id[]" multiple>

                          @foreach ($listDeputyShopDirectors as $worker)

                              <option value="{{ $worker->id }}" {{ (old('deputy_shop_director_id') == $worker->id) ? 'selected' : '' }}>
                                {{ $worker->full_name }}
                              </option>

                          @endforeach

                      </select>

                      @if ($errors->has('deputy_shop_director_id'))

                          <div class="text-danger">

                                {{ $errors->first('deputy_shop_director_id') }}

                          </div>

                      @endif

                  </div>

                  <div class="form-group">
                      <label class="control-label" for="">Наименование полное</label>
                      <input class="form-control" type="text" name="full_name" value="{{ old('full_name') }}">

                      @if ($errors->has('full_name'))

                          <div class="text-danger">

                                {{ $errors->first('full_name') }}

                          </div>

                      @endif

                  </div>

                  <div class="form-group">
                      <label class="control-label" for="">Наименование короткое</label>
                      <input class="form-control" type="text" name="short_name" value="{{ old('short_name') }}">

                      @if ($errors->has('short_name'))

                          <div class="text-danger">

                                {{ $errors->first('short_name') }}

                          </div>

                      @endif

                  </div>

                  <div class="form-group">
                      <label class="control-label" for="">Юр. адрес</label>
                      <textarea class="form-control" rows="6" type="text" name="address">{{ old('address') }}</textarea>

                      @if ($errors->has('address'))

                          <div class="text-danger">

                                {{ $errors->first('address') }}

                          </div>

                      @endif

                  </div>

                  <div class="form-group">
                      <label class="control-label" for="">Телефон</label>
                      <input class="form-control phone-mask" type="text" name="phone" value="{{ old('phone') }}">

                      @if ($errors->has('phone'))

                          <div class="text-danger">

                                {{ $errors->first('phone') }}

                          </div>

                      @endif

                  </div>

                  <div class="form-group">
                      <label class="control-label" for="">ОГРН</label>
                      <input class="form-control ogrn-mask" type="text" name="ogrn" value="{{ old('ogrn') }}">

                      @if ($errors->has('ogrn'))

                          <div class="text-danger">

                                {{ $errors->first('ogrn') }}

                          </div>

                      @endif

                  </div>

                  <div class="form-group">
                      <label class="control-label" for="">ИНН</label>
                      <input class="form-control inn-mask" type="text" name="inn" value="{{ old('inn') }}">

                      @if ($errors->has('inn'))

                          <div class="text-danger">

                                {{ $errors->first('inn') }}

                          </div>

                      @endif

                  </div>

                  <div class="form-group">
                      <label class="control-label" for="">КПП</label>
                      <input class="form-control kpp-mask" type="text" name="kpp" value="{{ old('kpp') }}">

                      @if ($errors->has('kpp'))

                          <div class="text-danger">

                                {{ $errors->first('kpp') }}

                          </div>

                      @endif

                  </div>

                  <div class="form-group">
                      <label class="control-label" for="">Банк</label>
                      <input class="form-control" type="text" name="bank" value="{{ old('bank') }}">

                      @if ($errors->has('bank'))

                          <div class="text-danger">

                                {{ $errors->first('bank') }}

                          </div>

                      @endif

                  </div>

                  <div class="form-group">
                      <label class="control-label" for="">БИК</label>
                      <input class="form-control bik-mask" type="text" name="bik" value="{{ old('bik') }}">

                      @if ($errors->has('bik'))

                          <div class="text-danger">

                                {{ $errors->first('bik') }}

                          </div>

                      @endif

                  </div>

                  <div class="form-group">
                      <label class="control-label" for="">р/с</label>
                      <input class="form-control" type="text" name="checking_account" value="{{ old('checking_account') }}">

                      @if ($errors->has('checking_account'))

                          <div class="text-danger">

                                {{ $errors->first('checking_account') }}

                          </div>

                      @endif

                  </div>

                  <div class="checkbox">
                      <label for=""><input type="checkbox" name="default" {{ (old('default')) ? 'checked' : '' }}>Магазин по умолчанию</label>
                  </div>

                  <input type="submit" value="Добавить" class="btn btn-success">
                  <a class="btn btn-info" href="{{ route('shop.index') }}">Назад</a>
              </form>
        </div>
    </div>

@stop