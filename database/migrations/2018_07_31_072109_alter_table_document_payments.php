<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableDocumentPayments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('document_prepayments', function (Blueprint $table) {
            $table->float('percentage_currrency')->after('cross_currrency');
            $table->string('type_payment', 20)->after('percentage_currrency');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('document_prepayments', function (Blueprint $table) {
            $table->dropColumn('percentage_currrency');
            $table->dropColumn('type_payment');
        });
    }
}
