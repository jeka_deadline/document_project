<?php

namespace App\Models\Shop;

use Illuminate\Database\Eloquent\Model;
use App\Models\User\Role;
use App\Models\Shop\ShopUserLeader;

/**
 * Model class for table `shops`.
 */
class Shop extends Model
{
    /**
     * {@inheritdoc}
     *
     * @access protected
     * @var array $fillable
     */
    protected $fillable = [
        'full_name',
        'short_name',
        'address',
        'phone',
        'ogrn',
        'inn',
        'kpp',
        'bank',
        'bik',
        'checking_account',
        'default',
    ];

    /**
     * Get general director for shop.
     *
     * @return null|\App\Models\UserWorker
     */
    public function getGeneralDirectorAttribute()
    {
        $roleGeneralDirector = Role::director()->first();

        $workers = $this->getWorkersByRoleId($roleGeneralDirector->id);

        if (isset($workers[ 0 ])) {
            return $workers[ 0 ];
        }

        return null;
    }

    /**
     * Get worker for this shop with role shop director.
     *
     * @return null|\App\Models\UserWorker
     */
    public function getShopDirectorAttribute()
    {
        $roleShopDirector = Role::shopDirector()->first();

        $workers = $this->getWorkersByRoleId($roleShopDirector->id);

        if (isset($workers[ 0 ])) {
            return $workers[ 0 ];
        }

        return null;
    }

    /**
     * Get workers for this shop with role deputy shop director.
     *
     * @return null|\App\Models\UserWorker[]
     */
    public function getShopDeputyDirectorsAttribute()
    {
        $roleShopDeputyDirector = Role::shopDeputyDirector()->first();

        $workers = $this->getWorkersByRoleId($roleShopDeputyDirector->id);

        if (!empty($workers)) {
            return $workers;
        }

        return null;
    }

    /**
     * Get list workers by role.
     *
     * @param int $roleId Role id.
     * @access private
     * @return
     */
    private function getWorkersByRoleId($roleId)
    {
       return ShopUserLeader::where([
            ['role_id', '=', $roleId],
            ['shop_id', '=', $this->id]
        ])
        ->with('worker')
        ->get()
        ->pluck('worker');
    }
}
