<table data-document-id="{{ $document->id }}" class="list-factory-paymnets">

    @foreach ($document->factoryPayments as $factoryPayment)

        <tr class="edit-factory-payment" data-url="{{ route('document.factory.payment.edit', compact('factoryPayment')) }}">
            <td><p style="width: 116px">{{ ($factoryPayment->date_dispatch) ? $factoryPayment->date_dispatch->format('d.m.Y') : '' }}</p></td> <!-- Отправка счета на ф-ку -->
            <td><p style="width: 116px">{{ ($factoryPayment->date_payment) ? $factoryPayment->date_payment->format('d.m.Y') : '' }}</p></td> <!-- Дата -->
            <td><p style="width: 116px">{{ $factoryPayment->invoice }}</p></td> <!-- Инвойс -->
            <td><p style="width: 116px">{{ $factoryPayment->sum }}</p></td> <!-- Сумма -->
            <td><p style="width: 116px">{{ $factoryPayment->bank_transfer }}</p></td> <!-- Банк. перевод -->
        </tr>

    @endforeach

</table>