<?php

namespace App\Services;

use DateTime;
use App\Models\ProjectAction;
use App\Models\ProjectActionRole;

/**
 * Admin service.
 */
class AdminService
{
    /**
     * Create or update holiday structired.
     *
     * @return void
     */
    public function updateCalendarStructure()
    {
        $calendarDataFile = storage_path('calendar/data.csv');
        $currentYear = date('Y');

        if (!file_exists($calendarDataFile)) {
            return false;
        }

        $calendarData = $this->getCalendarDataFromFile($calendarDataFile);

        if (empty($calendarData)) {
            return false;
        }

        $savePath = storage_path("calendar/years");

        if (!file_exists($savePath) || !is_dir($savePath)) {
            mkdir($savePath, 0777, true);
        }

        foreach ($calendarData as $index => $line) {
            if (!$index) {
                continue;
            }

            $year = substr($line, 0, 4);

            if ($year < $currentYear) {
                continue;
            }

            $structuredHolidays = $this->buildHolidaysByYear($year, $line);

            if (is_null($structuredHolidays)) {
                continue;
            }

            file_put_contents(storage_path("calendar/years/{$year}.php"), "<?php return\r\n" . var_export($structuredHolidays, true) . ';');
        }
    }

    /**
     * Get all actions for project.
     *
     * @return \App\Models\ProjectAction[]
     */
    public function getListProjectActions()
    {
        return ProjectAction::all();
    }

    /**
     * Get list project actions for role.
     *
     * @param int $roleId Role id.
     * @return array
     */
    public function getListProjectActionsByRoleId($roleId)
    {
        return ProjectAction::getListProjectActionsByRoleId($roleId);
    }

    /**
     * Update role for user.
     *
     * @param int $roleId Role id.
     * @param array $actions List actions for role.
     * @return void
     */
    public function updateRole($roleId, $actions)
    {
        $currentRoleActions = $this->getListProjectActionsByRoleId($roleId);
        $actions = ProjectAction::whereIn('id', array_keys($actions))->get();

        foreach ($actions as $action) {
            if (isset($currentRoleActions[ $action->id ])) {
                unset($currentRoleActions[ $action->id ]);
                continue;
            }

            ProjectActionRole::create([
                'role_id' => $roleId,
                'project_action_id' => $action->id,
            ]);
        }

        foreach ($currentRoleActions as $model) {
            $model->delete();
        }
    }

    /**
     * Get csv holidays content from file.
     *
     * @access private
     * @param string $fileName File name.
     * @return string[]
     */
    private function getCalendarDataFromFile($fileName)
    {
        $calendarData = [];

        if ($file = fopen($fileName, 'r')) {
            while(!feof($file)) {
                $calendarData[] = fgets($file);
            }

            fclose($file);
        }

        return $calendarData;
    }

    /**
     * Build holiday structured by year.
     *
     * @access private
     * @param int $year Year
     * @param string $line Line holiday by year.
     * @return null|array
     */
    private function buildHolidaysByYear($year, $line)
    {
        preg_match_all('#"(.*?)"#', $line, $holidayDaysByMonth);

        if (!isset($holidayDaysByMonth[ 1 ]) || empty($holidayDaysByMonth[ 1 ])) {
            return null;
        }

        $structuredHolidays = [];

        foreach ($holidayDaysByMonth[ 1 ] as $indexMonth => $holidayDaysString) {
            $holidayDays = explode(',', $holidayDaysString);

            foreach ($holidayDays as $day) {
                if (strpos($day, '*')) {
                    continue;
                }
                if (strpos($day, '+')) {
                    $day = str_replace('+', '', $day);
                }

                $holidayDate = sprintf('%s-%s-%s', $year, str_pad($indexMonth + 1, 2, 0, STR_PAD_LEFT), str_pad($day, 2, 0, STR_PAD_LEFT));

                $structuredHolidays[ $holidayDate ] = $holidayDate;
            }
        }

        return $structuredHolidays;
    }
}
