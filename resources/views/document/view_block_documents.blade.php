<table id="block-documents" class="table">
    <thead>
        <tr>
            <th>Название</th>
            <th>Дата</th>

            <?php if (Auth::user()->isAdmin()) : ?>

                <th>Операции</th>

            <?php endif; ?>

        </tr>
    </thead>
    <tbody>

        @foreach ($templates as $template)

            <tr>
                <td><a href="{{ route('document.template.with.document', compact('document', 'template')) }}" target="_blank">{{ $template->name }}</a></td>
                <td>{{ $template->created_at->format('d.m.Y H:i:s') }}</td>

                <?php if (Auth::user()->isAdmin()) : ?>

                    <td>
                        <form class="delete-form" method="post" action="{{ route('document.template.delete.relation.with.document', ['document' => $document, 'template' => $template]) }}" style="display: inline">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}

                            <button style="color: #337ab7; border: none; background: none"><i class="fa fa-trash fa-2x"></i></button>
                        </form>
                    </td>

                <?php endif; ?>
            </tr>

        @endforeach

    </tbody>
</table>