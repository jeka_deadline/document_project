<div class="modal fade" tabindex="-1" id="modal-new-payment" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Новая доплата</h4>
            </div>
            <form action="{{ route('document.new.payment', compact('document')) }}" data-calculation-url="{{ route('document.calculation.surcharge', compact('document')) }}" method="post">
                <div class="container-fluid">
                    <div class="row">
                          <div class="col-lg-3">
                              <div class="form-group">
                                  <label for="">Сумма договора</label>
                                  <input type="text" class="form-control" name="sum" value="{{ $document->sum }}" readonly>
                              </div>
                          </div>
                          <div class="col-lg-3">
                              <div class="form-group">
                                  <label for="">Внесенная сумма</label>
                                  <input type="text" class="form-control" name="enter_sum_eur" value="{{ $document->sumAllPayments }}" readonly>
                              </div>
                          </div>
                          <div class="col-lg-3">
                              <div class="form-group">
                                  <label for="">Предоплата %</label>
                                  <input type="text" class="form-control" name="percentage_prepayment" value="{{ $document->percentageAllPayments }}" readonly>
                              </div>
                          </div>
                          <div class="col-lg-3">
                              <div class="form-group">
                                  <label for="">Осталось внести EUR</label>
                                  <input type="text" class="form-control" name="remainder_eur" value="{{ $document->sum - $document->sumAllPayments }}" readonly>
                              </div>
                          </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-5">
                            <div class="form-group">
                                <label for="">Кросс курс</label>
                                <input type="text" class="form-control" value="{{ old('cross_currrency' , $currencies->cross_currrency) }}" name="cross_currrency">
                            </div>
                            <div class="form-group">
                                <label for="">Процент</label>
                                <input type="text" class="form-control" value="{{ old('percentage_currrency' , $document->percentage_currrency) }}" name="percentage_currrency">
                            </div>
                            <div class="form-group">
                                <label for="">Тип оплаты</label>
                                <select name="type_payment" id="" class="form-control">

                                    @foreach($typePayments as $key => $label)

                                        <option value="{{ $key }}">{{ $label }}</option>

                                    @endforeach

                                </select>
                            </div>
                        </div>
                        <div class="col-lg-7">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>EUR</th>
                                        <th>USD</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <div class="form-group">
                                                <label for="">Курс ЦБ</label>
                                                <input type="text" class="form-control" value="{{ $currencies->eur }}" disabled>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <label for="">Курс ЦБ</label>
                                                <input type="text" class="form-control" value="{{ $currencies->usd }}" disabled>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div class="form-group">
                                                <label for="">Курс ЦБ + %</label>
                                                <input type="text" class="form-control" value="{{ $currencies->eur_markup }}" disabled>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group">
                                                <label for="">Курс ЦБ + %</label>
                                                <input type="text" class="form-control" value="{{ $currencies->usd_markup }}" disabled>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <h3 style="text-align: center">Доплата</h3>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="">Евро</label>
                                <input type="text" class="form-control" name="enter_eur">
                                <div class="help-block">{{ $document->sum - $document->sumAllPayments }}</div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="">Доллары</label>
                                <input type="text" class="form-control" name="enter_usd">
                                <div class="help-block">{{ ($document->sum - $document->sumAllPayments) * old('cross_currrency' , $currencies->cross_currrency) }}</div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="">Рубли</label>
                                <input type="text" class="form-control" name="enter_rub">
                                <div class="help-block">{{ ($document->sum - $document->sumAllPayments) * $currencies->eur_markup }}</div>
                            </div>
                        </div>
                        <div style="text-align: center">
                            <a href="#" id="calculation-sum-surcharge" class="btn btn-warning">Расчитать</a>
                        </div>
                    </div>
                    <div class="row">
                        <h3 style="text-align: center">Вносимая сумма</h3>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="">Евро</label>
                                <input type="text" class="form-control" name="prepayment_amount_eur" readonly>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="">Рубли</label>
                                <input type="text" class="form-control" name="prepayment_amount_rub" readonly>
                            </div>
                        </div>
                        <br>
                    </div>
                </div>
            </form>
            <div class="modal-footer">
                <a href="#" id="modal-new-payment-submit" class="btn btn-info">Внести</a>
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
