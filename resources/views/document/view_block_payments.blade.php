<div id="block-payments" class="panel panel-primary">
    <div class="panel-body">
          <table class="table">
              <thead>
                  <tr>
                      <th>Оплата</th>
                      <th>Дата</th>
                      <th>€</th>
                      <th>$</th>
                      <th>₽</th>
                      <th>Курс ЦБ / цб + %</th>
                      <th>Крос курс</th>
                      <th>Сумма €</th>
                  </tr>
              </thead>
              <tbody>

                  @foreach ($documentPayments as $payment)

                      <tr>
                          <td>{{ $loop->iteration }}</td>
                          <td>{{ $payment->created_at->format('d.m.Y H:i:s') }}</td>
                          <td>{{ $payment->enter_eur }}</td>
                          <td>{{ $payment->enter_usd }}</td>
                          <td>{{ $payment->enter_rub }}</td>
                          <td>{{ $payment->course_cb_eur }} / {{ $payment->course_cb_eur_markup }}</td>
                          <td>{{ $payment->cross_currrency }}</td>
                          <td>{{ $payment->prepayment_amount_eur }}</td>
                      </tr>

                  @endforeach

              </tbody>
          </table>

          @if ($document->sum != $document->sumAllPayments)

              <div style="text-align: center;">
                  <a href="#" class="btn btn-info" id="btn-modal-new-payment">Новая оплата</a>
              </div>

          @endif

    </div>
</div>
