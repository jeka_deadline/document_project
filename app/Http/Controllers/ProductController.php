<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\AttachProductRequest;
use App\Services\FactoryService;
use App\Services\ProductService;
use App\Models\Document\Document;
use App\Models\Product;
use Illuminate\Support\Facades\Auth;

/**
 * Product controller.
 */
class ProductController extends Controller
{
    /**
     * Document service.
     *
     * @access private
     * @var \App\Services\FactoryService $factoryService
     */
    private $factoryService;

    /**
     * Product service.
     *
     * @access private
     * @var \App\Services\ProductService $productService
     */
    private $productService;

    /**
     * Controller constructor.
     *
     * @param \App\Services\FactoryService $documentService Document service
     * @param \App\Services\ProductService $productService Product service
     * @return void
     */
    public function __construct(FactoryService $factoryService, ProductService $productService)
    {
        $this->factoryService = $factoryService;
        $this->productService = $productService;
    }

    /**
     * Get attach form to document.
     *
     * @param \Illuminate\Http\Request $request Request.
     * @param \App\Models\Document\Document $document Document model.
     * @return string JSON response with modal content.
     */
    public function getAttachProductForm(Request $request, Document $document)
    {
        if (!Auth::user()->isCanAttachDocumentProduct()) {
            $this->accessDanied();
        }

        return response()->json([
            'content' => view('document/view_modal_new_product', [
                'document' => $document,
                'factories' => $this->factoryService->getListFactories(),
                'statuses' => $this->productService->getListProductStatuses($document->name),
                'locations' => $this->productService->getListProductLocations(),
            ])->render(),
        ]);
    }

    /**
     * Attach product to document.
     *
     * @param \App\Http\Requests\AttachProductRequest $request User request.
     * @param \App\Models\Document\Document $document Document model.
     * @return string JSON response with modal content.
     */
    public function attachProductToDocument(AttachProductRequest $request, Document $document)
    {
        if (!Auth::user()->isCanAttachDocumentProduct()) {
            $this->accessDanied();
        }

        $product = $this->productService->attachProductToDocument($document, $request->all());

        $response = [
            'status' => false,
        ];

        if ($product) {
            $response[ 'status' ] = true;
        }

        $response[ 'content' ] = view('document/view_block_products', compact('document'))->render();

        return response()->json($response);
    }

    /**
     * Get edit product form.
     *
     * @param \Illuminate\Http\Request $request Request.
     * @param \App\Models\Product $product Product model.
     * @return string JSON response with modal content.
     */
    public function getEditProductForm(Request $request, Product $product)
    {
        if (!Auth::user()->isCanEditAttachDocumentProduct()) {
            $this->accessDanied();
        }

        return response()->json([
            'content' => view('document/view_modal_edit_product', [
                'product' => $product,
                'factories' => $this->factoryService->getListFactories(),
                'statuses' => $this->productService->getListProductStatuses($product->document->name),
                'locations' => $this->productService->getListProductLocations(),
            ])->render(),
        ]);
    }

    /**
     * Update product.
     *
     * @param \App\Http\Requests\AttachProductRequest $request User request.
     * @param \App\Models\Product $product Product model.
     * @return string JSON response with modal content.
     */
    public function updateProduct(AttachProductRequest $request, Product $product)
    {
        if (!Auth::user()->isCanEditAttachDocumentProduct()) {
            $this->accessDanied();
        }

        $result = $this->productService->updateProduct($product, $request->all());

        $response = [
            'status' => false,
        ];

        if ($result) {
            $response[ 'status' ] = true;
        }

        $response[ 'content' ] = view('document/view_block_products', [
            'document' => Document::where('id', $product->document_id)->first(),
        ])->render();

        return response()->json($response);
    }

    /**
     * Delete product.
     *
     * @param \Illuminate\Http\Request $request Request.
     * @param \App\Models\Product $product Product model.
     * @return string JSON response.
     */
    public function delete(Request $request, Product $product)
    {
        if (!Auth::user()->isCanDeleteAttachDocumentProduct()) {
            $this->accessDanied();
        }

        $response = [
            'status' => false,
        ];


        if ($product->delete()) {
            $response[ 'status' ] = true;
        }

        return response()->json($response);
    }
}
