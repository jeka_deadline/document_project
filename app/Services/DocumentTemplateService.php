<?php

namespace App\Services;

use DateTime;
use App\Models\Document\DocumentTemplate;
use App\Models\Document\Document;
use App\Models\Document\DocumentTemplateRelation;
use App\Models\Client\LegalClient;
use App\Libs\Petrovich\Petrovich;
use App\Models\User\Worker;

/**
 * Document template service
 */
class DocumentTemplateService
{
    /**
     * Get list document templates with pagination.
     *
     * @param int $countOnPage Count items on page
     * @return
     */
    public function paginate($countOnPage = 15)
    {
        return DocumentTemplate::paginate($countOnPage);
    }

    /**
     * Create new document template.
     *
     * @param array $fields Document template fields
     * @return \App\Models\Document\DocumentTemplate
     */
    public function createNewTemplate($fields)
    {
        return DocumentTemplate::create($fields);
    }

    /**
     * Update document template.
     *
     * @param \App\Models\Document\DocumentTemplate $template Document template model
     * @param array $field Document template fields
     * @return void
     */
    public function updateTemplate(DocumentTemplate $template, $fields)
    {
        $template->update($fields);
    }

    /**
     * Get list document types.
     *
     * @return array
     */
    public function getDocumentTemplateTypes()
    {
        return DocumentTemplate::getTypes();
    }

    /**
     * Delete relation between document and document template.
     *
     * @param App\Models\Document\Document $document Document model.
     * @param App\Models\Document\DocumentTemplate $template Document template model.
     * @return void
     */
    public function deleteRelationTemplateAndDocument(Document $document, DocumentTemplate $template)
    {
        $attached = DocumentTemplateRelation::where([
            'document_id' => $document->id,
            'document_template_id' => $template->id
        ])->firstOrFail();

        $attached->delete();
    }

    /**
     * Generate page document templates with document.
     *
     * @param \App\Models\Document\Document $document Document model.
     * @param \App\Models\Document\DocumentTemplate $documentTemplate Document template model.
     * @return string
     */
    public function generateTemplatePage(Document $document, DocumentTemplate $documentTemplate)
    {
        $relation = DocumentTemplateRelation::where([
            [
                'document_id', '=', $document->id,
            ],
            [
                'document_template_id', '=', $documentTemplate->id,
            ]
        ])->firstOrFail();

        $shop = null;

        if ($document->shop) {
            $shop = $document->shop;
        }

        $replaced = [
            '{sum}' => $document->sum,
            '{number}' => $document->register_number,
            '{fullNameClient}' => $document->clientFullName,
            '{date}' => $document->created_at->format('d.m.Y'),
            '{percentageCurrrency}' => $document->percentage_currrency,
            '{periodExcecution}' => $document->period_execution,
            '{dateFinish}' => $document->date_finish->format('d.m.Y'),
            '{fullNameDirector}' => '',
            '{organizationFullName}' => '',
            '{percentagePrepayment}' => 0,
            '{ogrn}' => '',
            '{inn}' => '',
            '{kpp}' => '',
            '{checkingAccount}' => '',
            '{addressOrganization}' => '',
            '{bik}' => '',
            '{bank}' => '',
            '{recipeSum}' => $document->recipeSum,
            '{clientAddress}' => $document->clientAddress,
            '{clientPhone}' => $document->clientPhones,
            '{clientEmail}' => $document->clientEmail,
            '{clientAdditionalPhones}' => $document->clientAdditionalPhones,
            '{legalClientFullName}' => '',
            '{legalClientOgrn}' => '',
            '{legalClientInn}' => '',
            '{legalClientKpp}' => '',
            '{legalClientBank}' => '',
            '{legalClientBik}' => '',
            '{legalClientCheckingAccount}' => '',
            '{legalClientAddressOrganization}' => '',
            '{fullNameGeneralDirector}' => '',
            '{fullNameDeputyDirector}' => '',
            '{dateVeksel}' => '',
            '{dateFinishVeksel}' => '',
            '{numberVeksel}' => '',
            '{firstPrepaymentSum}' => number_format($document->firstPrepaymentSum, 2, '.', ''),
            '{recipeFirstPrepaymentSum}' => $document->recipeFirstPrepaymentSum,
        ];

        if ($document->client instanceof LegalClient) {
            $replaced[ '{legalClientFullName}' ] = $document->client->full_name;
            $replaced[ '{legalClientOgrn}' ] = $document->client->ogrn;
            $replaced[ '{legalClientInn}' ] = $document->client->inn;
            $replaced[ '{legalClientKpp}' ] = $document->client->kpp;
            $replaced[ '{legalClientBank}' ] = $document->client->bank;
            $replaced[ '{legalClientBik}' ] = $document->client->bik;
            $replaced[ '{legalClientCheckingAccount}' ] = $document->client->checking_account;
            $replaced[ '{legalClientAddressOrganization}' ] = $document->client->address;
        }

        if (!is_null($shop)) {
            $replaced[ '{organizationFullName}' ] = $document->shop->full_name;
            $replaced[ '{ogrn}' ] = $document->shop->ogrn;
            $replaced[ '{inn}' ] = $document->shop->inn;
            $replaced[ '{kpp}' ] = $document->shop->kpp;
            $replaced[ '{checkingAccount}' ] = $document->shop->checking_account;
            $replaced[ '{addressOrganization}' ] = $document->shop->address;
            $replaced[ '{bik}' ] = $document->shop->bik;
            $replaced[ '{bank}' ] = $document->shop->bank;

            if ($shop->shopDirector) {
                $replaced[ '{fullNameDirector}' ] = $shop->shopDirector->fullName;
                $replaced[ '{fullNameDirector|G}' ] = $this->getFullNameWithCase($shop->shopDirector, Petrovich::CASE_GENITIVE);
                $replaced[ '{fullNameDirector|D}' ] = $this->getFullNameWithCase($shop->shopDirector, Petrovich::CASE_DATIVE);
                $replaced[ '{fullNameDirector|A}' ] = $this->getFullNameWithCase($shop->shopDirector, Petrovich::CASE_ACCUSATIVE);
                $replaced[ '{fullNameDirector|I}' ] = $this->getFullNameWithCase($shop->shopDirector, Petrovich::CASE_INSTRUMENTAL);
                $replaced[ '{fullNameDirector|P}' ] = $this->getFullNameWithCase($shop->shopDirector, Petrovich::CASE_PREPOSITIONAL);
            }

            if ($shop->generalDirector) {
                $replaced[ '{fullNameGeneralDirector}' ] = $shop->generalDirector->fullName;
                $replaced[ '{fullNameGeneralDirector|G}' ] = $this->getFullNameWithCase($shop->generalDirector, Petrovich::CASE_GENITIVE);
                $replaced[ '{fullNameGeneralDirector|D}' ] = $this->getFullNameWithCase($shop->generalDirector, Petrovich::CASE_DATIVE);
                $replaced[ '{fullNameGeneralDirector|A}' ] = $this->getFullNameWithCase($shop->generalDirector, Petrovich::CASE_ACCUSATIVE);
                $replaced[ '{fullNameGeneralDirector|I}' ] = $this->getFullNameWithCase($shop->generalDirector, Petrovich::CASE_INSTRUMENTAL);
                $replaced[ '{fullNameGeneralDirector|P}' ] = $this->getFullNameWithCase($shop->generalDirector, Petrovich::CASE_PREPOSITIONAL);
            }

            if ($shop->shopDeputyDirectors) {
                $deputies = [];

                foreach ($shop->shopDeputyDirectors as $deputyDirector) {
                    $deputies[ 'n' ][] = $deputyDirector->fullName;
                    $deputies[ 'g' ][] = $this->getFullNameWithCase($deputyDirector, Petrovich::CASE_GENITIVE);
                    $deputies[ 'd' ][] = $this->getFullNameWithCase($deputyDirector, Petrovich::CASE_DATIVE);
                    $deputies[ 'a' ][] = $this->getFullNameWithCase($deputyDirector, Petrovich::CASE_ACCUSATIVE);
                    $deputies[ 'i' ][] = $this->getFullNameWithCase($deputyDirector, Petrovich::CASE_INSTRUMENTAL);
                    $deputies[ 'p' ][] = $this->getFullNameWithCase($deputyDirector, Petrovich::CASE_PREPOSITIONAL);
                }

                $replaced[ '{fullNameDeputyDirector}' ] = implode(', ', $deputies[ 'n' ]);
                $replaced[ '{fullNameDeputyDirector|G}' ] = implode(', ', $deputies[ 'g' ]);
                $replaced[ '{fullNameDeputyDirector|D}' ] = implode(', ', $deputies[ 'd' ]);
                $replaced[ '{fullNameDeputyDirector|A}' ] = implode(', ', $deputies[ 'a' ]);
                $replaced[ '{fullNameDeputyDirector|I}' ] = implode(', ', $deputies[ 'i' ]);
                $replaced[ '{fullNameDeputyDirector|P}' ] = implode(', ', $deputies[ 'p' ]);
            }
        }

        $dateTime = new DateTime($relation->created_at);
        $date = $dateTime->format('d.m.Y');
        $replaced[ '{dateVeksel}' ] = $date;
        $replaced[ '{numberVeksel}' ] = str_pad($relation->id, 4, '0', STR_PAD_LEFT) . $date;
        $finishDateTime = $dateTime->modify('+20years');
        $replaced[ '{dateFinishVeksel}' ] = $finishDateTime->format('d.m.Y');

        if ($document->firstPrepayment) {
            $replaced[ '{percentagePrepayment}' ] = number_format($document->firstPrepayment->percentage_prepayment, 2, '.', '');
        }

        return strtr($documentTemplate->body, $replaced);
    }

    /**
     * Get full name worker with case.
     *
     * @access private
     * @param \App\Models\User\Worker $worker Worker model.
     * @param int $case Case.
     * @return string
     */
    private function getFullNameWithCase(Worker $worker, $case = Petrovich::CASE_NOMENATIVE)
    {
        $petrovich = new Petrovich();

        if ($worker->lastname) {
            $sex = $petrovich->detectGender($worker->lastname);
        } else {
            $sex = $petrovich->detectGender($worker->name);
        }

        $petrovich = new Petrovich($sex);

        return sprintf('%s %s %s',
            ($worker->surname) ? $petrovich->lastname($worker->surname, $case) : '',
            ($worker->name) ? $petrovich->firstname($worker->name, $case) : '',
            ($worker->lastname) ? $petrovich->middlename($worker->lastname, $case) : ''
        );
    }
}
