<?php

namespace App\Models\Document;

use Illuminate\Database\Eloquent\Model;
use App\Models\Document\Document;

/**
 * Model class for table `document_prepayments`.
 */
class DocumentPrepayment extends Model
{
    /**
     * {@inheritdoc}
     *
     * @var array $fillable
     */
    protected $fillable = [
        'document_id',
        'percentage_prepayment',
        'percentage_remainder',
        'remainder_eur',
        'enter_eur',
        'enter_usd',
        'enter_rub',
        'prepayment_amount_eur',
        'prepayment_amount_rub',
        'course_cb_eur',
        'course_cb_usd',
        'course_cb_eur_markup',
        'course_cb_usd_markup',
        'cross_currrency',
        'percentage_currrency',
        'type_payment',
    ];

    /**
     * Get human type payment.
     *
     * @return null|string
     */
    public function getHumanTypePaymentAttribute()
    {
        $types = Document::getPaymentTypes();

        if (isset($types[ $this->type_payment ])) {
            return $types[ $this->type_payment ];
        }

        return null;
    }
}
