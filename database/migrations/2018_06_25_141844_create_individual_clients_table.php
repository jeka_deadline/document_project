<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIndividualClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // create table individual clients
        Schema::create('individual_clients', function (Blueprint $table) {
            $table->increments('id');
            $table->string('surname', 50);
            $table->string('name', 50);
            $table->string('lastname', 50);
            $table->string('register_address');
            $table->string('shipping_address')->nullable();
            $table->string('email', 100);
            $table->mediumText('phones');
            $table->string('additional_surname', 50)->nullable();
            $table->string('additional_name', 50)->nullable();
            $table->string('additional_lastname', 50)->nullable();
            $table->mediumText('additional_phones')->nullable();
            $table->string('additional_email', 100)->nullable();
            $table->text('comment')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // drop table individual clients
        Schema::dropIfExists('individual_clients');
    }
}
