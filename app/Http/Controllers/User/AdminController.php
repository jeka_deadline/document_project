<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\AdminService;
use App\Models\Setting;
use App\Models\User\Role;
use Illuminate\Support\Facades\Auth;

/**
 * Admin controller.
 */
class AdminController extends Controller
{
    /**
     * Admin service.
     *
     * @var \App\Services\AdminService $adminService
     * @access private
     */
    private $adminService;

    /**
     * Controller constructor.
     *
     * @param \App\Services\AdminService $adminService Worker service
     * @return void
     */
    public function __construct (AdminService $adminService)
    {
        $this->adminService = $adminService;
    }

    /**
     * Create or update calendar holiday days by years.
     *
     * @param \Illuminate\Http\Request $request Request
     * @return string
     */
    public function updateCalendarStructure(Request $request)
    {
        if (!Auth::user()->isCanUpdateCalendar()) {
            $this->accessDanied();
        }

        $this->adminService->updateCalendarStructure();

        return back();
    }

    /**
     * View page with list settings.
     *
     * @param \Illuminate\Http\Request $request Request
     * @return \Illuminate\Support\Facades\View
     */
    public function listSettings(Request $request)
    {
        if (!Auth::user()->isCanViewSettings()) {
            $this->accessDanied();
        }

        $settings = Setting::paginate();

        return view('admin.settings.index', compact('settings'));
    }

    /**
     * View page with list roles.
     *
     * @param \Illuminate\Http\Request $request Request
     * @return \Illuminate\Support\Facades\View
     */
    public function listRoles(Request $request)
    {
        if (!Auth::user()->isAdmin()) {
            $this->accessDanied();
        }

        $roles = Role::withoutAdmin()
            ->get();

        return view('admin.roles.index', compact('roles'));
    }

    /**
     * View page edit role.
     *
     * @param \Illuminate\Http\Request $request Request
     * @param \App\Models\User\Role $role Role model
     * @return \Illuminate\Support\Facades\View
     */
    public function editRole(Request $request, Role $role)
    {
        if (!Auth::user()->isAdmin()) {
            $this->accessDanied();
        }

        return view('admin.roles.edit', [
            'listProjectActions' => $this->adminService->getListProjectActions(),
            'roleActions' => $this->adminService->getListProjectActionsByRoleId($role->id),
            'role' => $role,
        ]);
    }

    /**
     * Update role.
     *
     * @param \Illuminate\Http\Request $request Request
     * @param \App\Models\User\Role $role Role model
     * @return \Illuminate\Support\Facades\Redirect
     */
    public function updateRole(Request $request, Role $role)
    {
        if (!Auth::user()->isAdmin()) {
            $this->accessDanied();
        }

        $this->adminService->updateRole($role->id, $request->get('actions', []));

        return redirect(route('admin.roles.index'));
    }

    /**
     * View page with form update setting.
     *
     * @param \Illuminate\Http\Request $request Request
     * @param \App\Models\Setting $setting Setting model.
     * @return \Illuminate\Support\Facades\View
     */
    public function editSetting(Request $request, Setting $setting)
    {
        if (!Auth::user()->isCanEditSetting()) {
            $this->accessDanied();
        }

        return view('admin.settings.edit', compact('setting'));
    }

    /**
     * Update setting.
     *
     * @param \Illuminate\Http\Request $request Request
     * @param \App\Models\Setting $setting Setting model.
     * @return \Illuminate\Support\Facades\Redirect
     */
    public function updateSetting(Request $request, Setting $setting)
    {
        if (!Auth::user()->isCanEditSetting()) {
            $this->accessDanied();
        }

        $setting->value = $request->value;

        $setting->save();

        return redirect(route('admin.settings.index'));
    }
}
