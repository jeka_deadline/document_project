<?php

namespace App\Http\Controllers\Document;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Document\DocumentTemplate;
use App\Http\Requests\CalculationSumRequest;
use App\Http\Requests\CreateDocumentRequest;
use App\Http\Requests\UpdateDocumentRequest;
use App\Http\Requests\CreateNewDocumentPaymentRequest;
use App\Http\Requests\AttachTemplateRequest;
use App\Http\Requests\AttachProductRequest;
use App\Services\DocumentService;
use App\Services\ProductService;
use App\Services\FactoryService;
use App\Services\FactoryPaymentService;
use App\Services\ServiceAccountService;
use Illuminate\Support\Facades\Auth;
use App\Models\Document\Document;
use App\Models\Product;
use App\DocumentSearch\DocumentSearch;

/**
 * Document controller.
 */
class DocumentController extends Controller
{
    /**
     * Document service.
     *
     * @access private
     * @var \App\Services\DocumentService $documentService
     */
    private $documentService;

    /**
     * Product service.
     *
     * @access private
     * @var \App\Services\ProductService $productService
     */
    private $productService;

    /**
     * Factory service.
     *
     * @access private
     * @var \App\Services\FactoryService $factoryService
     */
    private $factoryService;

    /**
     * Service account service.
     *
     * @access private
     * @var \App\Services\ServiceAccountService $serviceAccountService
     */
    private $serviceAccountService;

    /**
     * Factory payment service.
     *
     * @access private
     * @var \App\Services\FactoryPaymentService $factoryPaymentService
     */
    private $factoryPaymentService;

    /**
     * Controller constructor.
     *
     * @param \App\Services\DocumentService $documentService Document service
     * @param \App\Services\ProductService $productService Product service
     * @param \App\Services\FactoryService $factoryService Factory service
     * @param \App\Services\ServiceAccountService $serviceAccountService Service account service
     * @param \App\Services\FactoryPaymentService $factoryPaymentService Factory payment service
     * @return void
     */
    public function __construct(DocumentService $documentService, ProductService $productService, FactoryService $factoryService, ServiceAccountService $serviceAccountService, FactoryPaymentService $factoryPaymentService)
    {
        $this->documentService = $documentService;
        $this->productService = $productService;
        $this->factoryService = $factoryService;
        $this->serviceAccountService = $serviceAccountService;
        $this->factoryPaymentService = $factoryPaymentService;
    }

    /**
     * Display page document templates.
     *
     * @param \Illuminate\Http\Request $request Request
     * @return \Illuminate\Support\Facades\View
     */
    public function index(Request $request)
    {
        if (!Auth::user()->isCanViewListDocuments()) {
            $this->accessDanied();
        }

        return view('document.index', [
            'documents' => $this->documentService->filter($request),
            'documentsFilterSum' => $this->documentService->getFilterSum(),
            'documentsFilterEnterEur' => $this->documentService->getFilterEnterEur(),
            'listManagers' => $this->documentService->getListManagers(),
            'listTypePayments' => $this->documentService->getListTypePayments(),
            'documentMaxSum' => $this->documentService->getDocumentMaxSum(),
            'listPercentagePayments' => $this->documentService->getListPercentagePayments(),
        ]);
    }

    /**
     * Display page with document form for create new document.
     *
     * @param \Illuminate\Http\Request $request Request
     * @return \Illuminate\Support\Facades\View
     */
    public function create(Request $request)
    {
        if (!Auth::user()->isCanCreateDocument()) {
            $this->accessDanied();
        }

        return view('document.create', [
            'dropDownInformation' => $this->documentService->getDropDownInformation(),
            'currencies' => $this->documentService->getCurrencies(),
            'date' => date('Y-m-d'),
            'lastRegisterNumber' => $this->documentService->getLastRegisterNumber(),
        ]);
    }

    /**
     * Store new document template.
     *
     * @param \App\Http\Requests\CreateDocumentRequest $request Document create request
     * @return \Illuminate\Support\Facades\Redirect
     */
    public function store(CreateDocumentRequest $request)
    {
        if (!Auth::user()->isCanCreateDocument()) {
            $this->accessDanied();
        }

        $document = $this->documentService->createNewDocument($request->all(), Auth::user()->worker->id);

        if (!$document) {
              return redirect()->back();
        }

        return redirect()->route('document.view', [
            'document' => $document,
        ]);
    }

    /**
     * Display page with document information.
     *
     * @param \Illuminate\Http\Request $request Request
     * @param \App\Models\Document\Document $document Document model
     * @return \Illuminate\Support\Facades\View
     */
    public function view(Request $request, Document $document)
    {
        if (!Auth::user()->isCanViewDocument()) {
            $this->accessDanied();
        }

        return view('document.view', [
            'document' => $document,
            'currencies' => $this->documentService->getCurrencies($document->percentage_currrency),
            'typePayments' => $this->documentService->getListTypePayments(),
        ]);
    }

    /**
     * Calculation date finish document by count days period execution.
     *
     * @param \Illuminate\Http\Request $request Request
     * @return string JSON result with calculation date finish.
     */
    public function calculationDateFinish(Request $request)
    {
        return response()->json([
            'status' => true,
            'dateFinish' => $this->documentService->calculationDateFinish($request->get('period_execution', 0)),
        ]);
    }

    /**
     * Calculation user sum enter from calculation form.
     *
     * @param \App\Http\Requests\CalculationSumRequest $request Request from calculation form.
     * @return string JSON result calculation.
     */
    public function calculationSum(CalculationSumRequest $request)
    {
        $response = [
            'status' => true,
            'content' => $this->documentService->calculation($request->all()),
        ];

        return response()->json($response);
    }

    /**
     * Calculation user sum enter from form surcharge.
     *
     * @param \App\Http\Requests\CreateNewDocumentPaymentRequest $request Request from surcharge form.
     * @return string JSON result calculation.
     */
    public function calculationSumSurcharge(CreateNewDocumentPaymentRequest $request, Document $document)
    {
        if (!Auth::user()->isCanCreateDocumentNewPayment()) {
            $this->accessDanied();
        }

        $response = [
            'status' => true,
            'content' => $this->documentService->calculationSurcharge($request->all(), $document),
        ];

        return response()->json($response);
    }

    /**
     * Create new payment for document.
     *
     * @param \App\Http\Requests\CreateNewDocumentPaymentRequest $request Request from surcharge form.
     * @param \App\Models\Document\Document $document Document model.
     * @return string JSON result create new payment.
     */
    public function createNewDocumentPayment(CreateNewDocumentPaymentRequest $request, Document $document)
    {
        if (!Auth::user()->isCanCreateDocumentNewPayment()) {
            $this->accessDanied();
        }

        return response()->json($this->documentService->createNewDocumentPayment($request->all(), $document));
    }

    /**
     * Get attach document template for document.
     *
     * @param \Illuminate\Http\Request $request Request
     * @param \App\Models\Document\Document $document Document model.
     * @return string JSON result with render form.
     */
    public function getAttachDocumentTemplateFrom(Request $request, Document $document)
    {
        if (!Auth::user()->isCanAttachDocumentTemplate()) {
            $this->accessDanied();
        }

        $typeTemplate = $request->typeTemplate;

        return response()->json([
            'content' => view('document/view_modal_content_attach_templates', [
                'templates' => $this->documentService->getTemplatesForAttach($document, $typeTemplate),
                'document' => $document,
            ])->render(),
        ]);
    }

    /**
     * Attach document template to document.
     *
     * @param \Illuminate\Http\Request $request Request
     * @param \App\Http\Requests\AttachTemplateRequest $request Request from attach form.
     * @param \App\Models\Document\Document $document Document model.
     * @return string JSON result with attach template to document.
     */
    public function attachDocumentTemplate(AttachTemplateRequest $request, Document $document)
    {
        if (!Auth::user()->isCanAttachDocumentTemplate()) {
            $this->accessDanied();
        }

        $attachedTemplate = $this->documentService->attachTemplateToDocument($document, $request->template_id);

        $templates = $document->templates;

        $response = [
            'status' => false,
        ];

        if ($attachedTemplate) {
            $response[ 'status' ] = true;
        }

        $response[ 'content' ] = view('document/view_block_documents', compact('templates', 'document'))->render();

        return response()->json($response);
    }

    /**
     * Display page with document form for edit document information.
     *
     * @param \Illuminate\Http\Request $request Request
     * @param \App\Models\Document\Document $document Document model
     * @return \Illuminate\Support\Facades\View
     */
    public function edit(Request $request, Document $document)
    {
        if (!Auth::user()->isCanEditDocument()) {
            $this->accessDanied();
        }

        return view('document.edit', [
            'document' => $document,
            'dropDownInformation' => $this->documentService->getDropDownInformation(),
            'lastRegisterNumber' => $this->documentService->getLastRegisterNumber(),
            'listOrderStatuses' => $this->documentService->getListOrderStatuses($document->name),
        ]);
    }

    /**
     * Update document information.
     *
     * @param \App\Http\Requests\UpdateDocumentRequest $request Document request
     * @param \App\Models\Document\Document $document Document model
     * @return \Illuminate\Support\Facades\Redirect
     */
    public function update(UpdateDocumentRequest $request, Document $document)
    {
        if (!Auth::user()->isCanEditDocument()) {
            $this->accessDanied();
        }

        $this->documentService->updateDocument($request->all(), $document);

        return redirect()->route('document.index');
    }

    /**
     * Delete document.
     *
     * @param \Illuminate\Http\Request $request Request
     * @param \App\Models\Document $document Document model
     * @return \Illuminate\Support\Facades\Redirect
     */
    public function delete(Request $request, Document $document)
    {
        if (!Auth::user()->isCanDeleteDocument()) {
            $this->accessDanied();
        }

        $document->delete();

        return redirect(route('document.index'));
    }

    /**
     * Show main table document.
     *
     * @param \Illuminate\Http\Request $request Request
     * @return \Illuminate\Support\Facades\View
     */
    public function table(Request $request)
    {
        if (!Auth::user()->isCanViewTableDocuments()) {
            $this->accessDanied();
        }

        return view('document.table', [
            'documents' => DocumentSearch::apply($request),
            'listShops' => $this->documentService->getListShops(),
            'listDocumentTypes' => $this->documentService->getDocumentTypes(),
            'listPaymentTypes' => $this->documentService->getPaymentTypes(),
            'listMonths' => $this->documentService->getListMonths(),
            'listFactories' => $this->factoryService->getListFactories(),
            'maxServiceAccountSum' => $this->serviceAccountService->getMaxSum(),
            'maxFactoryPaymentSum' => $this->factoryPaymentService->getMaxSum(),
            'maxFactoryPaymentBankTransfer' => $this->factoryPaymentService->getMaxBankTransfer(),
        ]);
    }

    /**
     * Save document additional field.
     *
     * @param \Illuminate\Http\Request $request Request
     * @param \App\Models\Document\Document $document Document model
     * @return string JSON save additional fields status.
     */
    public function saveAdditionalField(Request $request, Document $document)
    {
        return response()->json([
            'status' => $this->documentService->saveAdditionalField($document, $request->key, $request->value),
        ]);
    }
}
