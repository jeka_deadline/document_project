<?php

namespace App\Models\Document;

use Illuminate\Database\Eloquent\Model;

class FactoryPayment extends Model
{
    protected $fillable = [
        'date_dispatch',
        'date_payment',
        'invoice',
        'sum',
        'bank_transfer',
    ];

    protected $dates = [
        'date_dispatch',
        'date_payment',
    ];

    public function document()
    {
        return $this->belongsTo('App\Models\Document\Document');
    }
}
