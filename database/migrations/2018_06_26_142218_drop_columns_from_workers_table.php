<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropColumnsFromWorkersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('workers', function (Blueprint $table) {
            $table->dropColumn('additional_surname');
            $table->dropColumn('additional_name');
            $table->dropColumn('additional_lastname');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('workers', function (Blueprint $table) {
            $table->string('additional_surname', 50)->nullable()->after('phones');
            $table->string('additional_name', 50)->nullable()->after('additional_surname');
            $table->string('additional_lastname', 50)->nullable()->after('additional_name');
        });
    }
}
