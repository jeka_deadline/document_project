<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ShopRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'full_name' => 'required|max:255',
            'short_name' => 'required|max:100',
            'address' => 'required',
            'phone' => 'required|regex:#^\+\d{11,13}$#',
            'ogrn' => 'required|regex:#^\d{13}$#',
            'inn' => 'required|regex:#^\d{10}$#',
            'kpp' => 'required|regex:#^\d{9}$#',
            'bank' => 'required|max:255',
            'bik' => 'required|regex:#^\d{9}$#',
            'checking_account' => 'required|max:255',
            'general_director_id' => 'required|exists:workers,id',
            'shop_director_id' => 'required|exists:workers,id',
            'deputy_shop_director_id' => 'required|exists:workers,id',
        ];
    }
}
