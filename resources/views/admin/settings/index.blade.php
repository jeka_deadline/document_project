@extends('layouts.bootstrap')

@section('breadcrumbs')

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Главная</a></li>
            <li class="breadcrumb-item active" aria-current="page">Настройки</li>
        </ol>
    </nav>

@stop
@section('content')

    <div class="row">
          <div class="col-lg-12">
                <table class="table">
                      <thead>
                            <tr>
                                  <th>Описание</th>
                                  <th>Значение</th>
                                  <th>Операции</th>
                            </tr>
                      </thead>
                      <tbody>

                          @foreach ($settings as $setting)

                              <tr>
                                  <td>
                                      {{ $setting->title }}
                                  </td>
                                  <td>
                                      {{ $setting->value }}
                                  </td>
                                  <td>
                                      <?php if (Auth::user()->isCanEditSetting()) : ?>

                                          <a href="{{ route('admin.settings.edit', ['setting' => $setting]) }}"><i class="fa fa-edit fa-2x"></i></a>

                                      <?php endif; ?>
                                  </td>
                              </tr>

                          @endforeach

                      </tbody>
                </table>

                {{ $settings->links() }}
          </div>
    </div>

@stop