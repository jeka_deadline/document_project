<?php

namespace App\Helpers;

class UUID
{
    public static function create()
    {
        return uniqid();
    }
}