<?php

namespace App\Models\Document;

use Illuminate\Database\Eloquent\Model;

/**
 * Model class for table `documents_templates_relations`.
 */
class DocumentTemplateRelation extends Model
{
    /**
     * {@inheritdoc}
     *
     * @var string $table
     */
    protected $table = 'documents_templates_relations';
}
