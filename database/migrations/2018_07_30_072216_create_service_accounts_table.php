<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_accounts', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('document_id')->unsigned();
            $table->foreign('document_id')
                ->references('id')
                ->on('documents')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->string('delivery', 3);
            $table->string('mkad', 10)->nullable();
            $table->double('km_outer_mkad')->nullable();
            $table->double('sum_delivery')->nullable();
            $table->integer('floor')->unsigned()->nullable();
            $table->double('manual')->nullable();
            $table->double('manual_large_sized')->nullable();
            $table->double('on_lift')->nullable();
            $table->double('sum_climb')->nullable();
            $table->string('assembly', 3);
            $table->double('sum_assembly')->nullable();
            $table->double('removing_packaging')->nullable();
            $table->string('type_payment', 15);
            $table->integer('payment_percentage')->unsigned();
            $table->double('course');
            $table->double('full_sum_eur');
            $table->double('full_sum_rub');
            $table->double('promotion_sum_eur')->nullable();
            $table->double('full_sum_eur_with_promotion')->nullable();
            $table->double('full_sum_rub_with_promotion')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_accounts');
    }
}
