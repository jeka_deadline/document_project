<table id="block-service-accounts" style="width: 100%">

    @if ($document->serviceAccount && Auth::user()->isCanViewServiceAccount())

        <tr>
            <td><a href="{{ route('document.service-account.view', ['serviceAccount' => $document->serviceAccount]) }}"><i class="fa fa-eye fa-2x"></i></a></td>
        </tr>

    @endif

</table>