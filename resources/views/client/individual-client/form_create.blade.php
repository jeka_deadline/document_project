<div class="row">
    <div class="col-lg-12">
          <h2>Добавление клиента</h2>
          <form method="post" action="{{ route('individual.client.store') }}">
              {{ csrf_field() }}

              <div class="form-group">
                  <label class="control-label" for="">Фамилия</label>
                  <input class="form-control" type="text" name="surname" value="{{ old('surname') }}">

                  @if ($errors->has('surname'))

                      <div class="text-danger">

                            {{ $errors->first('surname') }}

                      </div>

                  @endif

              </div>

              <div class="form-group">
                  <label class="control-label" for="">Имя</label>
                  <input class="form-control" type="text" name="name" value="{{ old('name') }}">

                  @if ($errors->has('name'))

                      <div class="text-danger">

                            {{ $errors->first('name') }}

                      </div>

                  @endif

              </div>

              <div class="form-group">
                  <label class="control-label" for="">Отчество</label>
                  <input class="form-control" type="text" name="lastname" value="{{ old('lastname') }}">

                  @if ($errors->has('lastname'))

                      <div class="text-danger">

                            {{ $errors->first('lastname') }}

                      </div>

                  @endif

              </div>

              <div class="form-group">
                  <label class="control-label" for="">Адрес регистрации</label>
                  <textarea class="form-control" name="register_address" id="" rows="6">{{ old('register_address') }}</textarea>

                  @if ($errors->has('register_address'))

                      <div class="text-danger">

                            {{ $errors->first('register_address') }}

                      </div>

                  @endif

              </div>

              <div class="checkbox">
                  <label for=""><input type="checkbox" name="register_address_equal_shipping_address" {{ (old('register_address_equal_shipping_address')) ? 'checked' : '' }}>Адрес доставки совпадает с адресом регистрации</label>
              </div>

              <div class="form-group">
                  <label class="control-label" for="">Адрес доставки</label>
                  <textarea class="form-control" name="shipping_address" id="" rows="6">{{ old('shipping_address') }}</textarea>

                  @if ($errors->has('shipping_address'))

                      <div class="text-danger">

                            {{ $errors->first('shipping_address') }}

                      </div>

                  @endif

              </div>

              <div class="form-group">
                  <label class="control-label" for="">Email</label>
                  <input class="form-control email-mask" type="text" name="email" value="{{ old('email') }}">

                  @if ($errors->has('email'))

                      <div class="text-danger">

                            {{ $errors->first('email') }}

                      </div>

                  @endif

              </div>

              <div class="block-phones">
                  <label for="">Телефоны</label>

                  @if (old('phones'))
                      @foreach (old('phones') as $phoneValue)

                          <div class="form-group target">
                              <div class="input-group">
                                  <input type="text" class="form-control phone-mask" name="phones[]" value="{{ $phoneValue }}">
                                  <span class="input-group-btn">
                                      <button type="button" class="btn btn-danger addel-delete"><i class="fa fa-remove"></i></button>
                                  </span>
                              </div>
                          </div>

                      @endforeach
                  @else

                      <div class="form-group target">
                          <div class="input-group">
                              <input type="text" class="form-control phone-mask" name="phones[]" value="">
                              <span class="input-group-btn">
                                  <button type="button" class="btn btn-danger addel-delete"><i class="fa fa-remove"></i></button>
                              </span>
                          </div>
                      </div>

                  @endif

                  @if ($errors->has('phones.*'))
                      @foreach ($errors->get('phones.*') as $messageArrays)
                          @foreach ($messageArrays as $message)

                              <div class="text-danger">{{ $message }}</div>

                          @endforeach
                      @endforeach
                  @endif

                  <button type="button" class="btn btn-success addel-add"><i class="fa fa-plus"></i></button>

              </div>

              <h3>Дополнительный контакт</h3>

              <div class="form-group">
                  <label class="control-label" for="">Фамилия</label>
                  <input class="form-control" type="text" name="additional_surname" value="{{ old('additional_surname') }}">

                  @if ($errors->has('additional_surname'))

                      <div class="text-danger">

                            {{ $errors->first('additional_surname') }}

                      </div>

                  @endif

              </div>

              <div class="form-group">
                  <label class="control-label" for="">Имя</label>
                  <input class="form-control" type="text" name="additional_name" value="{{ old('additional_name') }}">

                  @if ($errors->has('additional_name'))

                      <div class="text-danger">

                            {{ $errors->first('additional_name') }}

                      </div>

                  @endif

              </div>

              <div class="form-group">
                  <label class="control-label" for="">Отчество</label>
                  <input class="form-control" type="text" name="additional_lastname" value="{{ old('additional_lastname') }}">

                  @if ($errors->has('additional_lastname'))

                      <div class="text-danger">

                            {{ $errors->first('additional_lastname') }}

                      </div>

                  @endif

              </div>

              <div class="form-group">
                  <label class="control-label" for="">Email</label>
                  <input class="form-control email-mask" type="text" name="additional_email" value="{{ old('additional_email') }}">

                  @if ($errors->has('additional_email'))

                      <div class="text-danger">

                            {{ $errors->first('additional_email') }}

                      </div>

                  @endif

              </div>

              <div class="block-additional-phones">
                  <label for="">Телефоны</label>

                  @if (old('additional_phones'))
                      @foreach (old('additional_phones') as $phoneValue)

                          <div class="form-group target">
                              <div class="input-group">
                                  <input type="text" class="form-control phone-mask" name="additional_phones[]" value="{{ $phoneValue }}">
                                  <span class="input-group-btn">
                                      <button type="button" class="btn btn-danger addel-delete"><i class="fa fa-remove"></i></button>
                                  </span>
                              </div>
                          </div>

                      @endforeach
                  @else

                      <div class="form-group target">
                          <div class="input-group">
                              <input type="text" class="form-control phone-mask" name="additional_phones[]" value="">
                              <span class="input-group-btn">
                                  <button type="button" class="btn btn-danger addel-delete"><i class="fa fa-remove"></i></button>
                              </span>
                          </div>
                      </div>

                  @endif

                  <button type="button" class="btn btn-success addel-add"><i class="fa fa-plus"></i></button>
              </div>

              <div class="form-group">
                  <label class="control-label" for="">Комментарий</label>
                  <textarea class="form-control" type="text" name="comment">{{ old('comment') }}</textarea>

                  @if ($errors->has('comment'))

                      <div class="text-danger">

                            {{ $errors->first('comment') }}

                      </div>

                  @endif

              </div>

              @if (isset($showButtons))

                  <input type="submit" value="Добавить" class="btn btn-success">
                  <a class="btn btn-info" href="{{ route('individual.client.index') }}">Назад</a>

              @endif

          </form>
    </div>
</div>
