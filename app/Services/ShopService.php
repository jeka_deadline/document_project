<?php

namespace App\Services;

use App\Models\Shop\Shop;
use App\Models\User\Role;
use App\Models\Shop\ShopUserLeader;

/**
 * Shop service.
 */
class ShopService
{
    /**
     * Get list shops with pagination.
     *
     * @param int $countOnPage Count items on page
     * @return
     */
    public function paginate($countOnPage = 15)
    {
        return Shop::paginate($countOnPage);
    }

    /**
     * Create new shop.
     *
     * @param array $data User form request shop information
     * @return bool
     */
    public function createNewShop($data)
    {
        $generalDirectorWorkerId = $data[ 'general_director_id' ];
        $shopDirectorWorkerId = $data[ 'shop_director_id' ];
        $deputyDirectorWorkerIds = $data[ 'deputy_shop_director_id' ];

        if (isset($data[ 'default' ])) {
            $data[ 'default' ] = 1;
            Shop::query()->update(['default' => 0]);
        }

        $shop = Shop::create($data);

        if (!$shop) {
            return false;
        }

        $this->attachGeneralDirectorToShop($shop->id, $generalDirectorWorkerId);
        $this->attachShopDirectorToShop($shop->id, $shopDirectorWorkerId);
        $this->attachShopDeputyDirectorToShop($shop->id, $deputyDirectorWorkerIds);

        return true;
    }

    /**
     * Update shop.
     *
     * @param \App\Models\Shop\Shop $shop Shop model.
     * @param array $data User form request shop information
     * @return bool
     */
    public function updateShop(Shop $shop, $data)
    {
        $generalDirectorWorkerId = $data[ 'general_director_id' ];
        $shopDirectorWorkerId = $data[ 'shop_director_id' ];
        $deputyDirectorWorkerIds = $data[ 'deputy_shop_director_id' ];

        if (isset($data[ 'default' ]) && !$shop->default) {
            $data[ 'default' ] = 1;
            Shop::query()->update(['default' => 0]);
        } else {
            $data[ 'default' ] = 0;
        }

        $shop->update($data);

        $this->attachGeneralDirectorToShop($shop->id, $generalDirectorWorkerId);
        $this->attachShopDirectorToShop($shop->id, $shopDirectorWorkerId);
        $this->attachShopDeputyDirectorToShop($shop->id, $deputyDirectorWorkerIds);

        return true;
    }

    /**
     * Get shop deputy director ids.
     *
     * @param \App\Models\Shop\Shop $shop Shop model.
     * @return int[]
     */
    public function getShopDeputyDirectorIds(Shop $shop)
    {
        return $shop->shopDeputyDirectors->pluck('id', 'id')->all();
    }

    /**
     * Get shop director id.
     *
     * @param \App\Models\Shop\Shop $shop Shop model.
     * @return int|null
     */
    public function getShopDirectorId(Shop $shop)
    {
        return ($shop->shopDirector) ? $shop->shopDirector->id : null;
    }

    /**
     * Get shop general director id.
     *
     * @param \App\Models\Shop\Shop $shop Shop model.
     * @return int|null
     */
    public function getShopGeneralDirectorId(Shop $shop)
    {
        return ($shop->generalDirector) ? $shop->generalDirector->id : null;
    }

    /**
     * Attach general director to shop.
     *
     * @access private
     * @param int $shopId Shop id
     * @param int $generalDirectorWorkerId Worker id with role general director
     * @return void
     */
    private function attachGeneralDirectorToShop($shopId, $generalDirectorWorkerId)
    {
        $roleGeneralDirector = Role::director()->first();

        $shopLeader = ShopUserLeader::where('shop_id', $shopId)
            ->where('role_id', $roleGeneralDirector->id)
            ->first();

        if (!$shopLeader) {
            $shopLeader = new ShopUserLeader();
            $shopLeader->shop_id = $shopId;
            $shopLeader->role_id = $roleGeneralDirector->id;
        }

        $shopLeader->worker_id = $generalDirectorWorkerId;

        $shopLeader->save();
    }

    /**
     * Attach shop director to shop.
     *
     * @access private
     * @param int $shopId Shop id
     * @param int $shopDirectorWorkerId Worker id with role shop director
     * @return void
     */
    private function attachShopDirectorToShop($shopId, $shopDirectorWorkerId)
    {
        $roleShopDirector = Role::shopDirector()->first();

        $shopLeader = ShopUserLeader::where('shop_id', $shopId)
            ->where('role_id', $roleShopDirector->id)
            ->first();

        if (!$shopLeader) {
            $shopLeader = new ShopUserLeader();
            $shopLeader->shop_id = $shopId;
            $shopLeader->role_id = $roleShopDirector->id;
        }

        $shopLeader->worker_id = $shopDirectorWorkerId;

        $shopLeader->save();
    }

    /**
     * Attach deputy director to shop.
     *
     * @access private
     * @param int $shopId Shop id
     * @param int[] $deputyDirectorWorkerIds Worker id with role deputy shop director
     * @return void
     */
    private function attachShopDeputyDirectorToShop($shopId, $deputyDirectorWorkerIds)
    {
        $roleShopDeputyDirector = Role::shopDeputyDirector()->first();

        $oldShopLeaders = ShopUserLeader::where('shop_id', $shopId)
            ->where('role_id', $roleShopDeputyDirector->id)
            ->get()
            ->keyBy('worker_id')
            ->all();

        foreach ($deputyDirectorWorkerIds as $workerId) {

            if (isset($oldShopLeaders[ $workerId ])) {
                unset($oldShopLeaders[ $workerId ]);
                continue;
            }

            $shopLeader = new ShopUserLeader();
            $shopLeader->shop_id = $shopId;
            $shopLeader->role_id = $roleShopDeputyDirector->id;
            $shopLeader->worker_id = $workerId;

            $shopLeader->save();
        }

        ShopUserLeader::where('shop_id', $shopId)
            ->where('role_id', $roleShopDeputyDirector->id)
            ->whereIn('worker_id', array_keys($oldShopLeaders))
            ->delete();
    }
}
