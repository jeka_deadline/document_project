@extends('layouts.bootstrap')

@section('breadcrumbs')

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Главная</a></li>
            <li class="breadcrumb-item"><a href="{{ route('worker.index') }}">Список сотрудников</a></li>
            <li class="breadcrumb-item active" aria-current="page">Просмотр сотрудника</li>
        </ol>
    </nav>

@stop
@section('content')

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <table style="width: 100%">
                        <tr>
                            <td>
                                <h4>Профиль сотрудника</h4>
                            </td>

                            <?php if (Auth::user()->isCanEditWorker()) : ?>

                                <td style="text-align: right"><a class="btn btn-warning" style="font-size: 14px" href="{{ route('worker.edit', ['worker' => $worker]) }}">Редактировать</a></td>

                            <?php endif; ?>

                        </tr>
                    </table>
                </div>
                <div class="panel-body">
                    <div class="box box-info">
                        <div class="box-body">
                            <div class="col-lg-3">
                                <div  align="center">
                                    <img alt="User Pic" src="/images/nobody_m.original.jpg" id="profile-image1" class="img-circle img-responsive">
                                </div>
                                <br>
                                <!-- /input-group -->
                            </div>
                            <div class="col-lg-3">
                                <h4 style="color:#00b1b1;">{{ $worker->surname }} {{ $worker->name }} {{ $worker->lastname }}</h4>
                                </span>
                                <span>
                                    <p>{{ $worker->userRole->role->humanName }}</p>
                                </span>
                            </div>
                            <div class="col-lg-6">
                                <table class="table">
                                      <tr>
                                          <td>Фамилия:</td>
                                          <td>{{ $worker->surname }}</td>
                                      </tr>
                                      <tr>
                                          <td>Имя:</td>
                                          <td>{{ $worker->name }}</td>
                                      </tr>
                                      <tr>
                                          <td>Отчество:</td>
                                          <td>{{ $worker->lastname }}</td>
                                      </tr>
                                      <tr>
                                          <td>Email:</td>
                                          <td>{{ $worker->email }}</td>
                                      </tr>
                                      <tr>
                                          <td>Телефоны:</td>
                                          <td>
                                              @foreach ($worker->phones as $phone)

                                                  {{ $phone }}

                                                  <br>

                                              @endforeach
                                          </td>
                                      </tr>
                                      <tr>
                                          <td>Дополнительный email:</td>
                                          <td>{{ $worker->additional_email }}</td>
                                      </tr>
                                      <tr>
                                          <td>Дополнительные телефоны:</td>
                                          <td>

                                              @foreach ($worker->additional_phones as $phone)

                                                  {{ $phone }}

                                                  <br>

                                              @endforeach

                                          </td>
                                      </tr>
                                      <tr>
                                          <td>Комментарий:</td>
                                          <td>{{ $worker->comment }}</td>
                                      </tr>
                                </table>
                            </div>
                        </div>
                        <!-- /.box -->
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop