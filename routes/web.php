<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->middleware('access')->name('main');

Route::get('/news', 'NewsController@index')->name('news');
Route::post('/set-enter-pin', 'NewsController@enterPin')->name('set.enter.pin');

// Authentication Routes...
Route::get('login', 'Auth\LoginController@showLoginForm')->middleware('access')->name('login');
Route::post('login', 'Auth\LoginController@login')->middleware('access');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->middleware('access')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->middleware('access')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->middleware('access')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset')->middleware('access');

Route::group(['prefix' => '/workers', 'middleware' => ['auth', 'access']], function() {
    Route::get('/', 'User\WorkerController@index')->name('worker.index');
    Route::get('/create', 'User\WorkerController@create')->name('worker.create');
    Route::post('/store', 'User\WorkerController@store')->name('worker.store');
    Route::get('/edit/{worker}', 'User\WorkerController@edit')->name('worker.edit');
    Route::get('/view/{worker}', 'User\WorkerController@view')->name('worker.view');
    Route::put('/update/{worker}', 'User\WorkerController@update')->name('worker.update');
    Route::delete('/delete/{worker}', 'User\WorkerController@delete')->name('worker.delete');
});

Route::group(['prefix' => '/shops', 'middleware' => ['auth', 'access']], function() {
    Route::get('/', 'Shop\ShopController@index')->name('shop.index');
    Route::get('/create', 'Shop\ShopController@create')->name('shop.create');
    Route::post('/store', 'Shop\ShopController@store')->name('shop.store');
    Route::get('/edit/{shop}', 'Shop\ShopController@edit')->name('shop.edit');
    Route::get('/view/{shop}', 'Shop\ShopController@view')->name('shop.view');
    Route::put('/update/{shop}', 'Shop\ShopController@update')->name('shop.update');
    Route::delete('/delete/{shop}', 'Shop\ShopController@delete')->name('shop.delete');
});

Route::group(['prefix' => '/individual-clients', 'middleware' => ['auth', 'access']], function() {
    Route::get('/', 'Client\IndividualClientController@index')->name('individual.client.index');
    Route::get('/create', 'Client\IndividualClientController@create')->name('individual.client.create');
    Route::post('/store', 'Client\IndividualClientController@store')->name('individual.client.store');
    Route::get('/edit/{client}', 'Client\IndividualClientController@edit')->name('individual.client.edit');
    Route::get('/view/{client}', 'Client\IndividualClientController@view')->name('individual.client.view');
    Route::put('/update/{client}', 'Client\IndividualClientController@update')->name('individual.client.update');
    Route::delete('/delete/{client}', 'Client\IndividualClientController@delete')->name('individual.client.delete');
});

Route::group(['prefix' => '/legal-clients', 'middleware' => ['auth', 'access']], function() {
    Route::get('/', 'Client\LegalClientController@index')->name('legal.client.index');
    Route::get('/create', 'Client\LegalClientController@create')->name('legal.client.create');
    Route::post('/store', 'Client\LegalClientController@store')->name('legal.client.store');
    Route::get('/edit/{client}', 'Client\LegalClientController@edit')->name('legal.client.edit');
    Route::get('/view/{client}', 'Client\LegalClientController@view')->name('legal.client.view');
    Route::put('/update/{client}', 'Client\LegalClientController@update')->name('legal.client.update');
    Route::delete('/delete/{client}', 'Client\LegalClientController@delete')->name('legal.client.delete');
});

Route::group(['prefix' => '/ajax', 'middleware' => ['auth', 'access']], function() {
    Route::get('/get-client-contact-full-name-by-select', 'Client\ClientController@getClientContactFullName')->name('ajax.get.client.by.select');

});

Route::group(['prefix' => '/factory', 'middleware' => ['auth', 'access']], function() {
    Route::get('/', 'FactoryController@index')->name('factory.index');
    Route::get('/create', 'FactoryController@create')->name('factory.create');
    Route::post('/store', 'FactoryController@store')->name('factory.store');
    Route::get('/edit/{factory}', 'FactoryController@edit')->name('factory.edit');
    Route::get('/view/{factory}', 'FactoryController@view')->name('factory.view');
    Route::put('/update/{factory}', 'FactoryController@update')->name('factory.update');
    Route::delete('/delete/{factory}', 'FactoryController@delete')->name('factory.delete');
});

Route::group(['prefix' => '/documents', 'middleware' => ['auth', 'access']], function() {

    // documents
    Route::get('/', 'Document\DocumentController@index')->name('document.index');
    Route::get('/table', 'Document\DocumentController@table')->name('document.table.index');
    Route::get('/create', 'Document\DocumentController@create')->name('document.create');
    Route::post('/store', 'Document\DocumentController@store')->name('document.store');
    Route::get('/view/{document}', 'Document\DocumentController@view')->name('document.view');
    Route::get('/edit/{document}', 'Document\DocumentController@edit')->name('document.edit');
    Route::put('/update/{document}', 'Document\DocumentController@update')->name('document.update');
    Route::delete('/delete/{document}', 'Document\DocumentController@delete')->name('document.delete');
    Route::post('/calculation', 'Document\DocumentController@calculationSum')->name('document.calculation');
    Route::post('/calculation-surcharge/{document}', 'Document\DocumentController@calculationSumSurcharge')->name('document.calculation.surcharge');
    Route::post('/save-additional-field/{document}', 'Document\DocumentController@saveAdditionalField')->name('document.save.additionals');
    Route::post('/new-payment/{document}', 'Document\DocumentController@createNewDocumentPayment')->name('document.new.payment');
    Route::post('/calculation-finish-date', 'Document\DocumentController@calculationDateFinish')->name('document.calculation.date-finish');
    Route::get('/get-attach-template-form/{document}', 'Document\DocumentController@getAttachDocumentTemplateFrom')->name('document.get.attach.template.form');
    Route::post('/attach-template/{document}', 'Document\DocumentController@attachDocumentTemplate')->name('document.attach.template');
    Route::get('/get-attach-product-form/{document}', 'ProductController@getAttachProductForm')->name('document.get.attach.product.form');
    Route::post('/attach-product/{document}', 'ProductController@attachProductToDocument')->name('document.attach.product');
    Route::get('/get-edit-product-form/{product}', 'ProductController@getEditProductForm')->name('document.get.edit.product.form');
    Route::post('/update-product/{product}', 'ProductController@updateProduct')->name('document.update.product');
    Route::post('/delete-product/{product}', 'ProductController@delete')->name('document.product.delete');

    // document templates
    Route::get('/templates', 'Document\TemplateController@index')->name('document.template.index');
    Route::get('/templates/show-with-document/{document}/{template}', 'Document\TemplateController@showTemplateWithDocument')->name('document.template.with.document');
    Route::delete('/templates/delete-relation-with-document/{document}/{template}', 'Document\TemplateController@deleteTemplateRelationWithDocument')->name('document.template.delete.relation.with.document');
    Route::get('/templates/create', 'Document\TemplateController@create')->name('document.template.create');
    Route::post('/templates/store', 'Document\TemplateController@store')->name('document.template.store');
    Route::get('/templates/edit/{template}', 'Document\TemplateController@edit')->name('document.template.edit');
    Route::get('/templates/view/{template}', 'Document\TemplateController@view')->name('document.template.view');
    Route::put('/templates/update/{template}', 'Document\TemplateController@update')->name('document.template.update');
    Route::delete('/templates/delete/{template}', 'Document\TemplateController@delete')->name('document.template.delete');
    Route::get('/templates/to-pdf/{document}/{template}', 'Document\TemplateController@toPdf')->name('document.template.with.document.topdf');
    Route::get('/templates/to-doc/{document}/{template}', 'Document\TemplateController@toDoc')->name('document.template.with.document.todoc');

    Route::get('/service-account/get-add-form/{document}', 'Document\ServiceAccountController@getAddServiceAccountForm')->name('document.service-account.form');
    Route::get('/service-account/change-type-payment/{document}', 'Document\ServiceAccountController@changeTypePayment')->name('document.service-account.change-type-payment');
    Route::post('/service-account/attach-service-account/{document}', 'Document\ServiceAccountController@attachServiceAccountToDocument')->name('document.service-account.attach');
    Route::get('/service-account/view/{serviceAccount}', 'Document\ServiceAccountController@view')->name('document.service-account.view');
    Route::get('/service-account/pdf/{serviceAccount}', 'Document\ServiceAccountController@pdf')->name('document.service-account.pdf');

    Route::get('/factory-payments/get-form/{document}/', 'Document\FactoryPaymentController@getForm')->name('document.factory.payment.create');
    Route::post('/factory-payments/store/{document}/', 'Document\FactoryPaymentController@store')->name('document.factory.payment.store');
    Route::get('/factory-payments/edit/{factoryPayment}/', 'Document\FactoryPaymentController@edit')->name('document.factory.payment.edit');
    Route::post('/factory-payments/edit/{factoryPayment}/', 'Document\FactoryPaymentController@update')->name('document.factory.payment.update');
});

Route::group(['prefix' => '/admin', 'middleware' => ['auth', 'access']], function() {
    Route::get('/update-calendar', 'User\AdminController@updateCalendarStructure')->name('admin.calendar.update');
    Route::get('/roles', 'User\AdminController@listRoles')->name('admin.roles.index');
    Route::get('/roles/edit/{role}', 'User\AdminController@editRole')->name('admin.roles.edit');
    Route::put('/roles/update/{role}', 'User\AdminController@updateRole')->name('admin.roles.update');

    Route::group(['prefix' => '/settings', 'middleware' => ['auth', 'access']], function() {

        Route::get('/', 'User\AdminController@listSettings')->name('admin.settings.index');
        Route::get('/{setting}', 'User\AdminController@editSetting')->name('admin.settings.edit');
        Route::put('/{setting}', 'User\AdminController@updateSetting')->name('admin.settings.update');
    });
});
