<?php

namespace App\Services;

use App\Models\Factory;

/**
 * Factory service
 */
class FactoryService
{
    /**
     * Get list factories with pagination.
     *
     * @param int $countOnPage Count items on page
     * @return
     */
    public function paginate($countOnPage = 15)
    {
        return Factory::paginate($countOnPage);
    }

    /**
     * Create new factory.
     *
     * @param array $fields Factory fields
     * @return \App\Models\Factory
     */
    public function createNewFactory($fields)
    {
        return Factory::create($fields);
    }

    /**
     * Update factory.
     *
     * @param \App\Models\Factory $factory Factory model
     * @param array $field Factory fields
     * @return void
     */
    public function updateFactory(Factory $factory, $fields)
    {
        $factory->update($fields);
    }

    /**
     * Get list factories.
     *
     * @return \App\Models\Factory[]
     */
    public function getListFactories()
    {
        return Factory::all();
    }
}
