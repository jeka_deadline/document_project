@extends('layouts.bootstrap')

@section('breadcrumbs')

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Главная</a></li>
            <li class="breadcrumb-item"><a href="{{ route('factory.index') }}">Список фабрик</a></li>
            <li class="breadcrumb-item active" aria-current="page">Просмотр фабрики</li>
        </ol>
    </nav>

@stop
@section('content')

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <table style="width: 100%">
                        <tr>
                            <td>
                                <h4>Информация об фабрике</h4>
                            </td>
                            <td style="text-align: right"><a class="btn btn-warning" style="font-size: 14px" href="{{ route('factory.edit', ['factory' => $factory]) }}">Редактировать</a></td>
                        </tr>
                    </table>
                </div>
                <div class="panel-body">
                    <div class="box box-info">
                        <div class="box-body">
                            <div class="col-lg-3">
                                <div  align="center">
                                    <img alt="User Pic" src="/images/Factory.png" id="profile-image1" class="img-responsive">
                                </div>
                                <br>
                                <!-- /input-group -->
                            </div>
                            <div class="col-lg-3">
                                <div>
                                    <h3 style="color:#00b1b1;">Название</h3>
                                    <span>
                                        <p>{{ $factory->name }}</p>
                                    </span>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <table class="table">
                                      <tr>
                                          <td>Страна:</td>
                                          <td>{{ $factory->country }}</td>
                                      </tr>
                                      <tr>
                                          <td>Описание:</td>
                                          <td>{{ $factory->description }}</td>
                                      </tr>
                                </table>
                            </div>
                        </div>
                        <!-- /.box -->
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop