@extends('layouts.bootstrap')

@section('breadcrumbs')

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Главная</a></li>
            <li class="breadcrumb-item"><a href="{{ route('individual.client.index') }}">Клиенты: физические лица</a></li>
            <li class="breadcrumb-item active" aria-current="page">Просмотр клиента</li>
        </ol>
    </nav>

@stop
@section('content')

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <table style="width: 100%">
                        <tr>
                            <td>
                                <h4>Информация об клиенте</h4>
                            </td>

                            <?php if (Auth::user()->isCanEditIndividualClient()) : ?>

                                <td style="text-align: right"><a class="btn btn-warning" style="font-size: 14px" href="{{ route('individual.client.edit', ['client' => $client]) }}">Редактировать</a></td>

                            <?php endif; ?>

                        </tr>
                    </table>
                </div>
                <div class="panel-body">
                    <div class="box box-info">
                        <div class="box-body">
                            <div class="col-lg-3">
                                <div  align="center">
                                    <img alt="User Pic" src="/images/nobody_m.original.jpg" id="profile-image1" class="img-circle img-responsive">
                                </div>
                                <br>
                                <!-- /input-group -->
                            </div>
                            <div class="col-lg-3">
                                <h4 style="color:#00b1b1;">{{ $client->full_name }}</h4>
                                </span>
                                <span>
                                    <p>Aspirant</p>
                                </span>
                            </div>
                            <div class="col-lg-6">
                                <table class="table">
                                      <tr>
                                          <td>Фамилия:</td>
                                          <td>{{ $client->surname }}</td>
                                      </tr>
                                      <tr>
                                          <td>Имя:</td>
                                          <td>{{ $client->name }}</td>
                                      </tr>
                                      <tr>
                                          <td>Отчество:</td>
                                          <td>{{ $client->lastname }}</td>
                                      </tr>
                                      <tr>
                                          <td>Адрес регистрации:</td>
                                          <td>{{ $client->register_address }}</td>
                                      </tr>
                                      <tr>
                                          <td>Адрес доставки:</td>
                                          <td>{{ $client->shipping_address }}</td>
                                      </tr>
                                      <tr>
                                          <td>Email:</td>
                                          <td>{{ $client->email }}</td>
                                      </tr>
                                      <tr>
                                          <td>Телефоны:</td>
                                          <td>
                                              @foreach ($client->phones as $phone)

                                                  {{ $phone }}

                                                  <br>

                                              @endforeach
                                          </td>
                                      </tr>
                                      <tr>
                                          <td>Дополнительная фамилия:</td>
                                          <td>{{ $client->additional_surname }}</td>
                                      </tr>
                                      <tr>
                                          <td>Дополнительное имя:</td>
                                          <td>{{ $client->additional_name }}</td>
                                      </tr>
                                      <tr>
                                          <td>Дополнительное отчество:</td>
                                          <td>{{ $client->additional_lastname }}</td>
                                      </tr>
                                      <tr>
                                          <td>Дополнительный email:</td>
                                          <td>{{ $client->additional_email }}</td>
                                      </tr>
                                      <tr>
                                          <td>Дополнительные телефоны:</td>
                                          <td>

                                              @foreach ($client->additional_phones as $phone)

                                                  {{ $phone }}

                                                  <br>

                                              @endforeach

                                          </td>
                                      </tr>
                                      <tr>
                                          <td>Комментарий:</td>
                                          <td>{{ $client->comment }}</td>
                                      </tr>
                                </table>
                            </div>
                        </div>
                        <!-- /.box -->
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop