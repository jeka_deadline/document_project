<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeFieldsInClientTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('individual_clients', function (Blueprint $table) {
            $table->string('register_address')->nullable()->change();
            $table->string('email', 100)->nullable()->change();
        });

        Schema::table('legal_clients', function (Blueprint $table) {
            $table->string('email', 100)->nullable()->change();
            $table->string('general_director_email', 100)->nullable()->change();
            $table->string('personal_email', 100)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
