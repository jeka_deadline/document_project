<?php

namespace App\Http\Controllers\Shop;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Shop\Shop;
use App\Http\Requests\ShopRequest;
use App\Models\User\Worker;
use App\Services\ShopService;
use Illuminate\Support\Facades\Auth;

/**
 * Shop controller.
 */
class ShopController extends Controller
{
    /**
     * Shop service.
     *
     * @var \App\Services\WorkerService $shopService
     * @access private
     */
    private $shopService;

    /**
     * Controller constructor.
     *
     * @param \App\Services\ShopService $shopService Shop service
     * @return void
     */
    public function __construct(ShopService $shopService)
    {
        $this->shopService = $shopService;
    }

    /**
     * Display page shops.
     *
     * @param \Illuminate\Http\Request $request Request
     * @return \Illuminate\Support\Facades\View
     */
    public function index(Request $request)
    {
        if (!Auth::user()->isCanViewShops()) {
            $this->accessDanied();
        }

        return view('shop.shop.index', [
            'shops' => $this->shopService->paginate(),
        ]);
    }

    /**
     * Display page with shop form for create new shop.
     *
     * @param \Illuminate\Http\Request $request Request
     * @return \Illuminate\Support\Facades\View
     */
    public function create(Request $request)
    {
        if (!Auth::user()->isCanCreateShop()) {
            $this->accessDanied();
        }

        $listGeneralDirectors = Worker::generalDirectors();
        $listShopDirectors = Worker::shopDirectors();
        $listDeputyShopDirectors = Worker::deputyShopDirectors();

        return view('shop.shop.create', compact('listGeneralDirectors', 'listShopDirectors', 'listDeputyShopDirectors'));
    }

    /**
     * Store new shop.
     *
     * @param \App\Http\Requests\ShopRequest $request Shop request
     * @return \Illuminate\Support\Facades\Redirect
     */
    public function store(ShopRequest $request)
    {
        if (!Auth::user()->isCanCreateShop()) {
            $this->accessDanied();
        }

        $this->shopService->createNewShop($request->all());

        return redirect()->route('shop.index');
    }

    /**
     * Display page with shop information.
     *
     * @param \Illuminate\Http\Request $request Request
     * @param \App\Models\Shop\Shop $shop Shop model
     * @return \Illuminate\Support\Facades\View
     */
    public function view(Request $request, Shop $shop)
    {
        if (!Auth::user()->isCanViewShop()) {
            $this->accessDanied();
        }

        return view('shop.shop.view', compact('shop'));
    }

    /**
     * Display page with shop form for edit shop information.
     *
     * @param \Illuminate\Http\Request $request Request
     * @param \App\Models\Shop\Shop $shop Shop model
     * @return \Illuminate\Support\Facades\View
     */
    public function edit(Request $request, Shop $shop)
    {
        if (!Auth::user()->isCanEditShop()) {
            $this->accessDanied();
        }

        $listGeneralDirectors = Worker::generalDirectors();
        $listShopDirectors = Worker::shopDirectors();
        $listDeputyShopDirectors = Worker::deputyShopDirectors();

        $shopDeputyDirectorIds = $this->shopService->getShopDeputyDirectorIds($shop);
        $shopGeneralDirectorId = $this->shopService->getShopGeneralDirectorId($shop);
        $shopDirectorId = $this->shopService->getShopDirectorId($shop);

        return view('shop.shop.edit', [
            'listGeneralDirectors' => $listGeneralDirectors,
            'listShopDirectors' => $listShopDirectors,
            'listDeputyShopDirectors' => $listDeputyShopDirectors,
            'shop' => $shop,
            'shopGeneralDirectorId' => $shopGeneralDirectorId,
            'shopDirectorId' => $shopDirectorId,
            'shopDeputyDirectorIds' => $shopDeputyDirectorIds,
        ]);
    }

    /**
     * Update shop information.
     *
     * @param \App\Http\Requests\ShopRequest $request Shop request
     * @param \App\Models\Shop\Shop $shop Shop model
     * @return \Illuminate\Support\Facades\Redirect
     */
    public function update(ShopRequest $request, Shop $shop)
    {
        if (!Auth::user()->isCanEditShop()) {
            $this->accessDanied();
        }

        $this->shopService->updateShop($shop, $request->all());

        return redirect()->route('shop.index');
    }

    /**
     * Delete shop.
     *
     * @param \Illuminate\Http\Request $request Request
     * @param \App\Models\Shop\Shop $shop Shop model
     * @return \Illuminate\Support\Facades\Redirect
     */
    public function delete(Request $request, Shop $shop)
    {
        if (!Auth::user()->isCanDeleteShop()) {
            $this->accessDanied();
        }

        $shop->delete();

        return redirect(route('shop.index'));
    }
}
