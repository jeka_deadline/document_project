<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Document\Document;

class CreateDocumentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'                  => 'required|in:' . implode(',', array_keys(Document::getDocumentTypes())),
            'prepared'              => 'required',
            'manager_worker_id'     => 'required|exists:workers,id',
            'client'                => 'required',
            'contact_client'        => 'string|nullable',
            'type_payment'          => 'required|in:' . implode(',', array_keys(Document::getPaymentTypes())),
            'state'                 => 'required|in:' . implode(',', array_keys(Document::getStates())),
            'shop_id'               => 'required|exists:shops,id',
            'confirm_pro_form'      => 'max:100',
            'period_execution'      => 'required',
            'date_finish'           => 'required',
            'register_number'       => 'nullable|integer',
            'prepayment_amount_eur' => 'required',
            'percentage_prepayment' => 'required',
            'enter_eur'             => 'required_without_all:enter_usd,enter_rub|numeric|nullable',
            'enter_usd'             => 'required_without_all:enter_eur,enter_rub|numeric|nullable',
            'enter_rub'             => 'required_without_all:enter_eur,enter_usd|numeric|nullable',
        ];
    }
}
