<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\User\Role;
use App\Models\User\UserRole;
use App\Models\ProjectAction;
use App\Models\ProjectActionRole;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function worker()
    {
        return $this->hasOne('App\Models\User\Worker');
    }

    /**
     * Get worker full name.
     *
     * @return string|null
     */
    public function getFullNameAttribute()
    {
        if (!$this->worker) {
            return null;
        }

        return $this->worker->surname . ' ' . $this->worker->name . ' ' . $this->worker->lastname;
    }

    /**
     * Get worker role name.
     *
     * @return null|string.
     */
    public function getRoleNameAttribute()
    {
        if (!$this->worker || !$this->worker->userRole || !$this->worker->userRole->role) {
            return null;
        }

        return $this->worker->userRole->role->name;
    }

    /**
     * Get user role.
     *
     * @return \App\Models\User\Role
     */
    public function getRoleAttribute()
    {
        if (!$this->worker || !$this->worker->userRole || !$this->worker->userRole->role) {
            return null;
        }

        return $this->worker->userRole->role;
    }

    /**
     * Check if user is admin.
     *
     * @return bool
     */
    public function isAdmin()
    {
        $roleAdmin = Role::admin()->first();

        $userAdmin = UserRole::where('user_id', $this->id)
            ->where('role_id', $roleAdmin->id)
            ->first();

        return ($userAdmin) ? true : false;
    }

    // ====================================================================== access =====================================================

    /**
     * Check if user can view list document.
     *
     * @return bool
     */
    public function isCanViewListDocuments()
    {
        return $this->isCan(ProjectAction::ACTION_LIST_DOCUMENTS);
    }

    /**
     * Check if user can create document.
     *
     * @return bool
     */
    public function isCanCreateDocument()
    {
        switch ($this->roleName) {
            case Role::ROLE_ADMIN:
            case null:
                return false;
        }

        if ($this->isAdmin()) {
            return false;
        }

        return $this->isCan(ProjectAction::ACTION_CREATE_DOCUMENT);
    }

    /**
     * Check if user can update document.
     *
     * @return bool
     */
    public function isCanEditDocument()
    {
        return $this->isCan(ProjectAction::ACTION_EDIT_DOCUMENT);
    }

    /**
     * Check if user can view document.
     *
     * @return bool
     */
    public function isCanViewDocument()
    {
        return $this->isCan(ProjectAction::ACTION_VIEW_DOCUMENT);
    }

    /**
     * Check if user can delete document.
     *
     * @return bool
     */
    public function isCanDeleteDocument()
    {
        return $this->isCan(ProjectAction::ACTION_DELETE_DOCUMENT);
    }

    /**
     * Check if user can attach document template to document.
     *
     * @return bool
     */
    public function isCanAttachDocumentTemplate()
    {
        return $this->isCan(ProjectAction::ACTION_ATTACH_DOCUMENT_TEMPLATE);
    }

    /**
     * Check if user can create new payment to document.
     *
     * @return bool
     */
    public function isCanCreateDocumentNewPayment()
    {
        return $this->isCan(ProjectAction::ACTION_CREATE_NEW_PAYMENT);
    }

    /**
     * Check if user can attach product to document.
     *
     * @return bool
     */
    public function isCanAttachDocumentProduct()
    {
        return $this->isCan(ProjectAction::ACTION_ATTACH_PRODUCT);
    }

    /**
     * Check if user can edit attached product to document.
     *
     * @return bool
     */
    public function isCanEditAttachDocumentProduct()
    {
        return $this->isCan(ProjectAction::ACTION_EDIT_ATTACH_PRODUCT);
    }

    /**
     * Check if user can delete attached product to document.
     *
     * @return bool
     */
    public function isCanDeleteAttachDocumentProduct()
    {
        return $this->isCan(ProjectAction::ACTION_DELETE_ATTACH_PRODUCT);
    }

    /**
     * Check if user can create service account to document.
     *
     * @return bool
     */
    public function isCanCreateServiceAccount()
    {
        return $this->isCan(ProjectAction::ACTION_CREATE_SERVICE_ACCOUNT);
    }

    /**
     * Check if user can view service account document.
     *
     * @return bool
     */
    public function isCanViewServiceAccount()
    {
        return $this->isCan(ProjectAction::ACTION_VIEW_SERVICE_ACCOUNT);
    }

    /**
     * Check if user can view service account document in pdf format.
     *
     * @return bool
     */
    public function isCanViewPdfServiceAccount()
    {
        return $this->isCan(ProjectAction::ACTION_SERVICE_ACCOUNT_TO_PDF);
    }

    /**
     * Check if user can view list workers.
     *
     * @return bool
     */
    public function isCanViewWorkers()
    {
        return $this->isCan(ProjectAction::ACTION_LIST_WORKERS);
    }

    /**
     * Check if user can create worker.
     *
     * @return bool
     */
    public function isCanCreateWorker()
    {
        return $this->isCan(ProjectAction::ACTION_CREATE_WORKER);
    }

    /**
     * Check if user can edit worker.
     *
     * @return bool
     */
    public function isCanEditWorker()
    {
        return $this->isCan(ProjectAction::ACTION_EDIT_WORKER);
    }

    /**
     * Check if user can view worker.
     *
     * @return bool
     */
    public function isCanViewWorker()
    {
        return $this->isCan(ProjectAction::ACTION_VIEW_WORKER);
    }

    /**
     * Check if user can delete worker.
     *
     * @return bool
     */
    public function isCanDeleteWorker()
    {
        return $this->isCan(ProjectAction::ACTION_DELETE_WORKER);
    }

    /**
     * Check if user can view list shops.
     *
     * @return bool
     */
    public function isCanViewShops()
    {
        return $this->isCan(ProjectAction::ACTION_LIST_SHOPS);
    }

    /**
     * Check if user can create shop.
     *
     * @return bool
     */
    public function isCanCreateShop()
    {
        return $this->isCan(ProjectAction::ACTION_CREATE_SHOP);
    }

    /**
     * Check if user can view shop.
     *
     * @return bool
     */
    public function isCanViewShop()
    {
        return $this->isCan(ProjectAction::ACTION_VIEW_SHOP);
    }

    /**
     * Check if user can edit shop.
     *
     * @return bool
     */
    public function isCanEditShop()
    {
        return $this->isCan(ProjectAction::ACTION_EDIT_SHOP);
    }

    /**
     * Check if user can delete shop.
     *
     * @return bool
     */
    public function isCanDeleteShop()
    {
        return $this->isCan(ProjectAction::ACTION_DELETE_SHOP);
    }

    /**
     * Check if user can view list individual clients.
     *
     * @return bool
     */
    public function isCanViewIndividualClients()
    {
        return $this->isCan(ProjectAction::ACTION_LIST_INDIVIDUAL_CLIENTS);
    }

    /**
     * Check if user can create individual client.
     *
     * @return bool
     */
    public function isCanCreateIndividualClient()
    {
        return $this->isCan(ProjectAction::ACTION_CREATE_INDIVIDUAL_CLIENT);
    }

    /**
     * Check if user can edit individual client.
     *
     * @return bool
     */
    public function isCanEditIndividualClient()
    {
        return $this->isCan(ProjectAction::ACTION_EDIT_INDIVIDUAL_CLIENT);
    }

    /**
     * Check if user can view individual client.
     *
     * @return bool
     */
    public function isCanViewIndividualClient()
    {
        return $this->isCan(ProjectAction::ACTION_VIEW_INDIVIDUAL_CLIENT);
    }

    /**
     * Check if user can delete individual client.
     *
     * @return bool
     */
    public function isCanDeleteIndividualClient()
    {
        return $this->isCan(ProjectAction::ACTION_DELETE_INDIVIDUAL_CLIENT);
    }

    /**
     * Check if user can view list legal clients.
     *
     * @return bool
     */
    public function isCanViewLegalClients()
    {
        return $this->isCan(ProjectAction::ACTION_LIST_LEGAL_CLIENTS);
    }

    /**
     * Check if user can create legal client.
     *
     * @return bool
     */
    public function isCanCreateLegalClient()
    {
        return $this->isCan(ProjectAction::ACTION_CREATE_LEGAL_CLIENT);
    }

    /**
     * Check if user can edit legal client.
     *
     * @return bool
     */
    public function isCanEditLegalClient()
    {
        return $this->isCan(ProjectAction::ACTION_EDIT_LEGAL_CLIENT);
    }

    /**
     * Check if user can view legal client.
     *
     * @return bool
     */
    public function isCanViewLegalClient()
    {
        return $this->isCan(ProjectAction::ACTION_VIEW_LEGAL_CLIENT);
    }

    /**
     * Check if user can delete legal client.
     *
     * @return bool
     */
    public function isCanDeleteLegalClient()
    {
        return $this->isCan(ProjectAction::ACTION_DELETE_LEGAL_CLIENT);
    }

    /**
     * Check if user can view list factories.
     *
     * @return bool
     */
    public function isCanViewFactories()
    {
        return $this->isCan(ProjectAction::ACTION_LIST_FACTORIES);
    }

    /**
     * Check if user can create factory.
     *
     * @return bool
     */
    public function isCanCreateFactory()
    {
        return $this->isCan(ProjectAction::ACTION_CREATE_FACTORY);
    }

    /**
     * Check if user can edit factory.
     *
     * @return bool
     */
    public function isCanEditFactory()
    {
        return $this->isCan(ProjectAction::ACTION_EDIT_FACTORY);
    }

    /**
     * Check if user can view factory.
     *
     * @return bool
     */
    public function isCanViewFactory()
    {
        return $this->isCan(ProjectAction::ACTION_VIEW_FACTORY);
    }

    /**
     * Check if user can delete factory.
     *
     * @return bool
     */
    public function isCanDeleteFactory()
    {
        return $this->isCan(ProjectAction::ACTION_DELETE_FACTORY);
    }

    /**
     * Check if user can view settings list.
     *
     * @return bool
     */
    public function isCanViewSettings()
    {
        return $this->isCan(ProjectAction::ACTION_LIST_SETTINGS);
    }

    /**
     * Check if user can edit setting.
     *
     * @return bool
     */
    public function isCanEditSetting()
    {
        return $this->isCan(ProjectAction::ACTION_EDIT_SETTING);
    }

    /**
     * Check if user can view document templates list.
     *
     * @return bool
     */
    public function isCanViewDocumentTemplates()
    {
        return $this->isCan(ProjectAction::ACTION_LIST_DOCUMENT_TEMPLATES);
    }

    /**
     * Check if user can create document template.
     *
     * @return bool
     */
    public function isCanCreateDocumentTemplate()
    {
        return $this->isCan(ProjectAction::ACTION_CREATE_DOCUMENT_TEMPLATE);
    }

    /**
     * Check if user can edit document template.
     *
     * @return bool
     */
    public function isCanEditDocumentTemplate()
    {
        return $this->isCan(ProjectAction::ACTION_EDIT_DOCUMENT_TEMPLATE);
    }

    /**
     * Check if user can view document template.
     *
     * @return bool
     */
    public function isCanViewDocumentTemplate()
    {
        return $this->isCan(ProjectAction::ACTION_VIEW_DOCUMENT_TEMPLATE);
    }

    /**
     * Check if user can save as new document template.
     *
     * @return bool
     */
    public function isCanSaveAsDocumentTemplate()
    {
        return $this->isCan(ProjectAction::ACTION_SAVE_AS_DOCUMENT_TEMPLATE);
    }

    /**
     * Check if user can delete document template.
     *
     * @return bool
     */
    public function isCanDeleteDocumentTemplate()
    {
        return $this->isCan(ProjectAction::ACTION_DELETE_DOCUMENT_TEMPLATE);
    }

    /**
     * Check if user can view table documents.
     *
     * @return bool
     */
    public function isCanViewTableDocuments()
    {
        return $this->isCan(ProjectAction::ACTION_TABLE_DOCUMENTS);
    }

    /**
     * Check if user can update calendar.
     *
     * @return bool
     */
    public function isCanUpdateCalendar()
    {
        return $this->isCan(ProjectAction::ACTION_UPDATE_CALENDAR);
    }

    // ============================================================= end access ===========================================================

    /**
     * Check if user can access to action.
     *
     * @access private
     * @param string $action Action.
     * @return bool
     */
    private function isCan($action)
    {
        if ($this->isAdmin()) {
            return true;
        }

        if (!$this->role) {
            return false;
        }

        $projectAction = ProjectAction::where('name', $action)->first();

        if (!$projectAction) {
            return false;
        }

        $access = ProjectActionRole::where([
            ['project_action_id', '=', $projectAction->id],
            ['role_id', '=', $this->role->id],
        ])->first();

        if ($access) {
            return true;
        }

        return false;
    }
}
