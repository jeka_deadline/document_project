<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectActionRole extends Model
{
    protected $table = 'project_action_role';

    protected $fillable = [
        'role_id',
        'project_action_id',
    ];
}
