<?php

use Illuminate\Database\Seeder;

class SettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->insert([
            'title' => 'Пин для входа на сайт',
            'key' => 'enter-pin',
            'value' => '123456',
        ]);
    }
}
