<?php

namespace App\DocumentSearch\Filters;

use Illuminate\Database\Eloquent\Builder;

class Name implements Filterable
{
    public static function apply(Builder $builder, $value)
    {
        return $builder->where('name', $value);
    }
}