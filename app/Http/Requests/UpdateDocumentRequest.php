<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Document\Document;

class UpdateDocumentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'manager_worker_id'     => 'required|exists:workers,id',
            'state'                 => 'required|in:' . implode(',', array_keys(Document::getStates())),
            'order_state'           => 'nullable|in:' . implode(',', array_keys(Document::getFullListOrderStatuses())),
            'confirm_pro_form'      => 'max:100',
            'register_number'       => 'nullable|integer',
        ];
    }
}
