<?php

namespace App\Models\Client;

use Illuminate\Database\Eloquent\Model;

/**
 * Model for table `legal_clients`
 */
class LegalClient extends Model
{
    /**
     * {@inheritdoc}
     *
     * @access protected
     * @var array $fillable
     */
    protected $fillable = [
        'full_name',
        'short_name',
        'address',
        'email',
        'phone',
        'ogrn',
        'inn',
        'kpp',
        'bank',
        'bik',
        'checking_account',
        'general_director_surname',
        'general_director_name',
        'general_director_lastname',
        'general_director_phone',
        'general_director_email',
        'personal_surname',
        'personal_name',
        'personal_lastname',
        'personal_phone',
        'personal_email',
        'shipping_address',
    ];

    /**
     * Get legal client full name + director full name.
     *
     * @return array
     */
    public function getPersonalFullNameAttribute()
    {
        return $this->full_name . '/' . $this->general_director_surname . ' ' . $this->general_director_name . ' ' . $this->general_director_lastname;
    }

    /**
     * Get legal client contact full name.
     *
     * @return array
     */
    public function getContactFullNameAttribute()
    {
        return $this->personal_surname . ' ' . $this->personal_name . ' ' . $this->personal_lastname;
    }
}
