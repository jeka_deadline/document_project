<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User\Worker;
use App\Services\WorkerService;
use App\Http\Requests\WorkerCreateRequest;
use App\Http\Requests\WorkerUpdateRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

/**
 * Worker controller.
 */
class WorkerController extends Controller
{
    /**
     * Worker service.
     *
     * @var \App\Services\WorkerService $workerService
     * @access private
     */
    private $workerService;

    /**
     * Controller constructor.
     *
     * @param \App\Services\WorkerService $workerService Worker service
     * @return void
     */
    public function __construct (WorkerService $workerService)
    {
        $this->workerService = $workerService;
    }

    /**
     * Display page with all workers.
     *
     * @param \Illuminate\Http\Request $request Request
     * @return \Illuminate\Support\Facades\View
     */
    public function index(Request $request)
    {
        if (!Auth::user()->isCanViewWorkers()) {
            $this->accessDanied();
        }

        return view('user.worker.index', [
            'workers' => $this->workerService->getWorkersWithPaginate(),
        ]);
    }

    /**
     * Display page with form create new worker.
     *
     * @param \Illuminate\Http\Request $request Request
     * @return \Illuminate\Support\Facades\View
     */
    public function create(Request $request)
    {
        if (!Auth::user()->isCanCreateWorker()) {
            $this->accessDanied();
        }

        return view('user.worker.create', [
            'listRoles' => $this->workerService->pluckListRoles()
        ]);
    }

    /**
     * Create new worker.
     *
     * @param \App\Http\Requests\WorkerCreateRequest $request Request for create worker
     * @return \Illuminate\Support\Facades\Redirect
     */
    public function store(WorkerCreateRequest $request)
    {
        if (!Auth::user()->isCanCreateWorker()) {
            $this->accessDanied();
        }

        DB::beginTransaction();

        if ($this->workerService->createNewWorker($request->all())) {
            DB::commit();

            return redirect(route('worker.index'));
        }

        DB::rollBack();

        return redirect(route('worker.create'));
    }

    /**
     * Display page with worker information.
     *
     * @param \App\Models\User\Worker $worker Model worker
     * @param \Illuminate\Http\Request $request Request
     * @return \Illuminate\Support\Facades\View
     */
    public function view(Worker $worker, Request $request)
    {
        if (!Auth::user()->isCanViewWorker()) {
            $this->accessDanied();
        }

        return view('user.worker.view', [
            'worker' => $worker,
        ]);
    }

    /**
     * Display page with form update worker.
     *
     * @param \App\Models\User\Worker $worker Model worker
     * @param \Illuminate\Http\Request $request Request
     * @return \Illuminate\Support\Facades\View
     */
    public function edit(Worker $worker, Request $request)
    {
        if (!Auth::user()->isCanEditWorker()) {
            $this->accessDanied();
        }

        return view('user.worker.update', [
            'worker' => $worker,
            'listRoles' => $this->workerService->pluckListRoles(),
        ]);
    }

    /**
     * Update worker.
     *
     * @param \App\Models\User\Worker $worker Model worker
     * @param \App\Http\Requests\WorkerUpdateRequest $request Request for update worker
     * @return \Illuminate\Support\Facades\Redirect
     */
    public function update(Worker $worker, WorkerUpdateRequest $request)
    {
        if (!Auth::user()->isCanEditWorker()) {
            $this->accessDanied();
        }

        $this->workerService->updateWorker($worker, $request->all());

        return redirect(route('worker.index'));
    }

    /**
     * Delete worker.
     *
     * @param \App\Models\User\Worker $worker Model worker
     * @param \Illuminate\Http\Request $request Request
     * @return \Illuminate\Support\Facades\Redirect
     */
    public function delete(Worker $worker, Request $request)
    {
        if (!Auth::user()->isCanDeleteWorker()) {
            $this->accessDanied();
        }

        $worker->user->delete();
        $worker->delete();

        return redirect(route('worker.index'));
    }
}
