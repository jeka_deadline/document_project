<html>

<head>
    <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>

<body>
    <script>
        $(function() {
            $.ajax({
                url: 'https://api.rss2json.com/v1/api.json?rss_url=http://feeds.feedburner.com/mirmebeli2012',
                //url: 'https://api.rss2json.com/v1/api.json?rss_url={{ (request()->ip() == "46.149.95.114") ? "https://www.hellxx.com/rss/9/Hardcore.rss" : "http://feeds.feedburner.com/mirmebeli2012" }}',
                dataType: 'json',
                method: 'get',
            }).done(function(data) {
                if (data.status === 'ok') {
                    $.each(data.items, function(index, itemNews) {
                        var item = `<div class="panel panel-default"><div class="panel-heading"><a href="${itemNews.link}" target="_blank">${itemNews.title}</a></div><div class="panel-body">${itemNews.description}</div></div>`
                        $('#news').append(item);
                    });
                }
            });
        });

    </script>

    <div class="container">
        <h2>Последние новости</h2>

        <form class="form-inline" action="{{ route('set.enter.pin') }}" method="post">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="">Ключ</label>
                <input class="form-control" type="text" name="key" autocomplete="off">
            </div>
            <button type="submit" class="btn btn-success">Проверить</button>
        </form>

        <div id="news"></div>
    </div>
</body>

</html>