<?php

namespace App\Http\Controllers\Document;

use App\Models\Document\FactoryPayment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Document\Document;
use App\Services\FactoryPaymentService;

/**
 * Factory payment controller.
 */
class FactoryPaymentController extends Controller
{
    /**
     * Factory payment service.
     *
     * @access private
     * @var \App\Services\FactoryPaymentService $factoryPaymentService
     */
    private $factoryPaymentService;

    /**
     * Controller constructor.
     *
     * @param \App\Services\FactoryPaymentService $factoryPaymentService Factory payment service
     * @return void
     */
    public function __construct(FactoryPaymentService $factoryPaymentService)
    {
        $this->factoryPaymentService = $factoryPaymentService;
    }

    public function getForm(Request $request, Document $document)
    {
        $response[ 'content' ] = view('document.view_modal_new_factory_payment', compact('document'))->render();

        return response()->json($response);
    }

    public function store(Request $request, Document $document)
    {
        return response()->json([
            'status' => $this->factoryPaymentService->createNewPayment($request->all(), $document),
            'content' => view('document.view_table_factory_payments', compact('document'))->render(),
            'sumBunkTransfer' => $document->fullSumBankTransfer,
            'sumBankTransferRemainder' => $document->sumBankTransferRemainder,
            'documentId' => $document->id,
            'costPrice' => $document->costPrice,
        ]);
    }

    public function edit(Request $request, FactoryPayment $factoryPayment)
    {
        $response[ 'content' ] = view('document.view_modal_edit_factory_payment', compact('factoryPayment'))->render();

        return response()->json($response);
    }

    public function update(Request $request, FactoryPayment $factoryPayment)
    {
        return response()->json([
            'status' => $this->factoryPaymentService->updatePayment($request->all(), $factoryPayment),
            'content' => view('document.view_table_factory_payments', [
                'document' => $factoryPayment->document,
            ])->render(),
            'sumBunkTransfer' => $factoryPayment->document->fullSumBankTransfer,
            'sumBankTransferRemainder' => $factoryPayment->document->sumBankTransferRemainder,
            'documentId' => $factoryPayment->document->id,
            'costPrice' => $factoryPayment->document->costPrice,
        ]);
    }
}
