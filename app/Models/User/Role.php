<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

/**
 * Model class for table `roles`.
 */
class Role extends Model
{
    /**
     * Constant for role admin.
     *
     * @var string
     */
    const ROLE_ADMIN = 'admin';

    /**
     * Constant for role consultant.
     *
     * @var string
     */
    const ROLE_CONSULTANT = 'consultant';

    /**
     * Constant for role director.
     *
     * @var string
     */
    const ROLE_DIRECTOR = 'director';

    /**
     * Constant for role shop director.
     *
     * @var string
     */
    const ROLE_SHOP_DIRECTOR = 'shop_director';

    /**
     * Constant for role shop deputy director.
     *
     * @var string
     */
    const ROLE_SHOP_DEPUTY_DIRECTOR = 'shop_deputy_director';

    /**
     * Constant for role office manager.
     *
     * @var string
     */
    const ROLE_OFFICE_MANAGER = 'office_manager';

    /**
     * Constant for role manager.
     *
     * @var string
     */
    const ROLE_MANAGER = 'manager';

    /**
     * Scope for get role director.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeDirector(Builder $query)
    {
        return $query->where('name', self::ROLE_DIRECTOR);
    }

    /**
     * Scope for get role shop director.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeShopDirector(Builder $query)
    {
        return $query->where('name', self::ROLE_SHOP_DIRECTOR);
    }

    /**
     * Scope for get role shop deputy director.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeShopDeputyDirector(Builder $query)
    {
        return $query->where('name', self::ROLE_SHOP_DEPUTY_DIRECTOR);
    }

    /**
     * Scope for get role manager.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeManager(Builder $query)
    {
        return $query->where('name', self::ROLE_MANAGER);
    }

    /**
     * Scope for get role admin.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeAdmin(Builder $query)
    {
        return $query->where('name', self::ROLE_ADMIN);
    }

    /**
     * Scope for get role admin.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWithoutAdmin(Builder $query)
    {
        return $query->where('name', '<>', self::ROLE_ADMIN);
    }

    /**
     * Get human role name.
     *
     * @return string
     */
    public function getHumanNameAttribute()
    {
        switch ($this->name) {
            case self::ROLE_ADMIN:
                return 'Администратор';
            case self::ROLE_MANAGER:
                return 'Менеджер';
            case self::ROLE_DIRECTOR:
                return 'Генеральный директор';
            case self::ROLE_SHOP_DIRECTOR:
                return 'Директор магазина';
            case self::ROLE_CONSULTANT:
                return 'Консультант';
            case self::ROLE_OFFICE_MANAGER:
                return 'Офис менеджер';
            case self::ROLE_SHOP_DEPUTY_DIRECTOR:
                return 'Зам. директора магазина';
        }
    }
}
