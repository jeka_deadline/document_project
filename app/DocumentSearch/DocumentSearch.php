<?php

namespace App\DocumentSearch;

use App\Models\Document\Document;
use Illuminate\Http\Request;
use App\DocumentSearch\Filters\Name;
use App\DocumentSearch\Filters\Worker;
use App\DocumentSearch\Filters\Manager;
use App\DocumentSearch\Filters\Shop;
use App\DocumentSearch\Filters\RegisterNumber;
use App\DocumentSearch\Filters\PaymentType;
use App\DocumentSearch\Filters\Year;
use App\DocumentSearch\Filters\Month;
use App\DocumentSearch\Filters\DateFinish;
use App\DocumentSearch\Filters\Factory;
use App\DocumentSearch\Filters\ServiceAccount;
use App\DocumentSearch\Filters\Additional;
use App\DocumentSearch\Filters\ProformDateConfirm;
use App\DocumentSearch\Filters\FactoryPayment;

class DocumentSearch
{
    public static function apply(Request $request)
    {
        $query = (new Document)->newQuery();

        if ($request->has('name')) {
            $query = Name::apply($query, $request->name);
        }

        if ($request->has('worker')) {
            $query = Worker::apply($query, $request->worker);
        }

        if ($request->has('manager')) {
            $query = Manager::apply($query, $request->manager);
        }

        if ($request->has('shop')) {
            $query = Shop::apply($query, $request->shop);
        }

        if ($request->has('register_number')) {
            $query = RegisterNumber::apply($query, $request->register_number);
        }

        if ($request->has('payment_type')) {
            $query = PaymentType::apply($query, $request->payment_type);
        }

        if ($request->has('year')) {
            $query = Year::apply($query, $request->year);
        }

        if ($request->has('month')) {
            $query = Month::apply($query, $request->month);
        }

        if ($request->has('date_finish')) {
            $query = DateFinish::apply($query, $request->date_finish);
        }

        if ($request->has('factory')) {
            $query = Factory::apply($query, $request->factory);
        }

        if ($request->has('service_account')) {
            $query = ServiceAccount::apply($query, $request->service_account);
        }

        if ($request->has('factory_payment')) {
            $query = FactoryPayment::apply($query, $request->factory_payment);
        }

        if ($request->has('additional')) {
            $query = Additional::apply($query, $request->additional);
        }

        return $query->get();
    }
}