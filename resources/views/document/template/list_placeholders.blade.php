<label for="">Плейсхолдеры</label>
<table class="table table-bordered">
    <thead>
          <tr>
              <th>Вид</th>
              <th>Описание</th>
          </tr>
    </thead>
    <tbody>
        <tr>
            <td>{date}</td>
            <td>Дата создания документа</td>
        </tr>
        <tr>
            <td>{number}</td>
            <td>Номер договора</td>
        </tr>
        <tr>
            <td>{sum}</td>
            <td>Сумма договора</td>
        </tr>
        <tr>
            <td>{recipeSum}</td>
            <td>Сумма договора прописью</td>
        </tr>
        <tr>
            <td>{dateFinish}</td>
            <td>Дата погашения</td>
        </tr>
        <tr>
            <td>{fullNameClient}</td>
            <td>ФИО клиента</td>
        </tr>
        <tr>
            <td>{clientAddress}</td>
            <td>Адрес клиента</td>
        </tr>
        <tr>
            <td>{clientPhone}</td>
            <td>Телефон клиента</td>
        </tr>
        <tr>
            <td>{clientEmail}</td>
            <td>Email клиента</td>
        </tr>
        <tr>
            <td>{clientAdditionalPhones}</td>
            <td>Дополнительные номера клиента</td>
        </tr>
        <tr>
            <td>{legalClientFullName}</td>
            <td>Полное наименование организации (для юридических клиентов)</td>
        </tr>
        <tr>
            <td>{legalClientOgrn}</td>
            <td>ОГРН организации (для юридических клиентов)</td>
        </tr>
        <tr>
            <td>{legalClientInn}</td>
            <td>ИНН организации (для юридических клиентов)</td>
        </tr>
        <tr>
            <td>{legalClientKpp}</td>
            <td>КПП организации (для юридических клиентов)</td>
        </tr>
        <tr>
            <td>{legalClientBank}</td>
            <td>Банк (для юридических клиентов)</td>
        </tr>
        <tr>
            <td>{legalClientBik}</td>
            <td>БИК (для юридических клиентов)</td>
        </tr>
        <tr>
            <td>{legalClientCheckingAccount}</td>
            <td>р\с организации (для юридических клиентов)</td>
        </tr>
        <tr>
            <td>{legalClientAddressOrganization}</td>
            <td>Адрес организации (для юридических клиентов)</td>
        </tr>
        <tr>
            <td>{fullNameGeneralDirector}</td>
            <td>ФИО генерального директора</td>
        </tr>
        <tr>
            <td>{fullNameDirector}</td>
            <td>ФИО директора</td>
        </tr>
        <tr>
            <td>{fullNameDeputyDirector}</td>
            <td>ФИО зам. директора</td>
        </tr>
        <tr>
            <td>{organizationFullName}</td>
            <td>Полное наименование организации</td>
        </tr>
        <tr>
            <td>{percentagePrepayment}</td>
            <td>Процент предоплаты</td>
        </tr>
        <tr>
            <td>{percentageCurrrency}</td>
            <td>Процент курса</td>
        </tr>
        <tr>
            <td>{periodExcecution}</td>
            <td>Количество рабочих дней</td>
        </tr>
        <tr>
            <td>{ogrn}</td>
            <td>ОГРН организации</td>
        </tr>
        <tr>
            <td>{inn}</td>
            <td>ИНН организации</td>
        </tr>
        <tr>
            <td>{kpp}</td>
            <td>КПП организации</td>
        </tr>
        <tr>
            <td>{bik}</td>
            <td>БИК организации</td>
        </tr>
        <tr>
            <td>{bank}</td>
            <td>Банк организации</td>
        </tr>
        <tr>
            <td>{checkingAccount}</td>
            <td>р\с организации</td>
        </tr>
        <tr>
            <td>{addressOrganization}</td>
            <td>Адрес организации</td>
        </tr>
        <tr>
            <td>{dateVeksel}</td>
            <td>Дата векселя</td>
        </tr>
        <tr>
            <td>{dateFinishVeksel}</td>
            <td>Дата окончания векселя</td>
        </tr>
        <tr>
            <td>{numberVeksel}</td>
            <td>Номер векселя</td>
        </tr>
        <tr>
            <td>{firstPrepaymentSum}</td>
            <td>Сумма первой предоплаты для договора цифрами</td>
        </tr>
        <tr>
            <td>{recipeFirstPrepaymentSum}</td>
            <td>Сумма первой предоплаты для договора прописью</td>
        </tr>
        <tr>
            <td colspan=2>Падежи:<br>
              G - родительный,<br>
              D - дательный,<br>
              A - винительный,<br>
              I - творительный,<br>
              P - предложный<br>
              Пример:<br>
              {fullNameDirector|G} - Козаченко Елены Викторовны<br>
              {fullNameDirector|D} - Козаченко Елене Викторовне<br>
              {fullNameDirector|A} - Козаченко Елену Викторовну<br>
              {fullNameDirector|I} - Козаченко Еленой Викторовной<br>
              {fullNameDirector|P} - Козаченко Елене Викторовне
            </td>
        </tr>
    </tbody>
</table>