<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Factory;
use App\Http\Requests\FactoryRequest;
use App\Services\FactoryService;
use Illuminate\Support\Facades\Auth;

/**
 * Factory controller.
 */
class FactoryController extends Controller
{
    /**
     * Factory service.
     *
     * @access private
     * @var \App\Services\WorkerService $factoryService
     */
    private $factoryService;

    /**
     * Controller constructor.
     *
     * @param \App\Services\FactoryService $factoryService Factory service
     * @return void
     */
    public function __construct(FactoryService $factoryService)
    {
        $this->factoryService = $factoryService;
    }

    /**
     * Display page factories.
     *
     * @param \Illuminate\Http\Request $request Request
     * @return \Illuminate\Support\Facades\View
     */
    public function index(Request $request)
    {
        if (!Auth::user()->isCanViewFactories()) {
            $this->accessDanied();
        }

        return view('factory.index', [
            'factories' => $this->factoryService->paginate(),
        ]);
    }

    /**
     * Display page with factory form for create new factory.
     *
     * @param \Illuminate\Http\Request $request Request
     * @return \Illuminate\Support\Facades\View
     */
    public function create(Request $request)
    {
        if (!Auth::user()->isCanCreateFactory()) {
            $this->accessDanied();
        }

        return view('factory.create');
    }

    /**
     * Store new factory.
     *
     * @param \App\Http\Requests\FactoryRequest $request Factory request
     * @return \Illuminate\Support\Facades\Redirect
     */
    public function store(FactoryRequest $request)
    {
        if (!Auth::user()->isCanCreateFactory()) {
            $this->accessDanied();
        }

        $this->factoryService->createNewFactory($request->all());

        return redirect()->route('factory.index');
    }

    /**
     * Display page with factory information.
     *
     * @param \Illuminate\Http\Request $request Request
     * @param \App\Models\Factory $factory Factory model
     * @return \Illuminate\Support\Facades\View
     */
    public function view(Request $request, Factory $factory)
    {
        if (!Auth::user()->isCanViewFactory()) {
            $this->accessDanied();
        }

        return view('factory.view', compact('factory'));
    }


    /**
     * Display page with factory form for edit factory information.
     *
     * @param \Illuminate\Http\Request $request Request
     * @param \App\Models\Factory $factory Factory model
     * @return \Illuminate\Support\Facades\View
     */
    public function edit(Request $request, Factory $factory)
    {
        if (!Auth::user()->isCanEditFactory()) {
            $this->accessDanied();
        }

        return view('factory.edit', compact('factory'));
    }

    /**
     * Update factory information.
     *
     * @param \App\Http\Requests\FactoryRequest $request Factory request
     * @param \App\Models\Factory $factory Factory model
     * @return \Illuminate\Support\Facades\Redirect
     */
    public function update(FactoryRequest $request, Factory $factory)
    {
        if (!Auth::user()->isCanEditFactory()) {
            $this->accessDanied();
        }

        $this->factoryService->updateFactory($factory, $request->all());

        return redirect()->route('factory.index');
    }

    /**
     * Delete factory.
     *
     * @param \Illuminate\Http\Request $request Request
     * @param \App\Models\Factory $factory Factory model
     * @return \Illuminate\Support\Facades\Redirect
     */
    public function delete(Request $request, Factory $factory)
    {
        if (!Auth::user()->isCanDeleteFactory()) {
            $this->accessDanied();
        }

        $factory->delete();

        return redirect(route('factory.index'));
    }
}
