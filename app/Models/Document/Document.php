<?php

namespace App\Models\Document;

use Illuminate\Database\Eloquent\Model;
use App\Models\Document\DocumentAdditionalField;

/**
 * Model class for table `documents`.
 */
class Document extends Model
{
    /**
     * Document type exposition.
     *
     * @var string
     */
    const DOCUMENT_TYPE_EXPOSITION = 'exposition';

    /**
     * Document type order.
     *
     * @var string
     */
    const DOCUMENT_TYPE_ORDER = 'order';

    /**
     * Document payment type cash.
     *
     * @var string
     */
    const PAYMENT_TYPE_CASH = 'cash';

    /**
     * Document payment type non cash.
     *
     * @var string
     */
    const PAYMENT_TYPE_NON_CASH = 'non-cash';

    /**
     * Document payment type bill.
     *
     * @var string
     */
    const PAYMENT_TYPE_BILL = 'bill';

    /**
     * Document state in work.
     *
     * @var string
     */
    const STATE_IN_WORK = 'in-work';

    /**
     * Document state register.
     *
     * @var string
     */
    const STATE_REGISTER = 'register';

    /**
     * Document state developed.
     *
     * @var string
     */
    const STATE_DEVELOPED = 'developed';

    /**
     * Document client type legal.
     *
     * @var string
     */
    const CLIENT_TYPE_LEGAL = 'legal';

    /**
     * Document client type individual.
     *
     * @var string
     */
    const CLIENT_TYPE_INDIVIDUAL = 'individual';

    /**
     * Order status booking.
     *
     * @var string
     */
    const ORDER_STATUS_BOOKING = 'booking';

    /**
     * Order status full paid.
     *
     * @var string
     */
    const ORDER_STATUS_FULLY_PAID = 'fully-paid';

    /**
     * Order status delivery.
     *
     * @var string
     */
    const ORDER_STATUS_DELIVERY = 'delivery';

    /**
     * Order status assembly.
     *
     * @var string
     */
    const ORDER_STATUS_ASSEMBLY = 'assembly';

    /**
     * Order status closed.
     *
     * @var string
     */
    const ORDER_STATUS_CLOSED = 'closed';

    /**
     * Order status fulfilled.
     *
     * @var string
     */
    const ORDER_STATUS_FULFILLED = 'fulfilled';

    /**
     * Order status accepted.
     *
     * @var string
     */
    const ORDER_STATUS_ACCEPTED = 'accepted';

    /**
     * Order status production percentage.
     *
     * @var string
     */
    const ORDER_STATUS_PRODUCTION_PERCENTAGE = 'percentage';

    /**
     * Order status shipped production.
     *
     * @var string
     */
    const ORDER_STATUS_SHIPPED_PRODUCTION = 'shipped-production';

    /**
     * Order status transportation.
     *
     * @var string
     */
    const ORDER_STATUS_TRANSPORTATION = 'transportation';

    /**
     * Order status custom house.
     *
     * @var string
     */
    const ORDER_STATUS_CUSTOM_HOUSE = 'custom-house';

    /**
     * Order status warehouse.
     *
     * @var string
     */
    const ORDER_STATUS_WAREHOUSE = 'warehouse';

    /**
     * Order status shipment client.
     *
     * @var string
     */
    const ORDER_STATUS_SHIPMENT_CLIENT = 'shipmemt-client';

    /**
     * {@inheritdoc}
     *
     * @access protected
     * @var array $fillable
     */
    protected $fillable = [
        'name',
        'code',
        'worker_id',
        'date',
        'type_payment',
        'state',
        'register_number',
        'from',
        'shop_id',
        'sum',
        'percentage_currrency',
        'confirm_pro_form',
        'period_execution',
        'date_finish',
        'client_id',
        'client_name',
        'client_type',
        'manager_worker_id',
        'percentage_enter',
        'eur_enter',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @access protected
     * @var array $dates
     */
    protected $dates = [
        'created_at',
        'date_finish',
        'date',
    ];

    protected $appends = [
        'cost_price',
    ];

    /**
     * Get document types.
     *
     * @static
     * @return array
     */
    public static function getDocumentTypes()
    {
        return [
            self::DOCUMENT_TYPE_EXPOSITION => 'Экспозиция',
            self::DOCUMENT_TYPE_ORDER => 'Заказ',
        ];
    }

    /**
     * Get document payment types.
     *
     * @static
     * @return array
     */
    public static function getPaymentTypes()
    {
        return [
            self::PAYMENT_TYPE_CASH => 'Наличный',
            self::PAYMENT_TYPE_NON_CASH => 'Безналичный',
            self::PAYMENT_TYPE_BILL => 'Вексель',
        ];
    }

    /**
     * Get document states.
     *
     * @static
     * @return array
     */
    public static function getStates()
    {
        return [
            self::STATE_DEVELOPED => 'Разрабатывается',
            self::STATE_IN_WORK => 'В работе',
            self::STATE_REGISTER => 'Зарегистрирован',
        ];
    }

    /**
     * Get document exposition statuses.
     *
     * @static
     * @return array
     */
    public static function getExpositionStatuses()
    {
        return [
            self::ORDER_STATUS_BOOKING => 'Бронь',
            self::ORDER_STATUS_FULLY_PAID => 'Оплачен полностью',
            self::ORDER_STATUS_DELIVERY => 'Доставка',
            self::ORDER_STATUS_ASSEMBLY => 'Сборка',
            self::ORDER_STATUS_CLOSED => 'Закрыто',
            self::ORDER_STATUS_FULFILLED => 'Исполнено',
        ];
    }

    /**
     * Get document order statuses.
     *
     * @static
     * @return array
     */
    public static function getOrderStatuses()
    {
        return [
            self::ORDER_STATUS_ACCEPTED => 'Принят',
            self::ORDER_STATUS_PRODUCTION_PERCENTAGE => 'Производство = 70%',
            self::ORDER_STATUS_SHIPPED_PRODUCTION => 'Отгружено с производства',
            self::ORDER_STATUS_TRANSPORTATION => 'Транспортировка',
            self::ORDER_STATUS_CUSTOM_HOUSE => 'Таможня',
            self::ORDER_STATUS_WAREHOUSE => 'Склад / салон',
            self::ORDER_STATUS_SHIPMENT_CLIENT => 'Отгрузка клиенту',
            self::ORDER_STATUS_DELIVERY => 'Доставка',
            self::ORDER_STATUS_ASSEMBLY => 'Сборка',
        ];
    }

    /**
     * Get full list order statuses.
     *
     * @static
     * @return array
     */
    public static function getFullListOrderStatuses()
    {
        return array_merge(self::getOrderStatuses(), self::getExpositionStatuses());
    }

    /**
     * Get payment types for service acount.
     *
     * @static
     * @return array
     */
    public static function getTypePaymentsForServiceAccount()
    {
        $types = self::getPaymentTypes();

        unset($types[ self::PAYMENT_TYPE_BILL ]);

        return $types;
    }

    // ===================================================================================== virtual attributes =======================================================

    /**
     * Get document client.
     *
     * @return \App\Models\Client\IndividualClient|\App\Models\Client\LegalClient
     */
    public function getClientAttribute()
    {
        if ($this->client_type === self::CLIENT_TYPE_LEGAL) {
            return $this->legalClient;
        } else {
            return $this->individualClient;
        }
    }


    /**
     * Get russian month name.
     *
     * @return null|string
     */
    public function getRussianMonthNameAttribute()
    {
        $numberMonth = $this->created_at->month;

        switch ($numberMonth) {
            case 1:
                return 'Январь';
            case 2:
              return 'Февраль';
            case 3:
                return 'Март';
            case 4:
                return 'Апрель';
            case 5:
                return 'Май';
            case 6:
                return 'Июнь';
            case 7:
                return 'Июль';
            case 8:
                return 'Август';
            case 9:
                return 'Сентябрь';
            case 10:
                return 'Октябрь';
            case 11:
                return 'Ноябрь';
            case 12:
                return 'Декабрь';
            default:
                return null;
        }
    }

    /**
     * Get document human type.
     *
     * @return null|string
     */
    public function getHumanNameAttribute()
    {
        $listTypes = self::getDocumentTypes();

        if (isset($listTypes[ $this->attributes[ 'name' ] ])) {
            return $listTypes[ $this->attributes[ 'name' ] ];
        }

        return null;
    }

    /**
     * Get document human order statte.
     *
     * @return null|string
     */
    public function getHumanOrderStateAttribute()
    {
        $listStates = self::getFullListOrderStatuses();

        if (isset($listStates[ $this->attributes[ 'order_state' ] ])) {
            return $listStates[ $this->attributes[ 'order_state' ] ];
        }

        return null;
    }

    /**
     * Get document human type payment.
     *
     * @return null|string
     */
    public function getHumanTypePaymentAttribute()
    {
        $listPaymentTypes = self::getPaymentTypes();

        if (isset($listPaymentTypes[ $this->attributes[ 'type_payment' ] ])) {
            return $listPaymentTypes[ $this->attributes[ 'type_payment' ] ];
        }

        return null;
    }

    /**
     * Get document human state.
     *
     * @return null|string
     */
    public function getHumanStateAttribute()
    {
        $listStates = self::getStates();

        if (isset($listStates[ $this->attributes[ 'state' ] ])) {
            return $listStates[ $this->attributes[ 'state' ] ];
        }

        return null;
    }

    /**
     * Get document prepared worker full name.
     *
     * @return null|string
     */
    public function getWorkerFullNameAttribute()
    {
        if ($this->worker) {
            return $this->worker->fullName;
        }

        return null;;
    }

    /**
     * Get document manager full name.
     *
     * @return null|string
     */
    public function getManagerFullNameAttribute()
    {
        if ($this->manager) {
            return $this->manager->full_name;
        }

        return null;
    }

    /**
     * Get document client phones.
     *
     * @return null|string
     */
    public function getClientPhonesAttribute()
    {
        if ($this->isLegalClient()) {
            return $this->client->phone;
        }

        if ($this->isIndividualClient()) {
            return implode(', ', $this->client->phones);
        }

        return null;
    }

    /**
     * Get document client phones as array.
     *
     * @return null|string
     */
    public function getClientPhonesArrayAttribute()
    {
        if ($this->isLegalClient()) {
            return [$this->client->phone];
        }

        if ($this->isIndividualClient()) {
            return $this->client->phones;
        }

        return [];
    }

    /**
     * Get document client email.
     *
     * @return null|string
     */
    public function getClientEmailAttribute()
    {
        if (!$this->client) {
            return null;
        }

        return $this->client->email;
    }


    /**
     * Get document client shipping address.
     *
     * @return null|string
     */
    public function getClientShippingAddressAttribute()
    {
        if ($this->client) {
            return $this->client->shipping_address;
        }

        return null;
    }

    /**
     * Get document client comment.
     *
     * @return null|string
     */
    public function getClientCommentAttribute()
    {
        if ($this->isIndividualClient()) {
            return $this->client->comment;
        }

        return null;
    }

    /**
     * Get document client full name.
     *
     * @return null|string
     */
    public function getClientFullNameAttribute()
    {
        if ($this->isLegalClient()) {
            return $this->client->personalFullName;
        }

        if ($this->isIndividualClient()) {
            return $this->client->fullName;
        }

        return null;
    }

    /**
     * Get document client address.
     *
     * @return null|string
     */
    public function getClientAddressAttribute()
    {
        if ($this->isLegalClient()) {
            return $this->client->address;
        }

        if ($this->isIndividualClient()) {
            return $this->client->register_address;
        }

        return null;
    }

    /**
     * Get document client additional phones.
     *
     * @return null|string
     */
    public function getClientAdditionalPhonesAttribute()
    {
        if ($this->isLegalClient()) {
            return null;
        }

        if ($this->isIndividualClient()) {
            if (!$this->client->addionalPhones) {
                return null;
            }

            return implode(', ', $this->client->addionalPhones);
        }

        return null;
    }

    /**
     * Shop full name virtual attribute from document.
     *
     * @return null|string
     */
    public function getOrganizationAttribute()
    {
        if ($this->shop) {
            return $this->shop->full_name;
        }

        return null;
    }

    /**
     * Get shop address.
     *
     * @return null|string
     */
    public function getOrganizationAddressAttribute()
    {
        if (!$this->shop) {
            return null;
        }

        return $this->shop->address;
    }

    /**
     * Get shop phone.
     *
     * @return null|string
     */
    public function getOrganizationPhoneAttribute()
    {
        if (!$this->shop) {
            return null;
        }

        return $this->shop->phone;
    }

    /**
     * Get percentage document payments virtual attribute from document.
     *
     * @return null|float
     */
    public function getPercentageAllPaymentsAttribute()
    {
        $percentage = 0;

        foreach ($this->allPayments as $payment) {
            $percentage += $payment->percentage_prepayment;
        }

        return $percentage;
    }

    /**
     * Get sum document payments virtual attribute from document.
     *
     * @return null|float
     */
    public function getSumAllPaymentsAttribute()
    {
        $sum = 0;

        foreach ($this->allPayments as $payment) {
            $sum += $payment->prepayment_amount_eur;
        }

        return $sum;
    }

    public function getCanEditAttribute()
    {
        if ($this->ternimated_date || $this->performance_date) {
            return false;
        }

        return true;
    }

    /**
     * Get client contact full name.
     *
     * @return null|string
     */
    public function getClientContactFullNameAttribute()
    {
        if (!$this->client) {
            return null;
        }

        return $this->client->contactFullName;
    }

    /**
     * Get date proform.
     *
     * @return null|string
     */
    public function getDateProformAttribute()
    {
        $data = explode(' ', $this->confirm_pro_form);

        if (isset($data[ 0 ])) {
            return $data[ 0 ];
        }

        return null;
    }

    /**
     * Get additional attribute office.
     *
     * @return null|string
     */
    public function getAddionalOfficeAttribute()
    {
        if (!$this->additionalFields) {
            return null;
        }

        return $this->additionalFields->office;
    }

    /**
     * Get additional attribute proform office.
     *
     * @return null|string
     */
    public function getAddionalProformOfficeAttribute()
    {
        if (!$this->additionalFields) {
            return null;
        }

        return $this->additionalFields->proform_office;
    }

    /**
     * Get additional attribute proform number.
     *
     * @return null|string
     */
    public function getAddionalProformNumberAttribute()
    {
        if (!$this->additionalFields) {
            return null;
        }

        return $this->additionalFields->proform_number;
    }

    /**
     * Get additional attribute proform sum.
     *
     * @return null|string
     */
    public function getAddionalProformSumAttribute()
    {
        if (!$this->additionalFields) {
            return null;
        }

        return $this->additionalFields->proform_sum;
    }

    /**
     * Get additional attribute proform date confirm.
     *
     * @return null|string
     */
    public function getAddionalProformDateConfirmAttribute()
    {
        if (!$this->additionalFields) {
            return null;
        }

        return $this->additionalFields->proform_date_confirm;
    }

    /**
     * Get additional attribute proform volume.
     *
     * @return null|string
     */
    public function getAddionalProformVolumeAttribute()
    {
        if (!$this->additionalFields) {
            return null;
        }

        return $this->additionalFields->proform_volume;
    }

    /**
     * Get additional attribute am.
     *
     * @return null|string
     */
    public function getAddionalAmAttribute()
    {
        if (!$this->additionalFields) {
            return null;
        }

        return $this->additionalFields->am;
    }

    /**
     * Get additional attribute stalker delivery.
     *
     * @return null|string
     */
    public function getAddionalStalkerDeliveryAttribute()
    {
        if (!$this->additionalFields) {
            return null;
        }

        return $this->additionalFields->stalker_delivery;
    }

    /**
     * Get additional attribute stalker arrival.
     *
     * @return null|string
     */
    public function getAddionalStalkerArrivalAttribute()
    {
        if (!$this->additionalFields) {
            return null;
        }

        return $this->additionalFields->stalker_arrival;
    }

    /**
     * Get additional attribute production time.
     *
     * @return null|string
     */
    public function getAddionalProductionTimeAttribute()
    {
        if (!$this->additionalFields) {
            return null;
        }

        return $this->additionalFields->production_time;
    }

    /**
     * Get additional attribute volume.
     *
     * @return null|string
     */
    public function getAddionalVolumeAttribute()
    {
        if (!$this->additionalFields) {
            return null;
        }

        return $this->additionalFields->volume;
    }

    /**
     * Get additional attribute weight.
     *
     * @return null|string
     */
    public function getAddionalWeightAttribute()
    {
        if (!$this->additionalFields) {
            return null;
        }

        return $this->additionalFields->weight;
    }

    /**
     * Get additional attribute count package.
     *
     * @return null|string
     */
    public function getAddionalCountPackageAttribute()
    {
        if (!$this->additionalFields) {
            return null;
        }

        return $this->additionalFields->count_package;
    }

    /**
     * Get additional attribute packing list.
     *
     * @return null|string
     */
    public function getAddionalPackingListAttribute()
    {
        if (!$this->additionalFields) {
            return null;
        }

        return $this->additionalFields->packing_list;
    }

    /**
     * Get additional attribute coming stock date.
     *
     * @return null|string
     */
    public function getAddionalCommingStockDateAttribute()
    {
        if (!$this->additionalFields) {
            return null;
        }

        return $this->additionalFields->comming_stock_date;
    }

    /**
     * Get additional attribute coming count places.
     *
     * @return null|string
     */
    public function getAddionalCommingCountPlacesAttribute()
    {
        if (!$this->additionalFields) {
            return null;
        }

        return $this->additionalFields->comming_count_places;
    }

    /**
     * Get additional attribute consumption stock date.
     *
     * @return null|string
     */
    public function getAddionalConsumptionStockDateAttribute()
    {
        if (!$this->additionalFields) {
            return null;
        }

        return $this->additionalFields->consumption_stock_date;
    }

    /**
     * Get additional attribute consumption count places.
     *
     * @return null|string
     */
    public function getAddionalConsumptionCountPlacesAttribute()
    {
        if (!$this->additionalFields) {
            return null;
        }

        return $this->additionalFields->consumption_count_places;
    }

    /**
     * Get additional attribute stock remainder.
     *
     * @return null|string
     */
    public function getAddionalStockRemainderAttribute()
    {
        if (!$this->additionalFields) {
            return null;
        }

        return $this->additionalFields->stock_remainder;
    }

    /**
     * Get additional attribute state stock.
     *
     * @return null|string
     */
    public function getAddionalStateStockAttribute()
    {
        if (!$this->additionalFields) {
            return null;
        }

        return $this->additionalFields->state_stock;
    }

    /**
     * Get additional attribute return stock date.
     *
     * @return null|string
     */
    public function getAddionalReturnStockDateAttribute()
    {
        if (!$this->additionalFields) {
            return null;
        }

        return $this->additionalFields->return_stock_date;
    }

    /**
     * Get additional attribute return count package.
     *
     * @return null|string
     */
    public function getAddionalReturnCountPackageAttribute()
    {
        if (!$this->additionalFields) {
            return null;
        }

        return $this->additionalFields->return_count_package;
    }

    /**
     * Get additional attribute state return.
     *
     * @return null|string
     */
    public function getAddionalStateReturnAttribute()
    {
        if (!$this->additionalFields) {
            return null;
        }

        return $this->additionalFields->state_return;
    }

    public function getFullSumBankTransferAttribute()
    {
        $sum = 0;

        foreach ($this->factoryPayments as $payment) {
            $sum += $payment->bank_transfer;
        }

        return $sum;
    }

    public function getSumBankTransferRemainderAttribute()
    {
        $proformSum = $this->addionalProformSum;

        if (is_null($proformSum)) {
            return null;
        }

        $sum = 0;

        foreach ($this->factoryPayments as $payment) {
            $sum += $payment->sum;
        }

        return $proformSum - $sum;
    }

    public function getCostPriceAttribute()
    {
        $sum = $this->fullSumBankTransfer;

        if (!is_null($this->addionalProformSum)) {
            $sum += $this->addionalProformSum;
        }

        if (!is_null($this->addionalStalkerDelivery)) {
            $sum += $this->addionalStalkerDelivery;
        }

        if (!is_null($this->addionalStalkerArrival)) {
            $sum += $this->addionalStalkerArrival;
        }

        return $sum;
    }

    /**
     * Get recipe document sum.
     *
     * @return float
     */
    public function getRecipeSumAttribute()
    {
        return $this->convertNumberToRecipe($this->sum);
    }

    /**
     * Get sum first prepayment.
     *
     * @return float|null
     */
    public function getFirstPrepaymentSumAttribute()
    {
        if ($this->firstPrepayment) {
            return $this->firstPrepayment->prepayment_amount_rub;
        }

        return null;
    }

    /**
     * Get recipe sum first prepayment.
     *
     * @return string|null
     */
    public function getRecipeFirstPrepaymentSumAttribute()
    {
        if ($this->firstPrepayment) {
            return $this->convertNumberToRecipe($this->firstPrepayment->prepayment_amount_rub);
        }

        return null;
    }

    // ===================================================================================== end virtual attributes =======================================================

    /**
     * Get document firts payment.
     *
     * @return
     */
    public function firstPrepayment()
    {
        return $this->hasOne('App\Models\Document\DocumentPrepayment');
    }

    /**
     * Get document all payments.
     *
     * @return
     */
    public function allPayments()
    {
        return $this->hasMany('App\Models\Document\DocumentPrepayment');
    }

    /**
     * Get document all payments.
     *
     * @return
     */
    public function templates()
    {
        return $this->belongsToMany('App\Models\Document\DocumentTemplate', 'documents_templates_relations', 'document_id', 'document_template_id');
    }

    /**
     * Relation with table legal client.
     *
     * @return
     */
    public function legalClient()
    {
        return $this->belongsTo('App\Models\Client\LegalClient', 'client_id');
    }

    /**
     * Relation with table individual client.
     *
     * @return
     */
    public function individualClient()
    {
        return $this->belongsTo('App\Models\Client\IndividualClient', 'client_id');
    }

    /**
     * Relation with table workers role manager.
     *
     * @return
     */
    public function manager()
    {
        return $this->belongsTo('App\Models\User\Worker', 'manager_worker_id');
    }

    /**
     * Relation with table workers.
     *
     * @return
     */
    public function worker()
    {
        return $this->belongsTo('App\Models\User\Worker');
    }

    /**
     * Relation with table shops.
     *
     * @return
     */
    public function shop()
    {
        return $this->belongsTo('App\Models\Shop\Shop');
    }

    /**
     * Relation with table products.
     *
     * @return
     */
    public function products()
    {
        return $this->hasMany('App\Models\Product');
    }

    /**
     * Relation with table service accounts.
     *
     * @return
     */
    public function serviceAccount()
    {
        return $this->hasOne('App\Models\Document\ServiceAccount');
    }

    /**
     * Relation with table service accounts.
     *
     * @return
     */
    public function additionalFields()
    {
        return $this->hasOne('App\Models\Document\DocumentAdditionalField');
    }

    public function factoryPayments()
    {
        return $this->hasMany('App\Models\Document\FactoryPayment');
    }

    /**
     * Check if document client type is legal.
     *
     * @access private
     * @return bool
     */
    private function isLegalClient()
    {
        return ($this->client instanceof \App\Models\Client\LegalClient) ? true : false;
    }

    /**
     * Check if document client type is individual.
     *
     * @access private
     * @return bool
     */
    private function isIndividualClient()
    {
        return ($this->client instanceof \App\Models\Client\IndividualClient) ? true : false;
    }

    /**
     * Convert number to string.
     *
     * @access private
     * @param float $num Number.
     * @return string
     */
    private function convertNumberToRecipe($num)
    {
        $nol = 'ноль';
        $str[100]= array('','сто','двести','триста','четыреста','пятьсот','шестьсот', 'семьсот', 'восемьсот','девятьсот');
        $str[11] = array('','десять','одиннадцать','двенадцать','тринадцать', 'четырнадцать','пятнадцать','шестнадцать','семнадцать', 'восемнадцать','девятнадцать','двадцать');
        $str[10] = array('','десять','двадцать','тридцать','сорок','пятьдесят', 'шестьдесят','семьдесят','восемьдесят','девяносто');
        $sex = array(
            array('','один','два','три','четыре','пять','шесть','семь', 'восемь','девять'),// m
            array('','одна','две','три','четыре','пять','шесть','семь', 'восемь','девять') // f
        );
        $forms = array(
            array('копейка', 'копейки', 'копеек', 1), // 10^-2
            array('рубль', 'рубля', 'рублей',  0), // 10^ 0
            array('тысяча', 'тысячи', 'тысяч', 1), // 10^ 3
            array('миллион', 'миллиона', 'миллионов',  0), // 10^ 6
            array('миллиард', 'миллиарда', 'миллиардов',  0), // 10^ 9
            array('триллион', 'триллиона', 'триллионов',  0), // 10^12
        );
        $out = $tmp = array();
        // Поехали!
        $tmp = explode('.', str_replace(',','.', $num));
        $rub = number_format($tmp[ 0], 0,'','-');
        if ($rub== 0) $out[] = $nol;
        // нормализация копеек
        $kop = isset($tmp[1]) ? substr(str_pad($tmp[1], 2, '0', STR_PAD_RIGHT), 0,2) : '00';
        $segments = explode('-', $rub);
        $offset = sizeof($segments);
        if ((int)$rub== 0) { // если 0 рублей
            $o[] = $nol;
            $o[] = $this->morph( 0, $forms[1][ 0],$forms[1][1],$forms[1][2]);
        }
        else {
            foreach ($segments as $k=>$lev) {
                $sexi= (int) $forms[$offset][3]; // определяем род
                $ri = (int) $lev; // текущий сегмент
                if ($ri== 0 && $offset>1) {// если сегмент==0 & не последний уровень(там Units)
                    $offset--;
                    continue;
                }
                // нормализация
                $ri = str_pad($ri, 3, '0', STR_PAD_LEFT);
                // получаем циферки для анализа
                $r1 = (int)substr($ri, 0,1); //первая цифра
                $r2 = (int)substr($ri,1,1); //вторая
                $r3 = (int)substr($ri,2,1); //третья
                $r22= (int)$r2.$r3; //вторая и третья
                // разгребаем порядки
                if ($ri>99) $o[] = $str[100][$r1]; // Сотни
                if ($r22>20) {// >20
                    $o[] = $str[10][$r2];
                    $o[] = $sex[ $sexi ][$r3];
                }
                else { // <=20
                    if ($r22>9) $o[] = $str[11][$r22-9]; // 10-20
                    elseif($r22> 0) $o[] = $sex[ $sexi ][$r3]; // 1-9
                }
                // Рубли
                $o[] = $this->morph($ri, $forms[$offset][ 0],$forms[$offset][1],$forms[$offset][2]);
                $offset--;
            }
        }

        // Копейки
        $o[] = $kop;
        $o[] = $this->morph($kop,$forms[ 0][ 0],$forms[ 0][1],$forms[ 0][2]);
        return preg_replace("/\s{2,}/",' ',implode(' ',$o));
    }

    private function morph($n, $f1, $f2, $f5)
    {
        $n = abs(intval($n)) % 100;

        if ($n > 10 && $n < 20) {
            return $f5;
        }

        $n = $n % 10;

        if ($n > 1 && $n < 5) {
            return $f2;
        }

        if ($n == 1) {
            return $f1;
        }

        return $f5;
    }
}
