<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableClients extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('workers', function (Blueprint $table) {
            $table->string('lastname', 50)->nullable()->change();
        });

        Schema::table('individual_clients', function (Blueprint $table) {
            $table->string('lastname', 50)->nullable()->change();
        });

        Schema::table('legal_clients', function (Blueprint $table) {
            $table->string('general_director_lastname', 50)->nullable()->change();
            $table->string('personal_lastname', 50)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('workers', function (Blueprint $table) {
            $table->string('lastname', 50)->change();
        });

        Schema::table('individual_clients', function (Blueprint $table) {
            $table->string('lastname', 50)->change();
        });

        Schema::table('legal_clients', function (Blueprint $table) {
            $table->string('general_director_lastname', 50)->change();
            $table->string('personal_lastname', 50)->change();
        });
    }
}
