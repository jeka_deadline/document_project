@extends('layouts.bootstrap')

@section('breadcrumbs')

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Главная</a></li>
            <li class="breadcrumb-item active" aria-current="page">Список сотрудников</li>
        </ol>
    </nav>

@stop
@section('content')

    <div class="row">
          <div class="col-lg-12">

                <?php if (Auth::user()->isCanCreateWorker()) : ?>

                    <a class="btn btn-success" href="{{ route('worker.create') }}">Добавить сотрудника</a>

                <?php endif; ?>

                <br><br>
                <table class="table">
                      <thead>
                            <tr>
                                  <th>Фамилия</th>
                                  <th>Имя</th>
                                  <th>Отчество</th>
                                  <th>Email</th>
                                  <th>Телефоны</th>
                                  <th>Роль</th>
                                  <th>Операции</th>
                            </tr>
                      </thead>
                      <tbody>

                          @foreach ($workers as $worker)

                              <tr>
                                  <td>
                                      {{ $worker->surname }}
                                  </td>
                                  <td>
                                      {{ $worker->name }}
                                  </td>
                                  <td>
                                      {{ $worker->lastname }}
                                  </td>
                                  <td>
                                      {{ $worker->user->email }}
                                  </td>
                                  <td>

                                      @foreach ($worker->phones as $phone)

                                          {{ $phone }}

                                          <br>

                                      @endforeach

                                  </td>
                                  <td>{{ $worker->userRole->role->humanName }}</td>
                                  <td>

                                      <?php if (Auth::user()->isCanEditWorker()) : ?>

                                          <a href="{{ route('worker.edit', ['worker' => $worker]) }}"><i class="fa fa-edit fa-2x"></i></a>

                                      <?php endif; ?>
                                      <?php if (Auth::user()->isCanViewWorker()) : ?>

                                          <a href="{{ route('worker.view', ['worker' => $worker]) }}"><i class="fa fa-eye fa-2x"></i></a>

                                      <?php endif; ?>
                                      <?php if (Auth::user()->isCanDeleteWorker()) : ?>

                                          <form class="delete-form" method="post" action="{{ route('worker.delete', ['worker' => $worker]) }}" style="display: inline">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}

                                                <button style="color: #337ab7; border: none; background: none"><i class="fa fa-trash fa-2x"></i></button>
                                          </form>

                                      <?php endif; ?>

                                  </td>
                              </tr>

                          @endforeach

                      </tbody>
                </table>

                {{ $workers->links() }}
          </div>
    </div>

@stop