@extends('layouts.bootstrap')

@section('breadcrumbs')

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/">Главная</a></li>
            <li class="breadcrumb-item"><a href="{{ route('admin.settings.index') }}">Список настроек</a></li>
            <li class="breadcrumb-item active" aria-current="page">Обновление настройки</li>
        </ol>
    </nav>

@stop
@section('content')

    <div class="row">
        <div class="col-lg-12">
              <h2>Обновление фабрики: {{ $setting->title }}</h2>
              <form method="post" action="{{ route('admin.settings.update', ['setting' => $setting]) }}">
                  {{ csrf_field() }}
                  {{ method_field('PUT') }}

                  <div class="form-group">
                      <label class="control-label" for="">Значение</label>
                      <input class="form-control" type="text" name="value" value="{{ old('value', $setting->value) }}">

                      @if ($errors->has('value'))

                          <div class="text-danger">

                                {{ $errors->first('value') }}

                          </div>

                      @endif

                  </div>

                  <input type="submit" value="Обновить" class="btn btn-primary">
                  <a class="btn btn-info" href="{{ route('admin.settings.index') }}">Назад</a>
              </form>
        </div>
    </div>

@stop