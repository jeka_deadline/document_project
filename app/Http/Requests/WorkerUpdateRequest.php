<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class WorkerUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'surname' => 'required|max:50',
            'name' => 'required|max:50',
            'lastname' => 'max:50',
            'email' => 'required|email|max:100|unique:users,email,' . $this->worker->user->id,
            'password' => 'nullable|min:6',
            'role' => 'required',
            'additional_email' => 'nullable|email|max:100',
            'comment' => 'nullable|string',
        ];
    }
}
