@extends('layouts.bootstrap')

@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.2.0/css/ion.rangeSlider.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.2.0/css/ion.rangeSlider.skinFlat.min.css">
@stop

@section('content')

<style>
    td.edittable {
        border: 1px solid #ccc !important;
    }
</style>

    <div id="documents-table"">
        <form action="">
            <input type="submit" class="btn btn-info" value="Применить фильтр">
            <div style="overflow-x: scroll;">
                <table class="table">
                    <thead>
                        <tr>
                            <td>
                                <p>Тип продажи</p>
                                <div class="form-group">
                                    <select name="name" class="form-control">
                                        <option value=""></option>

                                        @foreach($listDocumentTypes as $key => $label)

                                            <option value="{{ $key }}" {{ (request()->input('name') == $key) ? 'selected' : '' }}>{{ $label }}</option>

                                        @endforeach

                                    </select>
                                </div>
                            </td>
                            <td>
                                <p>Подготовил</p>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="worker" value="{{ request()->input('worker') }}">
                                </div>
                            </td>
                            <td>
                                <p>Менеджер</p>
                                <div class="form-group">
                                    <input type="text" name="manager" class="form-control" value="{{ request()->input('manager') }}">
                                </div>
                            </td>
                            <td>
                                <p>Клиент</p>
                                <div class="form-group">
                                    <input type="text" class="form-control">
                                </div>
                            </td>
                            <td><p>Контактный телефон</p></td>
                            <td><p>Эл.почта</p></td>
                            <td>
                                <p>№ счета</p>
                                <div class="form-group">
                                    <input type="text" name="register_number" class="form-control" value="{{ request()->input('register_number') }}">
                                </div>
                            </td>
                            <td>
                                <p>Салон</p>
                                <div class="form-group">
                                    <select name="shop" class="form-control">
                                        <option value=""></option>

                                        @foreach ($listShops as $shop)

                                            <option value="{{ $shop->id }}" {{ (request()->input('shop') == $shop->id) ? 'selected' : '' }}>{{ $shop->full_name }}</option>

                                        @endforeach

                                    </select>
                                </div>
                            </td>
                            <td>
                                <p>б/нал</p>
                                <div class="form-group">
                                    <select name="payment_type" id="" class="form-control">
                                        <option value=""></option>

                                            @foreach ($listPaymentTypes as $key => $label)

                                                <option value="{{ $key }}" {{ (request()->input('payment_type') == $key) ? 'selected' : '' }}>{{ $label }}</option>

                                            @endforeach

                                    </select>
                                </div>
                            </td>
                            <td>
                                <p>Год</p>
                                <div class="form-group">
                                    <input type="text" name="year" class="form-control" value="{{ request()->input('year') }}">
                                </div>
                            </td>
                            <td><p>Дата</p></td>
                            <td>
                                <p>Месяц</p>
                                <div class="form-group">
                                    <select name="month" id="" class="form-control">
                                        <option value=""></option>

                                        @foreach ($listMonths as $key => $label)

                                            <option value="{{ $key }}" {{ (request()->input('month') == $key) ? 'selected' : '' }}>{{ $label }}</option>

                                        @endforeach

                                    </select>
                                </div>
                            </td>
                            <td>
                                <p>Офис</p>
                                <div class="form-group">
                                    <input type="text" name="additional[office]" class="form-control" value="{{ request()->input('additional.office') }}">
                                </div>
                            </td>
                            <td><p>Срок исполнения по договору</p></td>
                            <td>
                                <p>Срок поставки клиенту</p>
                                <div class="form-group">
                                    <input type="text" name="date_finish" value="{{ request()->input('date_finish') }}" class="form-control daterange">
                                </div>
                            </td>
                            <td>
                                <p>Поставщик</p>
                                <div class="form-group">
                                    <select name="factory" class="form-control">
                                        <option value=""></option>

                                        @foreach ($listFactories as $factory)

                                            <option value="{{ $factory->id }}" {{ (request()->input('factory') == $factory->id) ? 'selected' : '' }}>{{ $factory->name }}</option>

                                        @endforeach

                                    </select>
                                </div>
                            </td>
                            <td><p>Наименование</p></td>
                            <td><p>Модель</p></td>
                            <td><p>Кол-во</p></td>
                            <td><p>Сумма</p></td>
                            <td><p>Себестоимость</p></td>
                            <td><p>Форма оплаты</p></td>
                            <td><p>Дата</p></td>
                            <td><p>Евро</p></td>
                            <td><p>Рубль</p></td>
                            <td><p>Доллары</p></td>
                            <td><p>Курс</p></td>
                            <td><p>Крос курс</p></td>
                            <td><p>Сумма договора</p></td>
                            <td class="light_green"><p>Ост.</p></td>
                            <td class="light_purple"><p>% предоплаты</p></td>
                            <td><p>Сервис форма оплаты</p></td>
                            <td>
                                <p>Сервис дата</p>
                                <div class="form-group">
                                    <input type="text" name="service_account[date]" value="{{ request()->input('service_account.date') }}" class="form-control daterange">
                                </div>
                            </td>
                            <td style="text-align: center">
                                <p>Сервис евро</p>
                                <a href="#" class="show-price btn btn-primary" data-sign="{{ (request()->input('service_account.eur')) ? '+' : '-' }}" data-name="service_account[eur]">{{ (request()->input('service_account.eur')) ? '-' : '+' }}</a>
                                <div class="{{ (request()->input('service_account.eur')) ? 'form-group' : 'form-group hide' }}">

                                    @if (request()->input('service_account.eur'))

                                      <input
                                          name="service_account[eur]",
                                          type="text"
                                          data-min="0"
                                          data-max="{{ $maxServiceAccountSum }}"
                                          data-from="{{ preg_replace('#;.*$#', '', request()->input('service_account.eur')) }}"
                                          data-to="{{ preg_replace('#^(.*?);#', '', request()->input('service_account.eur', $maxServiceAccountSum)) }}"
                                          data-step="10"
                                          class="form-control pricerange">

                                    @else

                                        <input
                                          type="text"
                                          data-min="0"
                                          data-max="{{ $maxServiceAccountSum }}"
                                          data-from="{{ preg_replace('#;.*$#', '', request()->input('service_account.eur')) }}"
                                          data-to="{{ preg_replace('#^(.*?);#', '', request()->input('service_account.eur', $maxServiceAccountSum)) }}"
                                          data-step="10"
                                          class="form-control pricerange">

                                    @endif

                                </div>
                            </td>
                            <td><p>Сервис рубль</p></td>
                            <td><p>Сервис курс</p></td>
                            <td class="light_yellow">
                                <p>Проформа офис</p>
                                <div class="form-group">
                                    <input type="text" name="additional[proform_office]" value="{{ request()->input('additional.proform_office') }}" class="form-control">
                                </div>
                            </td>
                            <td class="light_yellow">
                                <p>Проформа номер</p>
                                <div class="form-group">
                                    <input type="text" name="additional[proform_number]" value="{{ request()->input('additional.proform_number') }}" class="form-control">
                                </div>
                            </td>
                            <td class="light_yellow">
                                <p>Проформа дата</p>
                            </td>
                            <td class="light_yellow">
                                <p>Проформа сумма</p>
                                <!-- <div class="form-group">
                                    <input type="text" class="form-control pricerange">
                                </div> -->
                            </td>
                            <td class="light_yellow">
                                <p>Проформа дата подтверждения</p>
                            </td>
                            <td class="light_yellow"><p>Проформа объем</p></td>
                            <td class="light_blue">
                                <p>Отправка счета на ф-ку</p>
                                <div class="form-group">
                                    <input type="text" name="factory_payment[date_dispatch]" value="{{ request()->input('factory_payment.date_dispatch')}}" class="form-control daterange">
                                </div>
                            </td>
                            <td class="light_blue">
                                <p>Дата</p>
                                <div class="form-group">
                                    <input type="text" name="factory_payment[date_payment]" value="{{ request()->input('factory_payment.date_payment')}}" class="form-control daterange">
                                </div>
                            </td>
                            <td class="light_blue">
                                <p>Инвойс</p>
                                <div class="form-group">
                                    <input type="text" name="factory_payment[invoice]" value="{{ request()->input('factory_payment.invoice')}}" class="form-control">
                                </div>
                            </td>
                            <td class="light_blue" style="text-align: center">
                                <p>Сумма</p>
                                <a href="#" class="show-price btn btn-primary" data-sign="{{ (request()->input('factory_payment.sum')) ? '+' : '-' }}" data-name="factory_payment[sum]">{{ (request()->input('factory_payment.sum')) ? '-' : '+' }}</a>
                                <div class="{{ (request()->input('factory_payment.sum')) ? 'form-group' : 'form-group hide' }}">

                                    @if (request()->input('factory_payment.sum'))

                                    <input
                                        name="factory_payment[sum]"
                                        type="text"
                                        data-min="0"
                                        data-max="{{ $maxFactoryPaymentSum }}"
                                        data-from="{{ preg_replace('#;.*$#', '', request()->input('factory_payment.sum')) }}"
                                        data-to="{{ preg_replace('#^(.*?);#', '', request()->input('factory_payment.sum', $maxFactoryPaymentSum)) }}"
                                        data-step="10"
                                        class="form-control pricerange">

                                    @else

                                      <input
                                        type="text"
                                        data-min="0"
                                        data-max="{{ $maxFactoryPaymentSum }}"
                                        data-from="{{ preg_replace('#;.*$#', '', request()->input('factory_payment.sum')) }}"
                                        data-to="{{ preg_replace('#^(.*?);#', '', request()->input('factory_payment.sum', $maxFactoryPaymentSum)) }}"
                                        data-step="10"
                                        class="form-control pricerange">

                                    @endif

                                </div>
                            </td>
                            <td class="light_blue" style="text-align: center">
                                <p>Банк. перевод</p>
                                <a href="#" class="show-price btn btn-primary" data-sign="{{ (request()->input('factory_payment.bank_transfer')) ? '+' : '-' }}" data-name="factory_payment[bank_transfer]">{{ (request()->input('factory_payment.bank_transfer')) ? '-' : '+' }}</a>
                                <div class="{{ (request()->input('factory_payment.bank_transfer')) ? 'form-group' : 'form-group hide' }}">

                                    @if (request()->input('factory_payment.bank_transfer'))

                                    <input
                                        name="factory_payment[bank_transfer]"
                                        type="text"
                                        data-min="0"
                                        data-max="{{ $maxFactoryPaymentBankTransfer }}"
                                        data-from="{{ preg_replace('#;.*$#', '', request()->input('factory_payment.bank_transfer')) }}"
                                        data-to="{{ preg_replace('#^(.*?);#', '', request()->input('factory_payment.bank_transfer', $maxFactoryPaymentBankTransfer)) }}"
                                        data-step="10"
                                        class="form-control pricerange">

                                    @else

                                      <input
                                        type="text"
                                        data-min="0"
                                        data-max="{{ $maxFactoryPaymentBankTransfer }}"
                                        data-from="{{ preg_replace('#;.*$#', '', request()->input('factory_payment.bank_transfer')) }}"
                                        data-to="{{ preg_replace('#^(.*?);#', '', request()->input('factory_payment.bank_transfer', $maxFactoryPaymentBankTransfer)) }}"
                                        data-step="10"
                                        class="form-control pricerange">

                                    @endif

                                </div>
                            </td>
                            <td class="light_blue">
                                <p>Остаток</p>
                            </td>
                            <td>
                                <p>Банк. перевод</p>
                            </td>
                            <td>
                                <p>а/м</p>
                                <div class="form-group">
                                    <input type="text" name="additional[am]" value="{{ request()->input('additional.am') }}" class="form-control">
                                </div>
                            </td>
                            <td><p>Stalker_доставка</p></td>
                            <td><p>Stalker_заезд</p></td>
                            <td><p>Срок изготовления</p></td>
                            <td><p>Объем куб.м</p></td>
                            <td><p>Вес кг</p></td>
                            <td><p>Кол-во кор.</p></td>
                            <td><p>Packing list</p></td>
                            <td class="light_orange">
                                <p>Приход склад/La Casa/дата</p>
                                <div class="form-group">
                                    <input type="text" name="additional[comming_stock_date]" value="{{ request()->input('additional.comming_stock_date') }}" class="form-control">
                                </div>
                            </td>
                            <td class="light_orange">
                                <p>Приход кол-во мест</p>
                                <div class="form-group">
                                    <input type="text" name="additional[comming_count_places]" value="{{ request()->input('additional.comming_count_places') }}" class="form-control">
                                </div>
                            </td>
                            <td class="light_orange">
                                <p>Расход склад/La Casa/дата</p>
                                <div class="form-group">
                                    <input type="text" name="additional[consumption_stock_date]" value="{{ request()->input('additional.consumption_stock_date') }}" class="form-control">
                                </div>
                            </td>
                            <td class="light_orange">
                                <p>Расход кол-во мест</p>
                                <div class="form-group">
                                    <input type="text" name="additional[consumption_count_places]" value="{{ request()->input('additional.consumption_count_places') }}" class="form-control">
                                </div>
                            </td>
                            <td class="light_orange">
                                <p>Склад/La Casa/остаток</p>
                                <div class="form-group">
                                    <input type="text" name="additional[stock_remainder]" value="{{ request()->input('additional.stock_remainder') }}" class="form-control">
                                </div>
                            </td>
                            <td class="light_orange">
                                <p>Состояние</p>
                                <div class="form-group">
                                    <input type="text" name="additional[state_stock]" value="{{ request()->input('additional.state_stock') }}"  class="form-control">
                                </div>
                            </td>
                            <td class="light_orange">
                                <p>Возврат склад/La Casa/дата</p>
                                <div class="form-group">
                                    <input type="text" name="additional[return_stock_date]" value="{{ request()->input('additional.return_stock_date') }}" class="form-control">
                                </div>
                            </td>
                            <td class="light_orange">
                                <p>Возврат кол-во кор.</p>
                                <div class="form-group">
                                    <input type="text" name="additional[return_count_package]" value="{{ request()->input('additional.return_count_package') }}"  class="form-control">
                                </div>
                            </td>
                            <td class="light_orange">
                                <p>Состояние</p>
                                <div class="form-group">
                                    <input type="text" name="additional[state_return]" value="{{ request()->input('additional.state_return') }}"  class="form-control">
                                </div>
                            </td>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach($documents as $document)

                            <tr data-url="{{ route('document.save.additionals', compact('document')) }}">
                                <td>{{ $document->humanName }}</td> <!--Тип продажи -->
                                <td>{{ $document->workerFullName }}</td> <!-- Подготовил -->
                                <td>{{ $document->managerFullName }}</td> <!-- Менеджер -->
                                <td>{{ $document->clientFullName }}</td> <!-- Клиент -->
                                <td> <!-- Контактный телефон -->

                                    @foreach ($document->clientPhonesArray as $phone)

                                        <p>{{ $phone }}</p>

                                    @endforeach

                                </td>
                                <td>{{ $document->clientEmail }}</td> <!-- Эл.почта -->
                                <td>{{ $document->register_number }}</td> <!-- № счета -->
                                <td>{{ $document->organization }}</td> <!-- Салон -->
                                <td>{{ $document->humanTypePayment }}</td> <!-- б/нал -->
                                <td>{{ $document->created_at->year }}</td> <!-- Год -->
                                <td>{{ $document->created_at->day }}</td> <!-- Дата -->
                                <td>{{ $document->russianMonthName }}</td> <!-- Месяц -->
                                <td class="edittable" data-key="office">{{ $document->addionalOffice }}</td> <!-- Офис -->
                                <td>{{ $document->period_execution }}</td> <!-- Срок исполнения по договору -->
                                <td>{{ $document->date_finish->format('d.m.Y') }}</td> <!-- Срок поставки клиенту -->
                                <td> <!-- Поставщик -->

                                    @foreach ($document->products as $product)

                                        <p>{{ $product->factory->name }}</p>

                                    @endforeach

                                </td>
                                <td> <!-- Наименование -->

                                    @foreach ($document->products as $product)

                                        <p>{{ $product->name }}</p>

                                    @endforeach

                                </td>
                                <td> <!-- Модель -->

                                    @foreach ($document->products as $product)

                                       <p>{{ $product->model }}</p>

                                    @endforeach

                                </td>
                                <td> <!-- Кол-во -->

                                    @foreach ($document->products as $product)

                                       <p>{{ $product->count }}</p>

                                    @endforeach

                                </td>
                                <td> <!-- Сумма -->

                                    @foreach ($document->products as $product)

                                       <p>{{ $product->price }}</p>

                                    @endforeach

                                </td>
                                <td class="td_cost_price">{{ $document->costPrice }}</td> <!-- Себестоимость -->
                                <td>

                                    @foreach ($document->allPayments as $payment )

                                        <p>{{ $payment->humanTypePayment }}</p>

                                    @endforeach

                                </td> <!-- Форма оплаты -->
                                <td> <!-- Дата -->

                                    @foreach ($document->allPayments as $payment )

                                        <p>{{ $payment->created_at->format('d.m.Y H:i:s') }}</p>

                                    @endforeach

                                </td>
                                <td> <!-- Евро -->

                                    @foreach ($document->allPayments as $payment )

                                        <p>{{ $payment->enter_eur }}</p>

                                    @endforeach

                                </td>
                                <td> <!-- Рубль -->

                                    @foreach ($document->allPayments as $payment )

                                        <p>{{ $payment->enter_rub }}</p>

                                    @endforeach

                                </td>
                                <td> <!-- Доллары -->

                                    @foreach ($document->allPayments as $payment )

                                        <p>{{ $payment->enter_usd }}</p>

                                    @endforeach

                                </td>
                                <td> <!-- Курс -->

                                    @foreach ($document->allPayments as $payment )

                                        <p>{{ $payment->course_cb_eur_markup }}</p>

                                    @endforeach

                                </td>
                                <td> <!-- Крос курс -->

                                    @foreach ($document->allPayments as $payment )

                                        <p>{{ $payment->cross_currrency }}</p>

                                    @endforeach

                                </td>
                                <td>{{ $document->sum }}</td> <!-- Сумма договора -->
                                <td class="light_green">{{ $document->sum - $document->sumAllPayments }}</td> <!-- Ост. -->
                                <td class="light_purple">{{ $document->percentageAllPayments }}</td> <!-- % предоплаты -->
                                <td>{{ ($document->serviceAccount) ? $document->serviceAccount->humanTypePayment : '' }}</td> <!-- Сервис форма оплаты -->
                                <td>{{ ($document->serviceAccount) ? $document->serviceAccount->created_at->format('d.m.Y H:i:s') : '' }}</td> <!-- Сервис дата -->
                                <td>{{ ($document->serviceAccount) ? $document->serviceAccount->finishSumEur : '' }}</td> <!-- Сервис евро -->
                                <td>{{ ($document->serviceAccount) ? $document->serviceAccount->finishSumRub : '' }}</td> <!-- Сервис рубль -->
                                <td>{{ ($document->serviceAccount) ? $document->serviceAccount->course : '' }}</td> <!-- Сервис курс -->
                                <td class="edittable light_yellow" data-key="proform_office">{{ $document->addionalProformOffice }}</td> <!-- Проформа офис -->
                                <td class="edittable light_yellow" data-key="proform_number">{{ $document->addionalProformNumber }}</td> <!-- Проформа номер -->
                                <td class="light_yellow">{{ $document->dateProform }}</td> <!-- Проформа дата -->
                                <td class="edittable light_yellow" data-key="proform_sum">{{ $document->addionalProformSum }}</td> <!-- Проформа сумма -->
                                <td class="edittable light_yellow" data-key="proform_date_confirm">{{ $document->addionalProformDateConfirm }}</td> <!-- Проформа дата подтверждения -->
                                <td class="edittable light_yellow" data-key="proform_volume">{{ $document->addionalProformVolume }}</td> <!-- Проформа объем -->
                                <td class="light_blue" colspan="5">
                                      <div style="text-align: center">
                                          <a href="#" id="btn-modal-new-factory-payment" class="btn btn-primary" data-url="{{ route('document.factory.payment.create', compact('document')) }}">+</a>
                                      </div>

                                      @include('document.view_table_factory_payments', compact('document'))

                                </td>
                                <td class="td_bank_transfer_remainder light_blue">{{ $document->sumBankTransferRemainder }}</td> <!-- Остаток -->
                                <td class="td_bank_transfer">{{ $document->fullSumBankTransfer}}</td> <!-- Банк. перевод -->
                                <td class="edittable" data-key="am">{{ $document->addionalAm }}</td> <!-- а/м -->
                                <td class="edittable" data-key="stalker_delivery">{{ $document->addionalStalkerDelivery }}</td> <!-- Stalker_доставка -->
                                <td class="edittable" data-key="stalker_arrival">{{ $document->addionalStalkerArrival }}</td> <!-- Stalker_заезд -->
                                <td class="edittable" data-key="production_time">{{ $document->addionalProductionTime }}</td> <!-- Срок изготовления -->
                                <td class="edittable" data-key="volume">{{ $document->addionalVolume }}</td> <!-- Объем куб.м -->
                                <td class="edittable" data-key="weight">{{ $document->addionalWeight }}</td> <!-- Вес кг -->
                                <td class="edittable" data-key="count_package">{{ $document->addionalCountPackage }}</td> <!-- Кол-во кор. -->
                                <td class="edittable" data-key="packing_list">{{ $document->addionalPackingList }}</td> <!-- Packing list -->
                                <td class="edittable light_orange" data-key="comming_stock_date">{{ $document->addionalCommingStockDate }}</td> <!-- Приход склад/La Casa/дата -->
                                <td class="edittable light_orange" data-key="comming_count_places">{{ $document->addionalCommingCountPlaces }}</td> <!-- Приход кол-во мест -->
                                <td class="edittable light_orange" data-key="consumption_stock_date">{{ $document->addionalConsumptionStockeDate }}</td> <!-- Расход склад/La Casa/дата -->
                                <td class="edittable light_orange" data-key="consumption_count_places">{{ $document->addionalConsumptionCountPlaces }}</td> <!-- Расход кол-во мест -->
                                <td class="edittable light_orange" data-key="stock_remainder">{{ $document->addionalStockRemainder }}</td> <!-- Склад/La Casa/остаток -->
                                <td class="edittable light_orange" data-key="state_stock">{{ $document->addionalStateStock }}</td> <!-- Состояние -->
                                <td class="edittable light_orange" data-key="return_stock_date">{{ $document->addionalReturnStockDate }}</td> <!-- Возврат склад/La Casa/дата -->
                                <td class="edittable light_orange" data-key="return_count_package">{{ $document->addionalreturnCountPackage }}</td> <!-- Возврат кол-во кор. -->
                                <td class="edittable light_orange" data-key="state_return">{{ $document->addionalStateReturn }}</td> <!-- Состояние -->
                            </tr>

                        @endforeach

                    </tbody>
                </table>
            </div>
        </form>
    </div>

@stop

@section('javascript')
    @parent

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.2.0/js/ion.rangeSlider.min.js"></script>
    <script src="/js/document.js"></script>

    <script>
        $(function() {
            $('.pricerange').ionRangeSlider({
                type: "double",
                grid: true,
                prefix: "€"
            });

            $('.daterange').daterangepicker({
                autoUpdateInput: false,
                locale: {
                    cancelLabel: 'Clear'
                }
            });

            $('.daterange').on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
            });

            $('.daterange').on('cancel.daterangepicker', function(ev, picker) {
                $(this).val('');
            });

            $(document).on('click', '.show-price', function( e ) {
                e.preventDefault();

                $(this).next().toggleClass('hide');

                var sign = $(this).html();
                $(this).html($(this).attr('data-sign'));
                $(this).attr('data-sign', sign);

                var input = $(this).parent().find('input');

                if ($(input).attr('name') !== undefined) {
                    $(input).removeAttr('name');
                } else {
                    $(input).attr('name', $(this).data('name'));
                }
            });
        });
    </script>

@stop