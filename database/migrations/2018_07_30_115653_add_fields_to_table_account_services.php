<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToTableAccountServices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('service_accounts', function (Blueprint $table) {
            $table->date('date_delivery')->nullable()->after('sum_delivery');
            $table->date('date_assembly')->nullable()->after('sum_assembly');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('service_accounts', function (Blueprint $table) {
            $table->dropColumn('date_delivery');
            $table->dropColumn('date_assembly');
        });
    }
}
