<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDocumentAndTableDocumentPrepayments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // create table documents
        Schema::create('documents', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 10);
            $table->string('code', 50);
            $table->integer('worker_id')->unsigned();
            $table->integer('manager_worker_id')->unsigned()->nullable();
            $table->date('date')->nullable();
            $table->integer('client_id');
            $table->string('client_type', 10);
            $table->string('type_payment', 8);
            $table->string('state', 9);
            $table->string('register_number', 50);
            $table->timestamp('from')->nullable();
            $table->integer('shop_id')->unsigned();
            $table->double('sum');
            $table->double('percentage_currrency');
            $table->string('confirm_pro_form', 100);
            $table->integer('period_execution');
            $table->date('date_finish');
            $table->timestamp('performance_date')->nullable();
            $table->timestamp('ternimated_date')->nullable();
            $table->string('text_ternimated', 255)->nullable();
            $table->timestamps();

            $table->foreign('worker_id')
                ->references('id')
                ->on('workers')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('manager_worker_id')
                ->references('id')
                ->on('workers')
                ->onDelete('set null')
                ->onUpdate('cascade');
        });

        // create table document prepayments
        Schema::create('document_prepayments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('document_id')->unsigned();
            $table->double('percentage_prepayment');
            $table->double('percentage_remainder');
            $table->double('remainder_eur');
            $table->double('enter_eur');
            $table->double('enter_usd');
            $table->double('enter_rub');
            $table->double('prepayment_amount_eur');
            $table->double('prepayment_amount_rub');
            $table->double('course_cb_eur');
            $table->double('course_cb_usd');
            $table->double('course_cb_eur_markup');
            $table->double('course_cb_usd_markup');
            $table->double('cross_currrency');
            $table->timestamps();

            $table->foreign('document_id')
                ->references('id')
                ->on('documents')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // drop table document prepayments
        Schema::dropIfExists('document_prepayments');

        // drop table documents
        Schema::dropIfExists('documents');
    }
}
